library svn_pro_mobile_flutter;

import 'dart:math';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:custom_refresh_indicator/custom_refresh_indicator.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:colorful_iconify_flutter/icons/noto.dart';
import 'package:intl/intl.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:get/get.dart';

// Constants
part './constants/svn_pro_roadmap.dart';

part './constants/svn_pro_itw.dart';

part './constants/svn_pro_global.dart';

part './theme/seven_theme.dart';

part './constants/enum_mapping.dart';

part './theme/seven_tokens/seven_elevation_tokens.dart';

// Primary
part './components/custom_colors.dart';

part './components/svn_pro_theme_data.dart';

part './components/svn_pro_text.dart';

part './components/svn_pro_btn.dart';

part './components/svn_pro_avatar.dart';

part './components/svn_pro_badge.dart';

part 'components/svn_pro_show_bottom_sheet.dart';

part './components/svn_pro_card.dart';

part './components/svn_pro_checkbox.dart';

part './components/svn_pro_divider.dart';

part './components/svn_pro_icon_button.dart';

part './components/svn_pro_icon.dart';

part './components/svn_pro_text_field.dart';

part './components/svn_pro_show_date_picker.dart';

part './components/svn_pro_floating_action_button.dart';

part './components/svn_pro_chip.dart';

part './components/svn_pro_list_tile.dart';

part './components/svn_pro_list.dart';

part './components/svn_pro_switch.dart';

part './components/svn_pro_radio.dart';

part './components/svn_pro_show_dialog.dart';

part './components/svn_pro_segmented_button.dart';

part './components/svn_pro_show_time_picker.dart';

part './components/svn_pro_search.dart';

part './components/svn_pro_tabs.dart';

part './components/svn_pro_progress.dart';

part './components/svn_pro_menu_tile.dart';

part './components/svn_pro_menu.dart';

part './components/svn_pro_navigation_drawer.dart';

part './components/svn_pro_top_app_bar.dart';

part 'components/svn_pro_show_snackbar.dart';

// Secondary
part './components/svn_pro_show_action_menu.dart';

part './components/svn_pro_drawer_add_comment.dart';

part './components/svn_pro_card_dashboard_shortcut.dart';

part './components/svn_pro_feature_item.dart';

part './components/svn_pro_top_app_bar_show.dart';

part './components/svn_pro_date_field.dart';

part './components/svn_pro_select_drawer.dart';

part './components/svn_pro_select_modal.dart';

part './components/svn_pro_show_modal.dart';

part './components/svn_pro_button_sticky_container.dart';

part './components/svn_pro_empty_state.dart';

part './components/svn_pro_info_tag.dart';

part './components/svn_pro_semi_circle.dart';

part './components/svn_pro_circle.dart';

part './components/svn_pro_search_field.dart';

part './components/svn_pro_select.dart';

part './components/svn_pro_question_edit_itw.dart';

part './components/svn_pro_question_show_itw.dart';

part './components/svn_pro_card_roadmap.dart';

part './components/svn_pro_graph_toggle_mcq_roadmap.dart';

part './components/svn_pro_graph_number_roadmap.dart';

part 'components/svn_pro_logs_roadmap.dart';

part './components/svn_pro_card_survey_graph_itw.dart';

part './components/svn_pro_card_itw.dart';

part './components/svn_pro_answer_bubble_item_itw.dart';

part './components/svn_pro_comment_base_tile.dart';

part './components/svn_pro_tiptap_show.dart';

part './components/svn_pro_checkbox_tile.dart';

part './components/svn_pro_quiz_correction.dart';

part 'components/svn_pro_card_base_small_learn.dart';

part './components/svn_pro_list_item_itw.dart';

part 'components/svn_pro_card_base_big_learn.dart';

part './components/svn_pro_info_tag_reaction.dart';

part './components/svn_pro_top_app_bar_switch_app.dart';

part './components/svn_pro_show_select_drawer.dart';

part './components/svn_pro_show_filter_drawer.dart';

part './components/svn_pro_reaction_learn.dart';

part './components/svn_pro_medal_learn.dart';

part './components/svn_pro_card_itw_loader.dart';

part './components/svn_pro_pull_to_refresh.dart';

part './components/svn_pro_campaign_pack_loading_list.dart';

part 'components/form/svn_pro_form.dart';

part 'components/form/svn_pro_form_field.dart';

part 'components/form/svn_pro_form_field_question_edit_itw.dart';

part './components/svn_pro_interview_show_loader.dart';

part './components/svn_pro_text_field_skeleton.dart';

part './components/svn_pro_checkbox_tile_skeleton.dart';

part './components/svn_pro_card_skeleton.dart';

part './components/svn_pro_comment_head_skeleton.dart';

part './components/svn_pro_comment_body_skeleton.dart';

part './components/svn_pro_list_skeleton.dart';

part 'components/form/svn_pro_form_field_multi_choice_list.dart';

part './components/svn_pro_multi_choice_list.dart';

part './components/form/svn_pro_form_field_text_field.dart';

part './components/svn_pro_target_draft_itw.dart';

part './components/svn_pro_trainings_loading_list.dart';

part './components/svn_pro_card_training_loader.dart';

part './components/svn_pro_training_show_loader.dart';

part 'components/svn_pro_playlist_show_skeleton.dart';

part './components/svn_pro_module_show_skeleton.dart';

part './components/svn_pro_image.dart';

part './components/svn_pro_server_error_page.dart';

part './components/svn_pro_skeleton_card_badge.dart';

part 'components/svn_pro_quiz_skeleton.dart';

part './components/svn_pro_face_to_face_evaluation_skeleton.dart';

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
