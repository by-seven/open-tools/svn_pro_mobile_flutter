part of '../svn_pro_mobile_flutter.dart';

extension StringToItwOneToOneFormAnswerableBy on String {
  ItwOneToOneFormAnswerableBy toItwOneToOneFormAnswerableBy() {
    switch (this) {
      case 'employee':
        return ItwOneToOneFormAnswerableBy.employee;
      case 'manager':
        return ItwOneToOneFormAnswerableBy.manager;
      case 'both':
        return ItwOneToOneFormAnswerableBy.both;
      case 'cross':
        return ItwOneToOneFormAnswerableBy.cross;
      case 'pairs':
        return ItwOneToOneFormAnswerableBy.pairs;
      case 'survey':
        return ItwOneToOneFormAnswerableBy.survey;
      default:
        throw Exception("Unknown ItwOneToOneFormAnswerableBy: $this");
    }
  }
}
