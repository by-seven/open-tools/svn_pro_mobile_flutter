part of '../svn_pro_mobile_flutter.dart';

enum SvnProTargetStatusRoadmap {
  notSet,
  notValid,
  inProgress,
  validated,
}
