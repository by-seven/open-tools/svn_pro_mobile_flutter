part of '../svn_pro_mobile_flutter.dart';

enum GlobalQuestionType {
  text,
  number,
  star,
  radio,
}

enum SevenApp {
  interview,
  roadmap,
  learn,
}

/// Extension to add custom date parsing functions to String objects.
/// This allows for easy conversion from string representations of dates
/// to DateTime objects, making it convenient to parse different date formats
/// directly on String instances without needing to manually create new parsers each time.
extension SevenDateTimeFormatter on DateTime {
  /// Formats the DateTime object into the format 'EEEE d, MMMM yyyy'.
  ///
  /// This format is typically used to display full day, date, and month names
  /// in a user-friendly manner, making it suitable for UI elements showing detailed dates.
  ///
  /// Example:
  /// ```dart
  /// final now = DateTime.now();
  /// print(now.toFullDate); // Outputs something like: "Monday 7, October 2024"
  /// ```
  String get toFullDate =>
      DateFormat('EEEE d, MMMM yyyy', Intl.defaultLocale).format(this);

  /// Formats the DateTime object into the backend format 'yyyy-MM-dd'.
  ///
  /// This format is typically used when sending or receiving dates
  /// from backend services or APIs in a standardized format.
  ///
  /// Example:
  /// ```dart
  /// final now = DateTime.now();
  /// print(now.toBackendDate); // Outputs something like: 2024-10-01
  /// ```
  String get toBackendDate =>
      DateFormat('yyyy-MM-dd', Intl.defaultLocale).format(this);

  /// Formats the DateTime object into the backend date-time format 'yyyy-MM-dd HH:mm:s'.
  ///
  /// This format includes both the date and the time, and is commonly used
  /// when interacting with backend services where both the date and time are required.
  ///
  /// Example:
  /// ```dart
  /// final now = DateTime.now();
  /// print(now.toBackendDateTime); // Outputs something like: 2024-10-01 14:35:12
  /// ```
  String get toBackendDateTime =>
      DateFormat('yyyy-MM-dd HH:mm:s', Intl.defaultLocale).format(this);

  /// Formats the DateTime object into the Material Design date format 'MM/dd/yyyy'.
  ///
  /// This format is typically used in Material Design guidelines, especially in
  /// Flutter applications, to display dates in a more common format for U.S. users.
  ///
  /// Example:
  /// ```dart
  /// final now = DateTime.now();
  /// print(now.toMaterialDate); // Outputs something like: 10/01/2024
  /// ```
  String get toMaterialDate =>
      DateFormat('MM/dd/yyyy', Intl.defaultLocale).format(this);

  /// Formats the DateTime object into a verbose short date format 'd MMM. y'.
  ///
  /// This format is often used in more user-friendly contexts where a readable
  /// short-form date is needed (e.g., for logs or history entries).
  ///
  /// Example:
  /// ```dart
  /// final now = DateTime.now();
  /// print(now.toVerboseShortDate); // Outputs something like: 1 Oct. 2024
  /// ```
  String get toVerboseShortDate =>
      DateFormat('d MMM. y', Intl.defaultLocale).format(this);

  /// Formats the DateTime object into a short date format 'd MMM'.
  ///
  /// This format omits the year and is useful for displaying just the day and month,
  /// especially for events that occur annually or where the year is implied.
  ///
  /// Example:
  /// ```dart
  /// final now = DateTime.now();
  /// print(now.toShortDate); // Outputs something like: 1 Oct
  /// ```
  String get toShortDate =>
      DateFormat('d MMM', Intl.defaultLocale).format(this);
}

extension StringToDateTime on String {
  /// Attempts to parse the string into a DateTime object based on the format 'EEEE d, MMMM yyyy'.
  ///
  /// This getter tries to convert the string into a DateTime object if the string is formatted
  /// as 'EEEE d, MMMM yyyy'. If the string cannot be parsed (e.g., due to an invalid format),
  /// it returns `null` instead of throwing a FormatException.
  ///
  /// Example:
  /// ```dart
  /// String dateString = "Monday 7, October 2024";
  /// DateTime? parsedDate = dateString.fullDate;
  /// print(parsedDate); // Outputs: 2024-10-07 00:00:00.000
  /// ```
  DateTime? get fullDate {
    return DateFormat('EEEE d, MMMM yyyy', Intl.defaultLocale).tryParse(this);
  }

  /// Attempts to parse the string into a DateTime object based on the backend format 'yyyy-MM-dd'.
  ///
  /// This getter tries to convert the string into a DateTime object if the string is formatted
  /// as 'yyyy-MM-dd'. If the string cannot be parsed (e.g., due to an invalid format),
  /// it returns `null` instead of throwing a FormatException.
  ///
  /// Example:
  /// ```dart
  /// String dateString = "2024-10-01";
  /// DateTime? parsedDate = dateString.backendDate;
  /// print(parsedDate); // Outputs: 2024-10-01 00:00:00.000
  /// ```
  DateTime? get backendDate =>
      DateFormat('yyyy-MM-dd', Intl.defaultLocale).tryParse(this);

  /// Attempts to parse the string into a DateTime object based on the backend date-time format 'yyyy-MM-dd HH:mm:s'.
  ///
  /// This getter tries to convert the string into a DateTime object if the string is formatted
  /// as 'yyyy-MM-dd HH:mm:s'. If the string cannot be parsed (e.g., due to an invalid format),
  /// it returns `null` instead of throwing a FormatException.
  ///
  /// Example:
  /// ```dart
  /// String dateTimeString = "2024-10-01 14:35:12";
  /// DateTime? parsedDateTime = dateTimeString.backendDateTime;
  /// print(parsedDateTime); // Outputs: 2024-10-01 14:35:12.000
  /// ```
  DateTime? get backendDateTime =>
      DateFormat('yyyy-MM-dd HH:mm:s', Intl.defaultLocale).tryParse(this);

  /// Attempts to parse the string into a DateTime object based on the Material Design date format 'MM/dd/yyyy'.
  ///
  /// This getter tries to convert the string into a DateTime object if the string is formatted
  /// as 'MM/dd/yyyy'. If the string cannot be parsed (e.g., due to an invalid format),
  /// it returns `null` instead of throwing a FormatException.
  ///
  /// Example:
  /// ```dart
  /// String dateString = "10/01/2024";
  /// DateTime? parsedDate = dateString.materialDate;
  /// print(parsedDate); // Outputs: 2024-10-01 00:00:00.000
  /// ```
  DateTime? get materialDate =>
      DateFormat('MM/dd/yyyy', Intl.defaultLocale).tryParse(this);

  /// Attempts to parse the string into a DateTime object based on the verbose short date format 'd MMM. y'.
  ///
  /// This getter tries to convert the string into a DateTime object if the string is formatted
  /// as 'd MMM. y'. If the string cannot be parsed (e.g., due to an invalid format),
  /// it returns `null` instead of throwing a FormatException.
  ///
  /// Example:
  /// ```dart
  /// String dateString = "1 Oct. 2024";
  /// DateTime? parsedDate = dateString.verboseShortDate;
  /// print(parsedDate); // Outputs: 2024-10-01 00:00:00.000
  /// ```
  DateTime? get verboseShortDate =>
      DateFormat('d MMM. y', Intl.defaultLocale).tryParse(this);

  /// Attempts to parse the string into a DateTime object based on the short date format 'd MMM'.
  ///
  /// This getter tries to convert the string into a DateTime object if the string is formatted
  /// as 'd MMM'. If the string cannot be parsed (e.g., due to an invalid format),
  /// it returns `null` instead of throwing a FormatException.
  ///
  /// Example:
  /// ```dart
  /// String dateString = "1 Oct";
  /// DateTime? parsedDate = dateString.shortDate;
  /// print(parsedDate); // Outputs: 2024-10-01 00:00:00.000
  /// ```
  DateTime? get shortDate =>
      DateFormat('d MMM', Intl.defaultLocale).tryParse(this);
}
