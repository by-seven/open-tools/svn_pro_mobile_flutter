part of '../svn_pro_mobile_flutter.dart';

enum SvnProCampaignType {
  oneToOne('OneToOne'),
  feedback('Feedback'),
  survey('Survey');

  final String backendName;

  const SvnProCampaignType(this.backendName);
}

enum SvnProQuestionItwType {
  open,
  rating,
  mcq,
}

enum SvnProQuestionEditItwStatus {
  enabled,
  error,
}

enum InterviewQuestionType {
  chapter('InterviewApp::Questions::Chapter'),
  paragraph('InterviewApp::Questions::Paragraph'),
  open('InterviewApp::Questions::Open'),
  rating('InterviewApp::Questions::Rating'),
  mcq('InterviewApp::Questions::Mcq'),
  createRoadmap('InterviewApp::Questions::CreateRoadmap'),
  updateRoadmap('InterviewApp::Questions::UpdateRoadmap');

  final String backendName;

  const InterviewQuestionType(this.backendName);
}

enum ItwOneToOneFormAnswerableBy {
  employee,
  manager,
  both,
  cross,
  pairs,
  survey,
}
