import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/theme/seven_tokens/seven_spacing_tokens.dart';

/// SevenPaddingTokens
/// Defines standardized padding configurations using EdgeInsets based on SevenSpacingTokens.
/// Ensures consistent padding values across the application components.
class SevenPaddingTokens {
  /// Padding of `SevenSpacingTokens.small` on all sides (8.0 pixels).
  static const EdgeInsets allSmall = EdgeInsets.all(SevenSpacingTokens.small);

  /// Padding of `SevenSpacingTokens.medium` on all sides (16.0 pixels).
  static const EdgeInsets allMedium = EdgeInsets.all(SevenSpacingTokens.medium);

  /// Padding of `SevenSpacingTokens.large` on all sides (24.0 pixels).
  static const EdgeInsets allLarge = EdgeInsets.all(SevenSpacingTokens.large);

  /// Horizontal padding of `SevenSpacingTokens.small` (8.0 pixels).
  static const EdgeInsets horizontalSmall =
      EdgeInsets.symmetric(horizontal: SevenSpacingTokens.small);

  /// Horizontal padding of `SevenSpacingTokens.medium` (16.0 pixels).
  static const EdgeInsets horizontalMedium =
      EdgeInsets.symmetric(horizontal: SevenSpacingTokens.medium);

  /// Vertical padding of `SevenSpacingTokens.small` (8.0 pixels).
  static const EdgeInsets verticalSmall =
      EdgeInsets.symmetric(vertical: SevenSpacingTokens.small);

  /// Vertical padding of `SevenSpacingTokens.medium` (16.0 pixels).
  static const EdgeInsets verticalMedium =
      EdgeInsets.symmetric(vertical: SevenSpacingTokens.medium);
}
