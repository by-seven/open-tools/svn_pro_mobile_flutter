/// SevenIconSizeTokens
/// Defines standardized icon sizes based on Material Design guidelines.
/// Ensures consistent icon sizes across the application.
class SevenIconSizeTokens {
  /// Small icon size (18.0 pixels) - used for compact layouts or minor icons.
  static const double small = 18.0;

  /// Medium icon size (24.0 pixels) - default icon size for primary icons.
  static const double medium = 24.0;

  /// Large icon size (36.0 pixels) - used for prominent icons or hero elements.
  static const double large = 36.0;
}
