/// SevenSpacingTokens
/// Defines standardized spacing values based on Material Design conventions.
/// These values are used for consistent margins and paddings across the application.
class SevenSpacingTokens {
  /// Extra small spacing (4.0 pixels) - minimal space for tightly spaced elements.
  static const double extraSmall = 4.0;

  /// Small spacing (8.0 pixels) - general-purpose space between related items.
  static const double small = 8.0;

  /// Medium spacing (16.0 pixels) - standard space between sections or larger items.
  static const double medium = 16.0;

  /// Large spacing (24.0 pixels) - significant space between distinct sections.
  static const double large = 24.0;

  /// Extra large spacing (32.0 pixels) - max space for separating major content areas.
  static const double extraLarge = 32.0;
}
