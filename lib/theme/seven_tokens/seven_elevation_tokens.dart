part of '../../svn_pro_mobile_flutter.dart';

/// SevenElevationTokens
/// Defines standard elevation values for components based on Material Design.
/// Ensures consistent shadows across the application.
class SvnProElevationTokens {
  static const double elevationNone = 0.0;
  static const double elevationMax = 100.0;
}
