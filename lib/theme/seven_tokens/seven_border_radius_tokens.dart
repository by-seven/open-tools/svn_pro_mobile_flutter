import 'package:flutter/material.dart';

/// SevenBorderRadiusTokens
/// Defines standard corner radius values for components based on Material Design.
/// Ensures consistent rounded corners across different UI elements.
class SevenBorderRadiusTokens {
  /// Small corner radius (4.0 pixels) - suitable for small components or subtle rounding.
  static const BorderRadius small = BorderRadius.all(Radius.circular(4.0));

  /// Medium corner radius (8.0 pixels) - standard radius for general-purpose components.
  static const BorderRadius medium = BorderRadius.all(Radius.circular(8.0));

  /// Large corner radius (16.0 pixels) - noticeable rounding for larger components.
  static const BorderRadius large = BorderRadius.all(Radius.circular(16.0));
}
