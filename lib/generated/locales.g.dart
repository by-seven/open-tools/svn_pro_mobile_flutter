// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'en_US': Locales.en_US,
    'fr_FR': Locales.fr_FR,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const custom_and_accessible_progress_for_life =
      'custom_and_accessible_progress_for_life';
  static const login = 'login';
  static const continue_ = 'continue_';
  static const or = 'or';
  static const sign_in_with_google = 'sign_in_with_google';
  static const oops_it_seems_this_email_is_not_registered =
      'oops_it_seems_this_email_is_not_registered';
  static const you_may_have_misswritten_your_email_please_try_again =
      'you_may_have_misswritten_your_email_please_try_again';
  static const try_again = 'try_again';
  static const email = 'email';
  static const password = 'password';
  static const i_forgot_my_password = 'i_forgot_my_password';
  static const check_your_mail = 'check_your_mail';
  static const this_is_the_first_time_you_log_in_to_aleph_we_just_sent_an_email_with_your_password_to_the_address =
      'this_is_the_first_time_you_log_in_to_aleph_we_just_sent_an_email_with_your_password_to_the_address';
  static const if_you_didn_t_receive_it_check_your_spams_or_click_on_the_button_below_to_send_it_again =
      'if_you_didn_t_receive_it_check_your_spams_or_click_on_the_button_below_to_send_it_again';
  static const back_to_login = 'back_to_login';
  static const send_it_again = 'send_it_again';
  static const wrong_password = 'wrong_password';
  static const we_sent_an_email_to_reset_your_password_to_the_address =
      'we_sent_an_email_to_reset_your_password_to_the_address';
  static const reset_password = 'reset_password';
  static const must_contain_at_least_8_characters =
      'must_contain_at_least_8_characters';
  static const confirm_password = 'confirm_password';
  static const password_and_confirmation_must_match =
      'password_and_confirmation_must_match';
  static const password_has_been_reset = 'password_has_been_reset';
  static const password_must_contain_at_least_8_characters =
      'password_must_contain_at_least_8_characters';
  static const welcome_on_board = 'welcome_on_board';
  static const all = 'all';
  static const to_do = 'to_do';
  static const Done = 'Done';
  static const start_feedback = 'start_feedback';
  static const manage_reviewers = 'manage_reviewers';
  static const view_answers = 'view_answers';
  static const check_my_answers = 'check_my_answers';
  static const nothing_to_show_for_now = 'nothing_to_show_for_now';
  static const all_your_interviews_and_surveys_will_show_up_here =
      'all_your_interviews_and_surveys_will_show_up_here';
  static const your_current_interviews_and_surveys_will_show_up_here =
      'your_current_interviews_and_surveys_will_show_up_here';
  static const my_trainings = 'my_trainings';
  static const catalog = 'catalog';
  static const my_interviews = 'my_interviews';
  static const my_team_interviews = 'my_team_interviews';
  static const current = 'current';
  static const archived = 'archived';
  static const your_current_targets_will_show_up_here =
      'your_current_targets_will_show_up_here';
  static const your_archived_targets_will_show_up_here =
      'your_archived_targets_will_show_up_here';
  static const aleph_apps = 'aleph_apps';
  static const profile = 'profile';
  static const profile_details = 'profile_details';
  static const my_library = 'my_library';
  static const my_badges = 'my_badges';
  static const language = 'language';
  static const log_out = 'log_out';
  static const recommend_aleph = 'recommend_aleph';
  static const help_and_support = 'help_and_support';
  static const first_name = 'first_name';
  static const last_name = 'last_name';
  static const email__1 = 'email__1';
  static const date_of_birth = 'date_of_birth';
  static const hire_date = 'hire_date';
  static const personal_information = 'personal_information';
  static const hr_information = 'hr_information';
  static const your_changes_will_be_lost = 'your_changes_will_be_lost';
  static const you_have_unsaved_changes_on_this_page_if_you_quit_without_saving_they_will_be_lost =
      'you_have_unsaved_changes_on_this_page_if_you_quit_without_saving_they_will_be_lost';
  static const ok_quit = 'ok_quit';
  static const access_level = 'access_level';
  static const admin = 'admin';
  static const manager_creator = 'manager_creator';
  static const manager = 'manager';
  static const employee = 'employee';
  static const save = 'save';
  static const profile_has_been_updated = 'profile_has_been_updated';
  static const select_date = 'select_date';
  static const enter_date = 'enter_date';
  static const date = 'date';
  static const ok = 'ok';
  static const clear_field = 'clear_field';
  static const some_fields_are_invalid = 'some_fields_are_invalid';
  static const please_fill_all_required_fields =
      'please_fill_all_required_fields';
  static const invalid_format = 'invalid_format';
  static const edit_profile_photo = 'edit_profile_photo';
  static const import_photo = 'import_photo';
  static const delete_current_photo = 'delete_current_photo';
  static const english = 'english';
  static const french = 'french';
  static const language_has_been_modified = 'language_has_been_modified';
  static const change_password = 'change_password';
  static const new_password = 'new_password';
  static const confirm_new_password = 'confirm_new_password';
  static const confirmation_and_password_must_match =
      'confirmation_and_password_must_match';
  static const accomplished_apprentice = 'accomplished_apprentice';
  static const trained_and_competent = 'trained_and_competent';
  static const certified_expert = 'certified_expert';
  static const training_champion = 'training_champion';
  static const training_completed = 'training_completed';
  static const the_selector = 'the_selector';
  static const content_lover = 'content_lover';
  static const informed_collector = 'informed_collector';
  static const resource_guardian = 'resource_guardian';
  static const modules_in_favorites = 'modules_in_favorites';
  static const the_approver = 'the_approver';
  static const positive_supporter = 'positive_supporter';
  static const enlightened_evaluator = 'enlightened_evaluator';
  static const master_of_emotions = 'master_of_emotions';
  static const reactions_to_modules = 'reactions_to_modules';
  static const enlightened_novice = 'enlightened_novice';
  static const knowledge_explorer = 'knowledge_explorer';
  static const thirsty_for_knowledge = 'thirsty_for_knowledge';
  static const curriculum_master = 'curriculum_master';
  static const modules_acquired = 'modules_acquired';
  static const if_you_add_a_module_to_favorites_it_will_show_up_here =
      'if_you_add_a_module_to_favorites_it_will_show_up_here';
  static const this_field_is_required = 'this_field_is_required';
  static const your_form_has_been_sent_thank_you =
      'your_form_has_been_sent_thank_you';
  static const do_you_want_to_recommend_aleph_to_someone_please_fill_out_this_form_our_team_will_contact_the_interested_person_very_soon =
      'do_you_want_to_recommend_aleph_to_someone_please_fill_out_this_form_our_team_will_contact_the_interested_person_very_soon';
  static const target_person_s_email = 'target_person_s_email';
  static const your_message = 'your_message';
  static const send = 'send';
  static const if_you_have_any_questions_or_require_assistance_please_feel_free_to_reach_out_to_the_aleph_team_by_filling_out_this_form_we_will_respond_to_your_message_very_soon =
      'if_you_have_any_questions_or_require_assistance_please_feel_free_to_reach_out_to_the_aleph_team_by_filling_out_this_form_we_will_respond_to_your_message_very_soon';
  static const your_issue = 'your_issue';
  static const are_you_sure_you_want_to_log_out =
      'are_you_sure_you_want_to_log_out';
  static const cancel = 'cancel';
  static const yes_log_out = 'yes_log_out';
  static const show_only_pinned_answers = 'show_only_pinned_answers';
  static const select_a_question = 'select_a_question';
  static const question = 'question';
  static const tag_category = 'tag_category';
  static const tag = 'tag';
  static const answers = 'answers';
  static const answer = 'answer';
  static const add_a_comment = 'add_a_comment';
  static const answer_pinned = 'answer_pinned';
  static const answer_removed_from_pins = 'answer_removed_from_pins';
  static const select_category = 'select_category';
  static const search = 'search';
  static const Comments = 'Comments';
  static const tap_on_the_pin_icon_to_collect_the_most_interesting_answers_your_pinned_answers_will_show_up_here =
      'tap_on_the_pin_icon_to_collect_the_most_interesting_answers_your_pinned_answers_will_show_up_here';
  static const oops_we_didn_t_find_any_results_matching_your_search =
      'oops_we_didn_t_find_any_results_matching_your_search';
  static const no_comments_yet = 'no_comments_yet';
  static const comments = 'comments';
  static const comment = 'comment';
  static const add_reviewers = 'add_reviewers';
  static const reviewers = 'reviewers';
  static const set_another_person_in_charge = 'set_another_person_in_charge';
  static const reviewer_set = 'reviewer_set';
  static const reviewers_set = 'reviewers_set';
  static const done = 'done';
  static const in_progress = 'in_progress';
  static const feedback_on_ = 'feedback_on_';
  static const not_started = 'not_started';
  static const Submitted_ = 'Submitted_';
  static const participant__ = 'participant__';
  static const all_your_team_s_interviews_and_surveys_will_show_up_here =
      'all_your_team_s_interviews_and_surveys_will_show_up_here';
  static const your_team_s_current_interviews_and_surveys_will_show_up_here =
      'your_team_s_current_interviews_and_surveys_will_show_up_here';
  static const send_reminder_notification = 'send_reminder_notification';
  static const search_by_name = 'search_by_name';
  static const all_reviewers_for_this_interview_will_show_up_here =
      'all_reviewers_for_this_interview_will_show_up_here';
  static const no_reviewers_yet = 'no_reviewers_yet';
  static const start_interview = 'start_interview';
  static const continue_interview = 'continue_interview';
  static const start_cross_review = 'start_cross_review';
  static const continue_cross_review = 'continue_cross_review';
  static const view_interviewee_answers = 'view_interviewee_answers';
  static const check_cross_review = 'check_cross_review';
  static const continue_feedback = 'continue_feedback';
  static const start_self_evaluation = 'start_self_evaluation';
  static const choose_reviewers = 'choose_reviewers';
  static const reviewer_successfully_added = 'reviewer_successfully_added';
  static const reviewer_removed = 'reviewer_removed';
  static const remove_reviewer = 'remove_reviewer';
  static const this_reviewer_has_started_to_answer_if_you_continue_their_answers_will_be_permanently_deleted =
      'this_reviewer_has_started_to_answer_if_you_continue_their_answers_will_be_permanently_deleted';
  static const yes_remove = 'yes_remove';
  static const submitted = 'submitted';
  static const you_must_select_at_least_1_reviewer =
      'you_must_select_at_least_1_reviewer';
  static const this_user_is_already_set_as_reviewer_and_can_t_be_added =
      'this_user_is_already_set_as_reviewer_and_can_t_be_added';
  static const modifications_to_reviewers_will_be_lost =
      'modifications_to_reviewers_will_be_lost';
  static const you_added_reviewers_to_this_interview_if_you_leave_without_saving_your_modifications_will_be_lost =
      'you_added_reviewers_to_this_interview_if_you_leave_without_saving_your_modifications_will_be_lost';
  static const yes_leave = 'yes_leave';
  static const you_will_no_longer_be_in_charge =
      'you_will_no_longer_be_in_charge';
  static const reminder_sent_the_employee_will_receive_an_email_in_a_few_moments =
      'reminder_sent_the_employee_will_receive_an_email_in_a_few_moments';
  static const you_won_t_be_able_to_participate_anymore =
      'you_won_t_be_able_to_participate_anymore';
  static const this_survey_will_no_longer_appear_in_your_my_interviews_section =
      'this_survey_will_no_longer_appear_in_your_my_interviews_section';
  static const yes_decline_survey = 'yes_decline_survey';
  static const survey_declined_successfully = 'survey_declined_successfully';
  static const start_survey = 'start_survey';
  static const continue_survey = 'continue_survey';
  static const decline_survey = 'decline_survey';
  static const you_will_lose_access_to_this_interview_in_the_my_team_interviews_section =
      'you_will_lose_access_to_this_interview_in_the_my_team_interviews_section';
  static const will_receive_a_notification = 'will_receive_a_notification';
  static const submit = 'submit';
  static const person_in_charge_ = 'person_in_charge_';
  static const status_ = 'status_';
  static const deadline_ = 'deadline_';
  static const participant_ = 'participant_';
  static const create_new_target = 'create_new_target';
  static const edit_target = 'edit_target';
  static const feedback_about_ = 'feedback_about_';
  static const summary = 'summary';
  static const interview_updates = 'interview_updates';
  static const answer__1 = 'answer__1';
  static const Comment = 'Comment';
  static const saved = 'saved';
  static const answers_saved_you_can_come_back_later_to_finish_the_interview =
      'answers_saved_you_can_come_back_later_to_finish_the_interview';
  static const your_answers_can_t_be_saved_please_check_your_connection =
      'your_answers_can_t_be_saved_please_check_your_connection';
  static const trying_to_reconnect = 'trying_to_reconnect';
  static const item_will_be_downloaded_see_notifications_for_further_details_1 =
      'item_will_be_downloaded_see_notifications_for_further_details_1';
  static const your_interview_has_been_submitted_your_answers_are_locked =
      'your_interview_has_been_submitted_your_answers_are_locked';
  static const please_answer_all_required_questions =
      'please_answer_all_required_questions';
  static const submitting_this_interview_will_lock_your_answers_ask_to_an_admin_to_unlock_them_if_needed =
      'submitting_this_interview_will_lock_your_answers_ask_to_an_admin_to_unlock_them_if_needed';
  static const answers_will_be_locked = 'answers_will_be_locked';
  static const none_of_the_targets_have_been_updated =
      'none_of_the_targets_have_been_updated';
  static const this_interview_contains_one_or_more_targets_do_you_want_to_continue_anyway =
      'this_interview_contains_one_or_more_targets_do_you_want_to_continue_anyway';
  static const if_you_confirm_all_target_creations_and_updates_will_be_applied_you_can_follow_this_up_in_roadmap_app =
      'if_you_confirm_all_target_creations_and_updates_will_be_applied_you_can_follow_this_up_in_roadmap_app';
  static const username_updated = 'username_updated';
  static const confirm = 'confirm';
  static const completed = 'completed';
  static const my_roadmap = 'my_roadmap';
  static const my_team_roadmaps = 'my_team_roadmaps';
  static const continue_self_evaluation = 'continue_self_evaluation';
  static const new_target = 'new_target';
  static const not_set = 'not_set';
  static const type_and_confirm_your_new_password =
      'type_and_confirm_your_new_password';
  static const current_targets = 'current_targets';
  static const archived_targets = 'archived_targets';
  static const starting_value_ = 'starting_value_';
  static const target_value_ = 'target_value_';
  static const target = 'target';
  static const last_events = 'last_events';
  static const update_value = 'update_value';
  static const not_set_yet = 'not_set_yet';
  static const apply = 'apply';
  static const target_achieved = 'target_achieved';
  static const value = 'value';
  static const comment_successfully_added = 'comment_successfully_added';
  static const value_successfully_updated = 'value_successfully_updated';
  static const please_change_the_target_value_or_add_a_comment_to_apply =
      'please_change_the_target_value_or_add_a_comment_to_apply';
  static const create_target = 'create_target';
  static const target_information = 'target_information';
  static const assign_target = 'assign_target';
  static const choose_target_indicator = 'choose_target_indicator';
  static const target_summary = 'target_summary';
  static const or_start_from_a_template_ = 'or_start_from_a_template_';
  static const for_non_quantifiable_targets_with_only_two_options_such_as_achieved_not_achieved =
      'for_non_quantifiable_targets_with_only_two_options_such_as_achieved_not_achieved';
  static const define_two_values_to_measure_the_target_false_will_be_the_starting_value_the_target_will_be_reached_when_switched_to_true =
      'define_two_values_to_measure_the_target_false_will_be_the_starting_value_the_target_will_be_reached_when_switched_to_true';
  static const for_non_quantifiable_objectives_such_as_ratings_you_can_create_multiple_options_to_qualify_your_target =
      'for_non_quantifiable_objectives_such_as_ratings_you_can_create_multiple_options_to_qualify_your_target';
  static const you_can_choose_which_option_or_options_will_be_considered_in_target_by_ticking_the_box_on_the_left =
      'you_can_choose_which_option_or_options_will_be_considered_in_target_by_ticking_the_box_on_the_left';
  static const for_quantifiable_targets_expressed_in_percentage =
      'for_quantifiable_targets_expressed_in_percentage';
  static const set_a_percentage_based_value_for_the_starting_and_target_value =
      'set_a_percentage_based_value_for_the_starting_and_target_value';
  static const for_quantifiable_targets_people_money_apples =
      'for_quantifiable_targets_people_money_apples';
  static const define_a_starting_and_a_target_value_to_measure_the_target_completion =
      'define_a_starting_and_a_target_value_to_measure_the_target_completion';
  static const new_target_from_scratch = 'new_target_from_scratch';
  static const search_template = 'search_template';
  static const title = 'title';
  static const description = 'description';
  static const deadline = 'deadline';
  static const previous = 'previous';
  static const next = 'next';
  static const target_indicator = 'target_indicator';
  static const true_false = 'true_false';
  static const percentage = 'percentage';
  static const number = 'number';
  static const multi_choice = 'multi_choice';
  static const starting_value = 'starting_value';
  static const target_value = 'target_value';
  static const target_assigned_to = 'target_assigned_to';
  static const view_all = 'view_all';
  static const read_all = 'read_all';
  static const target_indicator_ = 'target_indicator_';
  static const users_will_show_up_here = 'users_will_show_up_here';
  static const option = 'option';
  static const this_value_is_locked_and_cant_be_edited =
      'this_value_is_locked_and_cant_be_edited';
  static const add_option = 'add_option';
  static const tap_on_new_target_from_scratch_or_choose_a_template =
      'tap_on_new_target_from_scratch_or_choose_a_template';
  static const title_field_cant_be_empty = 'title_field_cant_be_empty';
  static const select_at_least_1_person = 'select_at_least_1_person';
  static const please_set_a_starting_value_and_a_target_value =
      'please_set_a_starting_value_and_a_target_value';
  static const come_back_later_to_see_your_trainings =
      'come_back_later_to_see_your_trainings';
  static const no_training_completed_yet = 'no_training_completed_yet';
  static const modules = 'modules';
  static const module = 'module';
  static const total_duration = 'total_duration';
  static const min = 'min';
  static const view_all_themes = 'view_all_themes';
  static const view_less = 'view_less';
  static const content = 'content';
  static const playlist = 'playlist';
  static const Module = 'Module';
  static const training_updated_on_date = 'training_updated_on_date';
  static const start_training = 'start_training';
  static const resume_training = 'resume_training';
  static const no_duration = 'no_duration';
  static const this_playlist_is_empty = 'this_playlist_is_empty';
  static const Modules = 'Modules';
  static const playlist_added_to_your_library =
      'playlist_added_to_your_library';
  static const duration = 'duration';
  static const yes_its_all_clear_to_me = 'yes_its_all_clear_to_me';
  static const youve_reached_the_end_did_you_learn_all_the_following_learning_objectives =
      'youve_reached_the_end_did_you_learn_all_the_following_learning_objectives';
  static const one_more_module_acquired = 'one_more_module_acquired';
  static const congratulations = 'congratulations';
  static const keep_it_up = 'keep_it_up';
  static const replies = 'replies';
  static const reply = 'reply';
  static const what_did_you_think_of_the_module =
      'what_did_you_think_of_the_module';
  static const your_feedback_helps_us_improve_this_module =
      'your_feedback_helps_us_improve_this_module';
  static const i_like = 'i_like';
  static const i_recommend = 'i_recommend';
  static const next_module = 'next_module';
  static const acquired = 'acquired';
  static const update_acquisition_date = 'update_acquisition_date';
  static const youve_reached_the_end_ready_for_the_quiz =
      'youve_reached_the_end_ready_for_the_quiz';
  static const to_complete_this_module_evaluate_yourself_with_a_quiz =
      'to_complete_this_module_evaluate_yourself_with_a_quiz';
  static const start_quiz = 'start_quiz';
  static const retake_quiz = 'retake_quiz';
  static const youve_reached_the_end_validate_your_evaluation_to_complete_this_module =
      'youve_reached_the_end_validate_your_evaluation_to_complete_this_module';
  static const to_successfully_complete_this_module_your_answers_must_be_reviewed_in_real_time_by_your_expert =
      'to_successfully_complete_this_module_your_answers_must_be_reviewed_in_real_time_by_your_expert';
  static const continue_evaluation = 'continue_evaluation';
  static const start_evaluation = 'start_evaluation';
  static const evaluation_done_you_have_acquired_this_module =
      'evaluation_done_you_have_acquired_this_module';
  static const this_module_has_been_acquired_but_you_can_still_restart_the_evaluation =
      'this_module_has_been_acquired_but_you_can_still_restart_the_evaluation';
  static const restart_evaluation = 'restart_evaluation';
  static const go_to_catalog = 'go_to_catalog';
  static const keep_it_up_your_answers_have_been_sent_to =
      'keep_it_up_your_answers_have_been_sent_to';
  static const acquisition_date_has_been_updated =
      'acquisition_date_has_been_updated';
  static const module_will_be_acquired = 'module_will_be_acquired';
  static const start_the_quiz = 'start_the_quiz';
  static const face_to_face_evaluation = 'face_to_face_evaluation';
  static const to_mark_this_module_as_acquired_you_must_confirm_that_you_have_understood_it =
      'to_mark_this_module_as_acquired_you_must_confirm_that_you_have_understood_it';
  static const to_mark_this_module_as_acquired_you_must_pass_the_assessment_quiz =
      'to_mark_this_module_as_acquired_you_must_pass_the_assessment_quiz';
  static const to_mark_this_module_as_acquired_you_must_pass_the_face_to_face_evaluation =
      'to_mark_this_module_as_acquired_you_must_pass_the_face_to_face_evaluation';
  static const not_yet = 'not_yet';
  static const do_it_later = 'do_it_later';
  static const start = 'start';
  static const comment_will_be_deleted = 'comment_will_be_deleted';
  static const this_is_a_permanent_action = 'this_is_a_permanent_action';
  static const ok_delete = 'ok_delete';
  static const comment_deleted = 'comment_deleted';
  static const new_badge_acquired = 'new_badge_acquired';
  static const view_my_badges = 'view_my_badges';
  static const modules_acquired__1 = 'modules_acquired__1';
  static const reactions_to_modules__1 = 'reactions_to_modules__1';
  static const trainings_completed = 'trainings_completed';
  static const modules_added_to_favorites = 'modules_added_to_favorites';
  static const module_acquired = 'module_acquired';
  static const reaction_to_modules = 'reaction_to_modules';
  static const trainings_completed__1 = 'trainings_completed__1';
  static const modules_added_to_favorites__1 = 'modules_added_to_favorites__1';
  static const module_added_to_favorites = 'module_added_to_favorites';
  static const like_recommend = 'like_recommend';
  static const Replies = 'Replies';
  static const your_answers_will_be_lost = 'your_answers_will_be_lost';
  static const if_you_leave_the_quiz_now_your_answers_will_be_lost_are_you_sure =
      'if_you_leave_the_quiz_now_your_answers_will_be_lost_are_you_sure';
  static const congratulations_you_just_acquired_a_new_module_keep_it_up =
      'congratulations_you_just_acquired_a_new_module_keep_it_up';
  static const here_is_one_more_module_acquired =
      'here_is_one_more_module_acquired';
  static const back_to_training = 'back_to_training';
  static const see_the_correction = 'see_the_correction';
  static const ready_to_try_again = 'ready_to_try_again';
  static const retry_quiz = 'retry_quiz';
  static const back_to_module = 'back_to_module';
  static const you_can_take_this_quiz_as_many_times_as_needed_100_of_correct_answers_will_mark_this_module_as_acquired =
      'you_can_take_this_quiz_as_many_times_as_needed_100_of_correct_answers_will_mark_this_module_as_acquired';
  static const quiz_results = 'quiz_results';
  static const only_right_answers_congratulations =
      'only_right_answers_congratulations';
  static const keep_going_check_your_mistakes_and_try_again =
      'keep_going_check_your_mistakes_and_try_again';
  static const check_your_mistakes_and_try_again =
      'check_your_mistakes_and_try_again';
  static const corrections = 'corrections';
  static const right_answer = 'right_answer';
  static const wrong_answer = 'wrong_answer';
  static const partially_right = 'partially_right';
  static const select_your_expert = 'select_your_expert';
  static const please_select_the_expert_who_reviewed_your_answers_after_validation_they_will_receive_a_copy_of_this_evaluation_by_email =
      'please_select_the_expert_who_reviewed_your_answers_after_validation_they_will_receive_a_copy_of_this_evaluation_by_email';
  static const my_expert = 'my_expert';
  static const validate_answers = 'validate_answers';
  static const select_an_expert = 'select_an_expert';
  static const item_will_be_downloaded_see_notifications_for_further_details__1 =
      'item_will_be_downloaded_see_notifications_for_further_details__1';
  static const please_fill_all_required_fields__1 =
      'please_fill_all_required_fields__1';
  static const answers_saved_you_can_come_back_later_to_finish_the_evaluation =
      'answers_saved_you_can_come_back_later_to_finish_the_evaluation';
  static const hmm_it_seems_there_is_no_expert_yet =
      'hmm_it_seems_there_is_no_expert_yet';
  static const you_must_select_an_expert_before_saving =
      'you_must_select_an_expert_before_saving';
  static const playlists = 'playlists';
  static const no_module_yet = 'no_module_yet';
  static const no_playlist_yet = 'no_playlist_yet';
  static const all_modules_available_will_show_up_here =
      'all_modules_available_will_show_up_here';
  static const all_playlists_available_will_show_up_here =
      'all_playlists_available_will_show_up_here';
  static const theme_ = 'theme_';
  static const favorites = 'favorites';
  static const filter = 'filter';
  static const show_only_favorites = 'show_only_favorites';
  static const theme = 'theme';
  static const clear_all = 'clear_all';
  static const follow_this_module_in_your_training =
      'follow_this_module_in_your_training';
  static const no = 'no';
  static const yes = 'yes';
  static const follow_this_playlist_in_your_training =
      'follow_this_playlist_in_your_training';
  static const this_module_is_part_of_the = 'this_module_is_part_of_the';
  static const this_playlist_is_part_of_the = 'this_playlist_is_part_of_the';
  static const results = 'results';
  static const result = 'result';
  static const module_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression =
      'module_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression';
  static const playlist_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression =
      'playlist_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression';
  static const username_commented = 'username_commented';
  static const interviews_are_done_when_all_participants_have_submitted_their_answers_including_cross_review =
      'interviews_are_done_when_all_participants_have_submitted_their_answers_including_cross_review';
  static const new_targets_will_be_added_to_roadmap_after_submitting_all_the_interviews_in_the_set =
      'new_targets_will_be_added_to_roadmap_after_submitting_all_the_interviews_in_the_set';
  static const assigned_to = 'assigned_to';
  static const option_list_cant_be_empty = 'option_list_cant_be_empty';
  static const options_cant_be_blank = 'options_cant_be_blank';
  static const not_in_target_ = 'not_in_target_';
  static const not_defined = 'not_defined';
  static const headline = 'headline';
  static const updated = 'updated';
  static const created = 'created';
  static const description_updated = 'description_updated';
  static const title_updated = 'title_updated';
  static const due_date_updated = 'due_date_updated';
  static const there_is_no_content_in_this_training_at_the_moment =
      'there_is_no_content_in_this_training_at_the_moment';
  static const you_are_already_in_charge_of_this_interview =
      'you_are_already_in_charge_of_this_interview';
  static const no_theme_yet = 'no_theme_yet';
  static const none = 'none';
  static const not_validated = 'not_validated';
  static const module_added_to_favorites__1 = 'module_added_to_favorites__1';
  static const module_removed_from_favorites = 'module_removed_from_favorites';
  static const your_team_current_targets_will_show_up_here =
      'your_team_current_targets_will_show_up_here';
  static const your_team_archived_targets_will_show_up_here =
      'your_team_archived_targets_will_show_up_here';
  static const person_in_charge_updated_successfully =
      'person_in_charge_updated_successfully';
  static const playlist_added_to_favorites = 'playlist_added_to_favorites';
  static const playlist_removed_from_favorites =
      'playlist_removed_from_favorites';
  static const this_module_is_not_accessible_for_you =
      'this_module_is_not_accessible_for_you';
  static const youve_reached_the_end_please_notify_your_expert_before_starting_the_evaluation =
      'youve_reached_the_end_please_notify_your_expert_before_starting_the_evaluation';
  static const one_reply = 'one_reply';
  static const favorite = 'favorite';
  static const an_error_occurred_please_try_again =
      'an_error_occurred_please_try_again';
  static const download_pdf = 'download_pdf';
  static const view_updates = 'view_updates';
  static const you_are_logged_out_please_sign_in_to_continue =
      'you_are_logged_out_please_sign_in_to_continue';
  static const if_you_add_a_playlist_to_favorites_it_will_show_up_here =
      'if_you_add_a_playlist_to_favorites_it_will_show_up_here';
}

class Locales {
  static const en_US = {
    'custom_and_accessible_progress_for_life':
        'Custom and accessible progress for life.',
    'login': 'Login',
    'continue_': 'Continue',
    'or': 'or',
    'sign_in_with_google': 'Sign in with Google',
    'oops_it_seems_this_email_is_not_registered':
        'Oops! It seems this email is not registered',
    'you_may_have_misswritten_your_email_please_try_again':
        'You may have misswritten your email, please try again.',
    'try_again': 'Try again',
    'email': 'Email',
    'password': 'Password',
    'i_forgot_my_password': 'I forgot my password',
    'check_your_mail': 'Check your email',
    'this_is_the_first_time_you_log_in_to_aleph_we_just_sent_an_email_with_your_password_to_the_address':
        'This is the first time you log in to OpenAleph. We just sent an email with your password to the address:',
    'if_you_didn_t_receive_it_check_your_spams_or_click_on_the_button_below_to_send_it_again':
        'If you didn\'t receive it, check your spams or click on the button below to send it again.',
    'back_to_login': 'Back to login',
    'send_it_again': 'Send it again',
    'wrong_password': 'Wrong password',
    'we_sent_an_email_to_reset_your_password_to_the_address':
        'We sent an email to reset your password to the address:',
    'reset_password': 'Reset password',
    'must_contain_at_least_8_characters': 'Must contain at least 8 characters',
    'confirm_password': 'Confirm password',
    'password_and_confirmation_must_match':
        'Password and confirmation must match',
    'password_has_been_reset': 'Password has been reset.',
    'password_must_contain_at_least_8_characters':
        'Password must contain at least 8 characters',
    'welcome_on_board': 'Welcome on board!',
    'all': 'All',
    'to_do': 'To do',
    'Done': 'Done',
    'start_feedback': 'Start feedback',
    'manage_reviewers': 'Manage reviewers',
    'view_answers': 'View answers',
    'check_my_answers': 'Check my answers',
    'nothing_to_show_for_now': 'Nothing to show for now',
    'all_your_interviews_and_surveys_will_show_up_here':
        'All your interviews and surveys will show up here.',
    'your_current_interviews_and_surveys_will_show_up_here':
        'Your current interviews and surveys will show up here.',
    'my_trainings': 'My Trainings',
    'catalog': 'Catalog',
    'my_interviews': 'My Interviews',
    'my_team_interviews': 'My Team Interviews',
    'current': 'Current',
    'archived': 'Archived',
    'your_current_targets_will_show_up_here':
        'Your current targets will show up here.',
    'your_archived_targets_will_show_up_here':
        'Your archived targets will show up here.',
    'aleph_apps': 'OpenAleph Apps',
    'profile': 'Profile',
    'profile_details': 'Profile details',
    'my_library': 'My library',
    'my_badges': 'My badges',
    'language': 'Language',
    'log_out': 'Log out',
    'recommend_aleph': 'Recommend OpenAleph',
    'help_and_support': 'Help & Support',
    'first_name': 'First name*',
    'last_name': 'Last name*',
    'email__1': 'Email*',
    'date_of_birth': 'Date of birth',
    'hire_date': 'Hire date',
    'personal_information': 'Personal information',
    'hr_information': 'HR information',
    'your_changes_will_be_lost': 'Your changes will be lost',
    'you_have_unsaved_changes_on_this_page_if_you_quit_without_saving_they_will_be_lost':
        'You have unsaved changes on this page: if you quit without saving, they will be lost.',
    'ok_quit': 'OK, quit',
    'access_level': 'Access level',
    'admin': 'Admin',
    'manager_creator': 'Manager_creator',
    'manager': 'Manager',
    'employee': 'Employee',
    'save': 'Save',
    'profile_has_been_updated': 'Profile has been updated.',
    'select_date': 'Select date',
    'enter_date': 'Enter date',
    'date': 'Date',
    'ok': 'OK',
    'clear_field': 'Clear field',
    'some_fields_are_invalid': 'Some fields are invalid.',
    'please_fill_all_required_fields': 'Please fill all required fields (*)',
    'invalid_format': 'Invalid format',
    'edit_profile_photo': 'Edit profile photo',
    'import_photo': 'Import photo',
    'delete_current_photo': 'Delete current photo',
    'english': 'English',
    'french': 'French',
    'language_has_been_modified': 'Language has been modified.',
    'change_password': 'Change password',
    'new_password': 'New password',
    'confirm_new_password': 'Confirm new password',
    'confirmation_and_password_must_match':
        'Confirmation and password must match',
    'accomplished_apprentice': 'Accomplished Apprentice',
    'trained_and_competent': 'Trained and Competent',
    'certified_expert': 'Certified Expert',
    'training_champion': 'Training Champion',
    'training_completed': 'training completed',
    'the_selector': 'The Selector',
    'content_lover': 'Content Lover',
    'informed_collector': 'Informed Collector',
    'resource_guardian': 'Resource Guardian',
    'modules_in_favorites': 'modules in favorites',
    'the_approver': 'The Approver',
    'positive_supporter': 'Positive Supporter',
    'enlightened_evaluator': 'Enlightened Evaluator',
    'master_of_emotions': 'Master of Emotions',
    'reactions_to_modules': 'reactions to modules',
    'enlightened_novice': 'Enlightened Novice',
    'knowledge_explorer': 'Knowledge Explorer',
    'thirsty_for_knowledge': 'Thirsty for Knowledge',
    'curriculum_master': 'Curriculum Master',
    'modules_acquired': 'modules acquired',
    'if_you_add_a_module_to_favorites_it_will_show_up_here':
        'If you add a Module to favorites, it will show up here.',
    'this_field_is_required': 'This field is required',
    'your_form_has_been_sent_thank_you': 'Your form has been sent. Thank you!',
    'do_you_want_to_recommend_aleph_to_someone_please_fill_out_this_form_our_team_will_contact_the_interested_person_very_soon':
        'Do you want to recommend OpenAleph to someone? Please fill out this form, our team will contact the interested person very soon!',
    'target_person_s_email': 'Target person\'s email*',
    'your_message': 'Your message*',
    'send': 'Send',
    'if_you_have_any_questions_or_require_assistance_please_feel_free_to_reach_out_to_the_aleph_team_by_filling_out_this_form_we_will_respond_to_your_message_very_soon':
        'If you have any questions or require assistance, please feel free to  reach out to the OpenAleph team by filling out this form. We will respond to your message very soon!',
    'your_issue': 'Your issue*',
    'are_you_sure_you_want_to_log_out': 'Are you sure you want to log out?',
    'cancel': 'Cancel',
    'yes_log_out': 'Yes, log out',
    'show_only_pinned_answers': 'Show only pinned answers',
    'select_a_question': 'Select a question',
    'question': 'Question',
    'tag_category': 'Tag category',
    'tag': 'Tag',
    'answers': '%s answers',
    'answer': '%s answer',
    'add_a_comment': 'Add a comment',
    'answer_pinned': 'Answer pinned.',
    'answer_removed_from_pins': 'Answer removed from pins.',
    'select_category': 'Select category',
    'search': 'Search',
    'Comments': 'Comments',
    'tap_on_the_pin_icon_to_collect_the_most_interesting_answers_your_pinned_answers_will_show_up_here':
        'Tap on the pin icon to collect the most interesting answers. Your pinned answers will show up here.',
    'oops_we_didn_t_find_any_results_matching_your_search':
        'Oops, we didn’t find any results matching your search.',
    'no_comments_yet': 'No comments yet',
    'comments': '%s comments',
    'comment': '%s comment',
    'add_reviewers': 'Add reviewers',
    'reviewers': 'Reviewers',
    'set_another_person_in_charge': 'Set another person in charge',
    'reviewer_set': 'reviewer set',
    'reviewers_set': 'reviewers set',
    'done': 'done',
    'in_progress': 'In progress',
    'feedback_on_': 'Feedback on',
    'not_started': 'Not started',
    'Submitted_': 'Submitted :',
    'participant__': 'Participant',
    'all_your_team_s_interviews_and_surveys_will_show_up_here':
        'All your team’s interviews and surveys will show up here.',
    'your_team_s_current_interviews_and_surveys_will_show_up_here':
        'Your team’s current interviews and surveys will show up here.',
    'send_reminder_notification': 'Send reminder notification',
    'search_by_name': 'Search by name',
    'all_reviewers_for_this_interview_will_show_up_here':
        'All reviewers for this interview will show up here.',
    'no_reviewers_yet': 'No reviewers yet',
    'start_interview': 'Start interview',
    'continue_interview': 'Continue interview',
    'start_cross_review': 'Start cross review',
    'continue_cross_review': 'Continue cross review',
    'view_interviewee_answers': 'View interviewee answers',
    'check_cross_review': 'Check cross review',
    'continue_feedback': 'Continue feedback',
    'start_self_evaluation': 'Start self-evaluation',
    'choose_reviewers': 'Choose reviewers',
    'reviewer_successfully_added': 'Reviewer successfully added.',
    'reviewer_removed': 'Reviewer removed.',
    'remove_reviewer': 'Remove reviewer?',
    'this_reviewer_has_started_to_answer_if_you_continue_their_answers_will_be_permanently_deleted':
        'This reviewer has started to answer. If you continue, their answers will be permanently deleted.',
    'yes_remove': 'Yes, remove',
    'submitted': 'Submitted',
    'you_must_select_at_least_1_reviewer':
        'You must select at least 1 reviewer.',
    'this_user_is_already_set_as_reviewer_and_can_t_be_added':
        'This user is already set as reviewer and can’t be added.',
    'modifications_to_reviewers_will_be_lost':
        'Modifications to reviewers will be lost',
    'you_added_reviewers_to_this_interview_if_you_leave_without_saving_your_modifications_will_be_lost':
        'You added reviewers to this interview. If you leave without saving, your modifications will be lost.',
    'yes_leave': 'Yes, leave',
    'you_will_no_longer_be_in_charge': 'You will no longer be in charge',
    'reminder_sent_the_employee_will_receive_an_email_in_a_few_moments':
        'Reminder sent. The employee will receive an email in a few moments.',
    'you_won_t_be_able_to_participate_anymore':
        'You won’t be able to participate anymore',
    'this_survey_will_no_longer_appear_in_your_my_interviews_section':
        'This survey will no longer appear in your “My interviews” section.',
    'yes_decline_survey': 'Yes, decline survey',
    'survey_declined_successfully': 'Survey declined successfully.',
    'start_survey': 'Start survey',
    'continue_survey': 'Continue survey',
    'decline_survey': 'Decline survey',
    'you_will_lose_access_to_this_interview_in_the_my_team_interviews_section':
        'You will lose access to this interview in the “My Team Interviews” section.',
    'will_receive_a_notification': 'will receive a notification.',
    'submit': 'Submit',
    'person_in_charge_': 'Person in charge',
    'status_': 'Status:',
    'deadline_': 'Deadline',
    'participant_': 'Participant',
    'create_new_target': 'Create new target',
    'edit_target': 'Edit target',
    'feedback_about_': 'Feedback about',
    'summary': 'Summary',
    'interview_updates': 'Interview updates',
    'answer__1': 'Answer',
    'Comment': 'Comment',
    'saved': 'Saved',
    'answers_saved_you_can_come_back_later_to_finish_the_interview':
        'Answers saved. You can come back later to finish the interview.',
    'your_answers_can_t_be_saved_please_check_your_connection':
        'Your answers can’t be saved, please check your connection.',
    'trying_to_reconnect': 'Trying to reconnect...',
    'item_will_be_downloaded_see_notifications_for_further_details_1':
        '1 item will be downloaded. See notifications for further details.',
    'your_interview_has_been_submitted_your_answers_are_locked':
        'Your interview has been submitted, your answers are locked.',
    'please_answer_all_required_questions':
        'Please answer all required questions (*).',
    'submitting_this_interview_will_lock_your_answers_ask_to_an_admin_to_unlock_them_if_needed':
        'Submitting this interview will lock your answers. Ask to an admin to unlock them if needed.',
    'answers_will_be_locked': 'Answers will be locked',
    'none_of_the_targets_have_been_updated':
        'None of the targets have been updated',
    'this_interview_contains_one_or_more_targets_do_you_want_to_continue_anyway':
        'This interview contains one or more targets. Do you want to continue anyway?',
    'if_you_confirm_all_target_creations_and_updates_will_be_applied_you_can_follow_this_up_in_roadmap_app':
        'If you confirm, all target creations and updates will be applied. You can follow this up in Roadmap app.',
    'username_updated': '%s updated',
    'confirm': 'Confirm',
    'completed': 'Completed',
    'my_roadmap': 'My Roadmap',
    'my_team_roadmaps': 'My Team Roadmaps',
    'continue_self_evaluation': 'Continue self-evaluation',
    'new_target': 'New target',
    'not_set': 'Not set',
    'type_and_confirm_your_new_password': 'Type and confirm your new password',
    'current_targets': 'current targets',
    'archived_targets': 'archived targets',
    'starting_value_': 'Starting value',
    'target_value_': 'Target value',
    'target': 'Target',
    'last_events': 'Last events',
    'update_value': 'Update value',
    'not_set_yet': 'Not set yet',
    'apply': 'Apply',
    'target_achieved': 'Target achieved!',
    'value': 'Value',
    'comment_successfully_added': 'Comment successfully added.',
    'value_successfully_updated': 'Value successfully updated.',
    'please_change_the_target_value_or_add_a_comment_to_apply':
        'Please change the target value or add a comment to apply.',
    'create_target': 'Create target',
    'target_information': 'Target information',
    'assign_target': 'Assign target',
    'choose_target_indicator': 'Choose target indicator',
    'target_summary': 'Target summary',
    'or_start_from_a_template_': 'Or start from a template:',
    'for_non_quantifiable_targets_with_only_two_options_such_as_achieved_not_achieved':
        'For non quantifiable targets with only two options, such as “Achieved / Not achieved”.',
    'define_two_values_to_measure_the_target_false_will_be_the_starting_value_the_target_will_be_reached_when_switched_to_true':
        'Define two values to measure the target. “False” will be the Starting value. The target will be reached when switched to “True”.',
    'for_non_quantifiable_objectives_such_as_ratings_you_can_create_multiple_options_to_qualify_your_target':
        'For non quantifiable objectives, such as ratings. You can create multiple options to qualify your target.',
    'you_can_choose_which_option_or_options_will_be_considered_in_target_by_ticking_the_box_on_the_left':
        'You can choose which option (or options) will be considered “In target” by ticking the box on the left.',
    'for_quantifiable_targets_expressed_in_percentage':
        'For quantifiable targets, expressed in percentage.',
    'set_a_percentage_based_value_for_the_starting_and_target_value':
        'Set a percentage-based value for the Starting and Target value.',
    'for_quantifiable_targets_people_money_apples':
        'For quantifiable targets (people, money, apples...).',
    'define_a_starting_and_a_target_value_to_measure_the_target_completion':
        'Define a Starting and a Target value to measure the target completion.',
    'new_target_from_scratch': 'New target from scratch',
    'search_template': 'Search template',
    'title': 'Title*',
    'description': 'Description',
    'deadline': 'Deadline',
    'previous': 'Previous',
    'next': 'Next',
    'target_indicator': 'Target indicator',
    'true_false': 'True / False',
    'percentage': 'Percentage',
    'number': 'Number',
    'multi_choice': 'Multi choice',
    'starting_value': 'Starting value*',
    'target_value': 'Target value*',
    'target_assigned_to': 'Target assigned to',
    'view_all': 'View all',
    'read_all': 'Read all',
    'target_indicator_': 'Target indicator',
    'users_will_show_up_here': 'Users will show up here.',
    'option': 'Option',
    'this_value_is_locked_and_cant_be_edited':
        'This value is locked and can’t be edited.',
    'add_option': 'Add option',
    'tap_on_new_target_from_scratch_or_choose_a_template':
        'Tap on “New target from scratch” or choose a template',
    'title_field_cant_be_empty': '“Title” field can’t be empty.',
    'select_at_least_1_person': 'Select at least 1 person.',
    'please_set_a_starting_value_and_a_target_value':
        'Please set a “Starting value” and a “Target value”.',
    'come_back_later_to_see_your_trainings':
        'Come back later to see your trainings!',
    'no_training_completed_yet': 'No training completed... yet!',
    'modules': '%s modules',
    'module': '%s module',
    'total_duration': 'Total duration',
    'min': 'min',
    'view_all_themes': 'View all themes',
    'view_less': 'View less',
    'content': 'Content',
    'playlist': 'Playlist',
    'Module': 'Module',
    'training_updated_on_date': 'Training updated',
    'start_training': 'Start training',
    'resume_training': 'Resume training',
    'no_duration': 'No duration',
    'this_playlist_is_empty': 'This playlist is empty.',
    'Modules': 'Modules',
    'playlist_added_to_your_library': 'Playlist added to your library.',
    'duration': 'Duration',
    'yes_its_all_clear_to_me': 'Yes, it’s all clear to me!',
    'youve_reached_the_end_did_you_learn_all_the_following_learning_objectives':
        'You\'ve reached the end! Did you learn all the following learning objectives?',
    'one_more_module_acquired': 'One more module acquired',
    'congratulations': 'Congratulations,',
    'keep_it_up': ', keep it up!',
    'replies': 'replies',
    'reply': 'Reply',
    'what_did_you_think_of_the_module': 'What did you think of the module?',
    'your_feedback_helps_us_improve_this_module':
        'Your feedback helps us improve this module.',
    'i_like': 'I like',
    'i_recommend': 'I recommend',
    'next_module': 'Next module',
    'acquired': 'Acquired',
    'update_acquisition_date': 'Update acquisition date',
    'youve_reached_the_end_ready_for_the_quiz':
        'You’ve reached the end! Ready for the quiz?',
    'to_complete_this_module_evaluate_yourself_with_a_quiz':
        'To complete this module, evaluate yourself with a quiz.',
    'start_quiz': 'Start quiz',
    'retake_quiz': 'Retake quiz',
    'youve_reached_the_end_validate_your_evaluation_to_complete_this_module':
        'You’ve reached the end!  Validate your evaluation to complete this module.',
    'to_successfully_complete_this_module_your_answers_must_be_reviewed_in_real_time_by_your_expert':
        'To successfully complete this module, your answers must be reviewed in real time by your expert.',
    'continue_evaluation': 'Continue evaluation',
    'start_evaluation': 'Start evaluation',
    'evaluation_done_you_have_acquired_this_module':
        'Evaluation done, you have acquired this module!',
    'this_module_has_been_acquired_but_you_can_still_restart_the_evaluation':
        'This module has been acquired, but you can still restart the evaluation.',
    'restart_evaluation': 'Restart evaluation',
    'go_to_catalog': 'Go to catalog',
    'keep_it_up_your_answers_have_been_sent_to':
        ', keep it up! Your answers have been sent to',
    'acquisition_date_has_been_updated': 'Acquisition date has been updated',
    'module_will_be_acquired': 'Module will be acquired',
    'start_the_quiz': 'Start the quiz',
    'face_to_face_evaluation': 'Face-to-face evaluation',
    'to_mark_this_module_as_acquired_you_must_confirm_that_you_have_understood_it':
        'To mark this module as “acquired”, you must confirm that you have understood it.',
    'to_mark_this_module_as_acquired_you_must_pass_the_assessment_quiz':
        'To mark this module as “acquired”, you must pass the assessment quiz.',
    'to_mark_this_module_as_acquired_you_must_pass_the_face_to_face_evaluation':
        'To mark this module as “acquired”, you must pass the face-to-face evaluation.',
    'not_yet': 'Not yet',
    'do_it_later': 'Do it later',
    'start': 'Start',
    'comment_will_be_deleted': 'Comment will be deleted',
    'this_is_a_permanent_action': 'This is a permanent action.',
    'ok_delete': 'OK, delete',
    'comment_deleted': 'Comment deleted.',
    'new_badge_acquired': 'New badge acquired!',
    'view_my_badges': 'View my badges',
    'modules_acquired__1': 'Modules acquired',
    'reactions_to_modules__1': 'Reactions to modules',
    'trainings_completed': 'Trainings completed',
    'modules_added_to_favorites': 'Modules added to favorites',
    'module_acquired': 'module acquired',
    'reaction_to_modules': 'reaction to modules',
    'trainings_completed__1': 'trainings completed',
    'modules_added_to_favorites__1': 'modules added to favorites',
    'module_added_to_favorites': 'module added to favorites',
    'like_recommend': 'Like & recommend',
    'Replies': 'Replies',
    'your_answers_will_be_lost': 'Your answers will be lost',
    'if_you_leave_the_quiz_now_your_answers_will_be_lost_are_you_sure':
        'If you leave the quiz now, your answers will be lost. Are you sure?',
    'congratulations_you_just_acquired_a_new_module_keep_it_up':
        'Congratulations, you just acquired a new module, keep it up!',
    'here_is_one_more_module_acquired': 'Here is one more module acquired',
    'back_to_training': 'Back to training',
    'see_the_correction': 'See the correction',
    'ready_to_try_again': 'Ready to try again?',
    'retry_quiz': 'Retry quiz',
    'back_to_module': 'Back to module',
    'you_can_take_this_quiz_as_many_times_as_needed_100_of_correct_answers_will_mark_this_module_as_acquired':
        'You can take this quiz as many times as needed. 100% of correct answers will mark this module as acquired.',
    'quiz_results': 'Quiz results',
    'only_right_answers_congratulations':
        'Only right answers! Congratulations,',
    'keep_going_check_your_mistakes_and_try_again':
        'Keep going! Check your mistakes and try again',
    'check_your_mistakes_and_try_again': 'Check your mistakes and try again',
    'corrections': 'Corrections',
    'right_answer': 'Right answer',
    'wrong_answer': 'Wrong answer',
    'partially_right': 'Partially right',
    'select_your_expert': 'Select your expert*',
    'please_select_the_expert_who_reviewed_your_answers_after_validation_they_will_receive_a_copy_of_this_evaluation_by_email':
        'Please select the expert who reviewed your answers. After validation, they will receive a copy of this evaluation by email.',
    'my_expert': 'My expert',
    'validate_answers': 'Validate answers',
    'select_an_expert': 'Select an expert',
    'item_will_be_downloaded_see_notifications_for_further_details__1':
        '1 item will be downloaded.\nSee notifications for further details.',
    'please_fill_all_required_fields__1':
        'Please fill all required fields (*).',
    'answers_saved_you_can_come_back_later_to_finish_the_evaluation':
        'Answers saved. You can come back later to finish the evaluation.',
    'hmm_it_seems_there_is_no_expert_yet':
        'Hmm, it seems there is no expert yet.',
    'you_must_select_an_expert_before_saving':
        'You must select an expert before saving.',
    'playlists': 'Playlists',
    'no_module_yet': 'No module yet',
    'no_playlist_yet': 'No playlist yet',
    'all_modules_available_will_show_up_here':
        'All modules availables will show up here.',
    'all_playlists_available_will_show_up_here':
        'All playlists availables will show up here.',
    'theme_': 'Theme:',
    'favorites': 'Favorites',
    'filter': 'Filter',
    'show_only_favorites': 'Show only favorites',
    'theme': 'Theme',
    'clear_all': 'Clear all',
    'follow_this_module_in_your_training':
        'Follow this module in your training?',
    'no': 'No',
    'yes': 'Yes',
    'follow_this_playlist_in_your_training':
        'Follow this playlist in your training?',
    'this_module_is_part_of_the': 'This module is part of the',
    'this_playlist_is_part_of_the': 'This playlist is part of the',
    'results': 'Results',
    'result': 'Result',
    'module_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression':
        'training. Would you like to follow it in this context to ensure a coherent progression?',
    'playlist_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression':
        'training. Would you like to follow it in this context to ensure a coherent progression?',
    'username_commented': '%s commented',
    'interviews_are_done_when_all_participants_have_submitted_their_answers_including_cross_review':
        'Interviews are “Done” when all participants have submitted their answers (including Cross Review).',
    'new_targets_will_be_added_to_roadmap_after_submitting_all_the_interviews_in_the_set':
        'New targets will be added to Roadmap after submitting all the interviews in the set.',
    'assigned_to': 'Assigned to',
    'option_list_cant_be_empty': 'Option list can’t be empty.',
    'options_cant_be_blank': 'Options can’t be blank.',
    'not_in_target_': 'Not in target',
    'not_defined': 'Not defined',
    'headline': 'These targets will be updated or created',
    'updated': 'Updated',
    'created': 'Created',
    'description_updated': 'Description updated',
    'title_updated': 'Title updated',
    'due_date_updated': 'Due date updated',
    'there_is_no_content_in_this_training_at_the_moment':
        'There is no content in this training at the moment.',
    'you_are_already_in_charge_of_this_interview':
        'You are already in charge of this interview.',
    'no_theme_yet': 'No theme yet',
    'none': 'None',
    'not_validated': 'Not validated',
    'module_added_to_favorites__1': 'Module added to favorites.',
    'module_removed_from_favorites': 'Module removed from favorites.',
    'your_team_current_targets_will_show_up_here':
        'Your team’s current targets will show up here.',
    'your_team_archived_targets_will_show_up_here':
        'Your team’s archived targets will show up here.',
    'person_in_charge_updated_successfully':
        'Person in charge updated successfully.',
    'playlist_added_to_favorites': 'Playlist added to favorites.',
    'playlist_removed_from_favorites': 'Playlist removed from favorites.',
    'this_module_is_not_accessible_for_you':
        'This module is not accessible for you',
    'youve_reached_the_end_please_notify_your_expert_before_starting_the_evaluation':
        'You\'ve reached the end! Please notify your expert before starting the evaluation.',
    'one_reply': 'reply',
    'favorite': 'Favorite',
    'an_error_occurred_please_try_again':
        'An error occurred, please try again.',
    'download_pdf': 'Download PDF',
    'view_updates': 'View updates',
    'you_are_logged_out_please_sign_in_to_continue':
        'You are logged out. Please sign in to continue.',
    'if_you_add_a_playlist_to_favorites_it_will_show_up_here':
        'If you add a Playlist to favorites, it will show up here.',
  };
  static const fr_FR = {
    'custom_and_accessible_progress_for_life':
        'Custom and accessible progress for life.',
    'login': 'Connexion',
    'continue_': 'Continuer',
    'or': 'ou',
    'sign_in_with_google': 'Se connecter avec Google',
    'oops_it_seems_this_email_is_not_registered':
        'Oups ! Il semble que cette adresse e-mail ne soit pas enregistrée',
    'you_may_have_misswritten_your_email_please_try_again':
        'Vous avez peut-être mal écrit votre adresse e-mail, veuillez réessayer.',
    'try_again': 'Réessayer',
    'email': 'Adresse e-mail',
    'password': 'Mot de passe',
    'i_forgot_my_password': 'Mot de passe oublié',
    'check_your_mail': 'Vérifiez votre boîte mail',
    'this_is_the_first_time_you_log_in_to_aleph_we_just_sent_an_email_with_your_password_to_the_address':
        'C\'est la première fois que vous vous connectez à OpenAleph. Nous venons d\'envoyer un e-mail avec votre mot de passe à l\'adresse suivante :',
    'if_you_didn_t_receive_it_check_your_spams_or_click_on_the_button_below_to_send_it_again':
        'Si vous ne l\'avez pas reçu, vérifiez vos spams ou cliquez sur le bouton ci-dessous pour l\'envoyer à nouveau.',
    'back_to_login': 'Retour à la page de connexion',
    'send_it_again': 'Renvoyer',
    'wrong_password': 'Mot de passe incorrect',
    'we_sent_an_email_to_reset_your_password_to_the_address':
        'Nous avons envoyé un e-mail pour réinitialiser votre mot de passe à l\'adresse suivante :',
    'reset_password': 'Nouveau mot de passe',
    'must_contain_at_least_8_characters': 'Doit contenir au moins 8 caractères',
    'confirm_password': 'Confirmer le mot de passe',
    'password_and_confirmation_must_match':
        'La confirmation et le mot de passe doivent être identiques',
    'password_has_been_reset': 'Le mot de passe a été réinitialisé.',
    'password_must_contain_at_least_8_characters':
        'Le mot de passe doit contenir au moins 8 caractères',
    'welcome_on_board': 'Bienvenue à bord !',
    'profile': 'Profil',
    'profile_details': 'Détails du profil',
    'my_library': 'Ma bibliothèque',
    'my_badges': 'Mes badges',
    'language': 'Langue',
    'log_out': 'Se déconnecter',
    'recommend_aleph': 'Recommander OpenAleph',
    'help_and_support': 'Aide & Assistance',
    'first_name': 'Prénom*',
    'last_name': 'Nom de famille*',
    'email__1': 'Email*',
    'date_of_birth': 'Date de naissance',
    'manager': 'Manager',
    'hire_date': 'Date d\'embauche',
    'personal_information': 'Informations personnelles',
    'hr_information': 'Informations RH',
    'your_changes_will_be_lost': 'Vos modifications seront perdues',
    'you_have_unsaved_changes_on_this_page_if_you_quit_without_saving_they_will_be_lost':
        'Vous avez des modifications non enregistrées sur cette page : si vous quittez sans enregistrer, elles seront perdues.',
    'cancel': 'Annuler',
    'ok_quit': 'OK, quitter',
    'access_level': 'Niveau d\'accès',
    'admin': 'Admin',
    'manager_creator': 'Manager Créateur',
    'employee': 'Employé',
    'save': 'Enregistrer',
    'profile_has_been_updated': 'Le profil a été mis à jour.',
    'select_date': 'Sélectionner une date',
    'enter_date': 'Saisir une date',
    'date': 'Date',
    'ok': 'OK',
    'clear_field': 'Vider le champ',
    'some_fields_are_invalid': 'Certains champs sont invalides.',
    'please_fill_all_required_fields':
        'Veuillez remplir tous les champs obligatoires (*)',
    'invalid_format': 'Format invalide',
    'edit_profile_photo': 'Modifier la photo de profil',
    'import_photo': 'Importer une photo',
    'delete_current_photo': 'Supprimer la photo actuelle',
    'english': 'Anglais',
    'french': 'Français',
    'language_has_been_modified': 'La langue a été modifiée.',
    'change_password': 'Changer le mot de passe',
    'new_password': 'Nouveau mot de passe',
    'confirm_new_password': 'Confirmer le nouveau mot de passe',
    'confirmation_and_password_must_match':
        'La confirmation et le mot de passe doivent être identiques',
    'accomplished_apprentice': 'Apprenti Accompli',
    'trained_and_competent': 'Formé et Compétent',
    'certified_expert': 'Expert Agréé',
    'training_champion': 'Champion de la Formation',
    'training_completed': 'formation complétée',
    'the_selector': 'Le Sélectionneur',
    'content_lover': 'Amoureux du Contenu',
    'informed_collector': 'Collecteur Averti',
    'resource_guardian': 'Gardien des Ressources',
    'modules_in_favorites': 'modules dans les favoris',
    'the_approver': 'L\'Approbateur',
    'positive_supporter': 'Supporter Positif',
    'enlightened_evaluator': 'Évaluateur Eclairé',
    'master_of_emotions': 'Maître des émotions',
    'reactions_to_modules': 'réactions aux modules',
    'enlightened_novice': 'Novice Éclairé',
    'knowledge_explorer': 'Explorateur de Connaissances',
    'thirsty_for_knowledge': 'Soif de Connaissances',
    'curriculum_master': 'Maître du Curriculum',
    'modules_acquired': 'modules acquis',
    'nothing_to_show_for_now': 'Rien à afficher pour le moment',
    'if_you_add_a_module_to_favorites_it_will_show_up_here':
        'Si vous ajoutez un module à vos favoris, il apparaîtra ici.',
    'this_field_is_required': 'Ce champ est requis',
    'your_form_has_been_sent_thank_you':
        'Votre formulaire a été envoyé. Merci !',
    'do_you_want_to_recommend_aleph_to_someone_please_fill_out_this_form_our_team_will_contact_the_interested_person_very_soon':
        'Voulez-vous recommander OpenAleph à quelqu\'un ? Veuillez remplir ce formulaire, notre équipe contactera la personne intéressée rapidement !',
    'target_person_s_email': 'E-mail de la personne ciblée*',
    'your_message': 'Votre message*',
    'send': 'Envoyer',
    'if_you_have_any_questions_or_require_assistance_please_feel_free_to_reach_out_to_the_aleph_team_by_filling_out_this_form_we_will_respond_to_your_message_very_soon':
        'Si vous avez des questions ou avez besoin d\'aide, n\'hésitez pas à contacter l\'équipe OpenAleph en remplissant ce formulaire. Nous répondrons à votre message rapidement !',
    'your_issue': 'Votre problème*',
    'are_you_sure_you_want_to_log_out':
        'Êtes-vous sûr de vouloir vous déconnecter ?',
    'yes_log_out': 'Oui, me déconnecter',
    'comments': '%s commentaires',
    'comment': '%s commentaire',
    'show_only_pinned_answers': 'Seulement réponses épinglées',
    'question': 'Question',
    'tag': 'Tag',
    'all': 'Tout',
    'tag_category': 'Catégorie',
    'answers': '%s réponses',
    'Comments': 'Commentaires',
    'add_a_comment': 'Ajouter un commentaire',
    'answer_pinned': 'Réponse épinglée.',
    'answer_removed_from_pins': 'Réponse retirée des épingles.',
    'tap_on_the_pin_icon_to_collect_the_most_interesting_answers_your_pinned_answers_will_show_up_here':
        'Tapez sur l\'icône épingle pour collecter les réponses les plus intéressantes. Vos réponses épinglées apparaîtront ici.',
    'select_a_question': 'Sélectionner une question',
    'no_comments_yet': 'Aucun commentaire',
    'answer': '%s réponse',
    'select_category': 'Choisir une catégorie',
    'search': 'Rechercher',
    'oops_we_didn_t_find_any_results_matching_your_search':
        'Oups, nous n\'avons pas trouvé de résultats correspondant à votre recherche.',
    'add_reviewers': 'Ajouter des évaluateurs',
    'reviewers': 'Évaluateurs',
    'set_another_person_in_charge': 'Définir une autre personne en charge',
    'reviewer_set': 'évaluateur défini',
    'reviewers_set': 'évaluateurs définis',
    'done': 'terminé',
    'in_progress': 'En cours',
    'feedback_on_': 'Feedback sur',
    'not_started': 'Pas commencé',
    'Submitted_': 'Envoyé :',
    'participant__': 'Participant',
    'all_your_team_s_interviews_and_surveys_will_show_up_here':
        'Tous les entretiens et les sondages de votre équipe apparaîtront ici.',
    'your_team_s_current_interviews_and_surveys_will_show_up_here':
        'Les entretiens et les sondages en cours de votre équipe apparaîtront ici.',
    'send_reminder_notification': 'Envoyer une notification de rappel',
    'search_by_name': 'Rechercher par nom',
    'all_reviewers_for_this_interview_will_show_up_here':
        'Tous les évaluateurs de cette interview apparaîtront ici.',
    'no_reviewers_yet': 'Pas encore d\'évaluateurs',
    'start_interview': 'Commencer l\'interview',
    'continue_interview': 'Continuer l\'interview',
    'start_cross_review': 'Commencer la Cross Review',
    'continue_cross_review': 'Continuer la Cross Review',
    'view_interviewee_answers': 'Voir les réponses de l\'interviewé',
    'check_cross_review': 'Voir la Cross Review',
    'continue_feedback': 'Continuer le feedback',
    'start_self_evaluation': 'Commencer l\'auto-évaluation',
    'choose_reviewers': 'Choisissez les évaluateurs',
    'reviewer_successfully_added': 'Évaluateurs ajoutés.',
    'reviewer_removed': 'Relecteur supprimé.',
    'remove_reviewer': 'Supprimer l\'évaluateur ?',
    'this_reviewer_has_started_to_answer_if_you_continue_their_answers_will_be_permanently_deleted':
        'Cet évaluateur a commencé à répondre. Si vous continuez, ses réponses seront définitivement supprimées.',
    'yes_remove': 'Oui, supprimer',
    'submitted': 'Envoyé',
    'you_must_select_at_least_1_reviewer':
        'Vous devez sélectionner au moins 1 élément.',
    'this_user_is_already_set_as_reviewer_and_can_t_be_added':
        'Cet utilisateur est déjà défini en tant que relecteur et ne peut pas être ajouté.',
    'modifications_to_reviewers_will_be_lost':
        'Les modifications apportées aux relecteurs seront perdues',
    'you_added_reviewers_to_this_interview_if_you_leave_without_saving_your_modifications_will_be_lost':
        'Vous avez ajouté des évaluateurs à cet interview. Si vous quittez sans enregistrer, vos modifications seront perdues.',
    'yes_leave': 'Oui, quitter',
    'you_will_no_longer_be_in_charge': 'Vous ne serez plus en charge',
    'reminder_sent_the_employee_will_receive_an_email_in_a_few_moments':
        'Rappel envoyé. L\'employé recevra un e-mail dans quelques instants.',
    'you_won_t_be_able_to_participate_anymore':
        'Vous ne pourrez plus participer',
    'this_survey_will_no_longer_appear_in_your_my_interviews_section':
        'Ce sondage n\'apparaîtra plus dans votre section "Mes Interviews".',
    'yes_decline_survey': 'Oui, refuser le sondage',
    'survey_declined_successfully': 'Sondage refusé.',
    'start_survey': 'Commencer le sondage',
    'continue_survey': 'Continuer le sondage',
    'decline_survey': 'Refuser le sondage',
    'my_interviews': 'Mes Interviews',
    'my_team_interviews': 'Mes Interviews d\'Équipe',
    'you_will_lose_access_to_this_interview_in_the_my_team_interviews_section':
        'Vous perdrez l\'accès à cet interview dans la section "Mes Interviews d\'Équipe".',
    'will_receive_a_notification': 'recevra une notification.',
    'to_do': 'À faire',
    'Done': 'Terminé',
    'start_feedback': 'Commencer le feedback',
    'manage_reviewers': 'Gérer les évaluateurs',
    'view_answers': 'Voir les réponses',
    'check_my_answers': 'Voir mes réponses',
    'all_your_interviews_and_surveys_will_show_up_here':
        'Tous vos interviews et sondages vont apparaître ici.',
    'your_current_interviews_and_surveys_will_show_up_here':
        'Vos interviews et sondages en cours vont apparaître ici.',
    'catalog': 'Catalogue',
    'my_trainings': 'Mes Formations',
    'current': 'Actuels',
    'archived': 'Archivés',
    'your_current_targets_will_show_up_here':
        'Vos objectifs actuels vont apparaître ici.',
    'your_archived_targets_will_show_up_here':
        'Vos objectifs archivés vont apparaître ici.',
    'aleph_apps': 'Applications OpenAleph',
    'submit': 'Envoyer',
    'person_in_charge_': 'Personne en charge',
    'status_': 'Statut :',
    'deadline_': 'Date limite :',
    'participant_': 'Participant',
    'create_new_target': 'Nouvel objectif',
    'edit_target': 'Modifier objectif',
    'feedback_about_': 'Feedback sur',
    'summary': 'Chapitres',
    'interview_updates': 'Mises à jour',
    'answer__1': 'Réponse',
    'Comment': 'Commentaire',
    'saved': 'Enregistré',
    'answers_saved_you_can_come_back_later_to_finish_the_interview':
        'Réponses enregistrées. Vous pouvez revenir plus tard pour terminer l\'interview.',
    'your_answers_can_t_be_saved_please_check_your_connection':
        'Les réponses ne peuvent pas être enregistrées, vérifiez votre connexion.',
    'trying_to_reconnect': 'Tentative de reconnexion...',
    'item_will_be_downloaded_see_notifications_for_further_details_1':
        '1 élément sera téléchargé. Voir les notifications pour plus de détails.',
    'your_interview_has_been_submitted_your_answers_are_locked':
        'Votre interview a été envoyé, vos réponses sont verrouillées.',
    'please_answer_all_required_questions':
        'Veuillez répondre aux questions obligatoires (*).',
    'submitting_this_interview_will_lock_your_answers_ask_to_an_admin_to_unlock_them_if_needed':
        'L\'envoi de cet interview va verrouiller vos réponses. Demandez à un admin de les débloquer si nécessaire.',
    'answers_will_be_locked': 'Les réponses seront verrouillées',
    'none_of_the_targets_have_been_updated': 'Aucun objectif a été mis à jour',
    'this_interview_contains_one_or_more_targets_do_you_want_to_continue_anyway':
        'Cet interview contient un ou plusieurs objectifs. Voulez-vous continuer quand même ?',
    'if_you_confirm_all_target_creations_and_updates_will_be_applied_you_can_follow_this_up_in_roadmap_app':
        'Si vous confirmez, toutes les créations et mises à jour d\'objectifs seront enregistrées. Vous pouvez suivre cela dans l\'application Roadmap.',
    'username_updated': '%s a mis à jour',
    'confirm': 'Confirmer',
    'completed': 'Terminé',
    'my_roadmap': 'Ma Roadmap',
    'my_team_roadmaps': 'Roadmaps de Mon Équipe',
    'continue_self_evaluation': 'Continuer l\'auto-évaluation',
    'new_target': 'Nouvel objectif',
    'not_set': 'Non défini',
    'type_and_confirm_your_new_password':
        'Saisissez votre nouveau mot de passe',
    'current_targets': 'objectifs actuels',
    'archived_targets': 'objectifs archivés',
    'starting_value_': 'Valeur de départ',
    'target_value_': 'Valeur de l\'objectif',
    'target': 'Objectif',
    'last_events': 'Dernières modifications',
    'update_value': 'Mettre à jour la valeur',
    'not_set_yet': 'Pas encore défini',
    'apply': 'Appliquer',
    'target_achieved': 'Objectif atteint !',
    'value': 'Valeur',
    'comment_successfully_added': 'Commentaire ajouté avec succès.',
    'value_successfully_updated': 'La valeur a été mise à jour avec succès.',
    'please_change_the_target_value_or_add_a_comment_to_apply':
        'Veuillez modifier la valeur de l\'objectif ou ajouter un commentaire pour appliquer.',
    'create_target': 'Nouvel objectif',
    'target_information': 'Informations sur l\'objectif',
    'assign_target': 'Assigner l\'objectif',
    'choose_target_indicator': 'Choisir l\'indicateur de l\'objectif',
    'target_summary': 'Résumé de l\'objectif',
    'or_start_from_a_template_': 'Ou commencer à partir d\'un modèle :',
    'for_non_quantifiable_targets_with_only_two_options_such_as_achieved_not_achieved':
        'Pour les objectifs non quantifiables avec seulement deux options, telles que « Atteint / Non atteint ».',
    'define_two_values_to_measure_the_target_false_will_be_the_starting_value_the_target_will_be_reached_when_switched_to_true':
        'Définissez deux valeurs pour mesurer l\'objectif. « Faux » sera la valeur de départ. L\'objectif sera atteint quand il passera à « Vrai ».',
    'for_non_quantifiable_objectives_such_as_ratings_you_can_create_multiple_options_to_qualify_your_target':
        'Pour des objectifs non quantifiables, tels que les évaluations, vous pouvez créer plusieurs options pour qualifier votre objectif.',
    'you_can_choose_which_option_or_options_will_be_considered_in_target_by_ticking_the_box_on_the_left':
        'Vous pouvez choisir quelle option (ou quelles options) sera considérée comme étant “Dans l\'objectif” en cochant la case à gauche.',
    'for_quantifiable_targets_expressed_in_percentage':
        'Pour les objectifs quantifiables, exprimés en pourcentage.',
    'set_a_percentage_based_value_for_the_starting_and_target_value':
        'Définissez une valeur basée sur un pourcentage pour la valeur de départ et la valeur de l\'objectif.',
    'for_quantifiable_targets_people_money_apples':
        'Pour des objectifs quantifiables (personnes, argent, pommes...).',
    'define_a_starting_and_a_target_value_to_measure_the_target_completion':
        'Définissez une valeur de départ et une valeur de l\'objectif pour mesurer son achèvement.',
    'new_target_from_scratch': 'Nouvel objectif à partir de zéro',
    'search_template': 'Chercher un modèle',
    'title': 'Titre*',
    'description': 'Description',
    'deadline': 'Deadline',
    'previous': 'Précédent',
    'next': 'Suivant',
    'target_indicator': 'Choisir l\'indicateur de l\'objectif',
    'true_false': 'Vrai / Faux',
    'percentage': 'Pourcentage',
    'number': 'Nombre',
    'multi_choice': 'Choix multiple',
    'starting_value': 'Valeur de départ*',
    'target_value': 'Valeur de l\'objectif*',
    'target_assigned_to': 'Objectif assigné à',
    'view_all': 'Voir tout',
    'read_all': 'Lire tout',
    'target_indicator_': 'Indicateur de l\'objectif',
    'users_will_show_up_here': 'Les utilisateurs vont apparaître ici.',
    'option': 'Option',
    'this_value_is_locked_and_cant_be_edited':
        'Cette valeur est verrouillée et ne peut pas être modifiée.',
    'add_option': 'Ajouter une option',
    'tap_on_new_target_from_scratch_or_choose_a_template':
        'Tapez sur « Nouvel objectif à partir de zéro » ou choisissez un modèle',
    'title_field_cant_be_empty': 'Le champ “Titre” ne peut pas être vide.',
    'select_at_least_1_person': 'Sélectionnez au moins 1 personne.',
    'please_set_a_starting_value_and_a_target_value':
        'Veuillez définir une « valeur de départ » et une « valeur de l\'objectif ».',
    'come_back_later_to_see_your_trainings':
        'Revenez plus tard pour voir vos formations !',
    'no_training_completed_yet':
        'Aucune formation terminée... pour le moment !',
    'modules': '%s modules',
    'module': '%s module',
    'total_duration': 'Durée totale',
    'min': 'min',
    'view_all_themes': 'Voir tous les thèmes',
    'view_less': 'Voir moins',
    'content': 'Contenu',
    'playlist': 'Playlist',
    'Module': 'Module',
    'training_updated_on_date': 'Formation mise à jour le',
    'start_training': 'Commencer la formation',
    'resume_training': 'Reprendre la formation',
    'no_duration': 'Pas de durée',
    'this_playlist_is_empty': 'La playlist est vide.',
    'Modules': 'Modules',
    'playlist_added_to_your_library': 'Playlist ajoutée à votre bibliothèque.',
    'duration': 'Durée',
    'yes_its_all_clear_to_me': 'Oui, tout est clair pour moi !',
    'youve_reached_the_end_did_you_learn_all_the_following_learning_objectives':
        'Vous avez atteint la fin ! Avez-vous compris tous les points suivants ?',
    'one_more_module_acquired': 'Encore un module acquis',
    'congratulations': 'Félicitations,',
    'keep_it_up': ', continuez comme ça !',
    'replies': 'réponses',
    'reply': 'Répondre',
    'what_did_you_think_of_the_module': 'Qu\'avez-vous pensé du module ?',
    'your_feedback_helps_us_improve_this_module':
        'Votre avis nous aide à améliorer ce module.',
    'i_like': 'J\'aime',
    'i_recommend': 'Je recommande',
    'next_module': 'Module suivant',
    'acquired': 'Acquis',
    'update_acquisition_date': 'Mettre à jour la date d\'acquisition',
    'youve_reached_the_end_ready_for_the_quiz':
        'Vous avez atteint la fin ! Prêt pour le quiz ?',
    'to_complete_this_module_evaluate_yourself_with_a_quiz':
        'Pour compléter ce module, évaluez-vous avec un quiz.',
    'start_quiz': 'Commencer le quiz',
    'retake_quiz': 'Retenter le quiz',
    'youve_reached_the_end_validate_your_evaluation_to_complete_this_module':
        'Vous avez atteint la fin! Validez votre évaluation pour compléter ce module.',
    'to_successfully_complete_this_module_your_answers_must_be_reviewed_in_real_time_by_your_expert':
        'Pour compléter ce module, vos réponses doivent être validées en temps réel par votre expert.',
    'continue_evaluation': 'Continuer l\'évaluation',
    'start_evaluation': 'Commencer l\'évaluation',
    'evaluation_done_you_have_acquired_this_module':
        'Évaluation terminée, vous avez acquis ce module !',
    'this_module_has_been_acquired_but_you_can_still_restart_the_evaluation':
        'Ce module a été acquis, mais vous pouvez quand même recommencer l\'évaluation.',
    'restart_evaluation': 'Recommencer l\'évaluation',
    'go_to_catalog': 'Aller au catalogue',
    'keep_it_up_your_answers_have_been_sent_to':
        ', continuez comme ça ! Vos réponses ont été envoyées à',
    'acquisition_date_has_been_updated':
        'La date d\'acquisition a été mise à jour',
    'module_will_be_acquired': 'Le module sera acquis',
    'start_the_quiz': 'Commencer le quiz',
    'face_to_face_evaluation': 'Évaluation en face-à-face',
    'to_mark_this_module_as_acquired_you_must_confirm_that_you_have_understood_it':
        'Pour marquer ce module comme « acquis », vous devez confirmer que vous l\'avez compris.',
    'to_mark_this_module_as_acquired_you_must_pass_the_assessment_quiz':
        'Pour marquer ce module comme « acquis », vous devez réussir le quiz d\'évaluation.',
    'to_mark_this_module_as_acquired_you_must_pass_the_face_to_face_evaluation':
        'Pour marquer ce module comme « acquis », vous devez passer l’évaluation en face-à-face.',
    'not_yet': 'Pas encore',
    'do_it_later': 'Plus tard',
    'start': 'Commencer',
    'comment_will_be_deleted': 'Le commentaire sera supprimé',
    'this_is_a_permanent_action': 'L\'action est irréversible.',
    'ok_delete': 'OK, supprimer',
    'comment_deleted': 'Commentaire supprimé.',
    'new_badge_acquired': 'Nouveau badge acquis !',
    'view_my_badges': 'Voir mes badges',
    'modules_acquired__1': 'Modules acquis',
    'reactions_to_modules__1': 'Réactions aux modules',
    'trainings_completed': 'Formations complétées',
    'modules_added_to_favorites': 'Modules ajoutés aux favoris',
    'module_acquired': 'module acquis',
    'reaction_to_modules': 'réactions aux modules',
    'trainings_completed__1': 'formations complétées',
    'modules_added_to_favorites__1': 'modules ajoutés aux favoris',
    'module_added_to_favorites': 'module ajouté aux favoris',
    'like_recommend': 'J\'aime & je recommande',
    'Replies': 'Réponses',
    'your_answers_will_be_lost': 'Vos réponses seront perdues',
    'if_you_leave_the_quiz_now_your_answers_will_be_lost_are_you_sure':
        'Si vous quittez le quiz maintenant, vos réponses seront perdues. Êtes-vous sûr ?',
    'congratulations_you_just_acquired_a_new_module_keep_it_up':
        'Félicitations, vous venez d\'acquérir un nouveau module, continuez comme ça !',
    'here_is_one_more_module_acquired': 'Encore un module acquis',
    'back_to_training': 'Retour à la formation',
    'see_the_correction': 'Voir la correction',
    'ready_to_try_again': 'Prêt à réessayer ?',
    'retry_quiz': 'Retenter le quiz',
    'back_to_module': 'Retour au module',
    'you_can_take_this_quiz_as_many_times_as_needed_100_of_correct_answers_will_mark_this_module_as_acquired':
        'Vous pouvez passer ce quiz autant de fois que nécessaire. 100% de bonnes réponses marqueront le module comme acquis.',
    'quiz_results': 'Résultats du quiz',
    'only_right_answers_congratulations':
        'Que de bonnes réponses ! Félicitations,',
    'keep_going_check_your_mistakes_and_try_again':
        'Vérifiez vos erreurs et essayez à nouveau',
    'check_your_mistakes_and_try_again': 'Vérifiez vos erreurs et réessayez',
    'corrections': 'Corrections',
    'right_answer': 'Réponse correcte',
    'wrong_answer': 'Réponse incorrecte',
    'partially_right': 'Partiellement correct',
    'select_your_expert': 'Sélectionner votre expert*',
    'please_select_the_expert_who_reviewed_your_answers_after_validation_they_will_receive_a_copy_of_this_evaluation_by_email':
        'Sélectionnez l\'expert qui a examiné vos réponses. Après validation, il recevra une copie de cette évaluation par e-mail.',
    'my_expert': 'Mon expert',
    'validate_answers': 'Valider les réponses',
    'select_an_expert': 'Sélectionner un expert',
    'item_will_be_downloaded_see_notifications_for_further_details__1':
        '1 élément sera téléchargé. Voir les notifications pour plus de détails.',
    'please_fill_all_required_fields__1':
        'Veuillez remplir tous les champs obligatoires (*).',
    'answers_saved_you_can_come_back_later_to_finish_the_evaluation':
        'Réponses enregistrées. Vous pouvez revenir plus tard pour terminer l\'évaluation.',
    'hmm_it_seems_there_is_no_expert_yet':
        'Hmm, il n\'y a pas encore d\'expert.',
    'you_must_select_an_expert_before_saving':
        'Vous devez sélectionner un expert avant d\'enregistrer.',
    'playlists': 'Playlists',
    'no_module_yet': 'Pas encore de module',
    'no_playlist_yet': 'Pas encore de playlist',
    'all_modules_available_will_show_up_here':
        'Tous les modules disponibles s\'afficheront ici.',
    'all_playlists_available_will_show_up_here':
        'Toutes les playlists disponibles s\'afficheront ici.',
    'theme_': 'Thème :',
    'favorites': 'Favoris',
    'filter': 'Filtrer',
    'show_only_favorites': 'Voir seulement les favoris',
    'theme': 'Thème',
    'clear_all': 'Effacer filtres',
    'follow_this_module_in_your_training':
        'Suivre ce module dans votre formation ?',
    'no': 'Non',
    'yes': 'Oui',
    'follow_this_playlist_in_your_training':
        'Suivre cette playlist dans votre formation ?',
    'this_module_is_part_of_the': 'Ce module fait partie de la formation',
    'this_playlist_is_part_of_the':
        'Cette playlist fait partie de la formation',
    'results': 'Résultats',
    'result': 'Résultat',
    'module_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression':
        '. Voulez-vous le suivre dans ce contexte pour assurer une progression cohérente ?',
    'playlist_training_would_you_like_to_follow_it_in_this_context_to_ensure_a_coherent_progression':
        '. Voulez-vous la suivre dans ce contexte pour assurer une progression cohérente ?',
    'username_commented': '%s a commenté',
    'interviews_are_done_when_all_participants_have_submitted_their_answers_including_cross_review':
        'Les interviews sont terminées quand tous les participants ont envoyé leurs réponses, y compris la Cross Review.',
    'new_targets_will_be_added_to_roadmap_after_submitting_all_the_interviews_in_the_set':
        'Les nouveaux objectifs seront ajoutés à la Roadmap après avoir soumis toutes les interviews.',
    'assigned_to': 'Assigné à',
    'option_list_cant_be_empty': 'La liste d\'options ne peut pas être vide.',
    'options_cant_be_blank': 'Les options ne peuvent pas être vides.',
    'not_in_target_': 'Hors de l\'objectif :',
    'not_defined': 'Non défini',
    'headline': 'Ces objectifs seront mis à jour ou créés',
    'updated': 'Mis à jour',
    'created': 'Créé',
    'description_updated': 'Description mise à jour',
    'title_updated': 'Titre mis à jour',
    'due_date_updated': 'Date limite mise à jour',
    'there_is_no_content_in_this_training_at_the_moment':
        'Il n\'y a actuellement aucun contenu dans cette formation.',
    'you_are_already_in_charge_of_this_interview':
        'Vous êtes déjà responsable de cette Interview.',
    'no_theme_yet': 'Aucun thème',
    'none': 'Aucune',
    'not_validated': 'Non validé',
    'module_added_to_favorites__1': 'Module ajouté aux favoris.',
    'module_removed_from_favorites': 'Module retiré des favoris.',
    'your_team_current_targets_will_show_up_here':
        'Les objectifs en cours de votre équipe apparaîtront ici.',
    'your_team_archived_targets_will_show_up_here':
        'Les objectifs archivés de votre équipe apparaîtront ici.',
    'person_in_charge_updated_successfully': 'Le responsable a été mis à jour.',
    'playlist_added_to_favorites': 'Playlist ajoutée aux favoris.',
    'playlist_removed_from_favorites': 'Playlist retirée des favoris.',
    'this_module_is_not_accessible_for_you':
        'Ce module n\'est pas accessible pour vous',
    'youve_reached_the_end_please_notify_your_expert_before_starting_the_evaluation':
        'Vous avez atteint la fin ! Notifiez votre expert avant de commencer l\'évaluation.',
    'one_reply': 'réponse',
    'favorite': 'Favori',
    'an_error_occurred_please_try_again':
        'Une erreur est survenue, veuillez réessayer.',
    'download_pdf': 'Télécharger le PDF',
    'view_updates': 'Voir les mises à jour',
    'you_are_logged_out_please_sign_in_to_continue':
        'Vous êtes déconnecté. Connectez-vous à nouveau pour continuer.',
    'if_you_add_a_playlist_to_favorites_it_will_show_up_here':
        'Si vous ajoutez une playlist à vos favoris, elle apparaîtra ici.',
  };
}
