part of '../svn_pro_mobile_flutter.dart';

class SvnProCardItwLoader extends StatelessWidget {
  const SvnProCardItwLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: SvnProCardItw(
        loading: true,
        type: SvnProCampaignType.feedback,
        title: 'Nom de la campagne Feedback p dazdzaz dzaadzdza dazdza ',
        featureItem: const SvnProFeatureItem(
          featureText: 'Feedback on',
          description: 'John Doe',
          color: SvnProFeatureItemColor.defaultColor,
        ),
        infoTag: const SvnProInfoTag(
          type: SvnProInfoTagType.info,
          label: 'not available',
        ),
        deadline: DateTime.now(),
      ),
    );
  }
}
