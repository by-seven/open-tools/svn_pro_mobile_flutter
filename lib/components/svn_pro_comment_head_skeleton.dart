part of '../svn_pro_mobile_flutter.dart';

class SvnProCommentHeadSkeleton extends StatelessWidget {
  const SvnProCommentHeadSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: SvnProCommentBaseTile(
        avatar: const SvnProAvatar(
          size: SvnProAvatarSizes.px32,
          variant: SvnProAvatarVariant.anonym,
        ),
        title: 'toto',
        tileIconBtnToggleable: const SvnProIconButton(
          variant: SvnProIconButtonVariant.standard,
          icon: SvnProIcon(
            iconData: MingCuteIcons.mgc_pin_2_line,
            size: 24,
          ),
          type: SvnProIconButtonType.toggleable,
          selectedIcon: SvnProIcon(
            iconData: MingCuteIcons.mgc_pin_2_fill,
            size: 24,
          ),
          isSelected: false,
        ),
        tileInfoTag: const SvnProInfoTag(
          label: 'toto',
          leading: SvnProInfoTagLeading.none,
        ),
        selectedStart: int.tryParse('5'),
        maxStart: 5,
        tileDescription:
            'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
        tileSubInfoTagList: const [SvnProInfoTag(label: 'toto')],
        tileDivider: 1,
      ),
    );
  }
}
