part of '../svn_pro_mobile_flutter.dart';

class SvnProGraphToggleMcqRoadmap extends StatelessWidget {
  final SvnProTargetStatusRoadmap status;
  final String labelStatus;

  const SvnProGraphToggleMcqRoadmap(
      {super.key,
      this.status = SvnProTargetStatusRoadmap.notSet,
      this.labelStatus = ''});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SvnProIcon(
          iconData: genIcon(),
          size: 48,
          color: genColor(context),
        ),
        const SizedBox(
          height: 8,
        ),
        SvnProText(
          text: labelStatus,
          size: SvnProTextSize.titleMl,
          weight: SvnProTextWeight.medium,
          color: genColor(context),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  genIcon() {
    switch (status) {
      case SvnProTargetStatusRoadmap.notSet:
        return MingCuteIcons.mgc_round_line;
      case SvnProTargetStatusRoadmap.notValid:
        return MingCuteIcons.mgc_close_circle_line;
      case SvnProTargetStatusRoadmap.inProgress:
        return MingCuteIcons.mgc_minus_circle_line;
      case SvnProTargetStatusRoadmap.validated:
        return MingCuteIcons.mgc_check_circle_fill;
    }
  }

  genColor(context) {
    switch (status) {
      case SvnProTargetStatusRoadmap.notSet:
        return Theme.of(context).colorScheme.onSurfaceVariant;
      case SvnProTargetStatusRoadmap.notValid:
        return Theme.of(context).colorScheme.error;
      case SvnProTargetStatusRoadmap.inProgress:
        return Theme.of(context).extension<CustomColors>()!.warningCustom1;
      case SvnProTargetStatusRoadmap.validated:
        return Theme.of(context).extension<CustomColors>()!.successColor;
    }
  }
}
