part of '../svn_pro_mobile_flutter.dart';

// class SvnProSearchTile {
//   final SvnProAvatar svnProAvatar;
//   final String title;
//   final String subtitle;
//   final int id;

//   const SvnProSearchTile({
//     required this.svnProAvatar,
//     required this.title,
//     required this.subtitle,
//     required this.id,
//   });
// }

class SvnProSearch extends StatelessWidget {
  final SearchController searchController = SearchController();

  final Iterable<SvnProListTile> suggestedList;
  final void Function(SvnProListTile)? onSelected;
  final void Function(String)? onSearchChanged;
  final Iterable<Widget>? trailing;
  final String? hintText;

  SvnProSearch({
    super.key,
    required this.suggestedList,
    this.onSelected,
    this.onSearchChanged,
    this.trailing,
    this.hintText,
  });

  @override
  Widget build(BuildContext context) {
    return SearchAnchor(
      searchController: searchController,
      builder: (BuildContext context, SearchController controller) {
        return SearchBar(
          hintText: hintText,
          controller: controller,
          padding: const MaterialStatePropertyAll<EdgeInsets>(
            EdgeInsets.symmetric(horizontal: 16.0),
          ),
          onTap: () {
            controller.openView();
          },
          onChanged: (_) {
            controller.openView();
          },
          leading: const Icon(Icons.search),
          trailing: trailing,
        );
      },
      suggestionsBuilder: (BuildContext context, SearchController controller) {
        if (onSearchChanged != null) {
          onSearchChanged!(controller.text);
        }

        return suggestedList.map((SvnProListTile item) {
          return item.copyWith(
            onTap: () {
              if (onSelected != null) {
                onSelected!(item);
              }
              if (item.onTap != null) {
                item.onTap!();
              }
              // to apply the text value in searchController
              controller.closeView(item.title);
            },
          );
        }).toList();
      },
    );
  }
}
