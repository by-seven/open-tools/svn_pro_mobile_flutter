part of '../svn_pro_mobile_flutter.dart';

enum SvnProCardRoadmapType {
  percentage,
  number,
  boolean,
  multi,
}

class SvnProCardRoadmap extends StatelessWidget {
  final SvnProCardRoadmapType type;
  final String title;
  final String? desc;
  final bool archived;
  final DateTime? deadline;
  final String? userAssigned;
  final SvnProInfoTagType status;
  final String? labelStatus;
  final VoidCallback? onTap;

  const SvnProCardRoadmap(
      {super.key,
      this.type = SvnProCardRoadmapType.percentage,
      required this.title,
      this.desc,
      this.archived = false,
      this.deadline,
      this.userAssigned,
      this.status = SvnProInfoTagType.info,
      this.labelStatus,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    late String strDeadline;

    strDeadline = deadline?.toShortDate ?? 'not_set'.tr;

    return SvnProCard(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                SvnProIcon(
                  iconData: getIconDateType(),
                  color: archived
                      ? Theme.of(context).colorScheme.onSurfaceVariant
                      : Theme.of(context).colorScheme.primary,
                ),
                const SizedBox(
                  width: 8,
                ),
                Flexible(
                  child: SvnProText(
                    text: title,
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.semiBold,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            buildFeatureItem(userAssigned) ?? const SizedBox.shrink(),
            buildDescription(desc) ??
                const SizedBox(
                  height: 8,
                ),
            Row(
              children: [
                SvnProInfoTag(
                  // type: genInfoTagType(),
                  type: archived ? SvnProInfoTagType.info : status,
                  label: labelStatus ?? '',
                ),
                const Spacer(),
                SvnProInfoTag(
                  variant: SvnProInfoTagVariant.text,
                  type: SvnProInfoTagType.info,
                  leading: SvnProInfoTagLeading.icon,
                  icon: const SvnProIcon(
                    iconData: MingCuteIcons.mgc_calendar_month_line,
                  ),
                  label: strDeadline,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Flexible? buildFeatureItem(String? userAssigned) {
    if (userAssigned?.isNotEmpty ?? false) {
      return Flexible(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
        Row(children: [
          Expanded(
            child: SvnProFeatureItem(
              featureText: 'assigned_to'.tr,
              description: userAssigned!,
            ),
          ),
        ]),
        const SizedBox(height: 16)
      ]));
    }
    return null;
  }

  Flexible? buildDescription(String? userAssigned) {
    if (desc?.isNotEmpty ?? false) {
      return Flexible(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
        Row(
          children: [
            Flexible(
              child: SvnProText(
                text: desc,
                size: SvnProTextSize.bodyMedium,
                weight: SvnProTextWeight.regular,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
        const SizedBox(height: 16)
      ]));
    }
    return null;
  }

  getIconDateType() {
    switch (type) {
      case SvnProCardRoadmapType.percentage:
        return MingCuteIcons.mgc_sale_line;
      case SvnProCardRoadmapType.multi:
        return MingCuteIcons.mgc_checkbox_line;
      case SvnProCardRoadmapType.boolean:
        return MingCuteIcons.mgc_toggle_left_line;
      case SvnProCardRoadmapType.number:
        return MingCuteIcons.mgc_hashtag_line;
    }
  }
}
