part of '../svn_pro_mobile_flutter.dart';

class SvnProSearchField extends StatelessWidget {
  final TextEditingController? controller;
  final String? placeholder;
  final VoidCallback? onFilter;
  final bool prefixIcon;
  final ValueChanged<String>? onChanged;
  final bool isSelected;

  const SvnProSearchField({
    super.key,
    this.controller,
    this.placeholder,
    this.prefixIcon = false,
    this.onFilter,
    this.onChanged,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SvnProTextField(
            controller: controller,
            placeHolder: placeholder ?? 'search'.tr,
            prefixIcon: prefixIcon
                ? const SvnProIcon(
                    iconData: MingCuteIcons.mgc_search_2_line,
                  )
                : null,
            variant: SvnProTextFieldVariant.outlined,
            onChanged: onChanged,
          ),
        ),
        if (onFilter != null) ...[
          const SizedBox(width: 4),
          SvnProIconButton(
            variant: SvnProIconButtonVariant.filled,
            type: SvnProIconButtonType.toggleable,
            icon: const SvnProIcon(
              iconData: MingCuteIcons.mgc_settings_6_line,
            ),
            isSelected: isSelected,
            onPressed: onFilter,
          ),
        ],
      ],
    );
  }
}
