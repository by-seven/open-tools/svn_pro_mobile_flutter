part of '../svn_pro_mobile_flutter.dart';

/// Defines the visual variants of the text field.
enum SvnProTextFieldVariant {
  filled,
  outlined,
}

/// Defines the status of the text field (enabled, disabled, error).
enum SvnProTextFieldStatus {
  enabled,
  disabled,
  error,
}

/// A highly customizable and optimized text field widget with advanced
/// keyboard interaction features.
class SvnProTextField extends StatefulWidget {
  const SvnProTextField({
    super.key,
    this.initialValue,
    this.variant = SvnProTextFieldVariant.outlined,
    this.status = SvnProTextFieldStatus.enabled,
    this.errorText,
    this.supportingText,
    this.placeHolder,
    this.label,
    this.prefixIcon,
    this.suffixIcon,
    this.obscureText = false,
    this.controller,
    this.textArea = false,
    this.onTap,
    this.minLines = 1,
    this.maxLines = 8,
    this.keyboardType = TextInputType.text,
    this.onChanged,
    this.validator,
    this.readOnly = false,
  });

  /// Initial value for the text field.
  final String? initialValue;

  /// Visual variant of the text field (filled or outlined).
  final SvnProTextFieldVariant variant;

  /// Status of the text field (enabled, disabled, or error).
  final SvnProTextFieldStatus status;

  /// Error text displayed below the text field in case of validation error.
  final String? errorText;

  /// Supporting text displayed below the text field as helper information.
  final String? supportingText;

  /// Placeholder text displayed inside the text field.
  final String? placeHolder;

  /// Label text displayed above the text field.
  final String? label;

  /// Optional prefix icon.
  final Widget? prefixIcon;

  /// Optional suffix icon.
  final Widget? suffixIcon;

  /// Whether the text input is obscured (e.g., for passwords).
  final bool obscureText;

  /// Controller for managing the text input.
  final TextEditingController? controller;

  /// Whether the text field is a multi-line text area.
  final bool textArea;

  /// Callback triggered when the text field is tapped.
  final void Function()? onTap;

  /// Minimum number of lines for the text area.
  final int minLines;

  /// Maximum number of lines for the text area.
  final int maxLines;

  /// Keyboard type for the text input.
  final TextInputType? keyboardType;

  /// Callback triggered when the text input changes.
  final ValueChanged<String>? onChanged;

  /// Validator for validating the text input.
  final FormFieldValidator<String>? validator;

  /// Whether the text field is read-only.
  final bool readOnly;

  @override
  State<SvnProTextField> createState() => _SvnProTextFieldState();
}

class _SvnProTextFieldState extends State<SvnProTextField> {
  final FocusNode _focusNode = FocusNode();

  /// Builds the configuration for keyboard actions.
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      actions: [
        KeyboardActionsItem(
          focusNode: _focusNode,
          toolbarButtons: [
            (node) {
              return GestureDetector(
                onTap: node.unfocus,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SvnProText(
                      text: 'Done'.tr,
                      color: Theme.of(context).colorScheme.primary,
                      size: SvnProTextSize.labelLarge),
                ),
              );
            },
          ],
        ),
      ],
    );
  }

  @override
  void dispose() {
    // Clean up the FocusNode when the widget is disposed.
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final field = TextFormField(
      focusNode: _focusNode,
      readOnly: widget.readOnly,
      style: TextStyle(
        color: widget.status == SvnProTextFieldStatus.disabled
            ? Theme.of(context).colorScheme.onSurface.withOpacity(0.5)
            : Theme.of(context).colorScheme.onSurface,
      ),
      initialValue: widget.initialValue,
      enabled: widget.status != SvnProTextFieldStatus.disabled,
      decoration: InputDecoration(
        prefixIcon: widget.prefixIcon,
        suffixIcon: widget.suffixIcon,
        labelText: widget.label ?? widget.placeHolder,
        hintText: widget.label == null ? widget.placeHolder : null,
        helperText: widget.supportingText,
        errorText: widget.status == SvnProTextFieldStatus.error
            ? widget.errorText
            : null,
        filled: widget.variant == SvnProTextFieldVariant.filled,
        border: widget.variant == SvnProTextFieldVariant.filled
            ? const OutlineInputBorder(borderSide: BorderSide.none)
            : const OutlineInputBorder(),
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        alignLabelWithHint: true,
      ),
      obscureText: widget.obscureText,
      controller: widget.controller,
      minLines: widget.textArea ? widget.minLines : null,
      maxLines: widget.textArea ? widget.maxLines : 1,
      keyboardType: widget.textArea
          ? TextInputType.multiline
          : (widget.keyboardType ?? TextInputType.text),
      textInputAction:
          widget.textArea ? TextInputAction.newline : TextInputAction.done,
      onTap: widget.onTap,
      onChanged: widget.onChanged,
      validator: widget.validator,
    );

    if (!widget.textArea || GetPlatform.isAndroid) {
      return field;
    }

    return KeyboardActions(
      config: _buildConfig(context),
      disableScroll: true,
      child: field,
    );
  }
}
