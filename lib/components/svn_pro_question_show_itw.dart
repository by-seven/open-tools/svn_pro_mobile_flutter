part of '../svn_pro_mobile_flutter.dart';

class SvnProQuestionShowItw extends StatelessWidget {
  final String? title;
  final String? desc;
  final String? youtubeUrl;

  const SvnProQuestionShowItw({
    super.key,
    this.title,
    this.desc,
    this.youtubeUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (title != null) ...[
          SvnProText(
            text: title,
            size: SvnProTextSize.titleMl,
            weight: SvnProTextWeight.bold,
          ),
          const SizedBox(
            height: 6,
          ),
          const SvnProDivider(),
          const SizedBox(height: 12),
        ],
        HtmlWidget(
          desc ?? '',
          textStyle:
              TextStyle(color: Theme.of(context).colorScheme.onSurfaceVariant),

          // specify custom styling for an element
          // see supported inline styling below
          // customStylesBuilder: (element) {
          //   if (element.classes.contains('foo')) {
          //     return {'color': 'red'};
          //   }

          //   return null;
          // },

          // customWidgetBuilder: (element) {
          //   if (element.attributes['foo'] == 'bar') {
          //     // render a custom block widget that takes the full width
          //     return FooBarWidget();
          //   }

          //   if (element.attributes['fizz'] == 'buzz') {
          //     // render a custom widget inline with surrounding text
          //     return InlineCustomWidget(
          //       child: FizzBuzzWidget(),
          //     )
          //   }

          //   return null;
          // },

          // // this callback will be triggered when user taps a link
          // onTapUrl: (url) => print('tapped $url'),

          // // select the render mode for HTML body
          // // by default, a simple `Column` is rendered
          // // consider using `ListView` or `SliverList` for better performance
          // renderMode: RenderMode.column,
        ),
        if (youtubeUrl != null && youtubeUrl != '') ...<Widget>[
          const SizedBox(
            height: 16,
          ),
          YoutubePlayer(
            controller: YoutubePlayerController(
              initialVideoId: YoutubePlayer.convertUrlToId(youtubeUrl!)!,
              flags: const YoutubePlayerFlags(
                autoPlay: false,
              ),
            ),
          ),
        ],
      ],
    );
  }
}
