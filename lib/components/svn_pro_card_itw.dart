part of '../svn_pro_mobile_flutter.dart';

class SvnProCardItw extends StatelessWidget {
  final SvnProCampaignType type;
  final String title;
  final DateTime deadline;
  final SvnProFeatureItem? featureItem;
  final SvnProInfoTag infoTag;
  final VoidCallback? onTap;
  final bool loading;

  const SvnProCardItw({
    super.key,
    this.type = SvnProCampaignType.oneToOne,
    required this.title,
    required this.deadline,
    required this.infoTag,
    this.featureItem,
    this.onTap,
    this.loading = false,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProCard(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                if (loading)
                  const SvnProIcon(
                    iconData: MingCuteIcons.mgc_ABS_fill,
                    size: 32,
                  ),
                if (!loading)
                  SvnProIcon(
                    noto: getNotoIcon(),
                    size: 32,
                  ),
                const SizedBox(
                  width: 8,
                ),
                Flexible(
                  child: SvnProText(
                    text: title,
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.semiBold,
                  ),
                ),
              ],
            ),
            if (featureItem != null) ...[
              const SizedBox(
                height: 8,
              ),
              featureItem!,
            ],
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                infoTag.copyWith(
                  variant: SvnProInfoTagVariant.filled,
                  leading: SvnProInfoTagLeading.point,
                  compact: true,
                ),
                const Spacer(),
                SvnProInfoTag(
                  compact: true,
                  variant: SvnProInfoTagVariant.text,
                  type: SvnProInfoTagType.info,
                  leading: SvnProInfoTagLeading.icon,
                  icon: const SvnProIcon(
                    iconData: MingCuteIcons.mgc_calendar_month_line,
                  ),
                  label: deadline.toShortDate,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  getNotoIcon() {
    switch (type) {
      case SvnProCampaignType.oneToOne:
        return Noto.handshake;
      case SvnProCampaignType.feedback:
        return Noto.memo;
      case SvnProCampaignType.survey:
        return Noto.bar_chart;
    }
  }
}
