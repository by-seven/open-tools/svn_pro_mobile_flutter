part of '../svn_pro_mobile_flutter.dart';

class SvnProNavigationDestination {
  const SvnProNavigationDestination(this.label, this.icon, this.selectedIcon);

  final String label;
  final Widget icon;
  final Widget selectedIcon;
}

class SvnProNavigationSection {
  final String? header;
  final List<SvnProNavigationDestination> destinations;
  final bool divider;

  const SvnProNavigationSection({
    this.header,
    this.destinations = const <SvnProNavigationDestination>[],
    this.divider = false,
  });
}

class SvnProNavigationDrawer extends StatefulWidget {
  final List<SvnProNavigationSection> navigationSections;
  final List<Widget> pages;
  final void Function(int)? onScreenChanged;

  SvnProNavigationDrawer({
    super.key,
    this.navigationSections = const <SvnProNavigationSection>[],
    this.pages = const <Widget>[],
    this.onScreenChanged,
  }) {
    // FIXME: testing purpose
    // final countDestinations = navigationSections
    //     .map((navSection) => navSection.destinations)
    //     .expand((i) => i)
    //     .length;
    // if (countDestinations != pages.length) {
    //   throw 'nagivation drawer and pages props should have the same size';
    // }
  }

  @override
  State<SvnProNavigationDrawer> createState() => SvnProNavigationDrawerState();
}

class SvnProNavigationDrawerState extends State<SvnProNavigationDrawer> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  int screenIndex = 0;

  void handleScreenChanged(int selectedScreen) {
    if (widget.onScreenChanged != null) {
      widget.onScreenChanged!(selectedScreen);
    }
    setState(() {
      screenIndex = selectedScreen;
    });
  }

  void openDrawer() {
    scaffoldKey.currentState!.openDrawer();
  }

  void closeDrawer() {
    scaffoldKey.currentState!.closeDrawer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      // avoiding safe area
      body: widget.pages.isNotEmpty
          ? widget.pages[screenIndex]
          : const Center(child: SvnProText(text: 'no navigation given !')),
      drawer: NavigationDrawer(
        onDestinationSelected: handleScreenChanged,
        selectedIndex: screenIndex,
        children: widget.navigationSections
            .map((SvnProNavigationSection section) {
              return [
                section.header != null
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(28, 16, 16, 10),
                        child: SvnProText(
                            text: section.header!,
                            size: SvnProTextSize.bodyMedium,
                            weight: SvnProTextWeight.medium,
                            color:
                                Theme.of(context).colorScheme.onSurfaceVariant),
                      )
                    : const SizedBox(),
                ...section.destinations.map(
                  (SvnProNavigationDestination destination) {
                    return NavigationDrawerDestination(
                      label: Text(destination.label),
                      icon: destination.icon,
                      selectedIcon: destination.selectedIcon,
                    );
                  },
                ),
                section.divider
                    ? const Padding(
                        padding: EdgeInsets.fromLTRB(28, 16, 28, 10),
                        child: SvnProDivider(),
                      )
                    : const SizedBox(),
              ];
            })
            .expand((i) => i)
            .toList(),
        // children: <Widget>[
        //   Padding(
        //     padding: const EdgeInsets.fromLTRB(28, 16, 16, 10),
        //     child: SvnProText(
        //         text: 'header',
        //         size: SvnProTextSize.bodyMedium,
        //         weight: SvnProTextWeight.medium,
        //         color: Theme.of(context).colorScheme.onSurfaceVariant),
        //   ),
        //   ...destinations.map(
        //     (ExampleDestination destination) {
        //       return NavigationDrawerDestination(
        //         label: Text(destination.label),
        //         icon: destination.icon,
        //         selectedIcon: destination.selectedIcon,
        //       );
        //     },
        //   ),
        //   const Padding(
        //     padding: EdgeInsets.fromLTRB(28, 16, 28, 10),
        //     child: SvnProDivider(),
        //   ),
        // ],
      ),
    );
  }
}
