part of '../svn_pro_mobile_flutter.dart';

enum SvnProLogsVariantRoadmap {
  completion,
  targetInfo,
  achieved,
  comment,
}

class SvnProLogsRoadmap<T> extends StatelessWidget {
  final SvnProAvatar? avatar;
  final String title;
  final DateTime date;
  final SvnProLogsVariantRoadmap variant;
  final SvnProInfoTag? infoTag;
  final T? previousValue;
  final T? currentValue;
  final String? targetInfoType;
  final String? comment;

  const SvnProLogsRoadmap({
    super.key,
    this.avatar,
    this.title = '',
    required this.date,
    this.variant = SvnProLogsVariantRoadmap.completion,
    this.infoTag,
    this.previousValue,
    this.currentValue,
    this.targetInfoType,
    this.comment,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (variant != SvnProLogsVariantRoadmap.achieved)
          SvnProListTile(
            leadingVariant: SvnProListTileSideVariant.avatar,
            leadingAvatar: avatar!,
            title: (variant == SvnProLogsVariantRoadmap.comment
                ? 'username_commented'.trArgs([title])
                : 'username_updated'.trArgs([title])),
            condition: SvnProListTileCondition.line2,
            supportingText: DateFormat('E d MMMM, H:m').format(date),
          ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
          child: Column(
            children: [
              if (variant == SvnProLogsVariantRoadmap.completion)
                Row(
                  children: [
                    if (infoTag != null) ...[
                      infoTag!,
                      const SizedBox(
                        width: 16,
                      ),
                    ],
                    Flexible(
                      child: SvnProText(
                        text: (previousValue is int)
                            ? (previousValue as int).toString()
                            : previousValue as String?,
                        size: SvnProTextSize.titleSmall,
                        weight: SvnProTextWeight.medium,
                        color: Theme.of(context).colorScheme.primary,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    SvnProIcon(
                      iconData: MingCuteIcons.mgc_arrow_right_line,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Flexible(
                      child: SvnProText(
                        text: (currentValue is int)
                            ? (currentValue as int).toString()
                            : currentValue as String?,
                        size: SvnProTextSize.titleSmall,
                        weight: SvnProTextWeight.medium,
                        color: Theme.of(context).colorScheme.primary,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              if (variant == SvnProLogsVariantRoadmap.targetInfo) ...[
                Row(
                  children: [
                    SvnProInfoTag(
                      label: targetInfoType!,
                      type: SvnProInfoTagType.info,
                      leading: SvnProInfoTagLeading.none,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                SvnProText(
                  text: (previousValue is int)
                      ? (previousValue as int).toString()
                      : previousValue as String?,
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.regular,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                ),
                const SizedBox(
                  height: 8,
                ),
                const SvnProIcon(
                  iconData: MingCuteIcons.mgc_arrow_down_line,
                  size: 24,
                ),
                const SizedBox(
                  height: 8,
                ),
                SvnProText(
                  text: (currentValue is int)
                      ? (currentValue as int).toString()
                      : currentValue as String?,
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.regular,
                  color: Theme.of(context).colorScheme.onSurface,
                ),
              ],
              if ((variant == SvnProLogsVariantRoadmap.completion &&
                      comment != null) ||
                  (variant == SvnProLogsVariantRoadmap.comment &&
                      comment != null)) ...[
                const SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Flexible(
                      child: SvnProText(
                        text: comment!,
                        size: SvnProTextSize.bodyMedium,
                        weight: SvnProTextWeight.regular,
                        color: Theme.of(context).colorScheme.onSurface,
                      ),
                    ),
                  ],
                ),
              ],
              if (variant == SvnProLogsVariantRoadmap.achieved) ...[
                const SizedBox(
                  height: 20,
                ),
                const SvnProIcon(
                  noto: Noto.trophy,
                  size: 48,
                ),
                const SizedBox(
                  height: 12,
                ),
                SvnProText(
                  text: 'Target achieved!',
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.medium,
                  color: Theme.of(context).colorScheme.onSurface,
                ),
                SvnProText(
                  text: DateFormat('E d MMMM, H:m').format(date),
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.regular,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                ),
              ],
            ],
          ),
        ),
        const SvnProDivider(),
      ],
    );
  }
}
