part of '../svn_pro_mobile_flutter.dart';

class SvnProFilterDrawerGroup extends StatelessWidget {
  final String? label;
  final List<SvnProCheckboxTile> checkboxList;

  const SvnProFilterDrawerGroup({
    super.key,
    this.label,
    this.checkboxList = const [],
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 16,
        ),
        if (label != null)
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: SvnProText(
                  text: label,
                  size: SvnProTextSize.bodyLarge,
                  weight: SvnProTextWeight.semiBold,
                ),
              ),
            ],
          ),
        ...checkboxList,
      ],
    );
  }
}

class SvnProFilterDrawer extends StatelessWidget {
  final List<SvnProFilterDrawerGroup> groups;
  final SvnProBtnState? clearAllButtonState;
  final void Function()? onClearPressed;
  final void Function()? onApplyPressed;

  const SvnProFilterDrawer({
    super.key,
    this.groups = const [],
    this.clearAllButtonState = SvnProBtnState.enabled,
    this.onApplyPressed,
    this.onClearPressed,
  });

  void onClear(BuildContext context) {
    Navigator.pop(context, 'bad');
    onClearPressed!();
  }

  void onApply(BuildContext context) {
    Navigator.pop(context, 'good');
    onApplyPressed!();
  }

  @override
  Widget build(BuildContext context) {
    final mappedGroup = groups.map((i) => i.checkboxList.length);
    final sum = mappedGroup.reduce((acc, item) => acc + item);
    var maxHeight = sum * 54;
    maxHeight = maxHeight > 230 ? 230 : maxHeight;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
          child: Row(
            children: [
              SvnProText(
                text: 'filter'.tr,
                size: SvnProTextSize.titleMl,
                weight: SvnProTextWeight.semiBold,
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        const SvnProDivider(),
        ConstrainedBox(
          constraints: BoxConstraints(maxHeight: maxHeight.toDouble()),
          child: ListView(
            children: groups,
          ),
        ),
        SvnProButtonStickyContainer(
          variant: SvnProButtonStickyContainerVariant.horizontal,
          primaryBtn: SvnProBtn(
            text: 'apply'.tr,
            onPressed: () => onApply(context),
          ),
          secondaryBtn: SvnProBtn(
            variant: SvnProBtnVariants.outlined,
            svnProBtnState: clearAllButtonState,
            text: 'clear_all'.tr,
            onPressed: () => onClear(context),
          ),
        ),
      ],
    );
  }
}

svnProShowFilterDrawer(
  context, {
  required WidgetBuilder builder,
}) {
  return svnProShowBottomSheet(context, builder: builder);
}
