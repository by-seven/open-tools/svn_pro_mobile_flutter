part of '../svn_pro_mobile_flutter.dart';

class SvnProTiptapShow extends StatelessWidget {
  final String? html;

  const SvnProTiptapShow({
    super.key,
    this.html,
  });

  @override
  Widget build(BuildContext context) {
    return HtmlWidget(html ?? '',
        textStyle: TextStyle(color: Theme.of(context).colorScheme.onSurface),
        customStylesBuilder: (element) {
      if (element.localName == 'a') {
        final color = Theme.of(context).colorScheme.primary;
        return {
          'color': 'rgb(${color.red},${color.green},${color.blue})',
          'text-decoration-color':
              'rgb(${color.red},${color.green},${color.blue})',
          'text-decoration': 'underline',
        };
      }
      return null;
    }
        //   if (element.attributes['fizz'] == 'buzz') {
        //     // render a custom widget inline with surrounding text
        //     return InlineCustomWidget(
        //       child: FizzBuzzWidget(),
        //     )
        //   }

        //   return null;
        // },

        // // this callback will be triggered when user taps a link
        // onTapUrl: (url) => print('tapped $url'),

        // // select the render mode for HTML body
        // // by default, a simple `Column` is rendered
        // // consider using `ListView` or `SliverList` for better performance
        // renderMode: RenderMode.column,
        );
  }
}
