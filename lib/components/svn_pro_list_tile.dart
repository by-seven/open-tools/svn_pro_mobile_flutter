part of '../svn_pro_mobile_flutter.dart';

enum SvnProListTileCondition {
  line1,
  line2,
  line3Plus,
}

enum SvnProListTileSideVariant {
  none,
  icon,
  image,
  video,
  switchButton,
  checkBox,
  radioButton,
  avatar
}

class SvnProListTile extends StatelessWidget {
  final SvnProListTileCondition condition;
  final String? title;
  final String? supportingText;
  final void Function()? onTap;
  final VisualDensity density;
  final bool dense;
  final SvnProListTileSideVariant leadingVariant;
  final SvnProIcon? leadingIcon;
  final String? leadingImageUrl;
  final String? leadingVideoUrl;
  final bool leadingSwitchSelected;
  final void Function(bool)? leadingSwitchOnChanged;
  final SvnProCheckboxStatus? leadingCheckBoxStatus;
  final void Function(bool?)? leadingCheckBoxOnChanged;
  final dynamic leadingRadioEntry;
  final dynamic leadingRadioCurrentlySelected;
  final void Function(dynamic)? leadingRadioOnChanged;
  final SvnProAvatar? leadingAvatar;
  final SvnProListTileSideVariant trailingVariant;
  final SvnProIcon? trailingIcon;
  final String? trailingImageUrl;
  final String? trailingVideoUrl;
  final bool trailingSwitchSelected;
  final void Function(bool)? trailingSwitchOnChanged;
  final SvnProCheckboxStatus? trailingCheckBoxStatus;
  final void Function(bool?)? trailingCheckBoxOnChanged;
  final dynamic trailingRadioEntry;
  final dynamic trailingRadioCurrentlySelected;
  final void Function(dynamic)? trailingRadioOnChanged;
  final SvnProAvatar? trailingAvatar;

  const SvnProListTile({
    super.key,
    this.condition = SvnProListTileCondition.line1,
    this.title = '',
    this.supportingText,
    this.onTap,
    this.density = VisualDensity.standard,
    this.dense = false,
    this.leadingVariant = SvnProListTileSideVariant.none,
    this.leadingIcon,
    this.leadingImageUrl,
    this.leadingVideoUrl,
    this.leadingSwitchSelected = false,
    this.leadingSwitchOnChanged,
    this.leadingCheckBoxStatus,
    this.leadingCheckBoxOnChanged,
    this.leadingRadioEntry,
    this.leadingRadioCurrentlySelected,
    this.leadingRadioOnChanged,
    this.leadingAvatar,
    this.trailingVariant = SvnProListTileSideVariant.none,
    this.trailingIcon,
    this.trailingImageUrl,
    this.trailingVideoUrl,
    this.trailingSwitchSelected = false,
    this.trailingSwitchOnChanged,
    this.trailingCheckBoxStatus,
    this.trailingCheckBoxOnChanged,
    this.trailingRadioEntry,
    this.trailingRadioCurrentlySelected,
    this.trailingRadioOnChanged,
    this.trailingAvatar,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      visualDensity: VisualDensity.comfortable,
      dense: dense,
      title: SvnProText(
        text: title,
        size: SvnProTextSize.bodyLarge,
        weight: SvnProTextWeight.regular,
      ),
      subtitle: calculateSupportingText(),
      isThreeLine:
          condition == SvnProListTileCondition.line3Plus ? true : false,
      onTap: onTap,
      leading: calculateSideWidget(
        leadingVariant,
        leadingIcon,
        leadingImageUrl,
        leadingVideoUrl,
        leadingSwitchSelected,
        leadingSwitchOnChanged,
        leadingCheckBoxStatus,
        leadingCheckBoxOnChanged,
        leadingRadioEntry,
        leadingRadioCurrentlySelected,
        leadingRadioOnChanged,
        leadingAvatar,
      ),
      trailing: calculateSideWidget(
        trailingVariant,
        trailingIcon,
        trailingImageUrl,
        trailingVideoUrl,
        trailingSwitchSelected,
        trailingSwitchOnChanged,
        trailingCheckBoxStatus,
        trailingCheckBoxOnChanged,
        trailingRadioEntry,
        trailingRadioCurrentlySelected,
        trailingRadioOnChanged,
        trailingAvatar,
      ),
    );
  }

  Widget? calculateSideWidget(
      variant,
      icon,
      imageUrl,
      videoUrl,
      switchSelected,
      switchOnChanged,
      checkBoxStatus,
      checkBoxOnChanged,
      radioEntry,
      radioCurrentlySelected,
      radioOnChanged,
      avatar) {
    switch (variant) {
      case SvnProListTileSideVariant.none:
        return null;
      case SvnProListTileSideVariant.icon:
        return icon!;
      case SvnProListTileSideVariant.image:
      case SvnProListTileSideVariant.video:
        return SizedBox(
          width: variant == SvnProListTileSideVariant.image ? 56 : 114,
          height: variant == SvnProListTileSideVariant.image ? 56 : 64,
          child: SvnProImage(
              imageUrl: variant == SvnProListTileSideVariant.image
                  ? imageUrl!
                  : videoUrl!),
        );
      case SvnProListTileSideVariant.switchButton:
        return SvnProSwitch(
          selected: switchSelected,
          onChanged: switchOnChanged,
        );
      case SvnProListTileSideVariant.checkBox:
        return SvnProCheckbox(
          status: checkBoxStatus,
          onChanged: checkBoxOnChanged,
        );
      case SvnProListTileSideVariant.radioButton:
        return SvnProRadio(
          value: radioEntry,
          currentlySelected: radioCurrentlySelected,
          onChanged: radioOnChanged,
        );
      case SvnProListTileSideVariant.avatar:
        return (avatar as SvnProAvatar).copyWith(
          size: SvnProAvatarSizes.px40,
        );
    }
    return null;
  }

  SvnProText? calculateSupportingText() {
    return condition != SvnProListTileCondition.line1
        ? (supportingText != null
            ? SvnProText(
                text: supportingText!,
                size: SvnProTextSize.bodyMedium,
                weight: SvnProTextWeight.regular,
              )
            : const SvnProText(
                text: '',
                size: SvnProTextSize.bodyMedium,
                weight: SvnProTextWeight.regular,
              ))
        : null;
  }

  Widget copyWith({
    SvnProListTileCondition? condition,
    String? title,
    String? supportingText,
    void Function()? onTap,
    SvnProListTileSideVariant? leadingVariant,
    SvnProIcon? leadingIcon,
    String? leadingImageUrl,
    String? leadingVideoUrl,
    bool? leadingSwitchSelected,
    void Function(bool)? leadingSwitchOnChanged,
    SvnProCheckboxStatus? leadingCheckBoxStatus,
    void Function(bool?)? leadingCheckBoxOnChanged,
    dynamic leadingRadioEntry,
    dynamic leadingRadioCurrentlySelected,
    void Function(dynamic)? leadingRadioOnChanged,
    SvnProAvatar? leadingAvatar,
    SvnProListTileSideVariant? trailingVariant,
    SvnProIcon? trailingIcon,
    String? trailingImageUrl,
    String? trailingVideoUrl,
    bool? trailingSwitchSelected,
    void Function(bool)? trailingSwitchOnChanged,
    SvnProCheckboxStatus? trailingCheckBoxStatus,
    void Function(bool?)? trailingCheckBoxOnChanged,
    dynamic trailingRadioEntry,
    dynamic trailingRadioCurrentlySelected,
    void Function(dynamic)? trailingRadioOnChanged,
    SvnProAvatar? trailingAvatar,
  }) {
    return SvnProListTile(
      condition: condition ?? this.condition,
      title: title ?? this.title,
      supportingText: supportingText ?? this.supportingText,
      onTap: onTap ?? this.onTap,
      leadingVariant: leadingVariant ?? this.leadingVariant,
      leadingIcon: leadingIcon ?? this.leadingIcon,
      leadingImageUrl: leadingImageUrl ?? this.leadingImageUrl,
      leadingVideoUrl: leadingVideoUrl ?? this.leadingVideoUrl,
      leadingSwitchSelected:
          leadingSwitchSelected ?? this.leadingSwitchSelected,
      leadingSwitchOnChanged:
          leadingSwitchOnChanged ?? this.leadingSwitchOnChanged,
      leadingCheckBoxStatus:
          leadingCheckBoxStatus ?? this.leadingCheckBoxStatus,
      leadingCheckBoxOnChanged:
          leadingCheckBoxOnChanged ?? this.leadingCheckBoxOnChanged,
      leadingRadioEntry: leadingRadioEntry ?? this.leadingRadioEntry,
      leadingRadioCurrentlySelected:
          leadingRadioCurrentlySelected ?? this.leadingRadioCurrentlySelected,
      leadingRadioOnChanged:
          leadingRadioOnChanged ?? this.leadingRadioOnChanged,
      leadingAvatar: leadingAvatar ?? this.leadingAvatar,
      trailingVariant: trailingVariant ?? this.trailingVariant,
      trailingIcon: trailingIcon ?? this.trailingIcon,
      trailingImageUrl: trailingImageUrl ?? this.trailingImageUrl,
      trailingVideoUrl: trailingVideoUrl ?? this.trailingVideoUrl,
      trailingSwitchSelected:
          trailingSwitchSelected ?? this.trailingSwitchSelected,
      trailingSwitchOnChanged:
          trailingSwitchOnChanged ?? this.trailingSwitchOnChanged,
      trailingCheckBoxStatus:
          trailingCheckBoxStatus ?? this.trailingCheckBoxStatus,
      trailingCheckBoxOnChanged:
          trailingCheckBoxOnChanged ?? this.trailingCheckBoxOnChanged,
      trailingRadioEntry: trailingRadioEntry ?? this.trailingRadioEntry,
      trailingRadioCurrentlySelected:
          trailingRadioCurrentlySelected ?? this.trailingRadioCurrentlySelected,
      trailingRadioOnChanged:
          trailingRadioOnChanged ?? this.trailingRadioOnChanged,
      trailingAvatar: trailingAvatar ?? this.trailingAvatar,
    );
  }
}
