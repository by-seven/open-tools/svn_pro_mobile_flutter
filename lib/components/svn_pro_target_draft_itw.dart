part of '../svn_pro_mobile_flutter.dart';

class SvnProTargetDraftItw extends StatelessWidget {
  final String? title;
  final String? desc;
  final DateTime? dueDate;
  final VoidCallback? onEditTarget;
  final VoidCallback? onDeleteTarget;
  final SvnProInfoTag? svnProInfoTag;
  final bool enable;

  const SvnProTargetDraftItw({
    super.key,
    this.title,
    this.desc,
    this.dueDate,
    this.svnProInfoTag,
    this.onEditTarget,
    this.onDeleteTarget,
    this.enable = true,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProQuestionEditItw(
      title: title,
      description: desc,
      hasAnswerInput: false,
      questionInfoTag: svnProInfoTag,
      deadline: dueDate,
      content: Column(
        children: [
          const SvnProDivider(),
          const SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Flexible(
                child: SvnProText(
                  text:
                      'This target is still a draft. It will be added to Roadmap after submitting all the interviews in the set.',
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.regular,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Expanded(
                child: SvnProBtn(
                  variant: SvnProBtnVariants.tonal,
                  text: 'edit_target'.tr,
                  iconData: MingCuteIcons.mgc_add_line,
                  onPressed: onEditTarget,
                  svnProBtnState:
                      enable ? SvnProBtnState.enabled : SvnProBtnState.disabled,
                ),
              ),
              const SizedBox(
                width: 4,
              ),
              SvnProIconButton(
                variant: SvnProIconButtonVariant.outlined,
                icon: const SvnProIcon(
                  iconData: MingCuteIcons.mgc_delete_2_line,
                ),
                onPressed: onDeleteTarget,
                enable: enable,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
