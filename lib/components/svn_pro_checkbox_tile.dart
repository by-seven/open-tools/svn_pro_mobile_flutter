part of '../svn_pro_mobile_flutter.dart';

class SvnProCheckboxTile extends StatelessWidget {
  final bool? value;
  final ValueChanged<bool?>? onChanged;
  final String? title;
  final Color? color;
  final ListTileControlAffinity controlAffinity;

  const SvnProCheckboxTile(
      {super.key,
      this.value = false,
      this.onChanged,
      this.title,
      this.color,
      this.controlAffinity = ListTileControlAffinity.trailing});

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      controlAffinity: controlAffinity,
      title: title != null
          ? SvnProText(
              text: title,
              size: SvnProTextSize.bodyLarge,
              weight: SvnProTextWeight.regular,
              color: value != null && value == true
                  ? color ?? Theme.of(context).colorScheme.primary
                  : null,
            )
          : null,
      value: value,
      onChanged: onChanged,
    );
  }
}
