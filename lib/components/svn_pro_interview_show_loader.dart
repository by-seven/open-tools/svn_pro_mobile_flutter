part of '../svn_pro_mobile_flutter.dart';

class SvnProInterviewShowLoader extends StatelessWidget {
  const SvnProInterviewShowLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SvnProFeatureItem(
                featureText: 'hello toto',
                description: 'tata tata tata',
              ),
              const SvnProText(
                  text: 'Hello toto toto toto',
                  size: SvnProTextSize.headlineLarge),
              ...List.generate(
                3,
                (index) => [
                  const SizedBox(
                    height: 24,
                  ),
                  const SvnProCard(
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        children: [
                          SvnProText(
                              text: 'Hello toto toto toto',
                              size: SvnProTextSize.headlineLarge),
                          SvnProText(
                              text: 'Hello toto toto toto',
                              size: SvnProTextSize.headlineLarge),
                          SvnProText(
                              text: 'Hello toto toto toto',
                              size: SvnProTextSize.headlineLarge),
                          SvnProText(
                              text: 'Hello toto toto toto',
                              size: SvnProTextSize.headlineLarge),
                          // SvnProCard(
                          //   child: SvnProText(
                          //       text: 'Hello toto toto toto',
                          //       size: SvnProTextSize.headlineLarge),
                          // ),
                        ],
                      ),
                    ),
                  )
                ],
              ).expand((i) => i),
            ],
          ),
        ],
      ),
    );
  }
}
