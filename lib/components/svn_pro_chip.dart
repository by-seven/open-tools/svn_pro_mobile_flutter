part of '../svn_pro_mobile_flutter.dart';

enum SvnProChipVariant {
  input,
  assist,
  filter,
  suggestion,
}

enum SvnProChipConf {
  label,
  labelLeadingIcon,
  labelAvatar,
}

enum SvnProChipStyle {
  outlined,
  elevated,
}

enum SvnProChipState { enabled, disabled }

class SvnProChip extends StatelessWidget {
  final String text;
  final SvnProChipVariant variant;
  final SvnProChipConf conf;
  final bool selected;
  final void Function(bool)? onSelected;
  final void Function()? onDeleted;
  final SvnProAvatar? leadingAvatar;
  final IconData? leadingIconData;
  final void Function()? onPressed;
  final SvnProChipStyle style;
  final SvnProChipState state;
  final IconData? trailingIconData;

  const SvnProChip({
    super.key,
    required this.text,
    this.variant = SvnProChipVariant.input,
    this.conf = SvnProChipConf.label,
    // show selected
    this.selected = false,
    this.onSelected,
    // show closing icon
    this.onDeleted,
    this.leadingAvatar,
    this.leadingIconData,
    this.onPressed,
    this.style = SvnProChipStyle.outlined,
    this.state = SvnProChipState.enabled,
    this.trailingIconData,
  });

  @override
  Widget build(BuildContext context) {
    switch (variant) {
      case SvnProChipVariant.input:
        return InputChip(
          label: SvnProText(
            text: text,
            size: SvnProTextSize.titleSmall,
            weight: SvnProTextWeight.medium,
            color: Theme.of(context).colorScheme.onSurfaceVariant,
          ),
          selected: selected,
          onSelected: onSelected,
          onDeleted: onDeleted,
          avatar: conf == SvnProChipConf.label || selected
              ? null
              : (conf == SvnProChipConf.labelLeadingIcon
                  ? SvnProIcon(
                      iconData: leadingIconData,
                      color: Theme.of(context).colorScheme.primary,
                      size: 18)
                  : leadingAvatar),
          shape: conf == SvnProChipConf.labelAvatar
              ? const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(120),
                  ),
                )
              : null,
          onPressed:
              state == SvnProChipState.enabled ? (onPressed ?? () {}) : null,
          // visualDensity:
          //     conf == SvnProChipConf.labelAvatar ? VisualDensity.compact : null,
          elevation: style == SvnProChipStyle.elevated ? 4 : 0,
        );
      case SvnProChipVariant.assist:
        if (style == SvnProChipStyle.outlined) {
          return ActionChip(
            label: SvnProText(
              text: text,
              size: SvnProTextSize.titleSmall,
              weight: SvnProTextWeight.medium,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            avatar: conf == SvnProChipConf.label || selected
                ? null
                : (conf == SvnProChipConf.labelLeadingIcon
                    ? SvnProIcon(
                        iconData: leadingIconData,
                        color: Theme.of(context).colorScheme.primary,
                        size: 18)
                    : leadingAvatar),
            shape: conf == SvnProChipConf.labelAvatar
                ? const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(120),
                    ),
                  )
                : null,
            onPressed:
                state == SvnProChipState.enabled ? (onPressed ?? () {}) : null,
          );
        } else {
          return ActionChip.elevated(
            label: SvnProText(
              text: text,
              size: SvnProTextSize.titleSmall,
              weight: SvnProTextWeight.medium,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            avatar: conf == SvnProChipConf.label || selected
                ? null
                : (conf == SvnProChipConf.labelLeadingIcon
                    ? SvnProIcon(
                        iconData: leadingIconData,
                        color: Theme.of(context).colorScheme.primary,
                        size: 18)
                    : leadingAvatar),
            shape: conf == SvnProChipConf.labelAvatar
                ? const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(120),
                    ),
                  )
                : null,
            onPressed:
                state == SvnProChipState.enabled ? (onPressed ?? () {}) : null,
          );
        }
      case SvnProChipVariant.filter:
        if (style == SvnProChipStyle.outlined) {
          return FilterChip(
            label: SvnProText(
              text: text,
              size: SvnProTextSize.titleSmall,
              weight: SvnProTextWeight.medium,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            selected: selected,
            onSelected: state == SvnProChipState.enabled
                ? (onSelected ?? (bool _) {})
                : null,
            onDeleted: onDeleted,
            deleteIcon: trailingIconData != null
                ? SvnProIcon(
                    iconData: trailingIconData,
                  )
                : null,
            avatar: conf == SvnProChipConf.label || selected
                ? null
                : (conf == SvnProChipConf.labelLeadingIcon
                    ? SvnProIcon(
                        iconData: leadingIconData,
                        color: Theme.of(context).colorScheme.primary,
                        size: 18)
                    : leadingAvatar),
            shape: conf == SvnProChipConf.labelAvatar
                ? const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(120),
                    ),
                  )
                : null,
          );
        } else {
          return FilterChip.elevated(
            label: SvnProText(
              text: text,
              size: SvnProTextSize.titleSmall,
              weight: SvnProTextWeight.medium,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            selected: selected,
            onSelected: state == SvnProChipState.enabled
                ? (onSelected ?? (bool _) {})
                : null,
            onDeleted: onDeleted,
            deleteIcon: trailingIconData != null
                ? SvnProIcon(
                    iconData: trailingIconData,
                  )
                : null,
            avatar: conf == SvnProChipConf.label || selected
                ? null
                : (conf == SvnProChipConf.labelLeadingIcon
                    ? SvnProIcon(
                        iconData: leadingIconData,
                        color: Theme.of(context).colorScheme.primary,
                        size: 18)
                    : leadingAvatar),
            shape: conf == SvnProChipConf.labelAvatar
                ? const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(120),
                    ),
                  )
                : null,
          );
        }
      case SvnProChipVariant.suggestion:
        if (style == SvnProChipStyle.outlined) {
          return ActionChip(
            label: SvnProText(
              text: text,
              size: SvnProTextSize.titleSmall,
              weight: SvnProTextWeight.medium,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            avatar: conf == SvnProChipConf.label || selected
                ? null
                : (conf == SvnProChipConf.labelLeadingIcon
                    ? SvnProIcon(
                        iconData: leadingIconData,
                        color: Theme.of(context).colorScheme.primary,
                        size: 18)
                    : leadingAvatar),
            shape: conf == SvnProChipConf.labelAvatar
                ? const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(120),
                    ),
                  )
                : null,
            onPressed:
                state == SvnProChipState.enabled ? (onPressed ?? () {}) : null,
          );
        } else {
          return ActionChip.elevated(
            label: SvnProText(
              text: text,
              size: SvnProTextSize.titleSmall,
              weight: SvnProTextWeight.medium,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            avatar: conf == SvnProChipConf.label || selected
                ? null
                : (conf == SvnProChipConf.labelLeadingIcon
                    ? SvnProIcon(
                        iconData: leadingIconData,
                        color: Theme.of(context).colorScheme.primary,
                        size: 18)
                    : leadingAvatar),
            shape: conf == SvnProChipConf.labelAvatar
                ? const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(120),
                    ),
                  )
                : null,
            onPressed:
                state == SvnProChipState.enabled ? (onPressed ?? () {}) : null,
          );
        }
    }
  }
}
