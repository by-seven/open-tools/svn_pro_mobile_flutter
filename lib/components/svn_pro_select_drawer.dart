part of '../svn_pro_mobile_flutter.dart';

class SvnProSelectDrawer extends StatefulWidget {
  final String? label;
  final TextEditingController? controller;
  final WidgetBuilder builder;
  final String? initialValue;
  final FormFieldValidator<String>? validator;
  final SvnProTextFieldStatus state;

  const SvnProSelectDrawer({
    super.key,
    this.label,
    this.controller,
    required this.builder,
    this.initialValue,
    this.validator,
    this.state = SvnProTextFieldStatus.enabled,
  });

  @override
  State<SvnProSelectDrawer> createState() => _SvnProSelectDrawerState();
}

class _SvnProSelectDrawerState extends State<SvnProSelectDrawer> {
  final fieldController = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();

    if (widget.controller == null) {
      fieldController.text = widget.initialValue ?? '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SvnProTextField(
      readOnly: true,
      status: widget.state,
      variant: SvnProTextFieldVariant.outlined,
      suffixIcon: const SvnProIcon(
        iconData: MingCuteIcons.mgc_down_small_fill,
      ),
      label: widget.label,
      controller: widget.controller ?? fieldController,
      validator: widget.validator,
      onTap: () async {
        HapticFeedback.mediumImpact();
        final res =
            await svnProShowSelectDrawer(context, builder: widget.builder);

        if (res != null) {
          if (widget.controller != null) {
            widget.controller!.text = res;
          } else {
            fieldController.text = res;
          }
        }
      },
    );
  }
}
