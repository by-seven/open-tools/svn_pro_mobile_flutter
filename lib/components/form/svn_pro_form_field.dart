part of '../../svn_pro_mobile_flutter.dart';

// TODO: this class will inherit on every input field of SVN PRO LIB
// it should have a validator callback that return a string or null
// String? validator(T? value)
// it should have a builder arg that will be build in SvnProFormFieldState
// Widget Function(SvnProFormFieldState<T> field)
// SvnProFormFieldState<T> field is the context
// we can also define a status attr so that children classes could use field.status
class SvnProFormField<T> extends StatefulWidget {
  final Widget Function(SvnProFormFieldState) builder;
  final String? Function(T?)? validator;
  final T? initialValue;
  final String? comment;
  final SvnProQuestionEditItwStatus status;

  const SvnProFormField({
    super.key,
    required this.builder,
    this.validator,
    this.initialValue,
    this.comment,
    this.status = SvnProQuestionEditItwStatus.enabled,
  });

  @override
  State<SvnProFormField> createState() => SvnProFormFieldState<T>();
}

// TODO: here we need to define T? value, so Fields that inherit from this Widget can apply any type
class SvnProFormFieldState<T> extends State<SvnProFormField<T>> {
  late T? value = widget.initialValue;
  late String? comment = widget.comment;
  String? errorText;
  late SvnProQuestionEditItwStatus status = widget.status;

  @override
  Widget build(BuildContext context) {
    SvnProForm.maybeOf(context)?.register(this);
    return widget.builder(this);
  }

  bool validate() {
    setState(() {
      if (widget.validator != null) {
        errorText = widget.validator!(value);
      } else {
        errorText = null;
      }
    });
    final valid = errorText == null;

    if (valid) {
      setState(() {
        status = SvnProQuestionEditItwStatus.enabled;
      });
    } else {
      setState(() {
        status = SvnProQuestionEditItwStatus.error;
      });
    }
    return valid;
  }

  void didChangeValue(T? val) {
    setState(() {
      value = val;
    });

    SvnProForm.maybeOf(context)?.fieldDidChange();
  }

  void didChangeComment(String com) {
    setState(() {
      comment = com;
    });

    SvnProForm.maybeOf(context)?.fieldDidChange();
  }

  @override
  void deactivate() {
    SvnProForm.maybeOf(context)?.unregister(this);
    super.deactivate();
  }
}
