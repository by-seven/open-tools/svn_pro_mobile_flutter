part of '../../svn_pro_mobile_flutter.dart';

// TODO: here we need to inherit of SvnProFormField
// we also need to define super.validator so that it goes to parent
// we need to define builder for parent in order to him to be able to rebuild Widget ex:
// SvnProQuestionEditItw() : super(
//   builder: (SvnProFormFieldState<String> field) {
//     // we can use field like that field.status
//     return SvnProQuestionEditItw(...);
//   }
// )
class SvnProFormFieldQuestionEditItw<T> extends SvnProFormField {
  final ValueChanged<T?>? onChanged;

  SvnProFormFieldQuestionEditItw({
    super.key,
    GlobalQuestionType type = GlobalQuestionType.text,
    String? description,
    Widget? content,
    int maxStars = 5,
    ValueChanged<String>? onCommentChanged,
    void Function()? openCanBeCommented,
    List<String>? radioList,
    bool hasAnswerInput = true,
    SvnProInfoTag? questionInfoTag,
    DateTime? deadline,
    required String title,
    super.initialValue,
    super.comment,
    super.validator,
    super.status,
    this.onChanged,
  }) : super(
          builder: (SvnProFormFieldState field) {
            return SvnProQuestionEditItw<T>(
              type: type,
              title: title,
              initialValue: field.value,
              status: field.status,
              onChanged: (T? value) {
                field.didChangeValue(value);
                onChanged?.call(value);
              },
              description: description,
              content: content,
              maxStars: maxStars,
              comment: field.comment,
              onCommentChanged: (String comment) {
                field.didChangeComment(comment);
                onCommentChanged?.call(comment);
              },
              openCanBeCommented: openCanBeCommented,
              radioList: radioList,
              hasAnswerInput: hasAnswerInput,
              questionInfoTag: questionInfoTag,
              deadline: deadline,
            );
          },
        );

  @override
  SvnProFormFieldState createState() => _SvnProFormFieldQuestionEditItwState();
}

// TODO: we need to inherit from SvnProFormFieldState without defining override build function
// because parent class need to be able to rebuild this widget if SvnProForm is invalid
class _SvnProFormFieldQuestionEditItwState extends SvnProFormFieldState {}
