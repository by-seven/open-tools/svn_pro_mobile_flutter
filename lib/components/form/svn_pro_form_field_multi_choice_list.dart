part of '../../svn_pro_mobile_flutter.dart';

class SvnProFormFieldMultiChoiceList
    extends SvnProFormField<List<SvnProMultiChoiceData>> {
  final ValueChanged<List<SvnProMultiChoiceData>>? onChanged;

  SvnProFormFieldMultiChoiceList({
    super.key,
    super.initialValue,
    super.status,
    super.validator,
    bool? readOnly = false,
    this.onChanged,
  }) : super(builder: (SvnProFormFieldState field) {
          return SvnProMultiChoiceList(
            initialValue: field.value,
            status: field.status,
            errorText: field.errorText,
            readOnly: readOnly ?? false,
            onChanged: (List<SvnProMultiChoiceData> value) {
              field.didChangeValue(value);
              onChanged?.call(value);
            },
          );
        });

  @override
  SvnProFormFieldState createState() => _SvnProFormFieldMultiChoiceListState();
}

class _SvnProFormFieldMultiChoiceListState extends SvnProFormFieldState {}
