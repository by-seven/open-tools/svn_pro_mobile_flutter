part of '../../svn_pro_mobile_flutter.dart';

class SvnProFormFieldTextField extends SvnProFormField<String> {
  final ValueChanged<String?>? onChanged;

  SvnProFormFieldTextField({
    super.key,
    super.initialValue,
    super.status,
    super.validator,
    this.onChanged,
    bool? readOnly,
    String? label,
    TextEditingController? controller,
    bool textArea = false,
    int minLines = 1,
    int maxLines = 8,
    SvnProIcon? prefixIcon,
    String? placeHolder,
    TextInputType? keyboardType,
  }) : super(builder: (SvnProFormFieldState field) {
          return SvnProTextField(
            controller: controller,
            initialValue: controller != null ? null : field.value,
            status: field.status == SvnProQuestionEditItwStatus.enabled
                ? SvnProTextFieldStatus.enabled
                : SvnProTextFieldStatus.error,
            errorText: field.errorText,
            onChanged: (String? value) {
              field.didChangeValue(value);
              onChanged?.call(value);
            },
            readOnly: readOnly ?? false,
            label: label,
            textArea: textArea,
            minLines: minLines,
            maxLines: maxLines,
            prefixIcon: prefixIcon,
            placeHolder: placeHolder,
            keyboardType: keyboardType,
          );
        });

  @override
  SvnProFormFieldState createState() => _SvnProFormFieldTextFieldState();
}

class _SvnProFormFieldTextFieldState extends SvnProFormFieldState {}
