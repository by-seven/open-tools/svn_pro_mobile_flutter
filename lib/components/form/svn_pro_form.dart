part of '../../svn_pro_mobile_flutter.dart';

// TODO: here we need to code 2 static functions
// static FormState? maybeOf(BuildContext context)
// static FormState of(BuildContext context)
class SvnProForm extends StatefulWidget {
  final Widget child;
  final VoidCallback? onChanged;

  const SvnProForm({
    super.key,
    required this.child,
    this.onChanged,
  });

  static SvnProFormState? maybeOf(BuildContext context) {
    final _SvnProFormScope? scope =
        context.dependOnInheritedWidgetOfExactType<_SvnProFormScope>();

    return scope?.svnProFormState;
  }

  static SvnProFormState of(BuildContext context) {
    final svnProFormState = maybeOf(context);
    assert(() {
      if (svnProFormState == null) {
        throw FlutterError(
          'SvnProForm.of() was called with a context that does not contain a SvnProForm widget.\n'
          'No SvnProForm widget ancestor could be found starting from the context that '
          'was passed to SvnProForm.of(). This can happen because you are using a widget '
          'that looks for a SvnProForm ancestor, but no such ancestor exists.\n'
          'The context used was:\n'
          '  $context',
        );
      }
      return true;
    }());

    return svnProFormState!;
  }

  @override
  State<SvnProForm> createState() => SvnProFormState();
}

// TODO: here need to code validate functions
// bool validate()
// it's a force to rebuild and a loop on _fields array
// _fields it's a set of SvnProFormField
class SvnProFormState extends State<SvnProForm> {
  int generation = 0;
  final Set<SvnProFormFieldState> fields = <SvnProFormFieldState>{};

  @override
  Widget build(BuildContext context) {
    return _SvnProFormScope(
      svnProFormState: this,
      generation: generation,
      child: widget.child,
    );
  }

  void register(SvnProFormFieldState field) {
    fields.add(field);
  }

  void unregister(SvnProFormFieldState<dynamic> field) {
    fields.remove(field);
  }

// TODO: code global validate here
  bool validate() {
    setState(() {
      ++generation;
    });

    var valid = true;
    for (final SvnProFormFieldState field in fields) {
      if (field.validate() == false) {
        valid = false;
      }
    }

    return valid;
  }

  void fieldDidChange() {
    widget.onChanged?.call();
    // the following line is maybe not necessary...
    setState(() {
      ++generation;
    });
  }
}

// TODO: need to define SvnProFormScope like
// class _SvnProFormScope extends InheritedWidget {
// const _SvnProFormScope({required super.child})
// @override bool updateShouldNotify(_SvnProFormScope old)
// so that we can define the function maybeOf in SvnProForm
// https://www.youtube.com/watch?v=og-vJqLzg2c
// in order for children to be able to register in SvnProForm (field static attr)
// so in children we check if we are a child of SvnProForm if it's the case we register
// SvnProForm.maybeOf()?._register(this)
class _SvnProFormScope extends InheritedWidget {
  final int generation;
  final SvnProFormState svnProFormState;

  const _SvnProFormScope({
    required super.child,
    required this.generation,
    required this.svnProFormState,
  });

  @override
  bool updateShouldNotify(covariant _SvnProFormScope oldWidget) {
    return oldWidget.generation != generation;
  }
}
