part of '../svn_pro_mobile_flutter.dart';

enum SvnProFloatingActionButtonVariant {
  surface,
  primary,
  secondary,
  tertiary,
}

enum SvnProFloatingActionButtonSize {
  small,
  regular,
  large,
  extended,
}

class SvnProFloatingActionButton extends StatelessWidget {
  final SvnProFloatingActionButtonVariant variant;
  final SvnProFloatingActionButtonSize size;
  final String text;
  final SvnProIcon? icon;
  final Function()? onPressed;

  const SvnProFloatingActionButton({
    super.key,
    this.variant = SvnProFloatingActionButtonVariant.primary,
    this.size = SvnProFloatingActionButtonSize.regular,
    this.text = '',
    this.icon,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    switch (size) {
      case SvnProFloatingActionButtonSize.small:
        return FloatingActionButton.small(
          foregroundColor: calculateForgroundColor(context),
          backgroundColor: calculateBackgroundColor(context),
          onPressed: onPressed ?? () {},
          child: icon,
        );
      case SvnProFloatingActionButtonSize.regular:
        return FloatingActionButton(
          foregroundColor: calculateForgroundColor(context),
          backgroundColor: calculateBackgroundColor(context),
          onPressed: onPressed ?? () {},
          child: icon,
        );
      case SvnProFloatingActionButtonSize.large:
        return FloatingActionButton.large(
          foregroundColor: calculateForgroundColor(context),
          backgroundColor: calculateBackgroundColor(context),
          onPressed: onPressed ?? () {},
          child: icon,
        );
      case SvnProFloatingActionButtonSize.extended:
        return FloatingActionButton.extended(
          foregroundColor: calculateForgroundColor(context),
          backgroundColor: calculateBackgroundColor(context),
          onPressed: onPressed ?? () {},
          label: SvnProText(
            text: text,
            size: SvnProTextSize.titleSmall,
            weight: SvnProTextWeight.medium,
          ),
          icon: icon,
        );
    }
  }

  calculateForgroundColor(context) {
    switch (variant) {
      case SvnProFloatingActionButtonVariant.surface:
        return Theme.of(context).colorScheme.primary;
      case SvnProFloatingActionButtonVariant.primary:
        return Theme.of(context).colorScheme.onPrimaryContainer;
      case SvnProFloatingActionButtonVariant.secondary:
        return Theme.of(context).colorScheme.onSecondaryContainer;
      case SvnProFloatingActionButtonVariant.tertiary:
        return Theme.of(context).colorScheme.onTertiaryContainer;
    }
  }

  calculateBackgroundColor(context) {
    switch (variant) {
      case SvnProFloatingActionButtonVariant.surface:
        return Theme.of(context).colorScheme.surface;
      case SvnProFloatingActionButtonVariant.primary:
        return Theme.of(context).colorScheme.primaryContainer;
      case SvnProFloatingActionButtonVariant.secondary:
        return Theme.of(context).colorScheme.secondaryContainer;
      case SvnProFloatingActionButtonVariant.tertiary:
        return Theme.of(context).colorScheme.tertiaryContainer;
    }
  }
}
