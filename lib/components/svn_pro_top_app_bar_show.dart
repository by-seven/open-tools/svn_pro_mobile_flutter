part of '../svn_pro_mobile_flutter.dart';

class SvnProTopAppBarShow extends StatelessWidget {
  final Widget? body;
  final String? title;
  final bool elevation;
  final VoidCallback? onLeadingPressed;
  final Widget? fab;
  final SvnProTextSize titleSize;

  const SvnProTopAppBarShow({
    super.key,
    this.body,
    this.title,
    this.elevation = false,
    this.onLeadingPressed,
    this.fab,
    this.titleSize = SvnProTextSize.titleLarge,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProTopAppBar(
        elevation: elevation,
        body: body,
        title: title,
        leading: onLeadingPressed != null
            ? SvnProIconButton(
                variant: SvnProIconButtonVariant.standard,
                icon: const SvnProIcon(
                  iconData: MingCuteIcons.mgc_arrow_left_line,
                ),
                onPressed: onLeadingPressed,
              )
            : null,
        fab: fab,
        titleSize: titleSize);
  }
}
