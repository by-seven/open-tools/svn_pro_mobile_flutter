part of '../svn_pro_mobile_flutter.dart';

/// Displays a custom SnackBar with various customization options.
///
/// This function allows you to show a SnackBar with a floating behavior and several optional
/// parameters for customization, such as text content, duration, action buttons, and the ability
/// to close the current route before showing the SnackBar.
///
/// Example usage:
/// ```dart
/// svnProShowSnackbar(
///   context,
///   text: "Message shown in the SnackBar",
///   duration: 4,
///   closable: true,
///   labelAction: "Undo",
///   actionCallback: () {
///     print("Action button clicked!");
///   },
///   longAction: true,  // Allow larger tap area for the action button
///   popBefore: true,   // Pop the previous route before showing the SnackBar
/// );
/// ```
///
/// Args:
///   - `context` (`BuildContext`): The BuildContext of the widget calling the SnackBar.
///   - `text` (`String`): The message to be displayed in the SnackBar. This parameter is required.
///   - `duration` (`int`, optional): How long the SnackBar should be visible, in seconds. Default is 3 seconds.
///   - `closable` (`bool`, optional): Whether the SnackBar should show a close icon. Default is `false`.
///   - `labelAction` (`String?`, optional): The label for the action button, if one is needed.
///   - `longAction` (`bool`, optional): Whether to allow the action button to have a larger tap area. Default is `false`.
///   - `actionCallback` (`void Function()?`, optional): A callback function executed when the action button is pressed. Default is an empty function.
///   - `popBefore` (`bool`, optional): Whether to call `context.pop()` to close the current route (like a dialog or modal) before showing the SnackBar. Default is `false`.
///
/// Returns:
///   - `void`: This function does not return any value.
///
/// Note: The SnackBar will replace any currently displayed SnackBar before showing the new one.
void svnProShowSnackbar(
  BuildContext context, {
  required String
      text, // The message text to display in the SnackBar (required)
  int duration =
      3, // Duration in seconds for how long the SnackBar will be visible (default 3 seconds)
  bool closable =
      false, // Option to show a close icon on the SnackBar (default is false)
  String? labelAction, // Optional label for the action button, if needed
  bool longAction =
      false, // Determines whether the action button should have a larger tap area (default is false)
  void Function()?
      actionCallback, // Callback function that will be executed when the action button is pressed
  bool popBefore =
      false, // Whether to call `Navigator.pop()` to close the current route before showing the SnackBar
}) {
  // Check  if the context is mounted before showing the SnackBar
  if (!context.mounted) {
    return; // Stop execution if the context is no longer valid
  }
  // Check if the context is mounted before attempting to pop the current route
  if (popBefore && Navigator.canPop(context)) {
    Navigator.of(context).pop();
  }

  // Define the SnackBar with customizable options
  final snackbar = SnackBar(
    content: SvnProText(
      text: text, // Set the text for the SnackBar
      size: SvnProTextSize.bodyMedium, // Set the font size for the text
      weight: SvnProTextWeight.regular, // Set the font weight
      color: Theme.of(context)
          .colorScheme
          .surface, // Dynamically set the text color based on the theme
    ),
    behavior: SnackBarBehavior
        .floating, // Use a floating SnackBar for better visibility
    duration: Duration(seconds: duration), // Duration of the SnackBar
    showCloseIcon: closable, // Display a close icon if `closable` is true
    action: labelAction != null
        ? SnackBarAction(
            // If `labelAction` is provided, create an action button
            label: labelAction, // Set the action button label
            onPressed: actionCallback ??
                () {}, // Use the provided `actionCallback` or an empty function
          )
        : null, // No action button if `labelAction` is not provided
    actionOverflowThreshold: longAction
        ? 0
        : 1, // Allow larger tap area for the action button if `longAction` is true
  );

  if (context.mounted) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar() // Hide the currently visible SnackBar, if any
      ..showSnackBar(snackbar); // Show the new SnackBar
  }
}
