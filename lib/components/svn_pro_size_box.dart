import 'package:flutter/material.dart';

/// `SvnProSizedBox` is a utility class that provides standardized `SizedBox` widgets
/// for consistent spacing across the application. This class includes factory methods
/// for custom spacing as well as pre-defined constants for commonly used spacings.
///
/// The design philosophy here is to create a unified approach to spacing that enhances
/// code readability and maintainability by adhering to consistent vertical and
/// horizontal spacing values.
class SvnProSizedBox {
  /// Alias for a zero-sized `SizedBox`, often used in conditional widget builds.
  ///
  /// Example:
  /// ```dart
  /// return condition ? Text('Visible') : SvnProSizedBox.z;
  /// ```
  static const Widget z = SizedBox.shrink();

  // --- Predefined Vertical Spacing Widgets ---
  /// A vertical spacer with 4.0 logical pixels of height.
  static const Widget h4 = SizedBox(height: 4.0);

  /// A vertical spacer with 8.0 logical pixels of height.
  static const Widget h8 = SizedBox(height: 8.0);

  /// A vertical spacer with 12.0 logical pixels of height.
  static const Widget h12 = SizedBox(height: 12.0);

  /// A vertical spacer with 16.0 logical pixels of height.
  static const Widget h16 = SizedBox(height: 16.0);

  /// A vertical spacer with 24.0 logical pixels of height.
  static const Widget h24 = SizedBox(height: 24.0);

  /// A vertical spacer with 32.0 logical pixels of height.
  static const Widget h32 = SizedBox(height: 32.0);

  /// A vertical spacer with 48.0 logical pixels of height.
  static const Widget h48 = SizedBox(height: 48.0);

  /// A vertical spacer with 64.0 logical pixels of height.
  static const Widget h64 = SizedBox(height: 64.0);

  // --- Predefined Horizontal Spacing Widgets ---
  /// A horizontal spacer with 4.0 logical pixels of width.
  static const Widget w4 = SizedBox(width: 4.0);

  /// A horizontal spacer with 8.0 logical pixels of width.
  static const Widget w8 = SizedBox(width: 8.0);

  /// A horizontal spacer with 12.0 logical pixels of width.
  static const Widget w12 = SizedBox(width: 12.0);

  /// A horizontal spacer with 16.0 logical pixels of width.
  static const Widget w16 = SizedBox(width: 16.0);

  /// A horizontal spacer with 24.0 logical pixels of width.
  static const Widget w24 = SizedBox(width: 24.0);

  /// A horizontal spacer with 32.0 logical pixels of width.
  static const Widget w32 = SizedBox(width: 32.0);

  /// A horizontal spacer with 48.0 logical pixels of width.
  static const Widget w48 = SizedBox(width: 48.0);

  /// A horizontal spacer with 64.0 logical pixels of width.
  static const Widget w64 = SizedBox(width: 64.0);
}
