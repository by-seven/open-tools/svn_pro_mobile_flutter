part of '../svn_pro_mobile_flutter.dart';

class SvnProModuleShowSkeleton extends StatelessWidget {
  const SvnProModuleShowSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: ListView(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        children: [
          _buildSkeletonCoverImage(),
          _buildSkeletonContentHeader(),
          const SvnProDivider(variant: SvnProDividerVariant.inset),
          Padding(
            padding: const EdgeInsets.all(20),
            child: _buildSkeletonRichText(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: _buildSkeletonSubmissionButton(),
          ),
          const SizedBox(height: 16),
          _buildSkeletonPagination(),
        ],
      ),
    );
  }

  Widget _buildSkeletonCoverImage() => Skeleton.replace(
        width: double.infinity,
        height: 160,
        child: Container(color: Colors.grey[300]),
      );

  Widget _buildSkeletonContentHeader() => Padding(
        padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Skeleton.leaf(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(width: 100, height: 20, color: Colors.grey[300]),
                  Container(width: 50, height: 20, color: Colors.grey[300]),
                ],
              ),
            ),
            const SizedBox(height: 16),
            Wrap(
              spacing: 8.0,
              runSpacing: 8.0,
              children: List.generate(
                3,
                (_) => Container(
                  width: 80,
                  height: 30,
                  color: Colors.grey[300],
                ),
              ),
            ),
          ],
        ),
      );

  Widget _buildSkeletonRichText() => Container(
        width: double.infinity,
        height: 150,
        color: Colors.grey[300],
      );

  Widget _buildSkeletonSubmissionButton() => Skeleton.replace(
        width: double.infinity,
        height: 50,
        child: Container(color: Colors.grey[300]),
      );

  Widget _buildSkeletonPagination() => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Skeleton.replace(
              width: 48,
              height: 48,
              child: Container(color: Colors.grey[300]),
            ),
            Skeleton.replace(
              width: 48,
              height: 48,
              child: Container(color: Colors.grey[300]),
            ),
          ],
        ),
      );
}
