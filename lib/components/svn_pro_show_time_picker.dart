part of '../svn_pro_mobile_flutter.dart';

Future<TimeOfDay?> svnProShowTimePicker(context, {label}) {
  Future<TimeOfDay?> selectedTime24Hour = showTimePicker(
    context: context,
    helpText: label,
    initialTime: TimeOfDay.now(),
    builder: (BuildContext context, Widget? child) {
      return MediaQuery(
        data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
        child: child!,
      );
    },
  );
  return selectedTime24Hour;
}
