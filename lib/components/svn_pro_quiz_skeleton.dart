part of '../svn_pro_mobile_flutter.dart';

class SvnProQuizSkeleton extends StatelessWidget {
  const SvnProQuizSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.only(bottom: kToolbarHeight + 16.0),
            children: _buildContents(context),
          ),

          // Fixed bottom navigation
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: _buildBottomNavigation(context),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildContents(BuildContext context) {
    return [
      _buildHeader(context),
      _buildQuestionList(context),
    ];
  }

  Widget _buildHeader(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(24.0),
        child: Row(
          children: [
            const Skeletonizer(
              child: SvnProIcon(
                iconData: MingCuteIcons.mgc_close_line,
              ),
            ),
            const SizedBox(width: 24.0),
            Expanded(
              child: Skeletonizer(
                child: SvnProProgress(
                  isIndeterminate: false,
                  color: Theme.of(context).colorScheme.primary,
                  style: SvnProProgressStyle.linear,
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildQuestionList(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 24.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Skeletonizer(
                  child: SvnProText(
                      text:
                          'Quel ingrédient ne fait pas partie de ceux pour faire un mojito ?',
                      weight: SvnProTextWeight.semiBold,
                      size: SvnProTextSize.headlineSmall,
                      color: Theme.of(context).colorScheme.onSurface)),
            ),
            const SizedBox(width: 24.0),
            const Column(
              children: [
                Skeletonizer(
                  child: SvnProCheckboxTile(
                    title:
                        'Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur',
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ),
                Skeletonizer(
                  child: SvnProCheckboxTile(
                    title:
                        'Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur',
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ),
                Skeletonizer(
                  child: SvnProCheckboxTile(
                    title:
                        'Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur',
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ),
                Skeletonizer(
                  child: SvnProCheckboxTile(
                    title:
                        'Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur',
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ),
              ],
            )
          ],
        ));
  }

  Widget _buildBottomNavigation(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Skeletonizer(
              child: SvnProIconButton(
                variant: SvnProIconButtonVariant.standard,
                icon: const SvnProIcon(
                  size: 48.0,
                  iconData: MingCuteIcons.mgc_close_line,
                ),
                onPressed: () {},
              ),
            ),
            Skeletonizer(
              child: SvnProIconButton(
                variant: SvnProIconButtonVariant.standard,
                icon: const SvnProIcon(
                  size: 48.0,
                  iconData: MingCuteIcons.mgc_close_line,
                ),
                onPressed: () {},
              ),
            ),
          ],
        ));
  }
}
