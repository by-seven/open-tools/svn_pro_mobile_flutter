part of '../svn_pro_mobile_flutter.dart';

class SvnProDialog extends StatelessWidget {
  final SvnProIcon? icon;
  final String title;
  final String? supportingText;
  final bool divider;
  final ListView? list;
  final String? textBtnSecondary;
  final String? textBtnPrimary;

  const SvnProDialog({
    super.key,
    this.icon,
    this.title = '',
    this.supportingText,
    this.divider = false,
    this.list,
    this.textBtnSecondary = '',
    this.textBtnPrimary = '',
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      icon: icon,
      title: SvnProText(
        text: title,
        size: SvnProTextSize.headlineSmall,
        weight: SvnProTextWeight.regular,
      ),
      content: Column(
        children: [
          if (supportingText != null)
            Row(
              children: [
                Flexible(
                  child: SvnProText(
                    text: supportingText,
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.regular,
                  ),
                ),
              ],
            ),
          if (divider)
            const Column(
              children: [
                SizedBox(
                  height: 24,
                ),
                SvnProDivider(),
              ],
            ),
          if (list != null)
            SizedBox(
              width: double.maxFinite,
              height: 220,
              child: list,
            ),
        ],
      ),
      actions: <Widget>[
        if (textBtnSecondary != null)
          SvnProBtn(
            variant: SvnProBtnVariants.text,
            text: textBtnSecondary,
            onPressed: () => Navigator.pop(context, textBtnSecondary),
          ),
        if (textBtnPrimary != null)
          SvnProBtn(
            variant: SvnProBtnVariants.text,
            text: textBtnPrimary,
            onPressed: () {
              HapticFeedback.lightImpact();
              Navigator.pop(context, textBtnPrimary);
            },
          ),
        // TextButton(
        //   style: TextButton.styleFrom(
        //     textStyle: Theme.of(context).textTheme.labelLarge,
        //   ),
        //   child: const Text('Enable'),
        //   onPressed: () {
        //     Navigator.of(context).pop();
        //   },
        // ),
      ],
    );
  }
}

Future<String?> svnProShowDialog(
  BuildContext context, {
  required WidgetBuilder builder,
}) async {
  return showDialog<String>(
    context: context,
    builder: builder,
  );
}
