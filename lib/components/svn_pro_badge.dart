part of '../svn_pro_mobile_flutter.dart';

enum SvnProBadgeVariant { point, digit }

class SvnProBadge extends StatelessWidget {
  final SvnProBadgeVariant variant;
  final int number;
  final Widget? child;
  final bool visible;

  const SvnProBadge(
      {super.key,
      this.variant = SvnProBadgeVariant.point,
      this.number = 0,
      this.visible = true,
      this.child});

  @override
  Widget build(BuildContext context) {
    return Badge(
      isLabelVisible: visible,
      label: variant == SvnProBadgeVariant.point
          ? null
          : (number > 999 ? const Text('999+') : Text(number.toString())),
      child: child,
    );
  }
}
