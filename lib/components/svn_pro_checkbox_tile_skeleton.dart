part of '../svn_pro_mobile_flutter.dart';

class SvnProCheckboxTileSkeleton extends StatelessWidget {
  const SvnProCheckboxTileSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return const Skeletonizer(
      enabled: true,
      child: SvnProCheckboxTile(),
    );
  }
}
