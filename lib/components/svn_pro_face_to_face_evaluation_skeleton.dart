part of '../svn_pro_mobile_flutter.dart';

class SvnProFaceToFaceEvaluationSkeleton extends StatelessWidget {
  const SvnProFaceToFaceEvaluationSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: ListView.separated(
          shrinkWrap: true,
          padding: const EdgeInsets.all(20.0),
          scrollDirection: Axis.vertical,
          physics: const NeverScrollableScrollPhysics(),
          separatorBuilder: (context, index) => const SvnProDivider(),
          itemCount: 15,
          itemBuilder: (context, index) => const Padding(
              padding: EdgeInsets.only(bottom: 16.0),
              child: SvnProQuestionEditItw(
                title: '_________________________',
                description: '_________________________',
                hasAnswerInput: true,
              ))),
    );
  }
}
