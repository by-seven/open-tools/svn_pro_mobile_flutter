part of '../svn_pro_mobile_flutter.dart';

class SvnProEmptyState extends StatelessWidget {
  final SvnProIcon icon;
  final String? title;
  final String paragraph;
  final SvnProBtn? primary;
  final SvnProBtn? secondary;

  const SvnProEmptyState(
      {super.key,
      required this.icon,
      this.title,
      required this.paragraph,
      this.primary,
      this.secondary});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          icon.copyWith(size: 32),
          const SizedBox(
            height: 8,
          ),
          if (title != null) ...[
            SvnProText(
              text: title!,
              size: SvnProTextSize.titleMl,
              weight: SvnProTextWeight.semiBold,
              color: Theme.of(context).colorScheme.onSurface,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 4,
            )
          ],
          SvnProText(
            textAlign: TextAlign.center,
            text: paragraph,
            size: SvnProTextSize.bodyMedium,
            weight: SvnProTextWeight.regular,
            color: Theme.of(context).colorScheme.onSurfaceVariant,
            maxLines: 4,
            overflow: TextOverflow.ellipsis,
          ),
          if (primary != null) ...[
            const SizedBox(
              height: 16,
            ),
            primary!,
          ],
          if (secondary != null) ...[
            const SizedBox(
              height: 12,
            ),
            secondary!,
          ],
        ],
      ),
    );
  }
}
