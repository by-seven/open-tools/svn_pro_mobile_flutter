part of '../svn_pro_mobile_flutter.dart';

class SvnProCommentBodySkeleton extends StatelessWidget {
  const SvnProCommentBodySkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: SvnProCommentBaseTile(
        avatar: const SvnProAvatar(
          size: SvnProAvatarSizes.px32,
          variant: SvnProAvatarVariant.anonym,
        ),
        title: 'toto',
        supportingDate: DateTime.now(),
        tileDescription:
            'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
      ),
    );
  }
}
