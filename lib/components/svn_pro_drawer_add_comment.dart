part of '../svn_pro_mobile_flutter.dart';

Future<void> svnProDrawerAddComment(
  BuildContext context, {
  TextEditingController? controller,
  required Function(String) onPressed,
}) async {
  final currentController = controller ?? TextEditingController();
  final isButtonEnabled = ValueNotifier<bool>(false);

  currentController.addListener(() {
    isButtonEnabled.value = currentController.text.trim().isNotEmpty;
  });

  await svnProShowBottomSheet<String>(
    context,
    mode: SvnProBottomSheetMode.persistent,
    showHandle: true,
    builder: (context) {
      final bottomInset = MediaQuery.of(context).viewInsets.bottom;
      return Padding(
        padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          bottom: bottomInset == 0.0 ? 32 : 16,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: SvnProTextField(
                controller: currentController,
                textArea: true,
                variant: SvnProTextFieldVariant.outlined,
                placeHolder: 'add_a_comment'.tr,
                maxLines: 4,
              ),
            ),
            ValueListenableBuilder<bool>(
              valueListenable: isButtonEnabled,
              builder: (context, isEnabled, child) {
                return SvnProIconButton(
                  variant: SvnProIconButtonVariant.standard,
                  icon: const SvnProIcon(
                    iconData: MingCuteIcons.mgc_send_line,
                  ),
                  enable: isEnabled,
                  onPressed: isEnabled
                      ? () => onPressed(currentController.text)
                      : null,
                );
              },
            ),
          ],
        ),
      );
    },
  );
}
