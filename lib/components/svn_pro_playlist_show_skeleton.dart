part of '../svn_pro_mobile_flutter.dart';

class SvnProPlaylistShowSkeleton extends StatelessWidget {
  final bool showCoverImage;

  const SvnProPlaylistShowSkeleton({super.key, this.showCoverImage = true});

  @override
  Widget build(BuildContext context) {
    return SvnProTopAppBarShow(
      onLeadingPressed: null,
      title: '',
      body: Skeletonizer(
        child: Container(
          width: double.maxFinite,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.surfaceVariant,
          ),
          child: ListView(
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            children: _buildContent(context),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildContent(BuildContext context) {
    return [
      if (showCoverImage) _buildCoverImage(context),
      _buildDescription(context),
      _buildContentList(context),
    ];
  }

  Widget _buildCoverImage(BuildContext context) {
    return const SizedBox(
      width: double.maxFinite,
      height: 160,
    );
  }

  Widget _buildDescription(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          _buildPlaylistDetails(context),
        ],
      ),
    );
  }

  Widget _buildPlaylistDetails(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildDurationFeature(),
        const SizedBox(height: 16),
        _buildThemeList(),
        const SizedBox(height: 16),
        _buildDescriptionText(),
      ],
    );
  }

  Widget _buildDurationFeature() {
    return const SvnProFeatureItem(
        icon: SvnProIcon(
          iconData: MingCuteIcons.mgc_time_line,
        ),
        color: SvnProFeatureItemColor.primary,
        featureText: 'Total duration: ',
        description: "50");
  }

  Widget _buildDescriptionText() {
    return const SvnProText(
      size: SvnProTextSize.bodyMedium,
      weight: SvnProTextWeight.regular,
      text:
          'Lorem ipsum dolor sit amet consectetur. Tincidunt eget dolor imperdiet malesuada condimentum. Convallis eLorem ipsum dolor sit amet consectetur. Tincidunt eget dolor imperdiet malesuada condimentum',
    );
  }

  Widget _buildThemeList() {
    List<SvnProText> themeWidgets = [];
    List<String> themes = [
      'deluxe',
      'advanced',
      'light',
    ];

    for (var theme in themes) {
      themeWidgets.add(SvnProText(
        text: theme,
        size: SvnProTextSize.headlineLarge,
      ));
    }

    return (Wrap(
      spacing: 8.0,
      runSpacing: 8.0,
      alignment: WrapAlignment.start,
      children: themeWidgets,
    ));
  }

  Widget _buildContentList(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          children: [
            const SvnProDivider(),
            const SizedBox(height: 16),
            Row(
              children: [
                SvnProText(
                  text: 'Content',
                  color: Theme.of(context).colorScheme.onSurface,
                  size: SvnProTextSize.titleMl,
                  weight: FontWeight.w500,
                )
              ],
            ),
            const SizedBox(height: 20),
            ListView.separated(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: const AlwaysScrollableScrollPhysics(),
                separatorBuilder: (context, index) =>
                    const SizedBox(height: 20),
                itemCount: 8,
                itemBuilder: (context, index) => const SvnProCardBaseSmallLearn(
                      title: 'Potter ipsum wand elf parchment wingardium.',
                      showModule: true,
                      duration: '50min',
                      currentProgression: null,
                      targetProgression: null,
                      acquired: false,
                    ))
          ],
        ));
  }
}
