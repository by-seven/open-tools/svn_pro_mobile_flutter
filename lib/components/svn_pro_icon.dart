part of '../svn_pro_mobile_flutter.dart';

class SvnProIcon extends StatelessWidget {
  final Icon? icon;
  final IconData? iconData;
  final Color? color;
  final double? size;
  final String? noto;
  final String? svgPath;
  final String? package;

  const SvnProIcon({
    super.key,
    this.icon,
    this.iconData,
    this.color,
    this.size,
    this.noto,
    this.svgPath,
    this.package,
  });

  @override
  Widget build(BuildContext context) {
    if (icon != null) {
      return icon!;
    } else if (noto != null) {
      return Iconify(
        noto!,
        color: color,
        size: size,
      );
    } else if (svgPath != null) {
      return SvgPicture.asset(
        svgPath!,
        width: size,
        height: size,
        color: color,
        package: package,
      );
    } else {
      return Icon(iconData, color: color, size: size);
    }
  }

  SvnProIcon copyWith({
    Icon? icon,
    IconData? iconData,
    Color? color,
    double? size,
    String? noto,
  }) {
    return SvnProIcon(
      icon: icon ?? this.icon,
      iconData: iconData ?? this.iconData,
      color: color ?? this.color,
      size: size ?? this.size,
      noto: noto ?? this.noto,
    );
  }
}
