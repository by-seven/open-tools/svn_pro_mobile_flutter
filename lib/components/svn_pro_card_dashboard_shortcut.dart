part of '../svn_pro_mobile_flutter.dart';

enum SvnProCardDashboardShortcutVariant {
  myRoadmap,
  myTeamRoadmap,
  myInterview,
  myTeamInterview,
  myTrainings,
  catalog,
}

class SvnProCardDashboardShortcut extends StatelessWidget {
  final SvnProCardDashboardShortcutVariant variant;
  final int? hasBadge;
  final VoidCallback? onTap;

  const SvnProCardDashboardShortcut({
    super.key,
    required this.variant,
    this.hasBadge,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    genText();
    return SvnProCard(
      onTap: onTap,
      variant: SvnProCardVariant.elevated,
      child: SizedBox(
        height: 88,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 16, 16, 16),
              child: Row(
                children: [
                  SvnProIcon(
                    noto: genNoto(),
                    size: 48,
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  SizedBox(
                    width: 180,
                    child: SvnProText(
                      size: SvnProTextSize.titleMl,
                      weight: SvnProTextWeight.medium,
                      text: genText(),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ),
            const Spacer(),
            if (hasBadge != null) ...[
              SvnProBadge(
                variant: SvnProBadgeVariant.digit,
                number: hasBadge!,
              ),
              const SizedBox(
                width: 8,
              )
            ],
            SvnProIcon(
              iconData: MingCuteIcons.mgc_right_line,
              color: Theme.of(context).colorScheme.outline,
            ),
            const SizedBox(
              width: 16,
            ),
          ],
        ),
      ),
    );
  }

  String genNoto() {
    switch (variant) {
      case SvnProCardDashboardShortcutVariant.myRoadmap:
        return Noto.bullseye;
      case SvnProCardDashboardShortcutVariant.myTeamRoadmap:
        return Noto.busts_in_silhouette;
      case SvnProCardDashboardShortcutVariant.myInterview:
        return Noto.clipboard;
      case SvnProCardDashboardShortcutVariant.myTeamInterview:
        return Noto.busts_in_silhouette;
      case SvnProCardDashboardShortcutVariant.myTrainings:
        return Noto.books;
      case SvnProCardDashboardShortcutVariant.catalog:
        return Noto.jigsaw;
    }
  }

  String genText() {
    switch (variant) {
      case SvnProCardDashboardShortcutVariant.myRoadmap:
        return 'my_roadmap'.tr;
      case SvnProCardDashboardShortcutVariant.myTeamRoadmap:
        return 'my_team_roadmaps'.tr;
      case SvnProCardDashboardShortcutVariant.myInterview:
        return 'my_interviews'.tr;
      case SvnProCardDashboardShortcutVariant.myTeamInterview:
        return 'my_team_interviews'.tr;
      case SvnProCardDashboardShortcutVariant.myTrainings:
        return 'my_trainings'.tr;
      case SvnProCardDashboardShortcutVariant.catalog:
        return 'catalog'.tr;
    }
  }
}
