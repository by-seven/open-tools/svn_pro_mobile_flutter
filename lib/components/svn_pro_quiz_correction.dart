part of '../svn_pro_mobile_flutter.dart';

enum SvnProQuizCorrectionVariant {
  wrong,
  partiallyCorrect,
  correct,
}

class SvnProQuizCorrection extends StatelessWidget {
  final String? text;
  final SvnProQuizCorrectionVariant variant;

  const SvnProQuizCorrection({
    super.key,
    this.text,
    this.variant = SvnProQuizCorrectionVariant.wrong,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 8, 32, 8),
      child: Row(
        children: [
          SvnProIcon(
            size: 24,
            iconData: genIcon(),
            color: genColor(context),
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: SvnProText(
              text: text,
              size: SvnProTextSize.bodyLarge,
              weight: SvnProTextWeight.regular,
              color: genColor(context),
            ),
          ),
        ],
      ),
    );
  }

  genColor(context) {
    switch (variant) {
      case SvnProQuizCorrectionVariant.wrong:
        return Theme.of(context).colorScheme.error;
      case SvnProQuizCorrectionVariant.partiallyCorrect:
        return Theme.of(context).extension<CustomColors>()!.warningCustom1;
      case SvnProQuizCorrectionVariant.correct:
        return Theme.of(context).extension<CustomColors>()!.successColor;
    }
  }

  genIcon() {
    switch (variant) {
      case SvnProQuizCorrectionVariant.wrong:
        return MingCuteIcons.mgc_close_circle_fill;
      case SvnProQuizCorrectionVariant.partiallyCorrect:
        return MingCuteIcons.mgc_alert_fill;
      case SvnProQuizCorrectionVariant.correct:
        return MingCuteIcons.mgc_check_circle_fill;
    }
  }
}
