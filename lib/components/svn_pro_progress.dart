part of '../svn_pro_mobile_flutter.dart';

enum SvnProProgressStyle {
  circular,
  linear,
}

class SvnProProgress extends StatefulWidget {
  final SvnProProgressStyle style;
  final bool isIndeterminate;
  final int step;
  final int totalStep;
  final Color? color;

  const SvnProProgress({
    super.key,
    this.style = SvnProProgressStyle.circular,
    this.isIndeterminate = true,
    this.step = 0,
    this.totalStep = 10,
    this.color,
  });

  @override
  State<SvnProProgress> createState() => _SvnProProgressState();
}

class _SvnProProgressState extends State<SvnProProgress>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _progressAnimation;

  double _previousValue = 0.0;

  @override
  void initState() {
    super.initState();

    // Initialize animation controller
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    // Initialize progress animation
    _progressAnimation = Tween<double>(
      begin: _previousValue,
      end: widget.step / widget.totalStep,
    ).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeInOut),
    );
  }

  @override
  void didUpdateWidget(covariant SvnProProgress oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.step != widget.step) {
      // Update the animation tween
      _previousValue =
          _progressAnimation.value; // Capture the current animation value
      _progressAnimation = Tween<double>(
        begin: _previousValue,
        end: widget.step / widget.totalStep,
      ).animate(
        CurvedAnimation(parent: _controller, curve: Curves.easeInOut),
      );

      // Start the animation
      _controller.forward(from: 0.0);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.style) {
      case SvnProProgressStyle.circular:
        if (widget.isIndeterminate) {
          return CircularProgressIndicator(color: widget.color);
        } else {
          return AnimatedBuilder(
            animation: _progressAnimation,
            builder: (context, child) {
              return CircularProgressIndicator(
                color: widget.color,
                value: _progressAnimation.value,
              );
            },
          );
        }
      case SvnProProgressStyle.linear:
        if (widget.isIndeterminate) {
          return SizedBox(
            height: 4,
            child: LinearProgressIndicator(color: widget.color),
          );
        } else {
          return SizedBox(
            height: 4,
            child: AnimatedBuilder(
              animation: _progressAnimation,
              builder: (context, child) {
                return LinearProgressIndicator(
                  color: widget.color,
                  value: _progressAnimation.value,
                );
              },
            ),
          );
        }
    }
  }
}
