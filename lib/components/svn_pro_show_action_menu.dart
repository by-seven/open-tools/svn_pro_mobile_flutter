part of '../svn_pro_mobile_flutter.dart';

class SvnProActionMenuItem {
  final SvnProIcon icon;
  final String label;
  final void Function() callback;

  const SvnProActionMenuItem(
    this.icon,
    this.label,
    this.callback,
  );
}

class SvnProActionMenuTools {
  static const double minScreenSize = 0.10;
  static const double maxScreenSize = 0.90;
  static const double halfScreenSize = 0.50;

  static double caluculateMaxHeight(
      bool mouvUp, double totalOptionSize, Size dimensionScreen) {
    double maxHeight = totalOptionSize;

    if (totalOptionSize >= dimensionScreen.height * halfScreenSize) {
      maxHeight = (dimensionScreen.height * halfScreenSize);
      if (mouvUp) maxHeight = (dimensionScreen.height * maxScreenSize);
    }
    return maxHeight;
  }

  static double calculateTotalOptionSize(
      List<GlobalKey<State<StatefulWidget>>> tileKeys) {
    double height = 0;

    for (var key in tileKeys) {
      final RenderBox renderBox =
          key.currentContext!.findRenderObject() as RenderBox;
      height += renderBox.size.height;
    }
    return height;
  }

  static double initFirstSize(List list, Size dimensionScreen, ) {
    return list.length < 6
      ? dimensionScreen.height * SvnProActionMenuTools.minScreenSize
      : dimensionScreen.height * SvnProActionMenuTools.halfScreenSize;
  }

  static bool closeWidget(
      List<GlobalKey<State<StatefulWidget>>> tileKeys, PointerDownEvent event) {
    final RenderBox firstElementBox =
        tileKeys[0].currentContext!.findRenderObject() as RenderBox;
    final Offset localPosition = firstElementBox.globalToLocal(event.position);

    if (localPosition.dx >= 0 &&
        localPosition.dy >= 0 &&
        localPosition.dx <= firstElementBox.size.width &&
        localPosition.dy <= firstElementBox.size.height) {
      return true;
    }
    return false;
  }
}

void svnProShowActionMenu(
  BuildContext context, {
  required List<SvnProActionMenuItem> list,
}) {
  HapticFeedback.mediumImpact();
  final dimensionScreen = MediaQuery.of(context).size;
  final List<GlobalKey> tileKeys = List.generate(list.length, (_) => GlobalKey());
  double totalOptionSize = SvnProActionMenuTools.initFirstSize(list, dimensionScreen);
  bool mouvUp = false;
  bool isFirstElementScrollDown = false;

  svnProShowBottomSheet(
    context,
    showHandle: false,
    isScrollControlled: true,
    mode: SvnProBottomSheetMode.modal,
    builder: (context) => StatefulBuilder(
      builder: (context, setState) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (tileKeys.every((key) => key.currentContext != null)) {
            double height =
                SvnProActionMenuTools.calculateTotalOptionSize(tileKeys);
            setState(() {
              totalOptionSize = height.clamp(0,
                  dimensionScreen.height * SvnProActionMenuTools.maxScreenSize);
            });
          }
        });

        return Listener(
          onPointerDown: (event) {
            if (tileKeys[0].currentContext !=
                null) if (SvnProActionMenuTools.closeWidget(tileKeys, event))
              setState(() {
                isFirstElementScrollDown = true;
              });
          },
          onPointerMove: (event) {
            if (event.delta.dy < 0) {
              setState(() {
                mouvUp = true;
              });
            } else if (event.delta.dy > 0 && isFirstElementScrollDown) {
              Navigator.of(context).pop();
            }
          },
          child: SafeArea(
            child: AnimatedContainer(
              duration: Duration(milliseconds: 250),
              constraints: BoxConstraints(
                maxHeight: SvnProActionMenuTools.caluculateMaxHeight(
                    mouvUp, totalOptionSize, dimensionScreen),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: SvnProList(
                  divider: const SvnProDivider(),
                  tiles: List.generate(
                    list.length,
                    (index) => SvnProListTile(
                      key: tileKeys[index],
                      leadingVariant: SvnProListTileSideVariant.icon,
                      leadingIcon: list[index].icon,
                      title: list[index].label,
                      onTap: list[index].callback,
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    ),
  );
}
