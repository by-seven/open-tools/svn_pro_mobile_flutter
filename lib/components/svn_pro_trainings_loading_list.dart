part of '../svn_pro_mobile_flutter.dart';

class SvnProTrainingsLoadingList extends StatelessWidget {
  final bool acquired;

  const SvnProTrainingsLoadingList({super.key, required this.acquired});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 8,
      itemBuilder: (context, index) {
        return SvnProCardTrainingLoader(acquired: acquired);
      },
      separatorBuilder: (BuildContext context, int index) {
        return const SizedBox(
          height: 16,
        );
      },
    );
  }
}
