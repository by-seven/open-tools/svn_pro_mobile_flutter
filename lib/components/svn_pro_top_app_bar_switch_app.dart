part of '../svn_pro_mobile_flutter.dart';

class SvnProTopAppBarSwitchApp extends StatelessWidget {
  final SevenApp sevenApp;
  final Widget? body;
  final VoidCallback? onSwitchApp;
  final SvnProAvatar avatar;
  final bool switchable;

  const SvnProTopAppBarSwitchApp({
    super.key,
    this.sevenApp = SevenApp.interview,
    this.body,
    this.onSwitchApp,
    required this.avatar,
    this.switchable = true,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProTopAppBar(
      centerTitle: false,
      body: body,
      title: genTitle(),
      leading: switchable
          ? SvnProIconButton(
              variant: SvnProIconButtonVariant.tonal,
              icon: SvnProIcon(
                iconData: genIconData(),
              ),
              onPressed: onSwitchApp,
            )
          : null,
      actions: [
        avatar.copyWith(
          size: SvnProAvatarSizes.px32,
        ),
        const SizedBox(
          width: 16,
        ),
      ],
    );
  }

  genTitle() {
    switch (sevenApp) {
      case SevenApp.interview:
        return 'Interview';
      case SevenApp.roadmap:
        return 'Roadmap';
      case SevenApp.learn:
        return 'Learn';
    }
  }

  genIconData() {
    switch (sevenApp) {
      case SevenApp.interview:
        return MingCuteIcons.mgc_chat_3_line;
      case SevenApp.roadmap:
        return MingCuteIcons.mgc_target_line;
      case SevenApp.learn:
        return MingCuteIcons.mgc_mortarboard_line;
    }
  }
}
