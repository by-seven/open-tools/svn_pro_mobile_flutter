part of '../svn_pro_mobile_flutter.dart';

class SvnProCardTrainingLoader extends StatelessWidget {
  final bool acquired;

  const SvnProCardTrainingLoader({super.key, required this.acquired});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
        enabled: true,
        child: SvnProCardBaseSmallLearn(
          title:
              'Training avec un titre très long sur trois lignes de texte remplies',
          duration: '45min',
          nbrModules: 5,
          imgList: const [],
          acquired: acquired,
          currentProgression: null,
          targetProgression: null,
        ));
  }
}
