part of '../svn_pro_mobile_flutter.dart';

ThemeData svnProThemeData(context, {bool light = true}) {
  if (light) {
    return ThemeData(
      useMaterial3: true,
      textTheme: GoogleFonts.workSansTextTheme(Theme.of(context).textTheme),
      colorScheme: sevenLightColorScheme,
      extensions: [sevenLightCustomColors.harmonized(sevenLightColorScheme)],
      visualDensity: VisualDensity.standard,
    );
  }

  return ThemeData(
    useMaterial3: true,
    textTheme: GoogleFonts.workSansTextTheme(Theme.of(context).textTheme),
    colorScheme: sevenDarkColorScheme,
    extensions: [sevenDarkCustomColors.harmonized(sevenDarkColorScheme)],
    visualDensity: VisualDensity.standard,
  );
}
