part of '../svn_pro_mobile_flutter.dart';

enum SvnProTabsVariant {
  primary,
  secondary,
}

enum SvnProTabsConfiguration {
  labelOnly,
  iconOnly,
  labelIcon,
}

class SvnProTabsHeadProps extends StatelessWidget {
  final IconData? iconData;
  final String? label;
  final SvnProTabsConfiguration conf;

  const SvnProTabsHeadProps({
    super.key,
    this.iconData,
    this.label,
    this.conf = SvnProTabsConfiguration.labelOnly,
  });

  @override
  Widget build(BuildContext context) {
    switch (conf) {
      case SvnProTabsConfiguration.iconOnly:
        return Tab(icon: SvnProIcon(iconData: iconData));
      case SvnProTabsConfiguration.labelIcon:
        return Tab(text: label, icon: SvnProIcon(iconData: iconData));
      case SvnProTabsConfiguration.labelOnly:
      default:
        return Tab(text: label);
    }
  }
}

class SvnProTabs extends StatefulWidget {
  final bool isScrollable;
  final SvnProTabsVariant variant;
  final List<Widget> headProps;
  final List<Widget> views;
  final ValueChanged<int>? onPageChanged;

  const SvnProTabs({
    super.key,
    this.isScrollable = false,
    this.variant = SvnProTabsVariant.primary,
    required this.headProps,
    required this.views,
    this.onPageChanged,
  }) : assert(headProps.length == views.length,
            'HeadProps and views must have the same length.');

  @override
  State<SvnProTabs> createState() => _SvnProTabsState();
}

class _SvnProTabsState extends State<SvnProTabs>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController =
        TabController(length: widget.headProps.length, vsync: this);

    _tabController.addListener(() {
      widget.onPageChanged?.call(_tabController.index);
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.headProps.length,
      child: Column(
        children: [
          _buildTabBar(),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: widget.views,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTabBar() {
    final tabBar = TabBar(
      controller: _tabController,
      tabs: widget.headProps,
      isScrollable: widget.isScrollable,
    );

    return widget.variant == SvnProTabsVariant.secondary
        ? TabBar.secondary(
            controller: _tabController,
            tabs: widget.headProps,
            isScrollable: widget.isScrollable,
          )
        : tabBar;
  }
}
