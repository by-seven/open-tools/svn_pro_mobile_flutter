part of '../svn_pro_mobile_flutter.dart';

enum SvnProFeatureItemColor {
  defaultColor,
  primary,
}

class SvnProFeatureItem extends StatelessWidget {
  final String? featureText;
  final String? description;
  final SvnProFeatureItemColor color;
  final SvnProIcon icon;

  const SvnProFeatureItem({
    super.key,
    this.featureText,
    this.description,
    this.color = SvnProFeatureItemColor.defaultColor,
    this.icon = const SvnProIcon(
      iconData: MingCuteIcons.mgc_user_3_line,
    ),
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        icon.copyWith(
          color: genColor(context),
          size: 20,
        ),
        const SizedBox(
          width: 8,
        ),
        SvnProText(
          text: '$featureText: ',
          size: SvnProTextSize.bodyMedium,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        SvnProText(
          text: description,
          size: SvnProTextSize.bodyMedium,
          weight: SvnProTextWeight.medium,
          color: genColor(context),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  Color genColor(context) {
    switch (color) {
      case SvnProFeatureItemColor.defaultColor:
        return Theme.of(context).colorScheme.onSurface;
      case SvnProFeatureItemColor.primary:
        return Theme.of(context).colorScheme.onPrimaryContainer;
    }
  }
}

class SvnProFeatureListItem extends StatelessWidget {
  final List<SvnProFeatureItem> list;

  const SvnProFeatureListItem({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 8,
        ),
        ...list.asMap().entries.map(
          (entry) {
            final idx = entry.key;
            final value = entry.value;

            return [
              value,
              if (idx != list.length - 1) const SizedBox(height: 8),
            ];
          },
        ).expand((i) => i),
        const SizedBox(
          height: 8,
        ),
      ],
    );
  }
}
