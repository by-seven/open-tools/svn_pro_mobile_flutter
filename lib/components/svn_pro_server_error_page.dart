part of '../svn_pro_mobile_flutter.dart';

class SvnProServerErrorPage extends StatelessWidget {
  const SvnProServerErrorPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        SizedBox(
          height: 24,
        ),
        SvnProIcon(
          noto: Noto.warning,
          size: 24,
        ),
        SizedBox(
          height: 24,
        ),
        Expanded(
          child: SvnProText(
            text: 'An error occurred on server while fetching data',
            size: SvnProTextSize.titleMedium,
          ),
        ),
      ],
    );
  }
}
