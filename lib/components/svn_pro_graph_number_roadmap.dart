part of '../svn_pro_mobile_flutter.dart';

class SvnProGraphNumberRoadmap extends StatelessWidget {
  final int start;
  final int current;
  final int target;
  final bool percentage;
  final Color color;

  const SvnProGraphNumberRoadmap({
    super.key,
    this.start = 0,
    this.current = 0,
    required this.target,
    required this.color,
    this.percentage = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 40, right: 40),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvnProText(
            text: percentage
                ? '${(((current - start) * 100) / (target - start)).toStringAsFixed(0)}%'
                : '$current/$target',
            size: SvnProTextSize.titleMl,
            weight: SvnProTextWeight.medium,
            color: color,
          ),
          const SizedBox(
            height: 8,
          ),
          TweenAnimationBuilder<int>(
            tween: IntTween(
              begin: start,
              end: current,
            ),
            duration: const Duration(milliseconds: 300),
            builder: (context, animatedValue, child) {
              return SvnProProgress(
                style: SvnProProgressStyle.linear,
                isIndeterminate: false,
                step: percentage
                    ? (((animatedValue - start) * 100) / (target - start))
                        .floor()
                    : animatedValue - start,
                totalStep: percentage ? 100 : target - start,
                color: color,
              );
            },
          ),
        ],
      ),
    );
  }
}
