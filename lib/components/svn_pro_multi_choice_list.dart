part of '../svn_pro_mobile_flutter.dart';

class SvnProMultiChoiceItem extends StatelessWidget {
  final String? initialValue;
  final bool validated;
  final FormFieldValidator<String>? validator;
  final ValueChanged<bool?>? onSelectionChanged;
  final ValueChanged<String?>? onTextChanged;
  final VoidCallback? onDelete;
  final bool readOnly;
  final String? label;

  const SvnProMultiChoiceItem({
    super.key,
    this.initialValue,
    this.validated = false,
    this.validator,
    this.onSelectionChanged,
    this.onDelete,
    this.onTextChanged,
    this.readOnly = false,
    this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SvnProCheckbox(
          state: readOnly
              ? SvnProCheckboxState.disabled
              : SvnProCheckboxState.enabled,
          status: !validated
              ? SvnProCheckboxStatus.unselected
              : SvnProCheckboxStatus.selected,
          onChanged: onSelectionChanged,
        ),
        const SizedBox(
          width: 8,
        ),
        Expanded(
          child: SvnProFormFieldTextField(
            readOnly: readOnly,
            initialValue: initialValue,
            // controller: TextEditingController(text: initialValue),
            validator: validator,
            onChanged: onTextChanged,
            label: label,
          ),
        ),
        const SizedBox(
          width: 8,
        ),
        SvnProIconButton(
          enable: !readOnly,
          variant: SvnProIconButtonVariant.standard,
          icon: const SvnProIcon(
            iconData: MingCuteIcons.mgc_close_line,
          ),
          onPressed: onDelete,
        )
      ],
    );
  }
}

String? svnProMultiChoiceDataDefaultValidator(dynamic str) {
  if (str == null || str == '') {
    return "Can't be blank";
  }
  return null;
}

class SvnProMultiChoiceData {
  String? label;
  String? value;
  bool validated;
  FormFieldValidator<String>? validator;

  SvnProMultiChoiceData({
    this.value,
    this.validated = false,
    this.validator = svnProMultiChoiceDataDefaultValidator,
    this.label,
  });
}

class SvnProMultiChoiceList extends StatefulWidget {
  final List<SvnProMultiChoiceData>? initialValue;
  final ValueChanged<List<SvnProMultiChoiceData>>? onChanged;
  final SvnProQuestionEditItwStatus status;
  final String? errorText;
  final bool readOnly;

  const SvnProMultiChoiceList({
    super.key,
    this.initialValue = const <SvnProMultiChoiceData>[],
    this.onChanged,
    this.status = SvnProQuestionEditItwStatus.enabled,
    this.errorText,
    this.readOnly = false,
  });

  @override
  State<SvnProMultiChoiceList> createState() => _SvnProMultiChoiceListState();
}

class _SvnProMultiChoiceListState extends State<SvnProMultiChoiceList> {
  late List<SvnProMultiChoiceData> choiceList = widget.initialValue ?? [];
  late List<Key> childrenKeys;

  @override
  void initState() {
    super.initState();

    setState(() {
      childrenKeys = choiceList.map((data) => UniqueKey()).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListView.separated(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          separatorBuilder: (context, index) => const SizedBox(
            height: 16,
          ),
          itemCount: choiceList.length,
          itemBuilder: (context, index) => SvnProMultiChoiceItem(
            key: childrenKeys[index],
            initialValue: choiceList[index].value,
            validated: choiceList[index].validated,
            validator: choiceList[index].validator,
            label: choiceList[index].label,
            readOnly: widget.readOnly,
            onTextChanged: (String? newVal) {
              setState(() {
                choiceList[index].value = newVal;
                widget.onChanged?.call(choiceList);
              });
            },
            onSelectionChanged: (bool? newVal) {
              setState(() {
                if (newVal != null) {
                  choiceList[index].validated = newVal;
                  widget.onChanged?.call(choiceList);
                }
              });
            },
            onDelete: () {
              setState(() {
                choiceList.removeAt(index);
                widget.onChanged?.call(choiceList);
                for (int idx = 0; idx < childrenKeys.length; ++idx) {
                  childrenKeys[idx] = UniqueKey();
                }
              });
            },
          ),
        ),
        if (widget.status == SvnProQuestionEditItwStatus.error &&
            widget.errorText != null &&
            widget.errorText != '')
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SvnProText(
                weight: SvnProTextWeight.semiBold,
                text: widget.errorText,
                color: Theme.of(context).colorScheme.error,
              ),
            ],
          ),
        const SizedBox(
          height: 20,
        ),
        if (widget.readOnly == false)
          Row(
            children: [
              Expanded(
                child: SvnProBtn(
                  variant: SvnProBtnVariants.outlined,
                  text: 'add_option'.tr,
                  iconData: MingCuteIcons.mgc_add_line,
                  onPressed: () {
                    setState(() {
                      childrenKeys.add(UniqueKey());
                      choiceList.add(SvnProMultiChoiceData(
                        label: 'Option ${choiceList.length + 1}',
                      ));
                      widget.onChanged?.call(choiceList);
                    });
                  },
                ),
              ),
            ],
          ),
      ],
    );
  }
}
