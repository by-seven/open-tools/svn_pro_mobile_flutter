part of '../svn_pro_mobile_flutter.dart';

class SvnProCommentBaseTile extends StatelessWidget {
  final SvnProAvatar? avatar;
  final String? title;
  final DateTime? supportingDate;
  final SvnProIcon? titleIcon;
  final SvnProIconButton? tileIconBtnToggleable;
  final SvnProInfoTag? tileInfoTag;
  final int? selectedStart;
  final int? maxStart;
  final String? tileDescription;
  final List<SvnProInfoTag>? tileSubInfoTagList;
  final SvnProBtn? tileAction;
  final int? tileDivider;

  const SvnProCommentBaseTile({
    super.key,
    required this.avatar,
    required this.title,
    this.supportingDate,
    this.titleIcon,
    this.tileIconBtnToggleable,
    this.tileInfoTag,
    this.selectedStart,
    this.maxStart,
    this.tileDescription,
    this.tileSubInfoTagList,
    this.tileAction,
    this.tileDivider,
  }) : assert(
            (selectedStart == null && maxStart == null) ||
                (selectedStart != null && maxStart != null),
            'if you set selectedStart then maxStart must be set');

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildHeader(context),
        if (tileInfoTag != null) _buildInfoTag(),
        if (selectedStart != null && maxStart != null)
          _buildStarRating(context),
        if (tileDescription != null) _buildDescription(context),
        if (tileSubInfoTagList != null) _buildSubInfoTags(),
        if (tileAction != null) _buildAction(),
        if (tileDivider != null) _buildDivider(context),
      ],
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (avatar != null) avatar!.copyWith(size: SvnProAvatarSizes.px32),
          const SizedBox(width: 8),
          Expanded(child: _buildTitleAndDate(context)),
          if (tileIconBtnToggleable != null) tileIconBtnToggleable!,
        ],
      ),
    );
  }

  Widget _buildTitleAndDate(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            if (titleIcon != null)
              titleIcon!.copyWith(
                size: 16,
                color: Theme.of(context).colorScheme.onSurface,
              ),
            if (titleIcon != null) const SizedBox(width: 4),
            if (title != null)
              SvnProText(
                text: title,
                size: SvnProTextSize.bodyLarge,
                weight: SvnProTextWeight.semiBold,
                color: Theme.of(context).colorScheme.onSurface,
              ),
          ],
        ),
        if (supportingDate != null)
          SvnProText(
            text: DateFormat('E d MMMM, H:m').format(supportingDate!),
            size: SvnProTextSize.bodySmall,
            weight: SvnProTextWeight.regular,
            color: Theme.of(context).colorScheme.onSurfaceVariant,
          ),
      ],
    );
  }

  Widget _buildInfoTag() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          tileInfoTag!.copyWith(
            variant: SvnProInfoTagVariant.outlined,
            type: SvnProInfoTagType.info,
          ),
        ],
      ),
    );
  }

  Widget _buildStarRating(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: List.generate(
          maxStart!,
          (index) => Padding(
            padding: const EdgeInsets.only(right: 8),
            child: SvnProIcon(
              iconData: index < selectedStart!
                  ? MingCuteIcons.mgc_star_fill
                  : MingCuteIcons.mgc_star_line,
              size: 16,
              color: Theme.of(context).colorScheme.onSurface,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDescription(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          Flexible(
            child: SvnProText(
              text: tileDescription,
              size: SvnProTextSize.bodyMedium,
              weight: SvnProTextWeight.regular,
              color: Theme.of(context).colorScheme.onSurface,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSubInfoTags() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Align(
          alignment: Alignment.topLeft,
          child: Wrap(
            spacing: 8,
            runSpacing: 8,
            children: tileSubInfoTagList!.map((tag) {
              return tag.copyWith(
                variant: SvnProInfoTagVariant.filled,
                type: SvnProInfoTagType.info,
                leading: SvnProInfoTagLeading.none,
              );
            }).toList(),
          )),
    );
  }

  Widget _buildAction() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: tileAction!.copyWith(variant: SvnProBtnVariants.text),
      ),
    );
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: SvnProDivider(
        variant: SvnProDividerVariant.middleInsetSubHead,
        title: tileDivider! == 0
            ? 'no_comments_yet'.tr
            : 'comments'.trArgs([tileDivider.toString()]),
      ),
    );
  }
}
