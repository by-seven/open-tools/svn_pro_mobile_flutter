part of '../svn_pro_mobile_flutter.dart';

// mixin SvnFontMixin {
//   int weight = 400;
//   bool underlined = false;
// }
class SvnProTextWeight {
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight semiBold = FontWeight.w600;
  static const FontWeight bold = FontWeight.w700;
}

enum SvnProTextSize {
  labelLarge(size: 14, lineHeight: 20 / 14),
  labelMedium(size: 12, lineHeight: 16 / 12),
  labelSmall(size: 10, lineHeight: 16 / 10),
  bodyLarge(size: 16, lineHeight: 24 / 16),
  bodyMedium(size: 14, lineHeight: 20 / 14),
  bodySmall(size: 12, lineHeight: 16 / 12),
  titleLarge(size: 22, lineHeight: 28 / 22),
  titleMl(size: 20, lineHeight: 26 / 20),
  titleMedium(size: 16, lineHeight: 22 / 16),
  titleSmall(size: 14, lineHeight: 20 / 14),
  headlineLarge(size: 32, lineHeight: 40 / 32),
  headlineMedium(size: 28, lineHeight: 36 / 28),
  headlineSmall(size: 24, lineHeight: 32 / 24),
  displayLarge(size: 57, lineHeight: 64 / 57),
  displayMedium(size: 45, lineHeight: 52 / 45),
  displaySmall(size: 36, lineHeight: 44 / 36);

  final double size;
  final double lineHeight;

  const SvnProTextSize({required this.size, required this.lineHeight});
}

class SvnProText extends StatelessWidget {
  final String? text;
  final TextAlign textAlign;
  final Color? color;
  final SvnProTextSize size;
  final FontWeight weight;
  final bool underlined;
  final TextOverflow? overflow;
  final int? maxLines;
  final bool? softWrap;

  const SvnProText({
    super.key,
    this.text = '',
    this.textAlign = TextAlign.left,
    this.color,
    this.size = SvnProTextSize.labelSmall,
    this.weight = SvnProTextWeight.regular,
    this.underlined = false,
    this.overflow,
    this.maxLines,
    this.softWrap,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text ?? '',
      style: GoogleFonts.workSans(
        // textStyle: Theme.of(context).textTheme.labelSmall,
        color: color ?? Theme.of(context).colorScheme.onSurface,
        fontWeight: weight,
        fontSize: size.size,
        height: size.lineHeight,
        decoration: underlined ? TextDecoration.underline : TextDecoration.none,
        decorationThickness: 1,
        letterSpacing: 0,
      ),
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,
      softWrap: softWrap,
    );
  }
}

// class SvnProTextSize extends Equatable {
//   const SvnProTextSize({required this.size, required this.lineHeight});
//
//   final double size;
//   final double lineHeight;
//
//   @override
//   List<Object?> get props => [size, lineHeight];
// }
//
// class SvnProGenericText extends StatelessWidget {
//   const SvnProGenericText(
//       {super.key,
//       this.text = '',
//       this.textAlign = TextAlign.left,
//       this.color = null,
//       this.size,
//       this.weight = SvnProTextWeight.regular,
//       this.underlined = false});
//
//   final String text;
//   final TextAlign textAlign;
//   final Color? color;
//   final SvnProTextSize? size;
//   final FontWeight weight;
//   final bool underlined;
//
//   @override
//   Widget build(BuildContext context) {
//     return Text(
//       text,
//       style: TextStyle(
//           color: color,
//           fontWeight: weight,
//           fontSize: size?.size,
//           height: size?.lineHeight,
//           decoration:
//               underlined ? TextDecoration.underline : TextDecoration.none),
//       textAlign: textAlign,
//     );
//   }
// }
//
// class SvnProTextLabelSize {
//   static const SvnProTextSize large =
//       SvnProTextSize(size: 14, lineHeight: 20 / 14);
//   static const SvnProTextSize medium =
//       SvnProTextSize(size: 12, lineHeight: 16 / 12);
//   static const SvnProTextSize small =
//       SvnProTextSize(size: 10, lineHeight: 16 / 10);
// }
//
// class SvnProLabelTypo extends SvnProGenericText {
//   const SvnProText(
//       {super.key,
//       super.text,
//       super.textAlign,
//       super.color,
//       super.size = SvnProTextSize.labelSmall,
//       super.weight,
//       super.underlined});
//
//   @override
//   Widget build(BuildContext context) {
//     switch (size) {
//       case SvnProTextSize.labelSmall:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.labelSmall,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.labelMedium:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.labelMedium,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextLabelSize.large:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.labelLarge,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       default:
//         return super.build(context);
//     }
//   }
// }
//
// class SvnProBodyTypoFontProp {
//   static const SvnProTextSize large =
//       SvnProTextSize(size: 16, lineHeight: 24 / 16);
//   static const SvnProTextSize medium =
//       SvnProTextSize(size: 14, lineHeight: 20 / 14);
//   static const SvnProTextSize small =
//       SvnProTextSize(size: 12, lineHeight: 16 / 12);
// }
//
// class SvnProBodyTypo extends SvnProGenericText {
//   const SvnProText(
//       {super.key,
//       super.text,
//       super.textAlign,
//       super.color,
//       super.size = SvnProTextSize.bodySmall,
//       super.weight,
//       super.underlined});
//
//   @override
//   Widget build(BuildContext context) {
//     switch (size) {
//       case SvnProTextSize.bodySmall:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.titleSmall,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.bodyMedium:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.titleMedium,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.bodyLarge:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.titleLarge,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       default:
//         return super.build(context);
//     }
//   }
// }
//
// class SvnProTitleTypoFontProp {
//   static const SvnProTextSize large =
//       SvnProTextSize(size: 22, lineHeight: 28 / 22);
//   static const SvnProTextSize ml =
//       SvnProTextSize(size: 20, lineHeight: 26 / 20);
//   static const SvnProTextSize medium =
//       SvnProTextSize(size: 16, lineHeight: 22 / 16);
//   static const SvnProTextSize small =
//       SvnProTextSize(size: 14, lineHeight: 20 / 14);
// }
//
// class SvnProTitleTypo extends SvnProGenericText {
//   constSvnProText(
//       {super.key,
//       super.text,
//       super.textAlign,
//       super.color,
//       super.size = SvnProTextSize.titleSmall,
//       super.weight,
//       super.underlined});
//
//   @override
//   Widget build(BuildContext context) {
//     switch (size) {
//       case SvnProTextSize.titleSmall:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.titleSmall,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.titleMedium:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.titleMedium,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.titleMl:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             // textStyle: Theme.of(context).textTheme.titleMedium,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.titleLarge:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.titleLarge,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       default:
//         return super.build(context);
//     }
//   }
// }
//
// class SvnProHeadlineTypoFontProp {
//   static const SvnProTextSize large =
//       SvnProTextSize(size: 32, lineHeight: 40 / 32);
//   static const SvnProTextSize medium =
//       SvnProTextSize(size: 28, lineHeight: 36 / 28);
//   static const SvnProTextSize small =
//       SvnProTextSize(size: 24, lineHeight: 32 / 24);
// }
//
// class SvnProHeadlineTypo extends SvnProGenericText {
//   const SvnProText(
//       {super.key,
//       super.text,
//       super.textAlign,
//       super.color,
//       super.size = SvnProTextSize.headlineSmall,
//       super.weight,
//       super.underlined});
//
//   @override
//   Widget build(BuildContext context) {
//     switch (size) {
//       case SvnProTextSize.headlineSmall:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.headlineSmall,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.headlineMedium:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.headlineMedium,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProTextSize.headlineLarge:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.headlineLarge,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       default:
//         return super.build(context);
//     }
//   }
// }
//
// class SvnProDisplayTypoFontProp {
//   static const SvnProTextSize large =
//       SvnProTextSize(size: 57, lineHeight: 64 / 57);
//   static const SvnProTextSize medium =
//       SvnProTextSize(size: 45, lineHeight: 52 / 45);
//   static const SvnProTextSize small =
//       SvnProTextSize(size: 36, lineHeight: 44 / 36);
// }
//
// class SvnProDisplayTypo extends SvnProGenericText {
//   const SvnProDisplayTypo(
//       {super.key,
//       super.text,
//       super.textAlign,
//       super.color,
//       super.size = SvnProDisplayTypoFontProp.small,
//       super.weight,
//       super.underlined});
//
//   @override
//   Widget build(BuildContext context) {
//     switch (size) {
//       case SvnProDisplayTypoFontProp.small:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.displaySmall,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProDisplayTypoFontProp.medium:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.displayMedium,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       case SvnProDisplayTypoFontProp.large:
//         return Text(
//           text,
//           style: GoogleFonts.workSans(
//             textStyle: Theme.of(context).textTheme.displayLarge,
//             fontSize: size?.size,
//             height: size?.lineHeight,
//             fontWeight: weight,
//             color: color,
//             decoration:
//                 underlined ? TextDecoration.underline : TextDecoration.none,
//             decorationThickness: 1,
//           ),
//           textAlign: textAlign,
//         );
//       default:
//         return super.build(context);
//     }
//   }
// }
