part of '../svn_pro_mobile_flutter.dart';

class SvnProMenu extends StatelessWidget {
  final MenuAnchorChildBuilder? activatorBuilder;
  final List<SvnProMenuTile> tiles;

  const SvnProMenu({
    super.key,
    this.activatorBuilder,
    required this.tiles,
  });

  @override
  Widget build(BuildContext context) {
    return MenuAnchor(
      builder: activatorBuilder,
      menuChildren: tiles,
    );
  }

  static void handleController(controller) {
    if (controller.isOpen) {
      controller.close();
    } else {
      controller.open();
    }
  }
}
