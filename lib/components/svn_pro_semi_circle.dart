part of '../svn_pro_mobile_flutter.dart';

class SemiCirclePainter extends CustomPainter {
  final Color color;

  const SemiCirclePainter({super.repaint, required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    double radius = size.width / 2;
    Offset center = Offset(size.width / 2, size.height);

    // Draw semicircle
    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      pi,
      pi,
      true,
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class SvnProSemiCircle extends StatelessWidget {
  final Color color;
  final double size;

  const SvnProSemiCircle({super.key, required this.color, this.size = 24});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size,
      height: size,
      child: Transform.rotate(
        angle: pi / 2,
        child: CustomPaint(
          painter: SemiCirclePainter(color: color),
        ),
      ),
    );
  }
}
