part of '../svn_pro_mobile_flutter.dart';

class SvnProSelectModal extends StatelessWidget {
  final String? label;
  final TextEditingController? controller;
  final WidgetBuilder builder;

  const SvnProSelectModal({
    super.key,
    this.label,
    this.controller,
    required this.builder,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProTextField(
      variant: SvnProTextFieldVariant.outlined,
      suffixIcon: const SvnProIcon(
        iconData: MingCuteIcons.mgc_down_small_fill,
      ),
      label: label,
      controller: controller,
      onTap: () {
        svnProShowModal(
          context,
          builder: builder,
        );
      },
    );
  }
}
