part of '../svn_pro_mobile_flutter.dart';

enum SvnProShowModalVariant {
  topAction,
  bottomActionStacked,
  bottomActionHorizontal,
  noAction,
}

class SvnProModal extends StatelessWidget {
  final String title;
  final Widget? child;
  final SvnProShowModalVariant variant;
  final SvnProBtn? primaryBtn;
  final SvnProBtn? secondaryBtn;
  final void Function()? onPressed;
  final bool topBarElevation;

  const SvnProModal({
    super.key,
    this.title = '',
    this.child,
    this.variant = SvnProShowModalVariant.topAction,
    this.primaryBtn,
    this.secondaryBtn,
    this.onPressed,
    this.topBarElevation = false,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog.fullscreen(
      child: SvnProTopAppBar(
        title: title,
        body: child,
        elevation: topBarElevation,
        leading: SvnProIconButton(
          variant: SvnProIconButtonVariant.standard,
          icon: const SvnProIcon(
            iconData: MingCuteIcons.mgc_close_line,
          ),
          onPressed: onPressed ?? () => Navigator.of(context).pop('close'.tr),
        ),
        actions: variant == SvnProShowModalVariant.noAction
            ? null
            : (variant == SvnProShowModalVariant.topAction
                ? [
                    if (primaryBtn != null) primaryBtn!,
                  ]
                : null),
        bottomSticky: variant == SvnProShowModalVariant.noAction
            ? null
            : (variant == SvnProShowModalVariant.bottomActionStacked
                ? SvnProButtonStickyContainer(
                    variant: SvnProButtonStickyContainerVariant.stacked,
                    primaryBtn: primaryBtn!,
                    secondaryBtn: secondaryBtn,
                  )
                : (variant == SvnProShowModalVariant.bottomActionHorizontal
                    ? SvnProButtonStickyContainer(
                        variant: SvnProButtonStickyContainerVariant.horizontal,
                        primaryBtn: primaryBtn!,
                        secondaryBtn: secondaryBtn,
                      )
                    : null)),
      ),
    );
  }
}

Future<T?> svnProShowModal<T>(
  BuildContext context, {
  required WidgetBuilder builder,
}) async {
  return showDialog<T?>(
    context: context,
    builder: builder,
    barrierColor: Theme.of(context).colorScheme.surface,
  );
}
