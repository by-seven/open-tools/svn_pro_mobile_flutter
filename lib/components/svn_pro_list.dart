part of '../svn_pro_mobile_flutter.dart';

@Deprecated('Use ListView or ListView.separated instead. '
    "This widget will be removed, it's deprecated .")
class SvnProList extends StatelessWidget {
  final List<SvnProListTile> tiles;
  final SvnProDivider? divider;

  const SvnProList({
    super.key,
    required this.tiles,
    this.divider,
  });

  @override
  Widget build(BuildContext context) {
    if (divider == null) {
      return ListView(
        children: tiles,
      );
    } else {
      return ListView.separated(
        separatorBuilder: (context, idx) => divider!,
        itemBuilder: (context, idx) => tiles[idx],
        itemCount: tiles.length,
      );
    }
  }

// SvnProList copyWithEachTile({
//   List<SvnProListTile>? tiles,
//   bool? divider,
//   void Function(SvnProListTile)? onSelect,
// }) {
//   return SvnProList(
//     tiles: tiles ?? this.tiles,
//     divider: divider ?? this.divider,
//     onSelect: (onSelect as void Function(SvnProListTile)),
//   );
// }
}
