part of '../svn_pro_mobile_flutter.dart';

class SvnProCampaignPackLoadingList extends StatelessWidget {
  const SvnProCampaignPackLoadingList({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 5,
      itemBuilder: (context, index) {
        return const SvnProCardItwLoader();
      },
      separatorBuilder: (BuildContext context, int index) {
        return const SizedBox(
          height: 12,
        );
      },
    );
  }
}
