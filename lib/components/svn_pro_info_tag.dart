part of '../svn_pro_mobile_flutter.dart';

enum SvnProInfoTagType {
  success,
  primary,
  warning,
  error,
  info,
}

enum SvnProInfoTagVariant {
  filled,
  outlined,
  text,
}

enum SvnProInfoTagLeading {
  none,
  point,
  icon,
  emojiNoto,
}

class SvnProInfoTag extends StatelessWidget {
  final SvnProInfoTagType type;
  final SvnProInfoTagVariant variant;
  final bool compact;
  final SvnProInfoTagLeading leading;
  final SvnProIcon? icon;
  final String? label;

  const SvnProInfoTag({
    super.key,
    this.type = SvnProInfoTagType.success,
    this.variant = SvnProInfoTagVariant.filled,
    this.compact = false,
    this.leading = SvnProInfoTagLeading.point,
    this.icon,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: variant == SvnProInfoTagVariant.outlined
            ? Border.all(color: genForgroungColor(context))
            : null,
        borderRadius: const BorderRadius.all(
          Radius.circular(8),
        ),
        color: variant == SvnProInfoTagVariant.filled
            ? genBackgroundColor(context)
            : Colors.transparent,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: variant == SvnProInfoTagVariant.text ? 0 : 8,
          vertical: !compact ? 6 : 4,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (leading != SvnProInfoTagLeading.none) ...[
              getLeading(context), // code
              SizedBox(
                width: !compact ? 8 : 6,
              )
            ],
            ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 150),
              child: SvnProText(
                text: label?.toTitleCase(),
                size: !compact
                    ? SvnProTextSize.titleSmall
                    : SvnProTextSize.bodySmall,
                weight: SvnProTextWeight.medium,
                overflow: TextOverflow.ellipsis,
                color: genForgroungColorText(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  genBackgroundColor(context) {
    switch (type) {
      case SvnProInfoTagType.success:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.successColorContainer!;
      case SvnProInfoTagType.primary:
        return Theme.of(context).colorScheme.primaryContainer;
      case SvnProInfoTagType.warning:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.warningColorContainer!;
      case SvnProInfoTagType.error:
        return Theme.of(context).colorScheme.errorContainer;
      case SvnProInfoTagType.info:
        return Theme.of(context).colorScheme.surfaceContainerHigh;
    }
  }

  getLeading(context) {
    switch (leading) {
      case SvnProInfoTagLeading.point:
        return SvnProCircle(
          color: genForgroungColor(context),
          size: !compact ? 8 : 6,
        );
      case SvnProInfoTagLeading.icon:
        return icon!.copyWith(
            size: !compact ? 18 : 16, color: genForgroungColor2(context));
      case SvnProInfoTagLeading.emojiNoto:
        return icon!.copyWith(size: !compact ? 18 : 16);
      case SvnProInfoTagLeading.none:
        return const SizedBox();
    }
  }

  genForgroungColor(context) {
    switch (type) {
      case SvnProInfoTagType.success:
        return Theme.of(context).extension<CustomColors>()?.successColor!;
      case SvnProInfoTagType.primary:
        return Theme.of(context).colorScheme.primary;
      case SvnProInfoTagType.warning:
        return Theme.of(context).extension<CustomColors>()?.warningColor!;
      case SvnProInfoTagType.error:
        return Theme.of(context).colorScheme.error;
      case SvnProInfoTagType.info:
        if (variant == SvnProInfoTagVariant.outlined) {
          return Theme.of(context).colorScheme.onSurface;
        } else {
          return Theme.of(context).colorScheme.outline;
        }
    }
  }

  genForgroungColor2(context) {
    switch (type) {
      case SvnProInfoTagType.success:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context)
              .extension<CustomColors>()
              ?.successOnColorContainer!;
        } else {
          return Theme.of(context).extension<CustomColors>()?.successColor!;
        }
      case SvnProInfoTagType.primary:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context).colorScheme.primary;
        } else {
          return Theme.of(context).colorScheme.primary;
        }
      case SvnProInfoTagType.warning:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context)
              .extension<CustomColors>()
              ?.warningOnColorContainer!;
        } else {
          return Theme.of(context).extension<CustomColors>()?.warningColor!;
        }
      case SvnProInfoTagType.error:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context).colorScheme.onErrorContainer;
        } else {
          return Theme.of(context).colorScheme.error;
        }
      case SvnProInfoTagType.info:
        return Theme.of(context).colorScheme.onSurface;
    }
  }

  genForgroungColorText(BuildContext context) {
    switch (type) {
      case SvnProInfoTagType.success:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context)
              .extension<CustomColors>()
              ?.successOnColorContainer!;
        } else {
          return Theme.of(context).extension<CustomColors>()?.successColor!;
        }
      case SvnProInfoTagType.primary:
        if (variant == SvnProInfoTagVariant.text) {
          return Theme.of(context).colorScheme.primary;
        } else {
          return Theme.of(context).colorScheme.onPrimaryContainer;
        }
      case SvnProInfoTagType.warning:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context)
              .extension<CustomColors>()
              ?.warningOnColorContainer!;
        } else {
          return Theme.of(context).extension<CustomColors>()?.warningColor!;
        }
      case SvnProInfoTagType.error:
        if (variant == SvnProInfoTagVariant.filled) {
          return Theme.of(context).colorScheme.onErrorContainer;
        } else {
          return Theme.of(context).colorScheme.error;
        }
      case SvnProInfoTagType.info:
        return Theme.of(context).colorScheme.onSurface;
    }
  }

  SvnProInfoTag copyWith({
    SvnProInfoTagType? type,
    SvnProInfoTagVariant? variant,
    bool? compact,
    SvnProInfoTagLeading? leading,
    SvnProIcon? icon,
    String? label,
  }) {
    return SvnProInfoTag(
      type: type ?? this.type,
      variant: variant ?? this.variant,
      compact: compact ?? this.compact,
      leading: leading ?? this.leading,
      icon: icon ?? this.icon,
      label: label ?? this.label,
    );
  }
}
