part of '../svn_pro_mobile_flutter.dart';

class CirclePainter extends CustomPainter {
  final Color color;

  const CirclePainter({super.repaint, required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    double radius = size.width / 2;
    Offset center = Offset(size.width / 2, size.height / 2);

    // Draw semicircle
    canvas.drawCircle(
      center,
      radius,
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class SvnProCircle extends StatelessWidget {
  final Color color;
  final double size;

  const SvnProCircle({super.key, required this.color, this.size = 24});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size,
      height: size,
      child: CustomPaint(
        painter: CirclePainter(color: color),
      ),
    );
  }
}
