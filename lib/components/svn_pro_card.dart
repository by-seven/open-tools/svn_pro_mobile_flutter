part of '../svn_pro_mobile_flutter.dart';

enum SvnProCardVariant { outlined, elevated, filled }

class SvnProCard extends StatelessWidget {
  final void Function()? onTap;
  final Widget? child;
  final SvnProCardVariant variant;

  const SvnProCard(
      {super.key,
      this.variant = SvnProCardVariant.outlined,
      this.onTap,
      this.child});

  @override
  Widget build(BuildContext context) {
    switch (variant) {
      case SvnProCardVariant.outlined:
        return Card.outlined(
          margin: const EdgeInsets.all(0),
          // clipBehavior is necessary because, without it, the InkWell's animation
          // will extend beyond the rounded edges of the [Card] (see https://github.com/flutter/flutter/issues/109776)
          // This comes with a small performance cost, and you should not set [clipBehavior]
          // unless you need it.
          clipBehavior: Clip.hardEdge,
          child: InkWell(
            splashColor: Theme.of(context).colorScheme.surface,
            onTap: onTap,
            child: child,
          ),
        );
      case SvnProCardVariant.elevated:
        return Card(
          margin: const EdgeInsets.all(0),
          // clipBehavior is necessary because, without it, the InkWell's animation
          // will extend beyond the rounded edges of the [Card] (see https://github.com/flutter/flutter/issues/109776)
          // This comes with a small performance cost, and you should not set [clipBehavior]
          // unless you need it.
          clipBehavior: Clip.hardEdge,
          child: InkWell(
            splashColor: Theme.of(context).colorScheme.surface,
            onTap: onTap,
            child: child,
          ),
        );
      case SvnProCardVariant.filled:
        return Card.filled(
          margin: const EdgeInsets.all(0),
          // clipBehavior is necessary because, without it, the InkWell's animation
          // will extend beyond the rounded edges of the [Card] (see https://github.com/flutter/flutter/issues/109776)
          // This comes with a small performance cost, and you should not set [clipBehavior]
          // unless you need it.
          clipBehavior: Clip.hardEdge,
          child: InkWell(
            splashColor: Theme.of(context).colorScheme.surface,
            onTap: onTap,
            child: child,
          ),
        );
    }
  }
}
