part of '../svn_pro_mobile_flutter.dart';

enum SvnProAvatarVariant { photo, monogram, anonym, check, loading }

enum SvnProAvatarSizes {
  px88(44),
  px64(32),
  px40(20),
  px32(16),
  px24(12),
  px20(10),
  px16(8);

  final double value;

  const SvnProAvatarSizes(this.value);
}

class SvnProAvatar extends StatelessWidget {
  final SvnProAvatarVariant variant;
  final SvnProAvatarSizes size;
  final String? url;
  final String? char;
  final VoidCallback? onTap;

  const SvnProAvatar({
    super.key,
    this.variant = SvnProAvatarVariant.photo,
    this.size = SvnProAvatarSizes.px32,
    this.url,
    this.char,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    late Widget avatar;

    switch (variant) {
      case SvnProAvatarVariant.loading:
        avatar = ClipRRect(
          borderRadius: BorderRadius.circular(10000),
          child: SizedBox(
            width: size.value * 2,
            height: size.value * 2,
            child: const SvnProProgress(),
          ),
        );
      case SvnProAvatarVariant.photo:
        if (url != null) {
          avatar = ClipRRect(
            borderRadius: BorderRadius.circular(10000),
            child: SvnProImage(
              fit: BoxFit.cover,
              imageUrl: url!,
              width: size.value * 2,
              height: size.value * 2,
              placeholder: (context, url) => SvnProAvatar(
                variant: SvnProAvatarVariant.loading,
                size: size,
              ),
              errorWidget: (context, url, error) => SvnProAvatar(
                variant: SvnProAvatarVariant.monogram,
                size: size,
                char: char ?? '-',
              ),
            ),
          );
        } else {
          avatar = SvnProAvatar(
            variant: SvnProAvatarVariant.monogram,
            size: size,
            char: char ?? '-',
          );
        }
      case SvnProAvatarVariant.monogram:
        if (char!.length != 1) {
          throw 'Svn avatar must contain just 1 char';
        }

        avatar = CircleAvatar(
          backgroundColor: calculateColor(context, char!.toUpperCase()),
          radius: size.value,
          child: calculateText(context, char!.toUpperCase()),
        );
      case SvnProAvatarVariant.anonym:
        avatar = CircleAvatar(
          backgroundColor: Theme.of(context).colorScheme.primaryContainer,
          radius: size.value,
          child: Icon(
            MingCuteIcons.mgc_user_3_fill,
            color: Theme.of(context).colorScheme.primary,
            size: size.value,
          ),
        );
      case SvnProAvatarVariant.check:
        avatar = CircleAvatar(
          backgroundColor: Theme.of(context).colorScheme.primaryContainer,
          radius: size.value,
          child: Icon(
            MingCuteIcons.mgc_check_line,
            color: Theme.of(context).colorScheme.primary,
            size: size.value,
          ),
        );
    }

    return GestureDetector(
      onTap: onTap,
      child: avatar,
    );
  }
  Widget calculateText(context, String upperCase) {
    switch (size) {
      case SvnProAvatarSizes.px88:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.headlineLarge,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
      case SvnProAvatarSizes.px64:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.headlineSmall,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
      case SvnProAvatarSizes.px40:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.titleMedium,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
      case SvnProAvatarSizes.px32:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.titleSmall,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
      case SvnProAvatarSizes.px24:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.bodySmall,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
      case SvnProAvatarSizes.px20:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.labelMedium,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
      case SvnProAvatarSizes.px16:
        return SvnProText(
          text: upperCase,
          size: SvnProTextSize.labelSmall,
          weight: SvnProTextWeight.medium,
          color: Theme.of(context).extension<CustomColors>()?.blackColor!,
        );
    }
  }

  Color? calculateColor(context, String upperCase) {
    switch (upperCase.codeUnitAt(0)) {
      case 65:
      case 66:
      case 67:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground1!;
      case 68:
      case 69:
      case 70:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground2!;
      case 71:
      case 72:
      case 73:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground3!;
      case 74:
      case 75:
      case 76:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground4!;
      case 77:
      case 78:
      case 79:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground5!;
      case 80:
      case 81:
      case 82:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground6!;
      case 83:
      case 84:
      case 85:
      case 86:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground7!;
      default:
        return Theme.of(context)
            .extension<CustomColors>()
            ?.avatarMonogramBackground8!;
    }
  }

  Widget copyWith({
    SvnProAvatarVariant? variant,
    SvnProAvatarSizes? size,
    String? url,
    String? char,
    VoidCallback? onTap,
  }) {
    return SvnProAvatar(
      variant: variant ?? this.variant,
      size: size ?? this.size,
      url: url ?? this.url,
      char: char ?? this.char,
      onTap: onTap ?? this.onTap,
    );
  }
}
