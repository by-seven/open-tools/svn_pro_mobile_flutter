part of '../svn_pro_mobile_flutter.dart';

/// A generic widget for creating editable questions with different input types.
///
/// This widget supports various types of questions such as text input, numeric input,
/// star rating, and radio button selection. It allows customization of title,
/// description, and additional metadata like deadlines or tags.
///
/// ### Generic Type Parameter
/// - `T`: The type of the input value. It must be either `String` or `int`
///   to ensure compatibility with the supported input types.
///
/// ### Parameters
///
/// #### Required Parameters:
/// - `title`: (String) The title of the question. Displayed prominently at the top.
///
/// #### Optional Parameters:
/// - `description`: (String?) A detailed description or instruction for the question.
/// - `content`: (Widget?) Custom content to include below the question text.
/// - `type`: (GlobalQuestionType) Defines the type of question, such as text input
///   or star rating. Defaults to `GlobalQuestionType.text`.
/// - `maxStars`: (int) Maximum number of stars for star-based questions. Default is 5.
/// - `comment`: (String?) An optional pre-filled comment associated with the question.
/// - `onCommentChanged`: (ValueChanged<String>?) Callback triggered when the comment changes.
/// - `openCanBeCommented`: (void Function()?) Action to open a comment editor or modal.
/// - `radioList`: (List<String>?) List of options for radio button questions.
/// - `hasAnswerInput`: (bool) Determines whether the question includes an answer input field.
///   Defaults to `true`.
/// - `questionInfoTag`: (SvnProInfoTag?) Additional metadata for the question, such as status tags.
/// - `deadline`: (DateTime?) A deadline or due date associated with the question.
/// - `status`: (SvnProQuestionEditItwStatus) Defines the question's current state,
///   such as enabled or disabled. Defaults to `SvnProQuestionEditItwStatus.enabled`.
/// - `initialValue`: (T?) Initial value for the question's input field.
/// - `onChanged`: (ValueChanged<T?>?) Callback triggered when the answer changes.
///
/// ### Usage Example
///
/// ```dart
/// SvnProQuestionEditItw<String>(
///   title: 'What is your favorite color?',
///   description: 'Choose a color from the options below.',
///   type: GlobalQuestionType.text,
///   initialValue: 'Blue',
///   onChanged: (value) => print('Selected color: $value'),
/// );
/// ```
///
/// ### Assertions
/// - Ensures `T` is either `String` or `int` to prevent invalid input types.
///
/// ### Notes for Developers
/// - This widget is designed for extensibility. Additional question types can be added
///   by extending `GlobalQuestionType` and updating the input rendering logic in the
///   associated state class (`_SvnProQuestionItwState`).
class SvnProQuestionEditItw<T> extends StatefulWidget {
  /// The title of the question, displayed at the top.
  final String? title;

  /// A detailed description or instructions for the question.
  final String? description;

  /// Custom content to display below the question (e.g., a custom widget).
  final Widget? content;

  /// Specifies the type of question (text, numeric, etc.).
  final GlobalQuestionType type;

  /// Maximum number of stars for star-based questions.
  final int maxStars;

  /// Optional pre-filled comment for the question.
  final String? comment;

  /// Callback triggered when the comment changes.
  final ValueChanged<String>? onCommentChanged;

  /// Action to open a comment editor or modal.
  final void Function()? openCanBeCommented;

  /// List of options for radio button questions.
  final List<String>? radioList;

  /// Determines whether the question includes an answer input field.
  final bool hasAnswerInput;

  /// Metadata tag for the question (e.g., status, progress).
  final SvnProInfoTag? questionInfoTag;

  /// Optional deadline or due date for the question.
  final DateTime? deadline;

  /// Current state of the question (enabled, disabled, etc.).
  final SvnProQuestionEditItwStatus status;

  /// Initial value for the question's input field.
  final T? initialValue;

  /// Callback triggered when the answer changes.
  final ValueChanged<T?>? onChanged;

  /// Ensures `T` is either `String` or `int` for valid input types.
  const SvnProQuestionEditItw({
    super.key,
    required this.title,
    this.description,
    this.content,
    this.type = GlobalQuestionType.text,
    this.maxStars = 5,
    this.comment,
    this.onCommentChanged,
    this.openCanBeCommented,
    this.radioList,
    this.hasAnswerInput = true,
    this.questionInfoTag,
    this.deadline,
    this.status = SvnProQuestionEditItwStatus.enabled,
    this.initialValue,
    this.onChanged,
  });

  @override
  State<SvnProQuestionEditItw> createState() => _SvnProQuestionItwState();
}

class _SvnProQuestionItwState extends State<SvnProQuestionEditItw> {
  String? openAnswer;
  String? numberAnswer;
  int idxStarSelected = 0;
  String? currentlyRadioSelected;

  @override
  void initState() {
    super.initState();

    if (widget.initialValue != null) {
      if (widget.type == GlobalQuestionType.text) {
        setState(() {
          openAnswer = widget.initialValue as String;
        });
      } else if (widget.type == GlobalQuestionType.number) {
        setState(() {
          numberAnswer = (widget.initialValue as int?).toString();
        });
      } else if (widget.type == GlobalQuestionType.star) {
        setState(() {
          idxStarSelected = widget.initialValue;
        });
      } else if (widget.type == GlobalQuestionType.radio) {
        setState(() {
          currentlyRadioSelected = widget.initialValue;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SvnProCard(
      variant: SvnProCardVariant.outlined,
      child: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Flexible(
                  child: SvnProText(
                    text: widget.title,
                    size: SvnProTextSize.titleMedium,
                    weight: SvnProTextWeight.semiBold,
                    color: widget.status == SvnProQuestionEditItwStatus.enabled
                        ? Theme.of(context).colorScheme.onSurface
                        : Theme.of(context).colorScheme.error,
                  ),
                )
              ],
            ),
            if (widget.questionInfoTag != null || widget.deadline != null) ...[
              const SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  if (widget.questionInfoTag != null)
                    widget.questionInfoTag!.copyWith(
                      variant: SvnProInfoTagVariant.filled,
                      // type: SvnProInfoTagType.success,
                      leading: SvnProInfoTagLeading.none,
                      compact: true,
                    ),
                  const Spacer(),
                  if (widget.deadline != null)
                    SvnProInfoTag(
                      variant: SvnProInfoTagVariant.text,
                      type: SvnProInfoTagType.info,
                      leading: SvnProInfoTagLeading.icon,
                      icon: const SvnProIcon(
                        iconData: MingCuteIcons.mgc_calendar_month_line,
                      ),
                      label: DateFormat('d MMM').format(widget.deadline!),
                      compact: true,
                    )
                ],
              )
            ],
            if (widget.description != null &&
                widget.description?.trim() != null &&
                widget.description != '<p><br></p><p><br></p>') ...[
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Flexible(
                    child: SvnProQuestionShowItw(
                      desc: widget.description,
                    ),
                  ),
                ],
              ),
            ],
            if (widget.content != null) ...[
              const SizedBox(
                height: 24,
              ),
              widget.content!,
            ],
            if (widget.openCanBeCommented != null) ...[
              const SizedBox(
                height: 24,
              ),
              Row(
                children: [
                  SvnProBtn(
                    variant: SvnProBtnVariants.text,
                    text: 'Comments'.tr,
                    onPressed: widget.openCanBeCommented,
                  )
                ],
              ),
            ],
            if (widget.hasAnswerInput) ...genQuestionType(),
            if (widget.comment != null) ...[
              const SizedBox(
                height: 16,
              ),
              SvnProTextField(
                // controller: widget.commentController,
                initialValue: widget.comment,
                onChanged: widget.onCommentChanged,
                placeHolder: 'Comment'.tr,
                textArea: true,
                minLines: 8,
                maxLines: 8,
              ),
            ]
          ],
        ),
      ),
    );
  }

  List<Widget> genQuestionType() {
    switch (widget.type) {
      case GlobalQuestionType.text:
        return [
          const SizedBox(
            height: 16,
          ),
          SvnProTextField(
            initialValue: openAnswer,
            placeHolder: 'answer__1'.tr,
            label: 'answer__1'.tr,
            textArea: true,
            minLines: 8,
            maxLines: 8,
            onChanged: widget.onChanged as ValueChanged<String>?,
          ),
        ];
      case GlobalQuestionType.number:
        return [
          const SizedBox(
            height: 16,
          ),
          SvnProTextField(
            initialValue: numberAnswer,
            placeHolder: 'answer__1'.tr,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              if (widget.onChanged != null) {
                widget.onChanged!(int.tryParse(value));
              }
            },
          ),
        ];
      case GlobalQuestionType.star:
        return [
          const SizedBox(
            height: 24,
          ),
          Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: 48,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.maxStars,
                    itemBuilder: (context, index) => SvnProIconButton(
                      variant: SvnProIconButtonVariant.standard,
                      type: SvnProIconButtonType.toggleable,
                      isSelected:
                          idxStarSelected > 0 && (index + 1) <= idxStarSelected,
                      icon: const SvnProIcon(
                        iconData: MingCuteIcons.mgc_star_line,
                      ),
                      selectedIcon: const SvnProIcon(
                        iconData: MingCuteIcons.mgc_star_fill,
                      ),
                      onPressed: () {
                        setState(() {
                          idxStarSelected = index + 1;
                        });
                        if (widget.onChanged != null) {
                          widget.onChanged!(index + 1);
                        }
                      },
                    ),
                  ),
                ),
              )
            ],
          )
        ];
      case GlobalQuestionType.radio:
        return [
          const SizedBox(
            height: 24,
          ),
          ...(widget.radioList?.map(
                (str) => SvnProRadioListTile(
                  label: str,
                  value: str,
                  currentlySelected: currentlyRadioSelected,
                  onChanged: (newSelected) {
                    setState(() {
                      currentlyRadioSelected = newSelected;

                      if (widget.onChanged != null) {
                        widget.onChanged!(newSelected!);
                      }
                    });
                  },
                ),
              ) ??
              []),
        ];
    }
  }
}
