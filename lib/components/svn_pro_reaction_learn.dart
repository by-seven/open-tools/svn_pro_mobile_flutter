part of '../svn_pro_mobile_flutter.dart';

class SvnProReactionLearn extends StatelessWidget {
  final String label;
  final SvnProCheckboxStatus value;
  final String noto;
  final void Function(bool?) onSelected;

  const SvnProReactionLearn({
    super.key,
    required this.label,
    this.value = SvnProCheckboxStatus.unselected,
    required this.noto,
    required this.onSelected,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProCard(
      onTap: () {
        if (value == SvnProCheckboxStatus.unselected) {
          onSelected(true);
        } else if (value == SvnProCheckboxStatus.selected) {
          onSelected(false);
        }
      },
      child: SizedBox(
        height: 64,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 8, 16, 8),
          child: Row(
            children: [
              SvnProCheckbox(
                onChanged: onSelected,
                status: value,
              ),
              const SizedBox(width: 8),
              SvnProIcon(noto: noto, size: 32),
              const SizedBox(width: 16),
              SvnProText(
                  text: label,
                  size: SvnProTextSize.titleMedium,
                  weight: SvnProTextWeight.semiBold,
                  color: value == SvnProCheckboxStatus.selected
                      ? Theme.of(context).colorScheme.primary
                      : Theme.of(context).colorScheme.onSurfaceVariant),
            ],
          ),
        ),
      ),
    );
  }
}
