part of '../svn_pro_mobile_flutter.dart';

class SvnProTextFieldSkeleton extends StatelessWidget {
  const SvnProTextFieldSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return const Skeletonizer(
      enabled: true,
      child: SvnProTextField(),
    );
  }
}
