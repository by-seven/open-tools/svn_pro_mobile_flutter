part of '../svn_pro_mobile_flutter.dart';

class SvnProCardBaseSmallLearn extends StatelessWidget {
  final bool showModule;
  final String title;
  final List<String> imgList;
  final String? duration;
  final bool acquired;
  final int? nbrModules;
  final int? currentProgression;
  final int? targetProgression;
  final VoidCallback? onTap;

  const SvnProCardBaseSmallLearn({
    super.key,
    this.showModule = false,
    this.imgList = const [],
    required this.title,
    this.duration,
    this.acquired = false,
    this.nbrModules,
    this.currentProgression,
    this.targetProgression,
    this.onTap,
  }) : assert(
            currentProgression == null && targetProgression == null ||
                currentProgression != null && targetProgression != null,
            'either progression var should be null or both should be setted');

  @override
  Widget build(BuildContext context) {
    return SvnProCard(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.all(
                Radius.circular(8),
              ),
              child: genPhotoGrid(context),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (showModule)
                    Flexible(
                      child: SvnProText(
                        text: 'Module',
                        color: Theme.of(context).colorScheme.primary,
                        size: SvnProTextSize.bodySmall,
                        weight: SvnProTextWeight.medium,
                      ),
                    ),
                  Row(
                    children: [
                      Flexible(
                        child: SvnProText(
                          text: title,
                          color: Theme.of(context).colorScheme.onSurface,
                          size: SvnProTextSize.titleMedium,
                          weight: SvnProTextWeight.semiBold,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      if (duration != null) ...[
                        SvnProIcon(
                          iconData: MingCuteIcons.mgc_time_line,
                          size: 16,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        SvnProText(
                          text: duration,
                          size: SvnProTextSize.bodyMedium,
                          weight: SvnProTextWeight.regular,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                      ],
                      if (duration != null && nbrModules != null)
                        SvnProText(
                          text: '· ',
                          size: SvnProTextSize.bodyMedium,
                          weight: SvnProTextWeight.regular,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                      if (nbrModules != null)
                        SvnProText(
                          text: 'module'.trPlural(
                              'modules', nbrModules, [nbrModules.toString()]),
                          size: SvnProTextSize.bodyMedium,
                          weight: SvnProTextWeight.regular,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                      const Spacer(),
                      if (acquired)
                        SvnProIcon(
                          color: Theme.of(context)
                              .extension<CustomColors>()!
                              .successColor,
                          iconData: MingCuteIcons.mgc_check_circle_line,
                          size: 24,
                        ),
                    ],
                  ),
                  if (currentProgression != null) ...[
                    const SizedBox(
                      height: 8,
                    ),
                    SvnProProgress(
                      style: SvnProProgressStyle.linear,
                      isIndeterminate: false,
                      step: currentProgression!,
                      totalStep: targetProgression!,
                    ),
                  ],
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  genPhotoGrid(context) {
    if (imgList.length >= 4) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SvnProImage(
                width: 32,
                height: 32,
                imageUrl: imgList[0],
              ),
              SvnProImage(
                width: 32,
                height: 32,
                imageUrl: imgList[1],
              ),
            ],
          ),
          Row(
            children: [
              SvnProImage(
                width: 32,
                height: 32,
                imageUrl: imgList[2],
              ),
              SvnProImage(
                width: 32,
                height: 32,
                imageUrl: imgList[3],
              ),
            ],
          ),
        ],
      );
    } else if (imgList.length == 3) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SvnProImage(
                width: 32,
                height: 32,
                fit: BoxFit.cover,
                imageUrl: imgList[0],
              ),
              SvnProImage(
                width: 32,
                height: 32,
                fit: BoxFit.cover,
                imageUrl: imgList[1],
              ),
            ],
          ),
          SvnProImage(
            width: 64,
            height: 32,
            fit: BoxFit.cover,
            imageUrl: imgList[2],
          ),
        ],
      );
    } else if (imgList.length == 2) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvnProImage(
            width: 64,
            height: 32,
            fit: BoxFit.cover,
            imageUrl: imgList[0],
          ),
          SvnProImage(
            width: 64,
            height: 32,
            fit: BoxFit.cover,
            imageUrl: imgList[1],
          ),
        ],
      );
    } else if (imgList.length == 1) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvnProImage(
            width: 64,
            height: 64,
            fit: BoxFit.cover,
            imageUrl: imgList[0],
          ),
        ],
      );
    } else {
      return Container(
        width: 64,
        height: 64,
        color: Theme.of(context).colorScheme.surfaceContainerHighest,
        child: SvnProIcon(
          iconData: MingCuteIcons.mgc_folder_2_line,
          size: 40,
          color: Theme.of(context).colorScheme.onSurfaceVariant,
        ),
      );
    }
  }
}
