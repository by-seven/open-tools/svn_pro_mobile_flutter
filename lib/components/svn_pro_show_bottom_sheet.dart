part of '../svn_pro_mobile_flutter.dart';

enum SvnProBottomSheetMode { persistent, modal }

Future<T?> svnProShowBottomSheet<T>(
  BuildContext context, {
  SvnProBottomSheetMode mode = SvnProBottomSheetMode.modal,
  bool showHandle = true,
  bool isScrollControlled = false,
  required WidgetBuilder builder,
}) async {
  switch (mode) {
    case SvnProBottomSheetMode.modal:
      return await showModalBottomSheet<T>(
        context: context,
        showDragHandle: showHandle,
        builder: builder,
        isScrollControlled: isScrollControlled,
      );

    case SvnProBottomSheetMode.persistent:
      final controller = showBottomSheet(
        context: context,
        showDragHandle: showHandle,
        builder: builder,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Theme.of(context).colorScheme.outlineVariant),
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(25.0),
          ),
        ),
      );

      await controller.closed;
      return null;
  }
}
