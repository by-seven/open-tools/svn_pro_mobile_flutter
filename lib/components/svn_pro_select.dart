part of '../svn_pro_mobile_flutter.dart';

class SvnProSelectItem<T> {
  final String label;
  final T? value;
  final SvnProIcon? icon;

  const SvnProSelectItem({required this.label, this.icon, this.value});
}

class SvnProSelect<T> extends StatefulWidget {
  final String label;
  final List<SvnProSelectItem> items;
  final void Function(T?)? onSelected;
  final T? initialSelection;
  final bool? readOnly;

  const SvnProSelect({
    super.key,
    this.label = '',
    required this.items,
    this.onSelected,
    this.initialSelection,
    this.readOnly = false,
  });

  @override
  State<SvnProSelect> createState() => _SvnProSelectState<T>();
}

class _SvnProSelectState<T> extends State<SvnProSelect> {
  final TextEditingController controller = TextEditingController();
  SvnProSelectItem? selectedItem;

  @override
  void initState() {
    super.initState();

    setState(() {
      if (widget.initialSelection != null) {
        selectedItem = widget.items
            .firstWhere((item) => item.value == widget.initialSelection);
      } else {
        selectedItem = widget.items.first;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return DropdownMenu<T>(
      enabled: !(widget.readOnly ?? false),
      leadingIcon: selectedItem?.icon,
      expandedInsets: EdgeInsets.zero,
      controller: controller,
      enableFilter: false,
      requestFocusOnTap: false,
      label: Text(widget.label),
      initialSelection: widget.initialSelection,
      onSelected: (val) {
        // setState(() {
        //   selectedItem = val;
        // });
        widget.onSelected?.call(val);
      },
      dropdownMenuEntries: widget.items.map<DropdownMenuEntry<T>>(
        (SvnProSelectItem item) {
          return DropdownMenuEntry<T>(
            label: item.label,
            value: item.value ?? item.label,
            leadingIcon: item.icon,
          );
        },
      ).toList(),
    );
  }
}
