part of '../svn_pro_mobile_flutter.dart';

class SvnProCardSkeleton extends StatelessWidget {
  const SvnProCardSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return const Skeletonizer(
      enabled: true,
      child: Row(
        children: [
          Expanded(
            child: SvnProCard(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: SvnProText(
                    text: 'hello', size: SvnProTextSize.headlineLarge),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
