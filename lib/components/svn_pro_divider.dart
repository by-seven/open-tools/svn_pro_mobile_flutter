part of '../svn_pro_mobile_flutter.dart';

enum SvnProDividerVariant {
  full,
  inset,
  middleInset,
  middleInsetSubHead,
}

enum SvnProDividerPadding {
  px0(0),
  px4(5),
  px8(9),
  px16(17),
  px24(25),
  px40(41),
  px48(49),
  px64(65);

  final double value;

  const SvnProDividerPadding(this.value);
}

class SvnProDivider extends StatelessWidget {
  final bool vertical;
  final String title;
  final SvnProDividerPadding padding;
  final SvnProDividerVariant variant;

  const SvnProDivider(
      {super.key,
      this.variant = SvnProDividerVariant.full,
      this.vertical = false,
      this.title = '',
      this.padding = SvnProDividerPadding.px0});

  @override
  Widget build(BuildContext context) {
    if (vertical) {
      return VerticalDivider(
        width: padding.value,
        thickness: 0,
        indent: variant == SvnProDividerVariant.full ? 0 : 16,
        endIndent: variant == SvnProDividerVariant.middleInset ? 16 : 0,
      );
    } else {
      switch (variant) {
        case SvnProDividerVariant.full:
        case SvnProDividerVariant.inset:
        case SvnProDividerVariant.middleInset:
          return Divider(
            height: padding.value,
            thickness: 0,
            indent: variant == SvnProDividerVariant.full ? 0 : 16,
            endIndent: variant == SvnProDividerVariant.middleInset ? 16 : 0,
          );
        case SvnProDividerVariant.middleInsetSubHead:
          return Column(
            children: [
              Divider(
                height: padding.value,
                thickness: 0,
                indent: 16,
                endIndent: 16,
              ),
              // Subheader example from Material spec.
              // https://material.io/components/dividers#types
              Container(
                padding: const EdgeInsets.only(left: 16),
                child: Align(
                  alignment: AlignmentDirectional.centerStart,
                  child: SvnProText(
                    text: title,
                    size: SvnProTextSize.titleSmall,
                    weight: SvnProTextWeight.medium,
                  ),
                ),
              ),
            ],
          );
      }
    }
  }
}
