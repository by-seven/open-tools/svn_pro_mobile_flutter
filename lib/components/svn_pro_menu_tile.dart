part of '../svn_pro_mobile_flutter.dart';

enum SvnProMenuTileLeadingVariant {
  none,
  icon,
  indent,
  checkbox,
}

enum SvnProMenuTileTrailingVariant {
  none,
  icon,
  text,
}

class SvnProMenuTile extends StatelessWidget {
  final String title;
  final String? supportingText;
  final SvnProMenuTileLeadingVariant leadingVariant;
  final IconData? leadingIcon;
  final SvnProCheckbox? leadingCheckbox;
  final SvnProMenuTileTrailingVariant trailingVariant;
  final IconData? trailingIconData;
  final String? trailingText;
  final void Function()? onPressed;
  final VisualDensity? density;

  const SvnProMenuTile({
    super.key,
    required this.title,
    this.supportingText,
    this.leadingVariant = SvnProMenuTileLeadingVariant.none,
    this.leadingIcon,
    this.leadingCheckbox,
    this.trailingVariant = SvnProMenuTileTrailingVariant.none,
    this.trailingIconData,
    this.trailingText,
    this.onPressed,
    this.density,
  });

  @override
  Widget build(BuildContext context) {
    return MenuItemButton(
      style: MenuItemButton.styleFrom(
        visualDensity: VisualDensity.standard,
      ),
      leadingIcon: calculateLeading(),
      trailingIcon: calculateTrailing(),
      onPressed: onPressed ?? () {},
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvnProText(
            text: title,
            size: SvnProTextSize.bodyLarge,
            weight: SvnProTextWeight.regular,
          ),
          supportingText != null
              ? SvnProText(
                  text: supportingText!,
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.medium,
                )
              : const SizedBox()
        ],
      ),
    );
  }

  calculateTrailing() {
    switch (trailingVariant) {
      case SvnProMenuTileTrailingVariant.none:
        return null;
      case SvnProMenuTileTrailingVariant.icon:
        return SvnProIcon(
          iconData: trailingIconData,
        );
      case SvnProMenuTileTrailingVariant.text:
        return SvnProText(text: trailingText!);
    }
  }

  calculateLeading() {
    switch (leadingVariant) {
      case SvnProMenuTileLeadingVariant.none:
        return null;
      case SvnProMenuTileLeadingVariant.icon:
        return SvnProIcon(
          iconData: trailingIconData,
        );
      case SvnProMenuTileLeadingVariant.indent:
        return const SizedBox(
          width: 12,
        );
      case SvnProMenuTileLeadingVariant.checkbox:
        return leadingCheckbox;
    }
  }
}
