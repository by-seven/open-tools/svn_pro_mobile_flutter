part of '../svn_pro_mobile_flutter.dart';

/// Defines a set of custom colors, each comprised of 4 complementary tones.
///
/// See also:
///   * <https://m3.material.io/styles/color/the-color-system/custom-colors>
@immutable
class CustomColors extends ThemeExtension<CustomColors> {
  final Color? whiteColor;
  final Color? blackColor;
  final Color? sourceSuccess;
  final Color? success;
  final Color? onSuccess;
  final Color? successContainer;
  final Color? onSuccessContainer;
  final Color? sourceWarning;
  final Color? warning;
  final Color? onWarning;
  final Color? warningContainer;
  final Color? onWarningContainer;
  final Color? warningCustom1;
  final Color? sourceError;
  final Color? error;
  final Color? onError;
  final Color? errorContainer;
  final Color? onErrorContainer;

  final Color? primaryFixed;
  final Color? onPrimaryFixed;
  final Color? primaryFixedDim;
  final Color? onPrimaryFixedVariant;

  final Color? surfaceDim;
  final Color? surfaceBright;
  final Color? surfaceContainerHighest;
  final Color? surfaceContainerHigh;
  final Color? surfaceContainer;
  final Color? surfaceContainerLow;
  final Color? surfaceContainerLowest;

  final Color? stateErrorHovered;
  final Color? stateErrorPressed;
  final Color? stateOnErrorPressed;
  final Color? stateOnPrimaryPressed;
  final Color? stateOnPrimaryHovered;
  final Color? stateOnSurfaceDisabled;
  final Color? stateOnSurfaceVariantPressed;
  final Color? stateOutlineDisabled;
  final Color? stateOnPrimaryContainerPressed;
  final Color? statePrimaryHovered;
  final Color? statePrimaryPressed;

  final Color? successColor;
  final Color? successOnColor;
  final Color? successColorContainer;
  final Color? successOnColorContainer;
  final Color? warningColor;
  final Color? warningOnColor;
  final Color? warningColorContainer;
  final Color? warningOnColorContainer;
  final Color? avatarMonogramBackground1;
  final Color? avatarMonogramBackground2;
  final Color? avatarMonogramBackground3;
  final Color? avatarMonogramBackground4;
  final Color? avatarMonogramBackground5;
  final Color? avatarMonogramBackground6;
  final Color? avatarMonogramBackground7;
  final Color? avatarMonogramBackground8;
  final Color? stateLayersPrimaryOpacity008;
  final Color? stateLayersPrimaryOpacity012;
  final Color? stateLayersPrimaryOpacity016;
  final Color? stateLayersSurfaceTintOpacity008;
  final Color? stateLayersSurfaceTintOpacity012;
  final Color? stateLayersSurfaceTintOpacity016;
  final Color? stateLayersOnPrimaryOpacity008;
  final Color? stateLayersOnPrimaryOpacity012;
  final Color? stateLayersOnPrimaryOpacity016;
  final Color? stateLayersPrimaryContainerOpacity008;
  final Color? stateLayersPrimaryContainerOpacity012;
  final Color? stateLayersPrimaryContainerOpacity016;
  final Color? stateLayersOnPrimaryContainerOpacity008;
  final Color? stateLayersOnPrimaryContainerOpacity012;
  final Color? stateLayersOnPrimaryContainerOpacity016;
  final Color? stateLayersSecondaryOpacity008;
  final Color? stateLayersSecondaryOpacity012;
  final Color? stateLayersSecondaryOpacity016;
  final Color? stateLayersOnSecondaryOpacity008;
  final Color? stateLayersOnSecondaryOpacity012;
  final Color? stateLayersOnSecondaryOpacity016;
  final Color? stateLayersSecondaryContainerOpacity008;
  final Color? stateLayersSecondaryContainerOpacity012;
  final Color? stateLayersSecondaryContainerOpacity016;
  final Color? stateLayersTertiaryOpacity008;
  final Color? stateLayersTertiaryOpacity012;
  final Color? stateLayersTertiaryOpacity016;
  final Color? stateLayersOnTertiaryOpacity008;
  final Color? stateLayersOnTertiaryOpacity012;
  final Color? stateLayersOnTertiaryOpacity016;
  final Color? stateLayersTertiaryContainerOpacity008;
  final Color? stateLayersTertiaryContainerOpacity012;
  final Color? stateLayersTertiaryContainerOpacity016;
  final Color? stateLayersOnTertiaryContainerOpacity008;
  final Color? stateLayersOnTertiaryContainerOpacity012;
  final Color? stateLayersOnTertiaryContainerOpacity016;
  final Color? stateLayersErrorOpacity008;
  final Color? stateLayersErrorOpacity012;
  final Color? stateLayersErrorOpacity016;
  final Color? stateLayersOnErrorOpacity008;
  final Color? stateLayersOnErrorOpacity012;
  final Color? stateLayersOnErrorOpacity016;
  final Color? stateLayersErrorContainerOpacity008;
  final Color? stateLayersErrorContainerOpacity012;
  final Color? stateLayersErrorContainerOpacity016;
  final Color? stateLayersOnErrorContainerOpacity008;
  final Color? stateLayersOnErrorContainerOpacity012;
  final Color? stateLayersOnErrorContainerOpacity016;
  final Color? stateLayersBackgroundOpacity008;
  final Color? stateLayersBackgroundOpacity012;
  final Color? stateLayersBackgroundOpacity016;
  final Color? stateLayersOnBackgroundOpacity008;
  final Color? stateLayersOnBackgroundOpacity012;
  final Color? stateLayersOnBackgroundOpacity016;
  final Color? stateLayersSurfaceOpacity008;
  final Color? stateLayersSurfaceOpacity012;
  final Color? stateLayersSurfaceOpacity016;
  final Color? stateLayersOnSurfaceOpacity008;
  final Color? stateLayersOnSurfaceOpacity012;
  final Color? stateLayersOnSurfaceOpacity016;
  final Color? stateLayersInverseSurfaceOpacity008;
  final Color? stateLayersInverseSurfaceOpacity012;
  final Color? stateLayersInverseSurfaceOpacity016;
  final Color? stateLayersInverseOnSurfaceOpacity008;
  final Color? stateLayersInverseOnSurfaceOpacity012;
  final Color? stateLayersInverseOnSurfaceOpacity016;
  final Color? stateLayersInversePrimaryOpacity008;
  final Color? stateLayersInversePrimaryOpacity012;
  final Color? stateLayersInversePrimaryOpacity016;
  final Color? stateLayersPrimaryFixedOpacity008;
  final Color? stateLayersPrimaryFixedOpacity012;
  final Color? stateLayersPrimaryFixedOpacity016;
  final Color? stateLayersOnPrimaryFixedOpacity008;
  final Color? stateLayersOnPrimaryFixedOpacity012;
  final Color? stateLayersOnPrimaryFixedOpacity016;
  final Color? stateLayersPrimaryFixedDimOpacity008;
  final Color? stateLayersPrimaryFixedDimOpacity012;
  final Color? stateLayersPrimaryFixedDimOpacity016;
  final Color? stateLayersOnPrimaryFixedVariantOpacity008;
  final Color? stateLayersOnPrimaryFixedVariantOpacity012;
  final Color? stateLayersOnPrimaryFixedVariantOpacity016;
  final Color? stateLayersSecondaryFixedOpacity008;
  final Color? stateLayersSecondaryFixedOpacity012;
  final Color? stateLayersSecondaryFixedOpacity016;
  final Color? stateLayersOnSecondaryFixedOpacity008;
  final Color? stateLayersOnSecondaryFixedOpacity012;
  final Color? stateLayersOnSecondaryFixedOpacity016;
  final Color? stateLayersSecondaryFixedDimOpacity008;
  final Color? stateLayersSecondaryFixedDimOpacity012;
  final Color? stateLayersSecondaryFixedDimOpacity016;
  final Color? stateLayersOnSecondaryFixedVariantOpacity008;
  final Color? stateLayersOnSecondaryFixedVariantOpacity012;
  final Color? stateLayersOnSecondaryFixedVariantOpacity016;
  final Color? stateLayersTertiaryFixedOpacity008;
  final Color? stateLayersTertiaryFixedOpacity012;
  final Color? stateLayersTertiaryFixedOpacity016;
  final Color? stateLayersOnTertiaryFixedOpacity008;
  final Color? stateLayersOnTertiaryFixedOpacity012;
  final Color? stateLayersOnTertiaryFixedOpacity016;
  final Color? stateLayersTertiaryFixedDimOpacity008;
  final Color? stateLayersTertiaryFixedDimOpacity012;
  final Color? stateLayersTertiaryFixedDimOpacity016;
  final Color? stateLayersOnTertiaryFixedVariantOpacity008;
  final Color? stateLayersOnTertiaryFixedVariantOpacity012;
  final Color? stateLayersOnTertiaryFixedVariantOpacity016;
  final Color? stateLayersSurfaceDimOpacity008;
  final Color? stateLayersSurfaceDimOpacity012;
  final Color? stateLayersSurfaceDimOpacity016;
  final Color? stateLayersSurfaceBrightOpacity008;
  final Color? stateLayersSurfaceBrightOpacity012;
  final Color? stateLayersSurfaceBrightOpacity016;
  final Color? stateLayersSurfaceContainerLowestOpacity008;
  final Color? stateLayersSurfaceContainerLowestOpacity012;
  final Color? stateLayersSurfaceContainerLowestOpacity016;
  final Color? stateLayersSurfaceContainerLowOpacity008;
  final Color? stateLayersSurfaceContainerLowOpacity012;
  final Color? stateLayersSurfaceContainerLowOpacity016;
  final Color? stateLayersSurfaceContainerOpacity008;
  final Color? stateLayersSurfaceContainerOpacity012;
  final Color? stateLayersSurfaceContainerOpacity016;
  final Color? stateLayersSurfaceContainerHighOpacity008;
  final Color? stateLayersSurfaceContainerHighOpacity012;
  final Color? stateLayersSurfaceContainerHighOpacity016;
  final Color? stateLayersSurfaceContainerHighestOpacity008;
  final Color? stateLayersSurfaceContainerHighestOpacity012;
  final Color? stateLayersSurfaceContainerHighestOpacity016;
  final Color? stateLayersOnsecondarycontainerOpacity008;
  final Color? stateLayersOnsecondarycontainerOpacity012;
  final Color? stateLayersOnsecondarycontainerOpacity016;
  final Color? stateLayersOutlineOpacity008;
  final Color? stateLayersOutlineOpacity012;
  final Color? stateLayersOutlineOpacity016;
  final Color? stateLayersOutlinevariantOpacity008;
  final Color? stateLayersOutlinevariantOpacity012;
  final Color? stateLayersOutlinevariantOpacity016;
  final Color? stateLayersShadowOpacity008;
  final Color? stateLayersShadowOpacity012;
  final Color? stateLayersShadowOpacity016;
  final Color? stateLayersScrimOpacity008;
  final Color? stateLayersScrimOpacity012;
  final Color? stateLayersScrimOpacity016;
  final Color? stateLayersSurfacevariantOpacity008;
  final Color? stateLayersSurfacevariantOpacity012;
  final Color? stateLayersSurfacevariantOpacity016;
  final Color? stateLayersOnsurfacevariantOpacity008;
  final Color? stateLayersOnsurfacevariantOpacity012;
  final Color? stateLayersOnsurfacevariantOpacity016;

  const CustomColors({
    this.whiteColor,
    this.blackColor,
    this.sourceSuccess,
    this.success,
    this.onSuccess,
    this.successContainer,
    this.onSuccessContainer,
    this.sourceWarning,
    this.warning,
    this.onWarning,
    this.warningContainer,
    this.onWarningContainer,
    this.warningCustom1,
    this.sourceError,
    this.error,
    this.onError,
    this.errorContainer,
    this.onErrorContainer,
    this.primaryFixed,
    this.onPrimaryFixed,
    this.primaryFixedDim,
    this.onPrimaryFixedVariant,
    this.surfaceDim,
    this.surfaceBright,
    this.surfaceContainerHighest,
    this.surfaceContainerHigh,
    this.surfaceContainer,
    this.surfaceContainerLow,
    this.surfaceContainerLowest,
    this.stateErrorHovered,
    this.stateErrorPressed,
    this.stateOnErrorPressed,
    this.stateOnPrimaryPressed,
    this.statePrimaryPressed,
    this.stateOnSurfaceDisabled,
    this.stateOnSurfaceVariantPressed,
    this.stateOutlineDisabled,
    this.stateOnPrimaryContainerPressed,
    this.statePrimaryHovered,
    this.stateOnPrimaryHovered,
    required this.successColor,
    required this.successOnColor,
    required this.successColorContainer,
    required this.successOnColorContainer,
    required this.warningColor,
    required this.warningOnColor,
    required this.warningColorContainer,
    required this.warningOnColorContainer,
    required this.avatarMonogramBackground1,
    required this.avatarMonogramBackground2,
    required this.avatarMonogramBackground3,
    required this.avatarMonogramBackground4,
    required this.avatarMonogramBackground5,
    required this.avatarMonogramBackground6,
    required this.avatarMonogramBackground7,
    required this.avatarMonogramBackground8,
    required this.stateLayersPrimaryOpacity008,
    required this.stateLayersPrimaryOpacity012,
    required this.stateLayersPrimaryOpacity016,
    required this.stateLayersSurfaceTintOpacity008,
    required this.stateLayersSurfaceTintOpacity012,
    required this.stateLayersSurfaceTintOpacity016,
    required this.stateLayersOnPrimaryOpacity008,
    required this.stateLayersOnPrimaryOpacity012,
    required this.stateLayersOnPrimaryOpacity016,
    required this.stateLayersPrimaryContainerOpacity008,
    required this.stateLayersPrimaryContainerOpacity012,
    required this.stateLayersPrimaryContainerOpacity016,
    required this.stateLayersOnPrimaryContainerOpacity008,
    required this.stateLayersOnPrimaryContainerOpacity012,
    required this.stateLayersOnPrimaryContainerOpacity016,
    required this.stateLayersSecondaryOpacity008,
    required this.stateLayersSecondaryOpacity012,
    required this.stateLayersSecondaryOpacity016,
    required this.stateLayersOnSecondaryOpacity008,
    required this.stateLayersOnSecondaryOpacity012,
    required this.stateLayersOnSecondaryOpacity016,
    required this.stateLayersSecondaryContainerOpacity008,
    required this.stateLayersSecondaryContainerOpacity012,
    required this.stateLayersSecondaryContainerOpacity016,
    required this.stateLayersTertiaryOpacity008,
    required this.stateLayersTertiaryOpacity012,
    required this.stateLayersTertiaryOpacity016,
    required this.stateLayersOnTertiaryOpacity008,
    required this.stateLayersOnTertiaryOpacity012,
    required this.stateLayersOnTertiaryOpacity016,
    required this.stateLayersTertiaryContainerOpacity008,
    required this.stateLayersTertiaryContainerOpacity012,
    required this.stateLayersTertiaryContainerOpacity016,
    required this.stateLayersOnTertiaryContainerOpacity008,
    required this.stateLayersOnTertiaryContainerOpacity012,
    required this.stateLayersOnTertiaryContainerOpacity016,
    required this.stateLayersErrorOpacity008,
    required this.stateLayersErrorOpacity012,
    required this.stateLayersErrorOpacity016,
    required this.stateLayersOnErrorOpacity008,
    required this.stateLayersOnErrorOpacity012,
    required this.stateLayersOnErrorOpacity016,
    required this.stateLayersErrorContainerOpacity008,
    required this.stateLayersErrorContainerOpacity012,
    required this.stateLayersErrorContainerOpacity016,
    required this.stateLayersOnErrorContainerOpacity008,
    required this.stateLayersOnErrorContainerOpacity012,
    required this.stateLayersOnErrorContainerOpacity016,
    required this.stateLayersBackgroundOpacity008,
    required this.stateLayersBackgroundOpacity012,
    required this.stateLayersBackgroundOpacity016,
    required this.stateLayersOnBackgroundOpacity008,
    required this.stateLayersOnBackgroundOpacity012,
    required this.stateLayersOnBackgroundOpacity016,
    required this.stateLayersSurfaceOpacity008,
    required this.stateLayersSurfaceOpacity012,
    required this.stateLayersSurfaceOpacity016,
    required this.stateLayersOnSurfaceOpacity008,
    required this.stateLayersOnSurfaceOpacity012,
    required this.stateLayersOnSurfaceOpacity016,
    required this.stateLayersInverseSurfaceOpacity008,
    required this.stateLayersInverseSurfaceOpacity012,
    required this.stateLayersInverseSurfaceOpacity016,
    required this.stateLayersInverseOnSurfaceOpacity008,
    required this.stateLayersInverseOnSurfaceOpacity012,
    required this.stateLayersInverseOnSurfaceOpacity016,
    required this.stateLayersInversePrimaryOpacity008,
    required this.stateLayersInversePrimaryOpacity012,
    required this.stateLayersInversePrimaryOpacity016,
    required this.stateLayersPrimaryFixedOpacity008,
    required this.stateLayersPrimaryFixedOpacity012,
    required this.stateLayersPrimaryFixedOpacity016,
    required this.stateLayersOnPrimaryFixedOpacity008,
    required this.stateLayersOnPrimaryFixedOpacity012,
    required this.stateLayersOnPrimaryFixedOpacity016,
    required this.stateLayersPrimaryFixedDimOpacity008,
    required this.stateLayersPrimaryFixedDimOpacity012,
    required this.stateLayersPrimaryFixedDimOpacity016,
    required this.stateLayersOnPrimaryFixedVariantOpacity008,
    required this.stateLayersOnPrimaryFixedVariantOpacity012,
    required this.stateLayersOnPrimaryFixedVariantOpacity016,
    required this.stateLayersSecondaryFixedOpacity008,
    required this.stateLayersSecondaryFixedOpacity012,
    required this.stateLayersSecondaryFixedOpacity016,
    required this.stateLayersOnSecondaryFixedOpacity008,
    required this.stateLayersOnSecondaryFixedOpacity012,
    required this.stateLayersOnSecondaryFixedOpacity016,
    required this.stateLayersSecondaryFixedDimOpacity008,
    required this.stateLayersSecondaryFixedDimOpacity012,
    required this.stateLayersSecondaryFixedDimOpacity016,
    required this.stateLayersOnSecondaryFixedVariantOpacity008,
    required this.stateLayersOnSecondaryFixedVariantOpacity012,
    required this.stateLayersOnSecondaryFixedVariantOpacity016,
    required this.stateLayersTertiaryFixedOpacity008,
    required this.stateLayersTertiaryFixedOpacity012,
    required this.stateLayersTertiaryFixedOpacity016,
    required this.stateLayersOnTertiaryFixedOpacity008,
    required this.stateLayersOnTertiaryFixedOpacity012,
    required this.stateLayersOnTertiaryFixedOpacity016,
    required this.stateLayersTertiaryFixedDimOpacity008,
    required this.stateLayersTertiaryFixedDimOpacity012,
    required this.stateLayersTertiaryFixedDimOpacity016,
    required this.stateLayersOnTertiaryFixedVariantOpacity008,
    required this.stateLayersOnTertiaryFixedVariantOpacity012,
    required this.stateLayersOnTertiaryFixedVariantOpacity016,
    required this.stateLayersSurfaceDimOpacity008,
    required this.stateLayersSurfaceDimOpacity012,
    required this.stateLayersSurfaceDimOpacity016,
    required this.stateLayersSurfaceBrightOpacity008,
    required this.stateLayersSurfaceBrightOpacity012,
    required this.stateLayersSurfaceBrightOpacity016,
    required this.stateLayersSurfaceContainerLowestOpacity008,
    required this.stateLayersSurfaceContainerLowestOpacity012,
    required this.stateLayersSurfaceContainerLowestOpacity016,
    required this.stateLayersSurfaceContainerLowOpacity008,
    required this.stateLayersSurfaceContainerLowOpacity012,
    required this.stateLayersSurfaceContainerLowOpacity016,
    required this.stateLayersSurfaceContainerOpacity008,
    required this.stateLayersSurfaceContainerOpacity012,
    required this.stateLayersSurfaceContainerOpacity016,
    required this.stateLayersSurfaceContainerHighOpacity008,
    required this.stateLayersSurfaceContainerHighOpacity012,
    required this.stateLayersSurfaceContainerHighOpacity016,
    required this.stateLayersSurfaceContainerHighestOpacity008,
    required this.stateLayersSurfaceContainerHighestOpacity012,
    required this.stateLayersSurfaceContainerHighestOpacity016,
    this.stateLayersOnsecondarycontainerOpacity008,
    this.stateLayersOnsecondarycontainerOpacity012,
    this.stateLayersOnsecondarycontainerOpacity016,
    this.stateLayersOutlineOpacity008,
    this.stateLayersOutlineOpacity012,
    this.stateLayersOutlineOpacity016,
    this.stateLayersOutlinevariantOpacity008,
    this.stateLayersOutlinevariantOpacity012,
    this.stateLayersOutlinevariantOpacity016,
    this.stateLayersShadowOpacity008,
    this.stateLayersShadowOpacity012,
    this.stateLayersShadowOpacity016,
    this.stateLayersScrimOpacity008,
    this.stateLayersScrimOpacity012,
    this.stateLayersScrimOpacity016,
    this.stateLayersSurfacevariantOpacity008,
    this.stateLayersSurfacevariantOpacity012,
    this.stateLayersSurfacevariantOpacity016,
    this.stateLayersOnsurfacevariantOpacity008,
    this.stateLayersOnsurfacevariantOpacity012,
    this.stateLayersOnsurfacevariantOpacity016,
  });

  @override
  CustomColors copyWith({
    Color? whiteColor,
    Color? blackColor,
    Color? sourceSuccess,
    Color? success,
    Color? onSuccess,
    Color? successContainer,
    Color? onSuccessContainer,
    Color? sourceWarning,
    Color? warning,
    Color? onWarning,
    Color? warningContainer,
    Color? onWarningContainer,
    Color? warningCustom1,
    Color? sourceError,
    Color? error,
    Color? onError,
    Color? errorContainer,
    Color? onErrorContainer,
    Color? primaryFixed,
    Color? onPrimaryFixed,
    Color? primaryFixedDim,
    Color? onPrimaryFixedVariant,
    Color? surfaceDim,
    Color? surfaceBright,
    Color? surfaceContainerHighest,
    Color? surfaceContainerHigh,
    Color? surfaceContainer,
    Color? surfaceContainerLow,
    Color? surfaceContainerLowest,
    Color? stateErrorHovered,
    Color? stateErrorPressed,
    Color? stateOnErrorPressed,
    Color? stateOnPrimaryPressed,
    Color? stateOnPrimaryHovered,
    Color? stateOnSurfaceDisabled,
    Color? stateOnSurfaceVariantPressed,
    Color? stateOutlineDisabled,
    Color? stateOnPrimaryContainerPressed,
    Color? statePrimaryHovered,
    Color? statePrimaryPressed,
    Color? successColor,
    Color? successOnColor,
    Color? successColorContainer,
    Color? successOnColorContainer,
    Color? warningColor,
    Color? warningOnColor,
    Color? warningColorContainer,
    Color? warningOnColorContainer,
    Color? avatarMonogramBackground1,
    Color? avatarMonogramBackground2,
    Color? avatarMonogramBackground3,
    Color? avatarMonogramBackground4,
    Color? avatarMonogramBackground5,
    Color? avatarMonogramBackground6,
    Color? avatarMonogramBackground7,
    Color? avatarMonogramBackground8,
    Color? stateLayersPrimaryOpacity008,
    Color? stateLayersPrimaryOpacity012,
    Color? stateLayersPrimaryOpacity016,
    Color? stateLayersSurfaceTintOpacity008,
    Color? stateLayersSurfaceTintOpacity012,
    Color? stateLayersSurfaceTintOpacity016,
    Color? stateLayersOnPrimaryOpacity008,
    Color? stateLayersOnPrimaryOpacity012,
    Color? stateLayersOnPrimaryOpacity016,
    Color? stateLayersPrimaryContainerOpacity008,
    Color? stateLayersPrimaryContainerOpacity012,
    Color? stateLayersPrimaryContainerOpacity016,
    Color? stateLayersOnPrimaryContainerOpacity008,
    Color? stateLayersOnPrimaryContainerOpacity012,
    Color? stateLayersOnPrimaryContainerOpacity016,
    Color? stateLayersSecondaryOpacity008,
    Color? stateLayersSecondaryOpacity012,
    Color? stateLayersSecondaryOpacity016,
    Color? stateLayersOnSecondaryOpacity008,
    Color? stateLayersOnSecondaryOpacity012,
    Color? stateLayersOnSecondaryOpacity016,
    Color? stateLayersSecondaryContainerOpacity008,
    Color? stateLayersSecondaryContainerOpacity012,
    Color? stateLayersSecondaryContainerOpacity016,
    Color? stateLayersTertiaryOpacity008,
    Color? stateLayersTertiaryOpacity012,
    Color? stateLayersTertiaryOpacity016,
    Color? stateLayersOnTertiaryOpacity008,
    Color? stateLayersOnTertiaryOpacity012,
    Color? stateLayersOnTertiaryOpacity016,
    Color? stateLayersTertiaryContainerOpacity008,
    Color? stateLayersTertiaryContainerOpacity012,
    Color? stateLayersTertiaryContainerOpacity016,
    Color? stateLayersOnTertiaryContainerOpacity008,
    Color? stateLayersOnTertiaryContainerOpacity012,
    Color? stateLayersOnTertiaryContainerOpacity016,
    Color? stateLayersErrorOpacity008,
    Color? stateLayersErrorOpacity012,
    Color? stateLayersErrorOpacity016,
    Color? stateLayersOnErrorOpacity008,
    Color? stateLayersOnErrorOpacity012,
    Color? stateLayersOnErrorOpacity016,
    Color? stateLayersErrorContainerOpacity008,
    Color? stateLayersErrorContainerOpacity012,
    Color? stateLayersErrorContainerOpacity016,
    Color? stateLayersOnErrorContainerOpacity008,
    Color? stateLayersOnErrorContainerOpacity012,
    Color? stateLayersOnErrorContainerOpacity016,
    Color? stateLayersBackgroundOpacity008,
    Color? stateLayersBackgroundOpacity012,
    Color? stateLayersBackgroundOpacity016,
    Color? stateLayersOnBackgroundOpacity008,
    Color? stateLayersOnBackgroundOpacity012,
    Color? stateLayersOnBackgroundOpacity016,
    Color? stateLayersSurfaceOpacity008,
    Color? stateLayersSurfaceOpacity012,
    Color? stateLayersSurfaceOpacity016,
    Color? stateLayersOnSurfaceOpacity008,
    Color? stateLayersOnSurfaceOpacity012,
    Color? stateLayersOnSurfaceOpacity016,
    Color? stateLayersInverseSurfaceOpacity008,
    Color? stateLayersInverseSurfaceOpacity012,
    Color? stateLayersInverseSurfaceOpacity016,
    Color? stateLayersInverseOnSurfaceOpacity008,
    Color? stateLayersInverseOnSurfaceOpacity012,
    Color? stateLayersInverseOnSurfaceOpacity016,
    Color? stateLayersInversePrimaryOpacity008,
    Color? stateLayersInversePrimaryOpacity012,
    Color? stateLayersInversePrimaryOpacity016,
    Color? stateLayersPrimaryFixedOpacity008,
    Color? stateLayersPrimaryFixedOpacity012,
    Color? stateLayersPrimaryFixedOpacity016,
    Color? stateLayersOnPrimaryFixedOpacity008,
    Color? stateLayersOnPrimaryFixedOpacity012,
    Color? stateLayersOnPrimaryFixedOpacity016,
    Color? stateLayersPrimaryFixedDimOpacity008,
    Color? stateLayersPrimaryFixedDimOpacity012,
    Color? stateLayersPrimaryFixedDimOpacity016,
    Color? stateLayersOnPrimaryFixedVariantOpacity008,
    Color? stateLayersOnPrimaryFixedVariantOpacity012,
    Color? stateLayersOnPrimaryFixedVariantOpacity016,
    Color? stateLayersSecondaryFixedOpacity008,
    Color? stateLayersSecondaryFixedOpacity012,
    Color? stateLayersSecondaryFixedOpacity016,
    Color? stateLayersOnSecondaryFixedOpacity008,
    Color? stateLayersOnSecondaryFixedOpacity012,
    Color? stateLayersOnSecondaryFixedOpacity016,
    Color? stateLayersSecondaryFixedDimOpacity008,
    Color? stateLayersSecondaryFixedDimOpacity012,
    Color? stateLayersSecondaryFixedDimOpacity016,
    Color? stateLayersOnSecondaryFixedVariantOpacity008,
    Color? stateLayersOnSecondaryFixedVariantOpacity012,
    Color? stateLayersOnSecondaryFixedVariantOpacity016,
    Color? stateLayersTertiaryFixedOpacity008,
    Color? stateLayersTertiaryFixedOpacity012,
    Color? stateLayersTertiaryFixedOpacity016,
    Color? stateLayersOnTertiaryFixedOpacity008,
    Color? stateLayersOnTertiaryFixedOpacity012,
    Color? stateLayersOnTertiaryFixedOpacity016,
    Color? stateLayersTertiaryFixedDimOpacity008,
    Color? stateLayersTertiaryFixedDimOpacity012,
    Color? stateLayersTertiaryFixedDimOpacity016,
    Color? stateLayersOnTertiaryFixedVariantOpacity008,
    Color? stateLayersOnTertiaryFixedVariantOpacity012,
    Color? stateLayersOnTertiaryFixedVariantOpacity016,
    Color? stateLayersSurfaceDimOpacity008,
    Color? stateLayersSurfaceDimOpacity012,
    Color? stateLayersSurfaceDimOpacity016,
    Color? stateLayersSurfaceBrightOpacity008,
    Color? stateLayersSurfaceBrightOpacity012,
    Color? stateLayersSurfaceBrightOpacity016,
    Color? stateLayersSurfaceContainerLowestOpacity008,
    Color? stateLayersSurfaceContainerLowestOpacity012,
    Color? stateLayersSurfaceContainerLowestOpacity016,
    Color? stateLayersSurfaceContainerLowOpacity008,
    Color? stateLayersSurfaceContainerLowOpacity012,
    Color? stateLayersSurfaceContainerLowOpacity016,
    Color? stateLayersSurfaceContainerOpacity008,
    Color? stateLayersSurfaceContainerOpacity012,
    Color? stateLayersSurfaceContainerOpacity016,
    Color? stateLayersSurfaceContainerHighOpacity008,
    Color? stateLayersSurfaceContainerHighOpacity012,
    Color? stateLayersSurfaceContainerHighOpacity016,
    Color? stateLayersSurfaceContainerHighestOpacity008,
    Color? stateLayersSurfaceContainerHighestOpacity012,
    Color? stateLayersSurfaceContainerHighestOpacity016,
    Color? stateLayersOnsecondarycontainerOpacity008,
    Color? stateLayersOnsecondarycontainerOpacity012,
    Color? stateLayersOnsecondarycontainerOpacity016,
    Color? stateLayersOutlineOpacity008,
    Color? stateLayersOutlineOpacity012,
    Color? stateLayersOutlineOpacity016,
    Color? stateLayersOutlinevariantOpacity008,
    Color? stateLayersOutlinevariantOpacity012,
    Color? stateLayersOutlinevariantOpacity016,
    Color? stateLayersShadowOpacity008,
    Color? stateLayersShadowOpacity012,
    Color? stateLayersShadowOpacity016,
    Color? stateLayersScrimOpacity008,
    Color? stateLayersScrimOpacity012,
    Color? stateLayersScrimOpacity016,
    Color? stateLayersSurfacevariantOpacity008,
    Color? stateLayersSurfacevariantOpacity012,
    Color? stateLayersSurfacevariantOpacity016,
    Color? stateLayersOnsurfacevariantOpacity008,
    Color? stateLayersOnsurfacevariantOpacity012,
    Color? stateLayersOnsurfacevariantOpacity016,
  }) {
    return CustomColors(
      whiteColor: whiteColor ?? this.whiteColor,
      blackColor: blackColor ?? this.blackColor,
      sourceSuccess: sourceSuccess ?? this.sourceSuccess,
      success: success ?? this.success,
      onSuccess: onSuccess ?? this.onSuccess,
      successContainer: successContainer ?? this.successContainer,
      onSuccessContainer: onSuccessContainer ?? this.onSuccessContainer,
      sourceWarning: sourceWarning ?? this.sourceWarning,
      warning: warning ?? this.warning,
      onWarning: onWarning ?? this.onWarning,
      warningContainer: warningContainer ?? this.warningContainer,
      onWarningContainer: onWarningContainer ?? this.onWarningContainer,
      warningCustom1: warningCustom1 ?? this.warningCustom1,
      sourceError: sourceError ?? this.sourceError,
      error: error ?? this.error,
      onError: onError ?? this.onError,
      errorContainer: errorContainer ?? this.errorContainer,
      onErrorContainer: onErrorContainer ?? this.onErrorContainer,
      primaryFixed: primaryFixed ?? this.primaryFixed,
      onPrimaryFixed: onPrimaryFixed ?? this.onPrimaryFixed,
      primaryFixedDim: primaryFixedDim ?? this.primaryFixedDim,
      onPrimaryFixedVariant:
          onPrimaryFixedVariant ?? this.onPrimaryFixedVariant,
      surfaceDim: surfaceDim ?? this.surfaceDim,
      surfaceBright: surfaceBright ?? this.surfaceBright,
      surfaceContainerHighest:
          surfaceContainerHighest ?? this.surfaceContainerHighest,
      surfaceContainerHigh: surfaceContainerHigh ?? this.surfaceContainerHigh,
      surfaceContainer: surfaceContainer ?? this.surfaceContainer,
      surfaceContainerLow: surfaceContainerLow ?? this.surfaceContainerLow,
      surfaceContainerLowest:
          surfaceContainerLowest ?? this.surfaceContainerLowest,
      stateErrorHovered: stateErrorHovered ?? this.stateErrorHovered,
      stateErrorPressed: stateErrorPressed ?? this.stateErrorPressed,
      stateOnErrorPressed: stateOnErrorPressed ?? this.stateOnErrorPressed,
      stateOnPrimaryPressed:
          stateOnPrimaryPressed ?? this.stateOnPrimaryPressed,
      stateOnPrimaryHovered:
          stateOnPrimaryHovered ?? this.stateOnPrimaryHovered,
      stateOnSurfaceDisabled:
          stateOnSurfaceDisabled ?? this.stateOnSurfaceDisabled,
      stateOnSurfaceVariantPressed:
          stateOnSurfaceVariantPressed ?? this.stateOnSurfaceVariantPressed,
      stateOutlineDisabled: stateOutlineDisabled ?? this.stateOutlineDisabled,
      stateOnPrimaryContainerPressed:
          stateOnPrimaryContainerPressed ?? this.stateOnPrimaryContainerPressed,
      statePrimaryHovered: statePrimaryHovered ?? this.statePrimaryHovered,
      statePrimaryPressed: statePrimaryPressed ?? this.statePrimaryPressed,
      successColor: successColor ?? this.successColor,
      successOnColor: successOnColor ?? this.successOnColor,
      successColorContainer:
          successColorContainer ?? this.successColorContainer,
      successOnColorContainer:
          successOnColorContainer ?? this.successOnColorContainer,
      warningColor: warningColor ?? this.warningColor,
      warningOnColor: warningOnColor ?? this.warningOnColor,
      warningColorContainer:
          warningColorContainer ?? this.warningColorContainer,
      warningOnColorContainer:
          warningOnColorContainer ?? this.warningOnColorContainer,
      avatarMonogramBackground1:
          avatarMonogramBackground1 ?? this.avatarMonogramBackground1,
      avatarMonogramBackground2:
          avatarMonogramBackground2 ?? this.avatarMonogramBackground2,
      avatarMonogramBackground3:
          avatarMonogramBackground3 ?? this.avatarMonogramBackground3,
      avatarMonogramBackground4:
          avatarMonogramBackground4 ?? this.avatarMonogramBackground4,
      avatarMonogramBackground5:
          avatarMonogramBackground5 ?? this.avatarMonogramBackground5,
      avatarMonogramBackground6:
          avatarMonogramBackground6 ?? this.avatarMonogramBackground6,
      avatarMonogramBackground7:
          avatarMonogramBackground7 ?? this.avatarMonogramBackground7,
      avatarMonogramBackground8:
          avatarMonogramBackground8 ?? this.avatarMonogramBackground8,
      stateLayersPrimaryOpacity008:
          stateLayersPrimaryOpacity008 ?? this.stateLayersPrimaryOpacity008,
      stateLayersPrimaryOpacity012:
          stateLayersPrimaryOpacity012 ?? this.stateLayersPrimaryOpacity012,
      stateLayersPrimaryOpacity016:
          stateLayersPrimaryOpacity016 ?? this.stateLayersPrimaryOpacity016,
      stateLayersSurfaceTintOpacity008: stateLayersSurfaceTintOpacity008 ??
          this.stateLayersSurfaceTintOpacity008,
      stateLayersSurfaceTintOpacity012: stateLayersSurfaceTintOpacity012 ??
          this.stateLayersSurfaceTintOpacity012,
      stateLayersSurfaceTintOpacity016: stateLayersSurfaceTintOpacity016 ??
          this.stateLayersSurfaceTintOpacity016,
      stateLayersOnPrimaryOpacity008:
          stateLayersOnPrimaryOpacity008 ?? this.stateLayersOnPrimaryOpacity008,
      stateLayersOnPrimaryOpacity012:
          stateLayersOnPrimaryOpacity012 ?? this.stateLayersOnPrimaryOpacity012,
      stateLayersOnPrimaryOpacity016:
          stateLayersOnPrimaryOpacity016 ?? this.stateLayersOnPrimaryOpacity016,
      stateLayersPrimaryContainerOpacity008:
          stateLayersPrimaryContainerOpacity008 ??
              this.stateLayersPrimaryContainerOpacity008,
      stateLayersPrimaryContainerOpacity012:
          stateLayersPrimaryContainerOpacity012 ??
              this.stateLayersPrimaryContainerOpacity012,
      stateLayersPrimaryContainerOpacity016:
          stateLayersPrimaryContainerOpacity016 ??
              this.stateLayersPrimaryContainerOpacity016,
      stateLayersOnPrimaryContainerOpacity008:
          stateLayersOnPrimaryContainerOpacity008 ??
              this.stateLayersOnPrimaryContainerOpacity008,
      stateLayersOnPrimaryContainerOpacity012:
          stateLayersOnPrimaryContainerOpacity012 ??
              this.stateLayersOnPrimaryContainerOpacity012,
      stateLayersOnPrimaryContainerOpacity016:
          stateLayersOnPrimaryContainerOpacity016 ??
              this.stateLayersOnPrimaryContainerOpacity016,
      stateLayersSecondaryOpacity008:
          stateLayersSecondaryOpacity008 ?? this.stateLayersSecondaryOpacity008,
      stateLayersSecondaryOpacity012:
          stateLayersSecondaryOpacity012 ?? this.stateLayersSecondaryOpacity012,
      stateLayersSecondaryOpacity016:
          stateLayersSecondaryOpacity016 ?? this.stateLayersSecondaryOpacity016,
      stateLayersOnSecondaryOpacity008: stateLayersOnSecondaryOpacity008 ??
          this.stateLayersOnSecondaryOpacity008,
      stateLayersOnSecondaryOpacity012: stateLayersOnSecondaryOpacity012 ??
          this.stateLayersOnSecondaryOpacity012,
      stateLayersOnSecondaryOpacity016: stateLayersOnSecondaryOpacity016 ??
          this.stateLayersOnSecondaryOpacity016,
      stateLayersSecondaryContainerOpacity008:
          stateLayersSecondaryContainerOpacity008 ??
              this.stateLayersSecondaryContainerOpacity008,
      stateLayersSecondaryContainerOpacity012:
          stateLayersSecondaryContainerOpacity012 ??
              this.stateLayersSecondaryContainerOpacity012,
      stateLayersSecondaryContainerOpacity016:
          stateLayersSecondaryContainerOpacity016 ??
              this.stateLayersSecondaryContainerOpacity016,
      stateLayersTertiaryOpacity008:
          stateLayersTertiaryOpacity008 ?? this.stateLayersTertiaryOpacity008,
      stateLayersTertiaryOpacity012:
          stateLayersTertiaryOpacity012 ?? this.stateLayersTertiaryOpacity012,
      stateLayersTertiaryOpacity016:
          stateLayersTertiaryOpacity016 ?? this.stateLayersTertiaryOpacity016,
      stateLayersOnTertiaryOpacity008: stateLayersOnTertiaryOpacity008 ??
          this.stateLayersOnTertiaryOpacity008,
      stateLayersOnTertiaryOpacity012: stateLayersOnTertiaryOpacity012 ??
          this.stateLayersOnTertiaryOpacity012,
      stateLayersOnTertiaryOpacity016: stateLayersOnTertiaryOpacity016 ??
          this.stateLayersOnTertiaryOpacity016,
      stateLayersTertiaryContainerOpacity008:
          stateLayersTertiaryContainerOpacity008 ??
              this.stateLayersTertiaryContainerOpacity008,
      stateLayersTertiaryContainerOpacity012:
          stateLayersTertiaryContainerOpacity012 ??
              this.stateLayersTertiaryContainerOpacity012,
      stateLayersTertiaryContainerOpacity016:
          stateLayersTertiaryContainerOpacity016 ??
              this.stateLayersTertiaryContainerOpacity016,
      stateLayersOnTertiaryContainerOpacity008:
          stateLayersOnTertiaryContainerOpacity008 ??
              this.stateLayersOnTertiaryContainerOpacity008,
      stateLayersOnTertiaryContainerOpacity012:
          stateLayersOnTertiaryContainerOpacity012 ??
              this.stateLayersOnTertiaryContainerOpacity012,
      stateLayersOnTertiaryContainerOpacity016:
          stateLayersOnTertiaryContainerOpacity016 ??
              this.stateLayersOnTertiaryContainerOpacity016,
      stateLayersErrorOpacity008:
          stateLayersErrorOpacity008 ?? this.stateLayersErrorOpacity008,
      stateLayersErrorOpacity012:
          stateLayersErrorOpacity012 ?? this.stateLayersErrorOpacity012,
      stateLayersErrorOpacity016:
          stateLayersErrorOpacity016 ?? this.stateLayersErrorOpacity016,
      stateLayersOnErrorOpacity008:
          stateLayersOnErrorOpacity008 ?? this.stateLayersOnErrorOpacity008,
      stateLayersOnErrorOpacity012:
          stateLayersOnErrorOpacity012 ?? this.stateLayersOnErrorOpacity012,
      stateLayersOnErrorOpacity016:
          stateLayersOnErrorOpacity016 ?? this.stateLayersOnErrorOpacity016,
      stateLayersErrorContainerOpacity008:
          stateLayersErrorContainerOpacity008 ??
              this.stateLayersErrorContainerOpacity008,
      stateLayersErrorContainerOpacity012:
          stateLayersErrorContainerOpacity012 ??
              this.stateLayersErrorContainerOpacity012,
      stateLayersErrorContainerOpacity016:
          stateLayersErrorContainerOpacity016 ??
              this.stateLayersErrorContainerOpacity016,
      stateLayersOnErrorContainerOpacity008:
          stateLayersOnErrorContainerOpacity008 ??
              this.stateLayersOnErrorContainerOpacity008,
      stateLayersOnErrorContainerOpacity012:
          stateLayersOnErrorContainerOpacity012 ??
              this.stateLayersOnErrorContainerOpacity012,
      stateLayersOnErrorContainerOpacity016:
          stateLayersOnErrorContainerOpacity016 ??
              this.stateLayersOnErrorContainerOpacity016,
      stateLayersBackgroundOpacity008: stateLayersBackgroundOpacity008 ??
          this.stateLayersBackgroundOpacity008,
      stateLayersBackgroundOpacity012: stateLayersBackgroundOpacity012 ??
          this.stateLayersBackgroundOpacity012,
      stateLayersBackgroundOpacity016: stateLayersBackgroundOpacity016 ??
          this.stateLayersBackgroundOpacity016,
      stateLayersOnBackgroundOpacity008: stateLayersOnBackgroundOpacity008 ??
          this.stateLayersOnBackgroundOpacity008,
      stateLayersOnBackgroundOpacity012: stateLayersOnBackgroundOpacity012 ??
          this.stateLayersOnBackgroundOpacity012,
      stateLayersOnBackgroundOpacity016: stateLayersOnBackgroundOpacity016 ??
          this.stateLayersOnBackgroundOpacity016,
      stateLayersSurfaceOpacity008:
          stateLayersSurfaceOpacity008 ?? this.stateLayersSurfaceOpacity008,
      stateLayersSurfaceOpacity012:
          stateLayersSurfaceOpacity012 ?? this.stateLayersSurfaceOpacity012,
      stateLayersSurfaceOpacity016:
          stateLayersSurfaceOpacity016 ?? this.stateLayersSurfaceOpacity016,
      stateLayersOnSurfaceOpacity008:
          stateLayersOnSurfaceOpacity008 ?? this.stateLayersOnSurfaceOpacity008,
      stateLayersOnSurfaceOpacity012:
          stateLayersOnSurfaceOpacity012 ?? this.stateLayersOnSurfaceOpacity012,
      stateLayersOnSurfaceOpacity016:
          stateLayersOnSurfaceOpacity016 ?? this.stateLayersOnSurfaceOpacity016,
      stateLayersInverseSurfaceOpacity008:
          stateLayersInverseSurfaceOpacity008 ??
              this.stateLayersInverseSurfaceOpacity008,
      stateLayersInverseSurfaceOpacity012:
          stateLayersInverseSurfaceOpacity012 ??
              this.stateLayersInverseSurfaceOpacity012,
      stateLayersInverseSurfaceOpacity016:
          stateLayersInverseSurfaceOpacity016 ??
              this.stateLayersInverseSurfaceOpacity016,
      stateLayersInverseOnSurfaceOpacity008:
          stateLayersInverseOnSurfaceOpacity008 ??
              this.stateLayersInverseOnSurfaceOpacity008,
      stateLayersInverseOnSurfaceOpacity012:
          stateLayersInverseOnSurfaceOpacity012 ??
              this.stateLayersInverseOnSurfaceOpacity012,
      stateLayersInverseOnSurfaceOpacity016:
          stateLayersInverseOnSurfaceOpacity016 ??
              this.stateLayersInverseOnSurfaceOpacity016,
      stateLayersInversePrimaryOpacity008:
          stateLayersInversePrimaryOpacity008 ??
              this.stateLayersInversePrimaryOpacity008,
      stateLayersInversePrimaryOpacity012:
          stateLayersInversePrimaryOpacity012 ??
              this.stateLayersInversePrimaryOpacity012,
      stateLayersInversePrimaryOpacity016:
          stateLayersInversePrimaryOpacity016 ??
              this.stateLayersInversePrimaryOpacity016,
      stateLayersPrimaryFixedOpacity008: stateLayersPrimaryFixedOpacity008 ??
          this.stateLayersPrimaryFixedOpacity008,
      stateLayersPrimaryFixedOpacity012: stateLayersPrimaryFixedOpacity012 ??
          this.stateLayersPrimaryFixedOpacity012,
      stateLayersPrimaryFixedOpacity016: stateLayersPrimaryFixedOpacity016 ??
          this.stateLayersPrimaryFixedOpacity016,
      stateLayersOnPrimaryFixedOpacity008:
          stateLayersOnPrimaryFixedOpacity008 ??
              this.stateLayersOnPrimaryFixedOpacity008,
      stateLayersOnPrimaryFixedOpacity012:
          stateLayersOnPrimaryFixedOpacity012 ??
              this.stateLayersOnPrimaryFixedOpacity012,
      stateLayersOnPrimaryFixedOpacity016:
          stateLayersOnPrimaryFixedOpacity016 ??
              this.stateLayersOnPrimaryFixedOpacity016,
      stateLayersPrimaryFixedDimOpacity008:
          stateLayersPrimaryFixedDimOpacity008 ??
              this.stateLayersPrimaryFixedDimOpacity008,
      stateLayersPrimaryFixedDimOpacity012:
          stateLayersPrimaryFixedDimOpacity012 ??
              this.stateLayersPrimaryFixedDimOpacity012,
      stateLayersPrimaryFixedDimOpacity016:
          stateLayersPrimaryFixedDimOpacity016 ??
              this.stateLayersPrimaryFixedDimOpacity016,
      stateLayersOnPrimaryFixedVariantOpacity008:
          stateLayersOnPrimaryFixedVariantOpacity008 ??
              this.stateLayersOnPrimaryFixedVariantOpacity008,
      stateLayersOnPrimaryFixedVariantOpacity012:
          stateLayersOnPrimaryFixedVariantOpacity012 ??
              this.stateLayersOnPrimaryFixedVariantOpacity012,
      stateLayersOnPrimaryFixedVariantOpacity016:
          stateLayersOnPrimaryFixedVariantOpacity016 ??
              this.stateLayersOnPrimaryFixedVariantOpacity016,
      stateLayersSecondaryFixedOpacity008:
          stateLayersSecondaryFixedOpacity008 ??
              this.stateLayersSecondaryFixedOpacity008,
      stateLayersSecondaryFixedOpacity012:
          stateLayersSecondaryFixedOpacity012 ??
              this.stateLayersSecondaryFixedOpacity012,
      stateLayersSecondaryFixedOpacity016:
          stateLayersSecondaryFixedOpacity016 ??
              this.stateLayersSecondaryFixedOpacity016,
      stateLayersOnSecondaryFixedOpacity008:
          stateLayersOnSecondaryFixedOpacity008 ??
              this.stateLayersOnSecondaryFixedOpacity008,
      stateLayersOnSecondaryFixedOpacity012:
          stateLayersOnSecondaryFixedOpacity012 ??
              this.stateLayersOnSecondaryFixedOpacity012,
      stateLayersOnSecondaryFixedOpacity016:
          stateLayersOnSecondaryFixedOpacity016 ??
              this.stateLayersOnSecondaryFixedOpacity016,
      stateLayersSecondaryFixedDimOpacity008:
          stateLayersSecondaryFixedDimOpacity008 ??
              this.stateLayersSecondaryFixedDimOpacity008,
      stateLayersSecondaryFixedDimOpacity012:
          stateLayersSecondaryFixedDimOpacity012 ??
              this.stateLayersSecondaryFixedDimOpacity012,
      stateLayersSecondaryFixedDimOpacity016:
          stateLayersSecondaryFixedDimOpacity016 ??
              this.stateLayersSecondaryFixedDimOpacity016,
      stateLayersOnSecondaryFixedVariantOpacity008:
          stateLayersOnSecondaryFixedVariantOpacity008 ??
              this.stateLayersOnSecondaryFixedVariantOpacity008,
      stateLayersOnSecondaryFixedVariantOpacity012:
          stateLayersOnSecondaryFixedVariantOpacity012 ??
              this.stateLayersOnSecondaryFixedVariantOpacity012,
      stateLayersOnSecondaryFixedVariantOpacity016:
          stateLayersOnSecondaryFixedVariantOpacity016 ??
              this.stateLayersOnSecondaryFixedVariantOpacity016,
      stateLayersTertiaryFixedOpacity008: stateLayersTertiaryFixedOpacity008 ??
          this.stateLayersTertiaryFixedOpacity008,
      stateLayersTertiaryFixedOpacity012: stateLayersTertiaryFixedOpacity012 ??
          this.stateLayersTertiaryFixedOpacity012,
      stateLayersTertiaryFixedOpacity016: stateLayersTertiaryFixedOpacity016 ??
          this.stateLayersTertiaryFixedOpacity016,
      stateLayersOnTertiaryFixedOpacity008:
          stateLayersOnTertiaryFixedOpacity008 ??
              this.stateLayersOnTertiaryFixedOpacity008,
      stateLayersOnTertiaryFixedOpacity012:
          stateLayersOnTertiaryFixedOpacity012 ??
              this.stateLayersOnTertiaryFixedOpacity012,
      stateLayersOnTertiaryFixedOpacity016:
          stateLayersOnTertiaryFixedOpacity016 ??
              this.stateLayersOnTertiaryFixedOpacity016,
      stateLayersTertiaryFixedDimOpacity008:
          stateLayersTertiaryFixedDimOpacity008 ??
              this.stateLayersTertiaryFixedDimOpacity008,
      stateLayersTertiaryFixedDimOpacity012:
          stateLayersTertiaryFixedDimOpacity012 ??
              this.stateLayersTertiaryFixedDimOpacity012,
      stateLayersTertiaryFixedDimOpacity016:
          stateLayersTertiaryFixedDimOpacity016 ??
              this.stateLayersTertiaryFixedDimOpacity016,
      stateLayersOnTertiaryFixedVariantOpacity008:
          stateLayersOnTertiaryFixedVariantOpacity008 ??
              this.stateLayersOnTertiaryFixedVariantOpacity008,
      stateLayersOnTertiaryFixedVariantOpacity012:
          stateLayersOnTertiaryFixedVariantOpacity012 ??
              this.stateLayersOnTertiaryFixedVariantOpacity012,
      stateLayersOnTertiaryFixedVariantOpacity016:
          stateLayersOnTertiaryFixedVariantOpacity016 ??
              this.stateLayersOnTertiaryFixedVariantOpacity016,
      stateLayersSurfaceDimOpacity008: stateLayersSurfaceDimOpacity008 ??
          this.stateLayersSurfaceDimOpacity008,
      stateLayersSurfaceDimOpacity012: stateLayersSurfaceDimOpacity012 ??
          this.stateLayersSurfaceDimOpacity012,
      stateLayersSurfaceDimOpacity016: stateLayersSurfaceDimOpacity016 ??
          this.stateLayersSurfaceDimOpacity016,
      stateLayersSurfaceBrightOpacity008: stateLayersSurfaceBrightOpacity008 ??
          this.stateLayersSurfaceBrightOpacity008,
      stateLayersSurfaceBrightOpacity012: stateLayersSurfaceBrightOpacity012 ??
          this.stateLayersSurfaceBrightOpacity012,
      stateLayersSurfaceBrightOpacity016: stateLayersSurfaceBrightOpacity016 ??
          this.stateLayersSurfaceBrightOpacity016,
      stateLayersSurfaceContainerLowestOpacity008:
          stateLayersSurfaceContainerLowestOpacity008 ??
              this.stateLayersSurfaceContainerLowestOpacity008,
      stateLayersSurfaceContainerLowestOpacity012:
          stateLayersSurfaceContainerLowestOpacity012 ??
              this.stateLayersSurfaceContainerLowestOpacity012,
      stateLayersSurfaceContainerLowestOpacity016:
          stateLayersSurfaceContainerLowestOpacity016 ??
              this.stateLayersSurfaceContainerLowestOpacity016,
      stateLayersSurfaceContainerLowOpacity008:
          stateLayersSurfaceContainerLowOpacity008 ??
              this.stateLayersSurfaceContainerLowOpacity008,
      stateLayersSurfaceContainerLowOpacity012:
          stateLayersSurfaceContainerLowOpacity012 ??
              this.stateLayersSurfaceContainerLowOpacity012,
      stateLayersSurfaceContainerLowOpacity016:
          stateLayersSurfaceContainerLowOpacity016 ??
              this.stateLayersSurfaceContainerLowOpacity016,
      stateLayersSurfaceContainerOpacity008:
          stateLayersSurfaceContainerOpacity008 ??
              this.stateLayersSurfaceContainerOpacity008,
      stateLayersSurfaceContainerOpacity012:
          stateLayersSurfaceContainerOpacity012 ??
              this.stateLayersSurfaceContainerOpacity012,
      stateLayersSurfaceContainerOpacity016:
          stateLayersSurfaceContainerOpacity016 ??
              this.stateLayersSurfaceContainerOpacity016,
      stateLayersSurfaceContainerHighOpacity008:
          stateLayersSurfaceContainerHighOpacity008 ??
              this.stateLayersSurfaceContainerHighOpacity008,
      stateLayersSurfaceContainerHighOpacity012:
          stateLayersSurfaceContainerHighOpacity012 ??
              this.stateLayersSurfaceContainerHighOpacity012,
      stateLayersSurfaceContainerHighOpacity016:
          stateLayersSurfaceContainerHighOpacity016 ??
              this.stateLayersSurfaceContainerHighOpacity016,
      stateLayersSurfaceContainerHighestOpacity008:
          stateLayersSurfaceContainerHighestOpacity008 ??
              this.stateLayersSurfaceContainerHighestOpacity008,
      stateLayersSurfaceContainerHighestOpacity012:
          stateLayersSurfaceContainerHighestOpacity012 ??
              this.stateLayersSurfaceContainerHighestOpacity012,
      stateLayersSurfaceContainerHighestOpacity016:
          stateLayersSurfaceContainerHighestOpacity016 ??
              this.stateLayersSurfaceContainerHighestOpacity016,
      stateLayersOnsecondarycontainerOpacity008:
          stateLayersOnsecondarycontainerOpacity008 ??
              this.stateLayersOnsecondarycontainerOpacity008,
      stateLayersOnsecondarycontainerOpacity012:
          stateLayersOnsecondarycontainerOpacity012 ??
              this.stateLayersOnsecondarycontainerOpacity012,
      stateLayersOnsecondarycontainerOpacity016:
          stateLayersOnsecondarycontainerOpacity016 ??
              this.stateLayersOnsecondarycontainerOpacity016,
      stateLayersOutlineOpacity008:
          stateLayersOutlineOpacity008 ?? this.stateLayersOutlineOpacity008,
      stateLayersOutlineOpacity012:
          stateLayersOutlineOpacity012 ?? this.stateLayersOutlineOpacity012,
      stateLayersOutlineOpacity016:
          stateLayersOutlineOpacity016 ?? this.stateLayersOutlineOpacity016,
      stateLayersOutlinevariantOpacity008:
          stateLayersOutlinevariantOpacity008 ??
              this.stateLayersOutlinevariantOpacity008,
      stateLayersOutlinevariantOpacity012:
          stateLayersOutlinevariantOpacity012 ??
              this.stateLayersOutlinevariantOpacity012,
      stateLayersOutlinevariantOpacity016:
          stateLayersOutlinevariantOpacity016 ??
              this.stateLayersOutlinevariantOpacity016,
      stateLayersShadowOpacity008:
          stateLayersShadowOpacity008 ?? this.stateLayersShadowOpacity008,
      stateLayersShadowOpacity012:
          stateLayersShadowOpacity012 ?? this.stateLayersShadowOpacity012,
      stateLayersShadowOpacity016:
          stateLayersShadowOpacity016 ?? this.stateLayersShadowOpacity016,
      stateLayersScrimOpacity008:
          stateLayersScrimOpacity008 ?? this.stateLayersScrimOpacity008,
      stateLayersScrimOpacity012:
          stateLayersScrimOpacity012 ?? this.stateLayersScrimOpacity012,
      stateLayersScrimOpacity016:
          stateLayersScrimOpacity016 ?? this.stateLayersScrimOpacity016,
      stateLayersSurfacevariantOpacity008:
          stateLayersSurfacevariantOpacity008 ??
              this.stateLayersSurfacevariantOpacity008,
      stateLayersSurfacevariantOpacity012:
          stateLayersSurfacevariantOpacity012 ??
              this.stateLayersSurfacevariantOpacity012,
      stateLayersSurfacevariantOpacity016:
          stateLayersSurfacevariantOpacity016 ??
              this.stateLayersSurfacevariantOpacity016,
      stateLayersOnsurfacevariantOpacity008:
          stateLayersOnsurfacevariantOpacity008 ??
              this.stateLayersOnsurfacevariantOpacity008,
      stateLayersOnsurfacevariantOpacity012:
          stateLayersOnsurfacevariantOpacity012 ??
              this.stateLayersOnsurfacevariantOpacity012,
      stateLayersOnsurfacevariantOpacity016:
          stateLayersOnsurfacevariantOpacity016 ??
              this.stateLayersOnsurfacevariantOpacity016,
    );
  }

  @override
  CustomColors lerp(ThemeExtension<CustomColors>? other, double t) {
    if (other is! CustomColors) {
      return this;
    }
    return CustomColors(
      whiteColor: Color.lerp(whiteColor, other.whiteColor, t),
      blackColor: Color.lerp(blackColor, other.blackColor, t),
      sourceSuccess: Color.lerp(sourceSuccess, other.sourceSuccess, t),
      success: Color.lerp(success, other.success, t),
      onSuccess: Color.lerp(onSuccess, other.onSuccess, t),
      successContainer: Color.lerp(successContainer, other.successContainer, t),
      onSuccessContainer:
          Color.lerp(onSuccessContainer, other.onSuccessContainer, t),
      sourceWarning: Color.lerp(sourceWarning, other.sourceWarning, t),
      warning: Color.lerp(warning, other.warning, t),
      onWarning: Color.lerp(onWarning, other.onWarning, t),
      warningContainer: Color.lerp(warningContainer, other.warningContainer, t),
      onWarningContainer:
          Color.lerp(onWarningContainer, other.onWarningContainer, t),
      warningCustom1: Color.lerp(warningCustom1, other.warningCustom1, t),
      sourceError: Color.lerp(sourceError, other.sourceError, t),
      error: Color.lerp(error, other.error, t),
      onError: Color.lerp(onError, other.onError, t),
      errorContainer: Color.lerp(errorContainer, other.errorContainer, t),
      onErrorContainer: Color.lerp(onErrorContainer, other.onErrorContainer, t),
      primaryFixed: Color.lerp(primaryFixed, other.primaryFixed, t),
      onPrimaryFixed: Color.lerp(onPrimaryFixed, other.onPrimaryFixed, t),
      primaryFixedDim: Color.lerp(primaryFixedDim, other.primaryFixedDim, t),
      onPrimaryFixedVariant:
          Color.lerp(onPrimaryFixedVariant, other.onPrimaryFixedVariant, t),
      surfaceDim: Color.lerp(surfaceDim, other.surfaceDim, t),
      surfaceBright: Color.lerp(surfaceBright, other.surfaceBright, t),
      surfaceContainerHighest:
          Color.lerp(surfaceContainerHighest, other.surfaceContainerHighest, t),
      surfaceContainerHigh:
          Color.lerp(surfaceContainerHigh, other.surfaceContainerHigh, t),
      surfaceContainer: Color.lerp(surfaceContainer, other.surfaceContainer, t),
      surfaceContainerLow:
          Color.lerp(surfaceContainerLow, other.surfaceContainerLow, t),
      surfaceContainerLowest:
          Color.lerp(surfaceContainerLowest, other.surfaceContainerLowest, t),
      stateErrorHovered:
          Color.lerp(stateErrorHovered, other.stateErrorHovered, t),
      stateErrorPressed:
          Color.lerp(stateErrorPressed, other.stateErrorPressed, t),
      stateOnErrorPressed:
          Color.lerp(stateOnErrorPressed, other.stateOnErrorPressed, t),
      stateOnPrimaryPressed:
          Color.lerp(stateOnPrimaryPressed, other.stateOnPrimaryPressed, t),
      stateOnPrimaryHovered:
          Color.lerp(stateOnPrimaryHovered, other.stateOnPrimaryHovered, t),
      stateOnSurfaceDisabled:
          Color.lerp(stateOnSurfaceDisabled, other.stateOnSurfaceDisabled, t),
      stateOnSurfaceVariantPressed: Color.lerp(
          stateOnSurfaceVariantPressed, other.stateOnSurfaceVariantPressed, t),
      stateOutlineDisabled:
          Color.lerp(stateOutlineDisabled, other.stateOutlineDisabled, t),
      stateOnPrimaryContainerPressed: Color.lerp(stateOnPrimaryContainerPressed,
          other.stateOnPrimaryContainerPressed, t),
      statePrimaryHovered:
          Color.lerp(statePrimaryHovered, other.statePrimaryHovered, t),
      statePrimaryPressed:
          Color.lerp(statePrimaryPressed, other.statePrimaryPressed, t),
      successColor: Color.lerp(successColor, other.successColor, t),
      successOnColor: Color.lerp(successOnColor, other.successOnColor, t),
      successColorContainer:
          Color.lerp(successColorContainer, other.successColorContainer, t),
      successOnColorContainer:
          Color.lerp(successOnColorContainer, other.successOnColorContainer, t),
      warningColor: Color.lerp(warningColor, other.warningColor, t),
      warningOnColor: Color.lerp(warningOnColor, other.warningOnColor, t),
      warningColorContainer:
          Color.lerp(warningColorContainer, other.warningColorContainer, t),
      warningOnColorContainer:
          Color.lerp(warningOnColorContainer, other.warningOnColorContainer, t),
      avatarMonogramBackground1: Color.lerp(
          avatarMonogramBackground1, other.avatarMonogramBackground1, t),
      avatarMonogramBackground2: Color.lerp(
          avatarMonogramBackground2, other.avatarMonogramBackground2, t),
      avatarMonogramBackground3: Color.lerp(
          avatarMonogramBackground3, other.avatarMonogramBackground3, t),
      avatarMonogramBackground4: Color.lerp(
          avatarMonogramBackground4, other.avatarMonogramBackground4, t),
      avatarMonogramBackground5: Color.lerp(
          avatarMonogramBackground5, other.avatarMonogramBackground5, t),
      avatarMonogramBackground6: Color.lerp(
          avatarMonogramBackground6, other.avatarMonogramBackground6, t),
      avatarMonogramBackground7: Color.lerp(
          avatarMonogramBackground7, other.avatarMonogramBackground7, t),
      avatarMonogramBackground8: Color.lerp(
          avatarMonogramBackground8, other.avatarMonogramBackground8, t),
      stateLayersPrimaryOpacity008: Color.lerp(
          stateLayersPrimaryOpacity008, other.stateLayersPrimaryOpacity008, t),
      stateLayersPrimaryOpacity012: Color.lerp(
          stateLayersPrimaryOpacity012, other.stateLayersPrimaryOpacity012, t),
      stateLayersPrimaryOpacity016: Color.lerp(
          stateLayersPrimaryOpacity016, other.stateLayersPrimaryOpacity016, t),
      stateLayersSurfaceTintOpacity008: Color.lerp(
          stateLayersSurfaceTintOpacity008,
          other.stateLayersSurfaceTintOpacity008,
          t),
      stateLayersSurfaceTintOpacity012: Color.lerp(
          stateLayersSurfaceTintOpacity012,
          other.stateLayersSurfaceTintOpacity012,
          t),
      stateLayersSurfaceTintOpacity016: Color.lerp(
          stateLayersSurfaceTintOpacity016,
          other.stateLayersSurfaceTintOpacity016,
          t),
      stateLayersOnPrimaryOpacity008: Color.lerp(stateLayersOnPrimaryOpacity008,
          other.stateLayersOnPrimaryOpacity008, t),
      stateLayersOnPrimaryOpacity012: Color.lerp(stateLayersOnPrimaryOpacity012,
          other.stateLayersOnPrimaryOpacity012, t),
      stateLayersOnPrimaryOpacity016: Color.lerp(stateLayersOnPrimaryOpacity016,
          other.stateLayersOnPrimaryOpacity016, t),
      stateLayersPrimaryContainerOpacity008: Color.lerp(
          stateLayersPrimaryContainerOpacity008,
          other.stateLayersPrimaryContainerOpacity008,
          t),
      stateLayersPrimaryContainerOpacity012: Color.lerp(
          stateLayersPrimaryContainerOpacity012,
          other.stateLayersPrimaryContainerOpacity012,
          t),
      stateLayersPrimaryContainerOpacity016: Color.lerp(
          stateLayersPrimaryContainerOpacity016,
          other.stateLayersPrimaryContainerOpacity016,
          t),
      stateLayersOnPrimaryContainerOpacity008: Color.lerp(
          stateLayersOnPrimaryContainerOpacity008,
          other.stateLayersOnPrimaryContainerOpacity008,
          t),
      stateLayersOnPrimaryContainerOpacity012: Color.lerp(
          stateLayersOnPrimaryContainerOpacity012,
          other.stateLayersOnPrimaryContainerOpacity012,
          t),
      stateLayersOnPrimaryContainerOpacity016: Color.lerp(
          stateLayersOnPrimaryContainerOpacity016,
          other.stateLayersOnPrimaryContainerOpacity016,
          t),
      stateLayersSecondaryOpacity008: Color.lerp(stateLayersSecondaryOpacity008,
          other.stateLayersSecondaryOpacity008, t),
      stateLayersSecondaryOpacity012: Color.lerp(stateLayersSecondaryOpacity012,
          other.stateLayersSecondaryOpacity012, t),
      stateLayersSecondaryOpacity016: Color.lerp(stateLayersSecondaryOpacity016,
          other.stateLayersSecondaryOpacity016, t),
      stateLayersOnSecondaryOpacity008: Color.lerp(
          stateLayersOnSecondaryOpacity008,
          other.stateLayersOnSecondaryOpacity008,
          t),
      stateLayersOnSecondaryOpacity012: Color.lerp(
          stateLayersOnSecondaryOpacity012,
          other.stateLayersOnSecondaryOpacity012,
          t),
      stateLayersOnSecondaryOpacity016: Color.lerp(
          stateLayersOnSecondaryOpacity016,
          other.stateLayersOnSecondaryOpacity016,
          t),
      stateLayersSecondaryContainerOpacity008: Color.lerp(
          stateLayersSecondaryContainerOpacity008,
          other.stateLayersSecondaryContainerOpacity008,
          t),
      stateLayersSecondaryContainerOpacity012: Color.lerp(
          stateLayersSecondaryContainerOpacity012,
          other.stateLayersSecondaryContainerOpacity012,
          t),
      stateLayersSecondaryContainerOpacity016: Color.lerp(
          stateLayersSecondaryContainerOpacity016,
          other.stateLayersSecondaryContainerOpacity016,
          t),
      stateLayersTertiaryOpacity008: Color.lerp(stateLayersTertiaryOpacity008,
          other.stateLayersTertiaryOpacity008, t),
      stateLayersTertiaryOpacity012: Color.lerp(stateLayersTertiaryOpacity012,
          other.stateLayersTertiaryOpacity012, t),
      stateLayersTertiaryOpacity016: Color.lerp(stateLayersTertiaryOpacity016,
          other.stateLayersTertiaryOpacity016, t),
      stateLayersOnTertiaryOpacity008: Color.lerp(
          stateLayersOnTertiaryOpacity008,
          other.stateLayersOnTertiaryOpacity008,
          t),
      stateLayersOnTertiaryOpacity012: Color.lerp(
          stateLayersOnTertiaryOpacity012,
          other.stateLayersOnTertiaryOpacity012,
          t),
      stateLayersOnTertiaryOpacity016: Color.lerp(
          stateLayersOnTertiaryOpacity016,
          other.stateLayersOnTertiaryOpacity016,
          t),
      stateLayersTertiaryContainerOpacity008: Color.lerp(
          stateLayersTertiaryContainerOpacity008,
          other.stateLayersTertiaryContainerOpacity008,
          t),
      stateLayersTertiaryContainerOpacity012: Color.lerp(
          stateLayersTertiaryContainerOpacity012,
          other.stateLayersTertiaryContainerOpacity012,
          t),
      stateLayersTertiaryContainerOpacity016: Color.lerp(
          stateLayersTertiaryContainerOpacity016,
          other.stateLayersTertiaryContainerOpacity016,
          t),
      stateLayersOnTertiaryContainerOpacity008: Color.lerp(
          stateLayersOnTertiaryContainerOpacity008,
          other.stateLayersOnTertiaryContainerOpacity008,
          t),
      stateLayersOnTertiaryContainerOpacity012: Color.lerp(
          stateLayersOnTertiaryContainerOpacity012,
          other.stateLayersOnTertiaryContainerOpacity012,
          t),
      stateLayersOnTertiaryContainerOpacity016: Color.lerp(
          stateLayersOnTertiaryContainerOpacity016,
          other.stateLayersOnTertiaryContainerOpacity016,
          t),
      stateLayersErrorOpacity008: Color.lerp(
          stateLayersErrorOpacity008, other.stateLayersErrorOpacity008, t),
      stateLayersErrorOpacity012: Color.lerp(
          stateLayersErrorOpacity012, other.stateLayersErrorOpacity012, t),
      stateLayersErrorOpacity016: Color.lerp(
          stateLayersErrorOpacity016, other.stateLayersErrorOpacity016, t),
      stateLayersOnErrorOpacity008: Color.lerp(
          stateLayersOnErrorOpacity008, other.stateLayersOnErrorOpacity008, t),
      stateLayersOnErrorOpacity012: Color.lerp(
          stateLayersOnErrorOpacity012, other.stateLayersOnErrorOpacity012, t),
      stateLayersOnErrorOpacity016: Color.lerp(
          stateLayersOnErrorOpacity016, other.stateLayersOnErrorOpacity016, t),
      stateLayersErrorContainerOpacity008: Color.lerp(
          stateLayersErrorContainerOpacity008,
          other.stateLayersErrorContainerOpacity008,
          t),
      stateLayersErrorContainerOpacity012: Color.lerp(
          stateLayersErrorContainerOpacity012,
          other.stateLayersErrorContainerOpacity012,
          t),
      stateLayersErrorContainerOpacity016: Color.lerp(
          stateLayersErrorContainerOpacity016,
          other.stateLayersErrorContainerOpacity016,
          t),
      stateLayersOnErrorContainerOpacity008: Color.lerp(
          stateLayersOnErrorContainerOpacity008,
          other.stateLayersOnErrorContainerOpacity008,
          t),
      stateLayersOnErrorContainerOpacity012: Color.lerp(
          stateLayersOnErrorContainerOpacity012,
          other.stateLayersOnErrorContainerOpacity012,
          t),
      stateLayersOnErrorContainerOpacity016: Color.lerp(
          stateLayersOnErrorContainerOpacity016,
          other.stateLayersOnErrorContainerOpacity016,
          t),
      stateLayersBackgroundOpacity008: Color.lerp(
          stateLayersBackgroundOpacity008,
          other.stateLayersBackgroundOpacity008,
          t),
      stateLayersBackgroundOpacity012: Color.lerp(
          stateLayersBackgroundOpacity012,
          other.stateLayersBackgroundOpacity012,
          t),
      stateLayersBackgroundOpacity016: Color.lerp(
          stateLayersBackgroundOpacity016,
          other.stateLayersBackgroundOpacity016,
          t),
      stateLayersOnBackgroundOpacity008: Color.lerp(
          stateLayersOnBackgroundOpacity008,
          other.stateLayersOnBackgroundOpacity008,
          t),
      stateLayersOnBackgroundOpacity012: Color.lerp(
          stateLayersOnBackgroundOpacity012,
          other.stateLayersOnBackgroundOpacity012,
          t),
      stateLayersOnBackgroundOpacity016: Color.lerp(
          stateLayersOnBackgroundOpacity016,
          other.stateLayersOnBackgroundOpacity016,
          t),
      stateLayersSurfaceOpacity008: Color.lerp(
          stateLayersSurfaceOpacity008, other.stateLayersSurfaceOpacity008, t),
      stateLayersSurfaceOpacity012: Color.lerp(
          stateLayersSurfaceOpacity012, other.stateLayersSurfaceOpacity012, t),
      stateLayersSurfaceOpacity016: Color.lerp(
          stateLayersSurfaceOpacity016, other.stateLayersSurfaceOpacity016, t),
      stateLayersOnSurfaceOpacity008: Color.lerp(stateLayersOnSurfaceOpacity008,
          other.stateLayersOnSurfaceOpacity008, t),
      stateLayersOnSurfaceOpacity012: Color.lerp(stateLayersOnSurfaceOpacity012,
          other.stateLayersOnSurfaceOpacity012, t),
      stateLayersOnSurfaceOpacity016: Color.lerp(stateLayersOnSurfaceOpacity016,
          other.stateLayersOnSurfaceOpacity016, t),
      stateLayersInverseSurfaceOpacity008: Color.lerp(
          stateLayersInverseSurfaceOpacity008,
          other.stateLayersInverseSurfaceOpacity008,
          t),
      stateLayersInverseSurfaceOpacity012: Color.lerp(
          stateLayersInverseSurfaceOpacity012,
          other.stateLayersInverseSurfaceOpacity012,
          t),
      stateLayersInverseSurfaceOpacity016: Color.lerp(
          stateLayersInverseSurfaceOpacity016,
          other.stateLayersInverseSurfaceOpacity016,
          t),
      stateLayersInverseOnSurfaceOpacity008: Color.lerp(
          stateLayersInverseOnSurfaceOpacity008,
          other.stateLayersInverseOnSurfaceOpacity008,
          t),
      stateLayersInverseOnSurfaceOpacity012: Color.lerp(
          stateLayersInverseOnSurfaceOpacity012,
          other.stateLayersInverseOnSurfaceOpacity012,
          t),
      stateLayersInverseOnSurfaceOpacity016: Color.lerp(
          stateLayersInverseOnSurfaceOpacity016,
          other.stateLayersInverseOnSurfaceOpacity016,
          t),
      stateLayersInversePrimaryOpacity008: Color.lerp(
          stateLayersInversePrimaryOpacity008,
          other.stateLayersInversePrimaryOpacity008,
          t),
      stateLayersInversePrimaryOpacity012: Color.lerp(
          stateLayersInversePrimaryOpacity012,
          other.stateLayersInversePrimaryOpacity012,
          t),
      stateLayersInversePrimaryOpacity016: Color.lerp(
          stateLayersInversePrimaryOpacity016,
          other.stateLayersInversePrimaryOpacity016,
          t),
      stateLayersPrimaryFixedOpacity008: Color.lerp(
          stateLayersPrimaryFixedOpacity008,
          other.stateLayersPrimaryFixedOpacity008,
          t),
      stateLayersPrimaryFixedOpacity012: Color.lerp(
          stateLayersPrimaryFixedOpacity012,
          other.stateLayersPrimaryFixedOpacity012,
          t),
      stateLayersPrimaryFixedOpacity016: Color.lerp(
          stateLayersPrimaryFixedOpacity016,
          other.stateLayersPrimaryFixedOpacity016,
          t),
      stateLayersOnPrimaryFixedOpacity008: Color.lerp(
          stateLayersOnPrimaryFixedOpacity008,
          other.stateLayersOnPrimaryFixedOpacity008,
          t),
      stateLayersOnPrimaryFixedOpacity012: Color.lerp(
          stateLayersOnPrimaryFixedOpacity012,
          other.stateLayersOnPrimaryFixedOpacity012,
          t),
      stateLayersOnPrimaryFixedOpacity016: Color.lerp(
          stateLayersOnPrimaryFixedOpacity016,
          other.stateLayersOnPrimaryFixedOpacity016,
          t),
      stateLayersPrimaryFixedDimOpacity008: Color.lerp(
          stateLayersPrimaryFixedDimOpacity008,
          other.stateLayersPrimaryFixedDimOpacity008,
          t),
      stateLayersPrimaryFixedDimOpacity012: Color.lerp(
          stateLayersPrimaryFixedDimOpacity012,
          other.stateLayersPrimaryFixedDimOpacity012,
          t),
      stateLayersPrimaryFixedDimOpacity016: Color.lerp(
          stateLayersPrimaryFixedDimOpacity016,
          other.stateLayersPrimaryFixedDimOpacity016,
          t),
      stateLayersOnPrimaryFixedVariantOpacity008: Color.lerp(
          stateLayersOnPrimaryFixedVariantOpacity008,
          other.stateLayersOnPrimaryFixedVariantOpacity008,
          t),
      stateLayersOnPrimaryFixedVariantOpacity012: Color.lerp(
          stateLayersOnPrimaryFixedVariantOpacity012,
          other.stateLayersOnPrimaryFixedVariantOpacity012,
          t),
      stateLayersOnPrimaryFixedVariantOpacity016: Color.lerp(
          stateLayersOnPrimaryFixedVariantOpacity016,
          other.stateLayersOnPrimaryFixedVariantOpacity016,
          t),
      stateLayersSecondaryFixedOpacity008: Color.lerp(
          stateLayersSecondaryFixedOpacity008,
          other.stateLayersSecondaryFixedOpacity008,
          t),
      stateLayersSecondaryFixedOpacity012: Color.lerp(
          stateLayersSecondaryFixedOpacity012,
          other.stateLayersSecondaryFixedOpacity012,
          t),
      stateLayersSecondaryFixedOpacity016: Color.lerp(
          stateLayersSecondaryFixedOpacity016,
          other.stateLayersSecondaryFixedOpacity016,
          t),
      stateLayersOnSecondaryFixedOpacity008: Color.lerp(
          stateLayersOnSecondaryFixedOpacity008,
          other.stateLayersOnSecondaryFixedOpacity008,
          t),
      stateLayersOnSecondaryFixedOpacity012: Color.lerp(
          stateLayersOnSecondaryFixedOpacity012,
          other.stateLayersOnSecondaryFixedOpacity012,
          t),
      stateLayersOnSecondaryFixedOpacity016: Color.lerp(
          stateLayersOnSecondaryFixedOpacity016,
          other.stateLayersOnSecondaryFixedOpacity016,
          t),
      stateLayersSecondaryFixedDimOpacity008: Color.lerp(
          stateLayersSecondaryFixedDimOpacity008,
          other.stateLayersSecondaryFixedDimOpacity008,
          t),
      stateLayersSecondaryFixedDimOpacity012: Color.lerp(
          stateLayersSecondaryFixedDimOpacity012,
          other.stateLayersSecondaryFixedDimOpacity012,
          t),
      stateLayersSecondaryFixedDimOpacity016: Color.lerp(
          stateLayersSecondaryFixedDimOpacity016,
          other.stateLayersSecondaryFixedDimOpacity016,
          t),
      stateLayersOnSecondaryFixedVariantOpacity008: Color.lerp(
          stateLayersOnSecondaryFixedVariantOpacity008,
          other.stateLayersOnSecondaryFixedVariantOpacity008,
          t),
      stateLayersOnSecondaryFixedVariantOpacity012: Color.lerp(
          stateLayersOnSecondaryFixedVariantOpacity012,
          other.stateLayersOnSecondaryFixedVariantOpacity012,
          t),
      stateLayersOnSecondaryFixedVariantOpacity016: Color.lerp(
          stateLayersOnSecondaryFixedVariantOpacity016,
          other.stateLayersOnSecondaryFixedVariantOpacity016,
          t),
      stateLayersTertiaryFixedOpacity008: Color.lerp(
          stateLayersTertiaryFixedOpacity008,
          other.stateLayersTertiaryFixedOpacity008,
          t),
      stateLayersTertiaryFixedOpacity012: Color.lerp(
          stateLayersTertiaryFixedOpacity012,
          other.stateLayersTertiaryFixedOpacity012,
          t),
      stateLayersTertiaryFixedOpacity016: Color.lerp(
          stateLayersTertiaryFixedOpacity016,
          other.stateLayersTertiaryFixedOpacity016,
          t),
      stateLayersOnTertiaryFixedOpacity008: Color.lerp(
          stateLayersOnTertiaryFixedOpacity008,
          other.stateLayersOnTertiaryFixedOpacity008,
          t),
      stateLayersOnTertiaryFixedOpacity012: Color.lerp(
          stateLayersOnTertiaryFixedOpacity012,
          other.stateLayersOnTertiaryFixedOpacity012,
          t),
      stateLayersOnTertiaryFixedOpacity016: Color.lerp(
          stateLayersOnTertiaryFixedOpacity016,
          other.stateLayersOnTertiaryFixedOpacity016,
          t),
      stateLayersTertiaryFixedDimOpacity008: Color.lerp(
          stateLayersTertiaryFixedDimOpacity008,
          other.stateLayersTertiaryFixedDimOpacity008,
          t),
      stateLayersTertiaryFixedDimOpacity012: Color.lerp(
          stateLayersTertiaryFixedDimOpacity012,
          other.stateLayersTertiaryFixedDimOpacity012,
          t),
      stateLayersTertiaryFixedDimOpacity016: Color.lerp(
          stateLayersTertiaryFixedDimOpacity016,
          other.stateLayersTertiaryFixedDimOpacity016,
          t),
      stateLayersOnTertiaryFixedVariantOpacity008: Color.lerp(
          stateLayersOnTertiaryFixedVariantOpacity008,
          other.stateLayersOnTertiaryFixedVariantOpacity008,
          t),
      stateLayersOnTertiaryFixedVariantOpacity012: Color.lerp(
          stateLayersOnTertiaryFixedVariantOpacity012,
          other.stateLayersOnTertiaryFixedVariantOpacity012,
          t),
      stateLayersOnTertiaryFixedVariantOpacity016: Color.lerp(
          stateLayersOnTertiaryFixedVariantOpacity016,
          other.stateLayersOnTertiaryFixedVariantOpacity016,
          t),
      stateLayersSurfaceDimOpacity008: Color.lerp(
          stateLayersSurfaceDimOpacity008,
          other.stateLayersSurfaceDimOpacity008,
          t),
      stateLayersSurfaceDimOpacity012: Color.lerp(
          stateLayersSurfaceDimOpacity012,
          other.stateLayersSurfaceDimOpacity012,
          t),
      stateLayersSurfaceDimOpacity016: Color.lerp(
          stateLayersSurfaceDimOpacity016,
          other.stateLayersSurfaceDimOpacity016,
          t),
      stateLayersSurfaceBrightOpacity008: Color.lerp(
          stateLayersSurfaceBrightOpacity008,
          other.stateLayersSurfaceBrightOpacity008,
          t),
      stateLayersSurfaceBrightOpacity012: Color.lerp(
          stateLayersSurfaceBrightOpacity012,
          other.stateLayersSurfaceBrightOpacity012,
          t),
      stateLayersSurfaceBrightOpacity016: Color.lerp(
          stateLayersSurfaceBrightOpacity016,
          other.stateLayersSurfaceBrightOpacity016,
          t),
      stateLayersSurfaceContainerLowestOpacity008: Color.lerp(
          stateLayersSurfaceContainerLowestOpacity008,
          other.stateLayersSurfaceContainerLowestOpacity008,
          t),
      stateLayersSurfaceContainerLowestOpacity012: Color.lerp(
          stateLayersSurfaceContainerLowestOpacity012,
          other.stateLayersSurfaceContainerLowestOpacity012,
          t),
      stateLayersSurfaceContainerLowestOpacity016: Color.lerp(
          stateLayersSurfaceContainerLowestOpacity016,
          other.stateLayersSurfaceContainerLowestOpacity016,
          t),
      stateLayersSurfaceContainerLowOpacity008: Color.lerp(
          stateLayersSurfaceContainerLowOpacity008,
          other.stateLayersSurfaceContainerLowOpacity008,
          t),
      stateLayersSurfaceContainerLowOpacity012: Color.lerp(
          stateLayersSurfaceContainerLowOpacity012,
          other.stateLayersSurfaceContainerLowOpacity012,
          t),
      stateLayersSurfaceContainerLowOpacity016: Color.lerp(
          stateLayersSurfaceContainerLowOpacity016,
          other.stateLayersSurfaceContainerLowOpacity016,
          t),
      stateLayersSurfaceContainerOpacity008: Color.lerp(
          stateLayersSurfaceContainerOpacity008,
          other.stateLayersSurfaceContainerOpacity008,
          t),
      stateLayersSurfaceContainerOpacity012: Color.lerp(
          stateLayersSurfaceContainerOpacity012,
          other.stateLayersSurfaceContainerOpacity012,
          t),
      stateLayersSurfaceContainerOpacity016: Color.lerp(
          stateLayersSurfaceContainerOpacity016,
          other.stateLayersSurfaceContainerOpacity016,
          t),
      stateLayersSurfaceContainerHighOpacity008: Color.lerp(
          stateLayersSurfaceContainerHighOpacity008,
          other.stateLayersSurfaceContainerHighOpacity008,
          t),
      stateLayersSurfaceContainerHighOpacity012: Color.lerp(
          stateLayersSurfaceContainerHighOpacity012,
          other.stateLayersSurfaceContainerHighOpacity012,
          t),
      stateLayersSurfaceContainerHighOpacity016: Color.lerp(
          stateLayersSurfaceContainerHighOpacity016,
          other.stateLayersSurfaceContainerHighOpacity016,
          t),
      stateLayersSurfaceContainerHighestOpacity008: Color.lerp(
          stateLayersSurfaceContainerHighestOpacity008,
          other.stateLayersSurfaceContainerHighestOpacity008,
          t),
      stateLayersSurfaceContainerHighestOpacity012: Color.lerp(
          stateLayersSurfaceContainerHighestOpacity012,
          other.stateLayersSurfaceContainerHighestOpacity012,
          t),
      stateLayersSurfaceContainerHighestOpacity016: Color.lerp(
          stateLayersSurfaceContainerHighestOpacity016,
          other.stateLayersSurfaceContainerHighestOpacity016,
          t),
      stateLayersOnsecondarycontainerOpacity008: Color.lerp(
          stateLayersOnsecondarycontainerOpacity008,
          other.stateLayersOnsecondarycontainerOpacity008,
          t),
      stateLayersOnsecondarycontainerOpacity012: Color.lerp(
          stateLayersOnsecondarycontainerOpacity012,
          other.stateLayersOnsecondarycontainerOpacity012,
          t),
      stateLayersOnsecondarycontainerOpacity016: Color.lerp(
          stateLayersOnsecondarycontainerOpacity016,
          other.stateLayersOnsecondarycontainerOpacity016,
          t),
      stateLayersOutlineOpacity008: Color.lerp(
          stateLayersOutlineOpacity008, other.stateLayersOutlineOpacity008, t),
      stateLayersOutlineOpacity012: Color.lerp(
          stateLayersOutlineOpacity012, other.stateLayersOutlineOpacity012, t),
      stateLayersOutlineOpacity016: Color.lerp(
          stateLayersOutlineOpacity016, other.stateLayersOutlineOpacity016, t),
      stateLayersOutlinevariantOpacity008: Color.lerp(
          stateLayersOutlinevariantOpacity008,
          other.stateLayersOutlinevariantOpacity008,
          t),
      stateLayersOutlinevariantOpacity012: Color.lerp(
          stateLayersOutlinevariantOpacity012,
          other.stateLayersOutlinevariantOpacity012,
          t),
      stateLayersOutlinevariantOpacity016: Color.lerp(
          stateLayersOutlinevariantOpacity016,
          other.stateLayersOutlinevariantOpacity016,
          t),
      stateLayersShadowOpacity008: Color.lerp(
          stateLayersShadowOpacity008, other.stateLayersShadowOpacity008, t),
      stateLayersShadowOpacity012: Color.lerp(
          stateLayersShadowOpacity012, other.stateLayersShadowOpacity012, t),
      stateLayersShadowOpacity016: Color.lerp(
          stateLayersShadowOpacity016, other.stateLayersShadowOpacity016, t),
      stateLayersScrimOpacity008: Color.lerp(
          stateLayersScrimOpacity008, other.stateLayersScrimOpacity008, t),
      stateLayersScrimOpacity012: Color.lerp(
          stateLayersScrimOpacity012, other.stateLayersScrimOpacity012, t),
      stateLayersScrimOpacity016: Color.lerp(
          stateLayersScrimOpacity016, other.stateLayersScrimOpacity016, t),
      stateLayersSurfacevariantOpacity008: Color.lerp(
          stateLayersSurfacevariantOpacity008,
          other.stateLayersSurfacevariantOpacity008,
          t),
      stateLayersSurfacevariantOpacity012: Color.lerp(
          stateLayersSurfacevariantOpacity012,
          other.stateLayersSurfacevariantOpacity012,
          t),
      stateLayersSurfacevariantOpacity016: Color.lerp(
          stateLayersSurfacevariantOpacity016,
          other.stateLayersSurfacevariantOpacity016,
          t),
      stateLayersOnsurfacevariantOpacity008: Color.lerp(
          stateLayersOnsurfacevariantOpacity008,
          other.stateLayersOnsurfacevariantOpacity008,
          t),
      stateLayersOnsurfacevariantOpacity012: Color.lerp(
          stateLayersOnsurfacevariantOpacity012,
          other.stateLayersOnsurfacevariantOpacity012,
          t),
      stateLayersOnsurfacevariantOpacity016: Color.lerp(
          stateLayersOnsurfacevariantOpacity016,
          other.stateLayersOnsurfacevariantOpacity016,
          t),
    );
  }

  /// Returns an instance of [CustomColors] in which the following custom
  /// colors are harmonized with [dynamic]'s [ColorScheme.primary].
  ///
  /// See also:
  ///   * <https://m3.material.io/styles/color/the-color-system/custom-colors#harmonization>
  CustomColors harmonized(ColorScheme dynamic) {
    return copyWith();
  }
}
