part of '../svn_pro_mobile_flutter.dart';

enum SvnProShowDatePickerMode { day, input, rangeFull, rangeInput }

class SvnProDateTime {
  final DateTime? date;
  final DateTimeRange? range;

  const SvnProDateTime({this.date, this.range});
}

Future<SvnProDateTime?> svnProShowDatePicker<T>(
  context, {
  mode = SvnProShowDatePickerMode.day,
  firstDate,
  lastDate,
  helpText,
  DateTime? initialDate,
}) async {
  firstDate ??= DateTime(1970);
  lastDate ??= DateTime(2030);

  switch (mode) {
    case SvnProShowDatePickerMode.day:
      final ret = await showDatePicker(
        initialDate: initialDate,
        context: context,
        firstDate: firstDate,
        lastDate: lastDate,
        initialEntryMode: DatePickerEntryMode.calendar,
        helpText: helpText,
      );
      return SvnProDateTime(date: ret);
    case SvnProShowDatePickerMode.input:
      final ret = await showDatePicker(
          initialDate: initialDate,
          context: context,
          firstDate: firstDate,
          lastDate: lastDate,
          initialEntryMode: DatePickerEntryMode.input,
          helpText: helpText,
          keyboardType: TextInputType.text);
      return SvnProDateTime(date: ret);
    case SvnProShowDatePickerMode.rangeFull:
      final ret = await showDateRangePicker(
        context: context,
        firstDate: firstDate,
        lastDate: lastDate,
        initialEntryMode: DatePickerEntryMode.calendar,
        helpText: helpText,
      );
      return SvnProDateTime(range: ret);
    case SvnProShowDatePickerMode.rangeInput:
      final ret = await showDateRangePicker(
          context: context,
          firstDate: firstDate,
          lastDate: lastDate,
          initialEntryMode: DatePickerEntryMode.input,
          helpText: helpText,
          keyboardType: TextInputType.text);
      return SvnProDateTime(range: ret);
  }
  return null;
}
