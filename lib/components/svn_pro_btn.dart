part of '../svn_pro_mobile_flutter.dart';

enum SvnProBtnVariants { filled, outlined, text, elevated, tonal }

enum SvnProBtnState { enabled, disabled, loading }

enum SvnProBtnIconPosition { left, right }

class SvnProBtn extends StatelessWidget {
  final SvnProBtnIconPosition svnProBtnIconPosition;
  final String? text;
  final SvnProBtnState? svnProBtnState;
  final Color? forgroundColor;
  final Color? backgroundColor;
  final Widget? icon;
  final IconData? iconData;
  final Widget? child;
  final void Function()? onPressed;
  final SvnProBtnVariants variant;

  const SvnProBtn({
    super.key,
    this.variant = SvnProBtnVariants.filled,
    this.child,
    this.onPressed,
    this.icon,
    this.iconData,
    this.forgroundColor,
    this.backgroundColor,
    this.svnProBtnState = SvnProBtnState.enabled,
    this.text,
    this.svnProBtnIconPosition = SvnProBtnIconPosition.left,
  });

  @override
  Widget build(BuildContext context) {
    switch (variant) {
      case SvnProBtnVariants.filled:
        if (svnProBtnState == SvnProBtnState.loading) {
          return loadingBtn(context);
        }

        if (icon != null || iconData != null) {
          return FilledButton.icon(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            label: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
            icon: icon ??
                Icon(
                  iconData,
                  color: calculateTextColor(context),
                  size: 18,
                ),
          );
        } else {
          return FilledButton(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            child: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
          );
        }
      case SvnProBtnVariants.outlined:
        if (svnProBtnState == SvnProBtnState.loading) {
          return loadingBtn(context);
        }

        if (icon != null || iconData != null) {
          return OutlinedButton.icon(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            label: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
            icon: icon ??
                Icon(
                  iconData,
                  color: calculateTextColor(context),
                  size: 18,
                ),
          );
        } else {
          return OutlinedButton(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            child: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child,
          );
        }
      case SvnProBtnVariants.text:
        if (svnProBtnState == SvnProBtnState.loading) {
          return loadingBtn(context);
        }

        if (icon != null || iconData != null) {
          return TextButton.icon(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            label: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
            icon: icon ??
                Icon(
                  iconData,
                  color: calculateTextColor(context),
                  size: 18,
                ),
          );
        } else {
          return TextButton(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            child: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
          );
        }
      case SvnProBtnVariants.elevated:
        if (svnProBtnState == SvnProBtnState.loading) {
          return loadingBtn(context);
        }

        if (icon != null || iconData != null) {
          return ElevatedButton.icon(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            label: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
            icon: icon ??
                Icon(
                  iconData,
                  color: calculateTextColor(context),
                  size: 18,
                ),
          );
        } else {
          return ElevatedButton(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            child: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
          );
        }
      case SvnProBtnVariants.tonal:
        if (svnProBtnState == SvnProBtnState.loading) {
          return loadingBtn(context);
        }

        if (icon != null || iconData != null) {
          return FilledButton.tonalIcon(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            label: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
            icon: icon ??
                Icon(
                  iconData,
                  color: calculateTextColor(context),
                  size: 18,
                ),
          );
        } else {
          return FilledButton.tonal(
            onPressed: svnProBtnState == SvnProBtnState.disabled
                ? null
                : (onPressed ?? () {}),
            child: text != null
                ? SvnProText(
                    text: text!.toCapitalized(),
                    size: SvnProTextSize.bodyMedium,
                    weight: SvnProTextWeight.medium,
                    color: calculateTextColor(context),
                  )
                : child!,
          );
        }
    }
  }

  Widget loadingBtn(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        // foregroundColor: MaterialStateProperty.all<Color>(
        //   forgroundColor ?? calculateBoxColor(context),
        // ),
        backgroundColor: MaterialStateProperty.all<Color>(
          backgroundColor ?? calculateBoxColor(context),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(120),
            ),
            side: variant == SvnProBtnVariants.outlined
                ? BorderSide(
                    color: Theme.of(context)
                            .extension<CustomColors>()
                            ?.stateLayersOnSurfaceOpacity012 ??
                        Colors.red,
                  )
                : BorderSide.none,
          ),
        ),
      ),
      onPressed: null,
      child: Container(
        width: 24,
        height: 24,
        padding: const EdgeInsets.all(2.0),
        child: CircularProgressIndicator(
          color: calculateTextColor(context),
          strokeWidth: 2,
        ),
      ),
    );
  }

  Color calculateBoxColor(BuildContext context) {
    switch (variant) {
      case SvnProBtnVariants.filled:
      case SvnProBtnVariants.elevated:
      case SvnProBtnVariants.tonal:
        return Theme.of(context)
                .extension<CustomColors>()
                ?.stateLayersOnSurfaceOpacity012 ??
            Colors.red;
      case SvnProBtnVariants.outlined:
      case SvnProBtnVariants.text:
        return Colors.transparent;
      default:
        return Colors.red;
    }
  }

  Color calculateTextColor(BuildContext context) {
    switch (variant) {
      case SvnProBtnVariants.filled:
        if (svnProBtnState == SvnProBtnState.disabled) {
          return Theme.of(context).colorScheme.onSurface.withOpacity(0.38);
        } else if (svnProBtnState == SvnProBtnState.loading) {
          return Theme.of(context).colorScheme.primary;
        }
        return Theme.of(context).colorScheme.onPrimary;
      case SvnProBtnVariants.outlined:
      case SvnProBtnVariants.text:
      case SvnProBtnVariants.elevated:
        if (svnProBtnState == SvnProBtnState.disabled) {
          return Theme.of(context).colorScheme.onSurface.withOpacity(0.38);
        } else if (svnProBtnState == SvnProBtnState.loading) {
          return Theme.of(context).colorScheme.primary;
        }
        return Theme.of(context).colorScheme.primary;
      case SvnProBtnVariants.tonal:
        if (svnProBtnState == SvnProBtnState.disabled) {
          return Theme.of(context).colorScheme.onSurface.withOpacity(0.38);
        } else if (svnProBtnState == SvnProBtnState.loading) {
          return Theme.of(context).colorScheme.primary;
        }
        return Theme.of(context).colorScheme.onSecondaryContainer;
      default:
        return Colors.red;
    }
  }

  SvnProBtn copyWith({
    SvnProBtnVariants? variant,
    Widget? child,
    VoidCallback? onPressed,
    SvnProIcon? icon,
    IconData? iconData,
    Color? forgroundColor,
    Color? backgroundColor,
    SvnProBtnState? svnProBtnState,
    String? text,
    SvnProBtnIconPosition? svnProBtnIconPosition,
  }) {
    return SvnProBtn(
      variant: variant ?? this.variant,
      onPressed: onPressed ?? this.onPressed,
      icon: icon ?? this.icon,
      iconData: iconData ?? this.iconData,
      forgroundColor: forgroundColor ?? this.forgroundColor,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      svnProBtnState: svnProBtnState ?? this.svnProBtnState,
      text: text ?? this.text,
      svnProBtnIconPosition:
          svnProBtnIconPosition ?? this.svnProBtnIconPosition,
      child: child ?? this.child,
    );
  }
}
