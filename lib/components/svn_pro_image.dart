part of '../svn_pro_mobile_flutter.dart';

class SvnProImage extends StatelessWidget {
  final String imageUrl;
  final BoxFit fit;
  final double? width;
  final double? height;

  /// Optional builder to further customize the display of the image.
  final ImageWidgetBuilder? imageBuilder;

  /// Widget displayed while the target [imageUrl] is loading.
  final PlaceholderWidgetBuilder? placeholder;

  /// Widget displayed while the target [imageUrl] failed loading.
  final LoadingErrorWidgetBuilder? errorWidget;

  const SvnProImage({
    super.key,
    required this.imageUrl,
    this.fit = BoxFit.cover,
    this.width,
    this.height,
    this.imageBuilder,
    this.placeholder,
    this.errorWidget,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      fit: BoxFit.cover,
      imageUrl: imageUrl,
      width: width,
      height: height,
      placeholder: placeholder ??
          (context, url) => ClipRRect(
                borderRadius: BorderRadius.circular(10000),
                child: SizedBox(
                  width: width,
                  height: height,
                  child: const SvnProProgress(),
                ),
              ),
      errorWidget: errorWidget ??
          (context, url, error) => ClipRRect(
                borderRadius: BorderRadius.circular(10000),
                child: SizedBox(
                  width: width,
                  height: height,
                  child: const SvnProIcon(
                    noto: Noto.warning,
                    size: 24,
                  ),
                ),
              ),
    );
  }
}
