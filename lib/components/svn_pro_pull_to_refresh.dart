part of '../svn_pro_mobile_flutter.dart';

class SvnProPullToRefresh extends StatelessWidget {
  final MaterialIndicatorBuilder indicatorBuilder;
  final AsyncCallback onRefresh;
  final Widget child;

  const SvnProPullToRefresh({
    super.key,
    required this.indicatorBuilder,
    required this.onRefresh,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return CustomMaterialIndicator(
      onRefresh: onRefresh, // Your refresh logic
      indicatorBuilder: indicatorBuilder,
      child: child,
    );
  }
}
