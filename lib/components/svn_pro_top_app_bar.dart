part of '../svn_pro_mobile_flutter.dart';

/// A highly customizable and reusable top app bar widget for the SvnPro mobile library
///
/// Key Features:
/// - Customizable leading widget (e.g., back button).
/// - Configurable title with adjustable text size.
/// - Support for actions (e.g., icons or buttons on the right side of the app bar).
/// - Optional elevation for the app bar.
/// - Customizable status bar color.
/// - Support for a sticky bottom widget (e.g., a bottom navigation bar).
/// - Optional floating action button.
///
/// This widget is designed to be flexible and reusable, making it suitable for a wide
/// range of use cases in modern mobile applications.
class SvnProTopAppBar extends StatelessWidget {
  // Constants for elevation values
  /// The elevation value used when the app bar has elevation enabled.
  /// This corresponds to the maximum elevation token defined in the design system.
  static const double _elevationValue =
      SvnProElevationTokens.elevationMax; // 100.0

  /// The elevation value used when the app bar has no elevation.
  /// This corresponds to the "none" elevation token defined in the design system.
  static const double _noElevationValue =
      SvnProElevationTokens.elevationNone; // 0.0

  // Widget Properties
  final Widget? body;
  final Widget? leading;
  final String? title;
  final List<Widget>? actions;
  final bool elevation;
  final Widget? bottomSticky;
  final Color? statutColor;
  final bool centerTitle;
  final Widget? fab;
  final SvnProTextSize titleSize;

  /// Constructs an instance of [SvnProTopAppBar].
  ///
  /// All parameters are optional, allowing for flexible customization.
  ///
  /// Parameters:
  /// - [body]: The main content of the scaffold, displayed below the app bar.
  /// - [leading]: The widget to display at the start of the app bar (e.g., a back button).
  /// - [title]: The title of the app bar. If null, no title will be displayed.
  /// - [actions]: A list of widgets to display at the end of the app bar (e.g., action buttons).
  /// - [elevation]: Whether the app bar should have elevation (shadow). Defaults to false.
  /// - [bottomSticky]: A widget to display at the bottom of the screen (e.g., a sticky footer).
  /// - [statutColor]: The color of the status bar. If null, the default color will be used.
  /// - [centerTitle]: Whether the title should be centered within the app bar. Defaults to true.
  /// - [fab]: A floating action button to display above the body content.
  /// - [titleSize]: The size of the title text. Defaults to [SvnProTextSize.titleLarge].
  const SvnProTopAppBar({
    super.key,
    this.body,
    this.leading,
    this.title,
    this.actions,
    this.elevation = false,
    this.bottomSticky,
    this.statutColor,
    this.centerTitle = true,
    this.fab,
    this.titleSize = SvnProTextSize.titleLarge,
  });

  /// Sets the system UI overlay style (status bar color and icon brightness)
  /// based on the provided [statutColor] and [elevation] properties.
  ///
  /// This method ensures that the status bar appearance is consistent with the app bar design.
  void _setSystemUIOverlayStyle(BuildContext context) {
    final SystemUiOverlayStyle overlayStyle;

    if (statutColor != null) {
      // Use the provided status bar color
      overlayStyle = SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: statutColor,
        statusBarIconBrightness: Brightness.dark,
      );
    } else if (elevation) {
      // Use a custom color from the theme when elevation is enabled
      final customColors = Theme.of(context).extension<CustomColors>();
      overlayStyle = SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: customColors?.stateLayersSurfaceTintOpacity012,
        statusBarIconBrightness: Brightness.dark,
      );
    } else {
      // Use the default surface container color from the theme
      overlayStyle = SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Theme.of(context).colorScheme.surfaceContainer,
        statusBarIconBrightness: Brightness.dark,
      );
    }

    // Apply the system UI overlay style
    SystemChrome.setSystemUIOverlayStyle(overlayStyle);
  }

  @override
  Widget build(BuildContext context) {
    // Set the system UI overlay style based on the current configuration
    _setSystemUIOverlayStyle(context);

    return Scaffold(
      // App Bar Configuration
      appBar: AppBar(
        leading: leading,
        automaticallyImplyLeading: leading != null,
        centerTitle: centerTitle,
        title: title != null
            ? SvnProText(
                text: title!,
                size: titleSize,
                weight: SvnProTextWeight.regular,
              )
            : null,
        actions: actions,
        elevation: elevation ? _elevationValue : _noElevationValue,
        scrolledUnderElevation: elevation ? _elevationValue : _noElevationValue,
      ),

      // Body Content
      body: body,

      // Bottom Sticky Widget (e.g., bottom navigation bar)
      bottomNavigationBar: bottomSticky,

      // Floating Action Button
      floatingActionButton: fab,
    );
  }
}
