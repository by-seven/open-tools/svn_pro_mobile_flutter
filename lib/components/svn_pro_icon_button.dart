part of '../svn_pro_mobile_flutter.dart';

enum SvnProIconButtonVariant { filled, outlined, standard, tonal }

enum SvnProIconButtonType {
  button,
  toggleable,
}

class SvnProIconButton extends StatefulWidget {
  final SvnProIconButtonVariant variant;
  final bool enable;
  final SvnProIcon icon;
  final SvnProIcon? selectedIcon;
  final Function()? onPressed;
  final bool isSelected;
  final SvnProIconButtonType type;

  const SvnProIconButton({
    super.key,
    this.variant = SvnProIconButtonVariant.filled,
    this.enable = true,
    this.onPressed,
    required this.icon,
    this.selectedIcon,
    this.isSelected = false,
    this.type = SvnProIconButtonType.button,
  });

  @override
  State<SvnProIconButton> createState() => _SvnProIconButtonState();
}

class _SvnProIconButtonState extends State<SvnProIconButton> {
  // bool handleSelection = false;

  // @override
  // void didUpdateWidget(covariant SvnProIconButton oldWidget) {
  //   super.didUpdateWidget(oldWidget);
  //   setState(() {
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    switch (widget.variant) {
      case SvnProIconButtonVariant.filled:
        return IconButton.filled(
          selectedIcon: widget.selectedIcon,
          isSelected: widget.type == SvnProIconButtonType.button
              ? null
              // : (handleSelection ? handleSelection : widget.isSelected),
              : widget.isSelected,
          onPressed: widget.enable ? handleOnPressed : null,
          icon: widget.icon,
        );
      case SvnProIconButtonVariant.outlined:
        return IconButton.outlined(
          selectedIcon: widget.selectedIcon,
          isSelected: widget.type == SvnProIconButtonType.button
              ? null
              // : (handleSelection ? handleSelection : widget.isSelected),
              : widget.isSelected,
          onPressed: widget.enable ? handleOnPressed : null,
          icon: widget.icon,
        );
      case SvnProIconButtonVariant.standard:
        return IconButton(
          selectedIcon: widget.selectedIcon,
          isSelected: widget.type == SvnProIconButtonType.button
              ? null
              // : (handleSelection ? handleSelection : widget.isSelected),
              : widget.isSelected,
          onPressed: widget.enable ? handleOnPressed : null,
          icon: widget.icon,
        );
      case SvnProIconButtonVariant.tonal:
        return IconButton.filledTonal(
          selectedIcon: widget.selectedIcon,
          isSelected: widget.type == SvnProIconButtonType.button
              ? null
              // : (handleSelection ? handleSelection : widget.isSelected),
              : widget.isSelected,
          onPressed: widget.enable ? handleOnPressed : null,
          icon: widget.icon,
        );
    }
  }

  handleOnPressed() {
    if (widget.onPressed != null) {
      widget.onPressed!();
    }
    // if (widget.type == SvnProIconButtonType.toggleable) {
    //   setState(() {
    //     handleSelection = !handleSelection;
    //   });
    // }
  }
}
