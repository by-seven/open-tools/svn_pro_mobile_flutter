part of '../svn_pro_mobile_flutter.dart';

class SvnProDateField extends StatefulWidget {
  final TextEditingController? controller;
  final String label;
  final String? placeholder;
  final DateTime? initialDate;
  final ValueChanged<DateTime>? onChanged;
  final FormFieldValidator<String>? validator;
  final SvnProTextFieldStatus state;
  final bool? readOnly;

  const SvnProDateField({
    super.key,
    this.controller,
    required this.label,
    this.placeholder,
    this.initialDate,
    this.onChanged,
    this.validator,
    this.state = SvnProTextFieldStatus.enabled,
    this.readOnly = false,
  });

  @override
  State<SvnProDateField> createState() => _SvnProDateFieldState();
}

class _SvnProDateFieldState extends State<SvnProDateField> {
  final fieldController = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();

    if (widget.controller == null) {
      fieldController.text =
          widget.initialDate == null ? '' : widget.initialDate!.toMaterialDate;
      fieldController.addListener(() {
        if (widget.onChanged != null && !(widget.readOnly ?? false)) {
          widget
              .onChanged!(fieldController.text.materialDate ?? DateTime.now());
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SvnProTextField(
      readOnly: widget.readOnly ?? false,
      status: widget.state,
      onTap: () async {
        if (widget.readOnly ?? false) {
          return;
        }

        final svnDate = await svnProShowDatePicker(
          context,
          mode: SvnProShowDatePickerMode.input,
          initialDate: widget.controller == null
              ? (fieldController.text != ''
                  ? fieldController.text.materialDate
                  : widget.initialDate)
              : widget.controller!.text.materialDate,
        );

        if (svnDate!.date != null) {
          if (widget.controller != null) {
            widget.controller!.text = svnDate.date!.toMaterialDate;
          } else {
            fieldController.text = svnDate.date!.toMaterialDate;
          }
        }
      },
      controller: widget.controller ?? fieldController,
      validator: widget.validator,
      variant: SvnProTextFieldVariant.outlined,
      prefixIcon: const SvnProIcon(
        iconData: MingCuteIcons.mgc_calendar_month_line,
        color: null,
      ),
      label: widget.label,
      placeHolder: widget.placeholder ?? widget.label,
    );
  }
}
