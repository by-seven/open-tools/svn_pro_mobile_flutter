part of '../svn_pro_mobile_flutter.dart';

class SvnProCardBaseBigLearn extends StatelessWidget {
  final List<String> imgList;
  final String title;
  final int? nbrModules;
  final VoidCallback? onLiked;
  final List<SvnProInfoTag>? infoTagList;
  final int? duration;
  final int? likes;
  final int? recommended;
  final bool acquired;

  const SvnProCardBaseBigLearn({
    super.key,
    this.imgList = const [],
    required this.title,
    this.nbrModules,
    this.onLiked,
    this.infoTagList,
    this.duration,
    this.likes,
    this.recommended,
    this.acquired = false,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProCard(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 88,
            child: Container(
              color: Theme.of(context).colorScheme.surfaceContainerHighest,
              child: Stack(
                children: genPhotoGrid(context),
              ),
            ),
          ),
          Container(
            color: Theme.of(context).colorScheme.surfaceContainerLow,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Column(
                children: [
                  if (infoTagList != null) ...[
                    Row(
                      children: infoTagList!
                          .map(
                            (infoTag) => [
                              infoTag.copyWith(
                                variant: SvnProInfoTagVariant.outlined,
                                type: SvnProInfoTagType.info,
                                leading: SvnProInfoTagLeading.none,
                                compact: true,
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                            ],
                          )
                          .expand((i) => i)
                          .toList(),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                  ],
                  Row(
                    children: [
                      Flexible(
                        child: SvnProText(
                          text: title,
                          size: SvnProTextSize.titleMedium,
                          weight: SvnProTextWeight.semiBold,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Row(
                    children: [
                      if (duration != null) ...[
                        SvnProIcon(
                          iconData: MingCuteIcons.mgc_time_line,
                          size: 16,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        SvnProText(
                          text: '$duration min',
                          size: SvnProTextSize.bodyMedium,
                          weight: SvnProTextWeight.regular,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                      ],
                      if (nbrModules != null)
                        SvnProText(
                          text: 'module'.trPlural(
                              'modules', nbrModules, [nbrModules.toString()]),
                          size: SvnProTextSize.bodyMedium,
                          weight: SvnProTextWeight.regular,
                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                        ),
                    ],
                  ),
                  if ((likes != null && likes! != 0) ||
                      (recommended != null && recommended! != 0) ||
                      acquired)
                    const SizedBox(height: 16),
                  Row(
                    children: [
                      SvnProInfoTagReaction(
                        likes: likes,
                        recommended: recommended,
                      ),
                      const Spacer(),
                      if (acquired)
                        SvnProInfoTag(
                          label: 'acquired'.tr,
                          variant: SvnProInfoTagVariant.filled,
                          type: SvnProInfoTagType.success,
                          leading: SvnProInfoTagLeading.icon,
                          icon: const SvnProIcon(
                            iconData: MingCuteIcons.mgc_check_circle_fill,
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  genPhotoGrid(BuildContext context) {
    List<Widget> list = <Widget>[];

    if (imgList.isEmpty) {
      list.add(
        Positioned(
          left: 16,
          bottom: 16,
          child: SvnProIcon(
            iconData: MingCuteIcons.mgc_folder_2_line,
            size: 40,
            color: Theme.of(context)
                .colorScheme
                .onSurfaceVariant
                .withOpacity(0.38),
          ),
        ),
      );
    } else {
      list.add(
        Row(
          children: List.generate(
            imgList.length > 4 ? 4 : imgList.length,
            (index) => Expanded(
              child: SvnProImage(
                imageUrl: imgList[0],
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      );
    }

    list.add(
      Positioned(
        top: 12,
        right: 12,
        child: Row(children: [
          SvnProIconButton(
            variant: SvnProIconButtonVariant.standard,
            type: SvnProIconButtonType.toggleable,
            icon: const SvnProIcon(
              iconData: MingCuteIcons.mgc_heart_line,
            ),
            selectedIcon: const SvnProIcon(
              iconData: MingCuteIcons.mgc_heart_fill,
            ),
            onPressed: onLiked,
          ),
        ]),
      ),
    );

    return list;
  }
}
