part of '../svn_pro_mobile_flutter.dart';

enum SvnProButtonStickyContainerVariant {
  stacked,
  horizontal,
}

class SvnProButtonStickyContainer extends StatelessWidget {
  final SvnProButtonStickyContainerVariant variant;
  final SvnProBtn primaryBtn;
  final SvnProBtn? secondaryBtn;

  const SvnProButtonStickyContainer({
    super.key,
    this.variant = SvnProButtonStickyContainerVariant.stacked,
    required this.primaryBtn,
    this.secondaryBtn,
  });

  @override
  Widget build(BuildContext context) {
    if (variant == SvnProButtonStickyContainerVariant.stacked) {
      return SizedBox(
        width: double.maxFinite,
        child: Container(
          color: Theme.of(context).colorScheme.primaryContainer,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 12, 20, 12),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Expanded(child: primaryBtn),
                  ],
                ),
                if (secondaryBtn != null) ...[
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      Expanded(child: secondaryBtn!),
                    ],
                  ),
                ],
              ],
            ),
          ),
        ),
      );
    }

    return SizedBox(
      width: double.maxFinite,
      child: Container(
        color: Theme.of(context).colorScheme.primaryContainer,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 16, 20, 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (secondaryBtn != null) ...[
                Expanded(child: secondaryBtn!),
                const SizedBox(
                  width: 8,
                ),
              ],
              Expanded(child: primaryBtn),
            ],
          ),
        ),
      ),
    );
  }
}
