part of '../svn_pro_mobile_flutter.dart';

enum SvnProMedalLearnVariant {
  training,
  favorite,
  module,
  reaction,
}

enum SvnProMedalLearnLevel {
  lv1,
  lv2,
  lv3,
  lv4,
}

enum SvnProMedalPrestige {
  trainingFirst(
    SvnProMedalLearnVariant.training,
    SvnProMedalLearnLevel.lv1,
    1,
    'accomplished_apprentice',
    'training_completed',
  ),
  trainingSecond(
    SvnProMedalLearnVariant.training,
    SvnProMedalLearnLevel.lv2,
    3,
    'trained_and_competent',
    'training_completed',
  ),
  trainingThird(
    SvnProMedalLearnVariant.training,
    SvnProMedalLearnLevel.lv3,
    5,
    'certified_expert',
    'training_completed',
  ),
  trainingFourth(
    SvnProMedalLearnVariant.training,
    SvnProMedalLearnLevel.lv4,
    20,
    'training_champion',
    'training_completed',
  ),
  favoriteFirst(
    SvnProMedalLearnVariant.favorite,
    SvnProMedalLearnLevel.lv1,
    1,
    'the_selector',
    'module_added_to_favorites',
  ),
  favoriteSecond(
    SvnProMedalLearnVariant.favorite,
    SvnProMedalLearnLevel.lv2,
    5,
    'module_added_to_favorites',
    'module_added_to_favorites',
  ),
  favoriteThird(
    SvnProMedalLearnVariant.favorite,
    SvnProMedalLearnLevel.lv3,
    10,
    'informed_collector',
    'module_added_to_favorites',
  ),
  favoriteFourth(
    SvnProMedalLearnVariant.favorite,
    SvnProMedalLearnLevel.lv4,
    25,
    'resource_guardian',
    'module_added_to_favorites',
  ),
  reactionFirst(
    SvnProMedalLearnVariant.reaction,
    SvnProMedalLearnLevel.lv1,
    1,
    'the_approver',
    'reactions_to_modules',
  ),
  reactionSecond(
    SvnProMedalLearnVariant.reaction,
    SvnProMedalLearnLevel.lv2,
    5,
    'positive_supporter',
    'reactions_to_modules',
  ),
  reactionThird(
    SvnProMedalLearnVariant.reaction,
    SvnProMedalLearnLevel.lv3,
    15,
    'enlightened_evaluator',
    'reactions_to_modules',
  ),
  reactionFourth(
    SvnProMedalLearnVariant.reaction,
    SvnProMedalLearnLevel.lv4,
    30,
    'master_of_emotions',
    'reactions_to_modules',
  ),
  moduleFirst(
    SvnProMedalLearnVariant.module,
    SvnProMedalLearnLevel.lv1,
    1,
    'enlightened_novice',
    'modules_acquired',
  ),
  moduleSecond(
    SvnProMedalLearnVariant.module,
    SvnProMedalLearnLevel.lv2,
    5,
    'knowledge_explorer',
    'modules_acquired',
  ),
  moduleThird(
    SvnProMedalLearnVariant.module,
    SvnProMedalLearnLevel.lv3,
    10,
    'thirsty_for_knowledge',
    'modules_acquired',
  ),
  moduleFourth(
    SvnProMedalLearnVariant.module,
    SvnProMedalLearnLevel.lv4,
    25,
    'curriculum_master',
    'modules_acquired',
  );

  final SvnProMedalLearnVariant variant;
  final SvnProMedalLearnLevel level;
  final int weight;
  final String title;
  final String supportingText;

  const SvnProMedalPrestige(
    this.variant,
    this.level,
    this.weight,
    this.title,
    this.supportingText,
  );
}

// enum SvnProMedalLearnState { completed, notStarted, inProgress }

class SvnProMedalLearn extends StatelessWidget {
  final SvnProMedalLearnVariant variant;
  final int progression;

  const SvnProMedalLearn({
    super.key,
    this.variant = SvnProMedalLearnVariant.training,
    this.progression = 0,
  });

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: progression == 0 ? 0.5 : 1,
      child: SvnProCard(
        variant: SvnProCardVariant.elevated,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: [
              SvnProIcon(
                svgPath: 'assets/icons/medal_learn/${getSvgPath()}',
                package: 'svn_pro_mobile_flutter',
              ),
              const SizedBox(width: 12),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SvnProText(
                      size: SvnProTextSize.titleMedium,
                      weight: SvnProTextWeight.medium,
                      text: getCurrentPrestige()!.title.tr,
                    ),
                    Row(
                      children: [
                        if (progression != 0 &&
                            isPresigeCurrentlyCompletedOrOverlap(
                                    progression, variant) ==
                                false)
                          SvnProText(
                            size: SvnProTextSize.titleSmall,
                            weight: SvnProTextWeight.regular,
                            text: '${progression.toString()}/',
                          ),
                        SvnProText(
                          size: SvnProTextSize.titleSmall,
                          weight: SvnProTextWeight.regular,
                          text: '${getCurrentPrestige()!.weight.toString()} ',
                        ),
                        SvnProText(
                          size: SvnProTextSize.titleSmall,
                          weight: SvnProTextWeight.regular,
                          text: getCurrentPrestige()!.supportingText.tr,
                        ),
                      ],
                    ),
                    if (progression != 0 &&
                        isPresigeCurrentlyCompletedOrOverlap(
                                progression, variant) ==
                            false) ...[
                      const SizedBox(height: 12),
                      SvnProProgress(
                        style: SvnProProgressStyle.linear,
                        isIndeterminate: false,
                        step: progression,
                        totalStep: getCurrentPrestige()!.weight,
                      ),
                    ],
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getSvgPath() {
    if (variant == SvnProMedalLearnVariant.training &&
        isPresigeCurrentlyCompletedOrOverlap(progression, variant)) {
      return 'training_completed.svg';
    } else if (variant == SvnProMedalLearnVariant.training) {
      return 'training_in_progress.svg';
    } else if (variant == SvnProMedalLearnVariant.favorite &&
        isPresigeCurrentlyCompletedOrOverlap(progression, variant)) {
      return 'favorite_completed.svg';
    } else if (variant == SvnProMedalLearnVariant.favorite) {
      return 'favorite_in_progress.svg';
    } else if (variant == SvnProMedalLearnVariant.reaction &&
        isPresigeCurrentlyCompletedOrOverlap(progression, variant)) {
      return 'reaction_completed.svg';
    } else if (variant == SvnProMedalLearnVariant.reaction) {
      return 'reaction_in_progress.svg';
    } else if (variant == SvnProMedalLearnVariant.module &&
        isPresigeCurrentlyCompletedOrOverlap(progression, variant)) {
      return 'module_completed.svg';
    } else if (variant == SvnProMedalLearnVariant.module) {
      return 'module_in_progress.svg';
    }
    return '';
  }

  bool isPresigeCurrentlyCompletedOrOverlap(progression, variant) {
    if (variant == SvnProMedalLearnVariant.training &&
        (progression == SvnProMedalPrestige.trainingFirst.weight ||
            progression == SvnProMedalPrestige.trainingSecond.weight ||
            progression == SvnProMedalPrestige.trainingThird.weight ||
            progression >= SvnProMedalPrestige.trainingFourth.weight)) {
      return true;
    } else if (variant == SvnProMedalLearnVariant.training) {
      return false;
    } else if (variant == SvnProMedalLearnVariant.favorite &&
        (progression == SvnProMedalPrestige.favoriteFirst.weight ||
            progression == SvnProMedalPrestige.favoriteSecond.weight ||
            progression == SvnProMedalPrestige.favoriteThird.weight ||
            progression >= SvnProMedalPrestige.favoriteFourth.weight)) {
      return true;
    } else if (variant == SvnProMedalLearnVariant.favorite) {
      return false;
    } else if (variant == SvnProMedalLearnVariant.reaction &&
        (progression == SvnProMedalPrestige.reactionFirst.weight ||
            progression == SvnProMedalPrestige.reactionSecond.weight ||
            progression == SvnProMedalPrestige.reactionThird.weight ||
            progression >= SvnProMedalPrestige.reactionFourth.weight)) {
      return true;
    } else if (variant == SvnProMedalLearnVariant.reaction) {
      return false;
    } else if (variant == SvnProMedalLearnVariant.module &&
        (progression == SvnProMedalPrestige.moduleFirst.weight ||
            progression == SvnProMedalPrestige.moduleSecond.weight ||
            progression == SvnProMedalPrestige.moduleThird.weight ||
            progression >= SvnProMedalPrestige.moduleFourth.weight)) {
      return true;
    } else if (variant == SvnProMedalLearnVariant.module) {
      return false;
    }

    return false;
  }

  SvnProMedalPrestige? getCurrentPrestige() {
    if (variant == SvnProMedalLearnVariant.training) {
      if (progression <= SvnProMedalPrestige.trainingFirst.weight) {
        return SvnProMedalPrestige.trainingFirst;
      } else if (progression <= SvnProMedalPrestige.trainingSecond.weight) {
        return SvnProMedalPrestige.trainingSecond;
      } else if (progression <= SvnProMedalPrestige.trainingThird.weight) {
        return SvnProMedalPrestige.trainingThird;
      } else if (progression > SvnProMedalPrestige.trainingThird.weight) {
        return SvnProMedalPrestige.trainingFourth;
      }
    } else if (variant == SvnProMedalLearnVariant.favorite) {
      if (progression <= SvnProMedalPrestige.favoriteFirst.weight) {
        return SvnProMedalPrestige.favoriteFirst;
      } else if (progression <= SvnProMedalPrestige.favoriteSecond.weight) {
        return SvnProMedalPrestige.favoriteSecond;
      } else if (progression <= SvnProMedalPrestige.favoriteThird.weight) {
        return SvnProMedalPrestige.favoriteThird;
      } else if (progression > SvnProMedalPrestige.favoriteThird.weight) {
        return SvnProMedalPrestige.favoriteFourth;
      }
    } else if (variant == SvnProMedalLearnVariant.reaction) {
      if (progression <= SvnProMedalPrestige.reactionFirst.weight) {
        return SvnProMedalPrestige.reactionFirst;
      } else if (progression <= SvnProMedalPrestige.reactionSecond.weight) {
        return SvnProMedalPrestige.reactionSecond;
      } else if (progression <= SvnProMedalPrestige.reactionThird.weight) {
        return SvnProMedalPrestige.reactionThird;
      } else if (progression > SvnProMedalPrestige.reactionThird.weight) {
        return SvnProMedalPrestige.reactionFourth;
      }
    } else if (variant == SvnProMedalLearnVariant.module) {
      if (progression <= SvnProMedalPrestige.moduleFirst.weight) {
        return SvnProMedalPrestige.moduleFirst;
      } else if (progression <= SvnProMedalPrestige.moduleSecond.weight) {
        return SvnProMedalPrestige.moduleSecond;
      } else if (progression <= SvnProMedalPrestige.moduleThird.weight) {
        return SvnProMedalPrestige.moduleThird;
      } else if (progression > SvnProMedalPrestige.moduleThird.weight) {
        return SvnProMedalPrestige.moduleFourth;
      }
    }
    return null;
  }
}
