part of '../svn_pro_mobile_flutter.dart';

class SvnProInfoTagReaction extends StatelessWidget {
  final int? likes;
  final int? recommended;

  const SvnProInfoTagReaction({super.key, this.likes, this.recommended});

  @override
  Widget build(BuildContext context) {
    if (likes == null && recommended == null ||
        likes != null &&
            likes! == 0 &&
            recommended != null &&
            recommended! == 0) {
      return const SizedBox();
    }

    return SvnProCard(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (likes != null && likes! >= 1)
              const SvnProIcon(
                noto: Noto.thumbs_up,
                size: 16,
              ),
            if (recommended != null && recommended! >= 1) ...[
              const SizedBox(width: 4),
              const SvnProIcon(
                noto: Noto.star_struck,
                size: 16,
              ),
            ],
            if (likes != null && likes! >= 1 ||
                recommended != null && recommended! >= 1) ...[
              const SizedBox(width: 4),
              SvnProText(
                text: ((likes ?? 0) + (recommended ?? 0)).toString(),
                size: SvnProTextSize.labelLarge,
                weight: SvnProTextWeight.medium,
                color: Theme.of(context).colorScheme.onSurfaceVariant,
              ),
            ],
          ],
        ),
      ),
    );
  }
}
