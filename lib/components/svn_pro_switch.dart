part of '../svn_pro_mobile_flutter.dart';

enum SvnProSwitchState {
  enabled,
  disabled,
}

class SvnProSwitch extends StatelessWidget {
  final SvnProSwitchState state;
  final bool selected;
  final bool withIcons;
  final void Function(bool)? onChanged;

  const SvnProSwitch({
    super.key,
    this.state = SvnProSwitchState.enabled,
    this.selected = false,
    this.withIcons = false,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Switch(
      thumbIcon: withIcons
          ? MaterialStateProperty.resolveWith<Icon?>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.selected)) {
                  return const Icon(Icons.check);
                }
                return const Icon(Icons.close);
              },
            )
          : null,
      value: selected,
      onChanged: state == SvnProSwitchState.enabled
          ? (onChanged ?? (bool _) {})
          : null,
    );
  }
}
