part of '../svn_pro_mobile_flutter.dart';

class SvnProSelectDrawerSkeleton extends StatelessWidget {
  final String title;
  final List<SvnProRadioListTile> radioList;

  const SvnProSelectDrawerSkeleton({
    super.key,
    required this.title,
    this.radioList = const [],
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          const SizedBox(height: 8),
          Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Row(
              children: [
                Flexible(
                  child: SvnProText(
                    text: title,
                    size: SvnProTextSize.titleMl,
                    weight: SvnProTextWeight.semiBold,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 16),
          SizedBox(
            child: ListView.separated(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              separatorBuilder: (context, index) => const SvnProDivider(),
              itemCount: radioList.length,
              itemBuilder: (context, index) => radioList[index],
            ),
          ),
        ]),
      ),
    );
  }
}

Future<T?> svnProShowSelectDrawer<T>(
  context, {
  required WidgetBuilder builder,
}) async {
  return svnProShowBottomSheet(context,
      mode: SvnProBottomSheetMode.modal, builder: builder);
}
