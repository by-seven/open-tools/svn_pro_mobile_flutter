part of '../svn_pro_mobile_flutter.dart';

enum SvnProAnswerBubbleItemItwVariant {
  left,
  right,
  cross,
}

class SvnProAnswerBubbleItemItw extends StatelessWidget {
  final SvnProAvatar? avatar;
  final SvnProAnswerBubbleItemItwVariant variant;
  final String? title;
  final String? comment;
  final SvnProQuestionItwType answerType;
  final String? openAnswer;
  final int? selectedStartAnswer;
  final int? maxStartAnswer;
  final String? mcqAnswer;
  final int? numberCommentedAnswer;
  final VoidCallback? onCommentedAnswer;

  const SvnProAnswerBubbleItemItw({
    super.key,
    this.avatar,
    this.variant = SvnProAnswerBubbleItemItwVariant.left,
    this.title,
    this.comment,
    this.answerType = SvnProQuestionItwType.open,
    this.openAnswer,
    this.selectedStartAnswer,
    this.maxStartAnswer,
    this.mcqAnswer,
    this.numberCommentedAnswer,
    this.onCommentedAnswer,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (variant == SvnProAnswerBubbleItemItwVariant.left) ...[
          avatar!.copyWith(size: SvnProAvatarSizes.px32),
          const SizedBox(
            width: 8,
          ),
        ],
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment:
                    variant == SvnProAnswerBubbleItemItwVariant.right
                        ? MainAxisAlignment.end
                        : MainAxisAlignment.start,
                children: [
                  SvnProText(
                    text: variant == SvnProAnswerBubbleItemItwVariant.cross
                        ? 'Cross review'
                        : title,
                    size: SvnProTextSize.bodyLarge,
                    weight: SvnProTextWeight.semiBold,
                  ),
                ],
              ),
              const SizedBox(
                height: 4,
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: variant == SvnProAnswerBubbleItemItwVariant.cross
                            ? Theme.of(context).colorScheme.secondaryContainer
                            : Theme.of(context)
                                .colorScheme
                                .surfaceContainerHigh,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(
                              variant != SvnProAnswerBubbleItemItwVariant.left
                                  ? 8
                                  : 0),
                          topRight: Radius.circular(
                              variant != SvnProAnswerBubbleItemItwVariant.right
                                  ? 8
                                  : 0),
                          bottomRight: const Radius.circular(8),
                          bottomLeft: const Radius.circular(8),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment:
                              variant != SvnProAnswerBubbleItemItwVariant.right
                                  ? CrossAxisAlignment.start
                                  : CrossAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            genAnswerType(context),
                            if (comment != null) ...[
                              const SizedBox(
                                height: 8,
                              ),
                              Flexible(
                                child: SvnProText(
                                  text: comment,
                                  size: SvnProTextSize.bodyMedium,
                                  weight: SvnProTextWeight.regular,
                                  color: variant ==
                                          SvnProAnswerBubbleItemItwVariant.cross
                                      ? Theme.of(context)
                                          .colorScheme
                                          .onSecondaryContainer
                                      : Theme.of(context).colorScheme.onSurface,
                                ),
                              ),
                            ],
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              if (numberCommentedAnswer != null)
                Row(
                  mainAxisAlignment:
                      variant == SvnProAnswerBubbleItemItwVariant.right
                          ? MainAxisAlignment.end
                          : MainAxisAlignment.start,
                  children: [
                    SvnProBtn(
                      variant: SvnProBtnVariants.text,
                      text: 'comment'.trPlural(
                          'comments',
                          numberCommentedAnswer,
                          [numberCommentedAnswer.toString()]),
                      onPressed: onCommentedAnswer,
                    ),
                  ],
                ),
            ],
          ),
        ),
        if (variant == SvnProAnswerBubbleItemItwVariant.right) ...[
          const SizedBox(
            width: 8,
          ),
          avatar!.copyWith(size: SvnProAvatarSizes.px32),
        ],
      ],
    );
  }

  genAnswerType(context) {
    switch (answerType) {
      case SvnProQuestionItwType.open:
        return SvnProText(
          text: openAnswer,
          softWrap: true,
          size: SvnProTextSize.bodyMedium,
          weight: SvnProTextWeight.regular,
          color: variant == SvnProAnswerBubbleItemItwVariant.cross
              ? Theme.of(context).colorScheme.onSecondaryContainer
              : Theme.of(context).colorScheme.onSurface,
        );
      case SvnProQuestionItwType.rating:
        return Row(
          mainAxisAlignment: variant != SvnProAnswerBubbleItemItwVariant.right
              ? MainAxisAlignment.start
              : MainAxisAlignment.end,
          children: List.generate(
            maxStartAnswer!,
            (index) {
              return [
                SvnProIcon(
                  iconData: index < (selectedStartAnswer ?? 0) &&
                          (selectedStartAnswer ?? 0) != 0
                      ? MingCuteIcons.mgc_star_fill
                      : MingCuteIcons.mgc_star_line,
                  size: 16,
                  color: variant == SvnProAnswerBubbleItemItwVariant.cross
                      ? Theme.of(context).colorScheme.onSecondaryContainer
                      : Theme.of(context).colorScheme.onSurface,
                ),
                // SvnProIconButton(
                //   variant: SvnProIconButtonVariant.standard,
                //   type: SvnProIconButtonType.toggleable,
                //   isSelected:
                //       index <= selectedStartAnswer! && selectedStartAnswer != 0,
                //   icon: SvnProIcon(
                //     iconData: MingCuteIcons.mgc_star_line,
                //     color: variant == SvnProAnswerBubbleItemItwVariant.cross
                //         ? Theme.of(context).colorScheme.onSecondaryContainer
                //         : Theme.of(context).colorScheme.onSurface,
                //   ),
                //   selectedIcon: SvnProIcon(
                //     iconData: MingCuteIcons.mgc_star_fill,
                //     color: variant == SvnProAnswerBubbleItemItwVariant.cross
                //         ? Theme.of(context).colorScheme.onSecondaryContainer
                //         : Theme.of(context).colorScheme.onSurface,
                //   ),
                // ),
                const SizedBox(
                  width: 8,
                ),
              ];
            },
          ).expand((i) => i).toList(),
        );
      case SvnProQuestionItwType.mcq:
        return SvnProInfoTag(
          variant: SvnProInfoTagVariant.outlined,
          leading: SvnProInfoTagLeading.none,
          type: variant == SvnProAnswerBubbleItemItwVariant.cross
              ? SvnProInfoTagType.primary
              : SvnProInfoTagType.info,
          label: mcqAnswer,
        );
      // return Container(
      //   decoration: BoxDecoration(
      //     borderRadius: BorderRadius.circular(8),
      //     border: Border.all(
      //       width: 1,
      //       color: variant == SvnProAnswerBubbleItemItwVariant.cross
      //           ? Theme.of(context).colorScheme.onSecondaryContainer
      //           : Theme.of(context).colorScheme.onSurface,
      //     ),
      //   ),
      //   child: Padding(
      //     padding: const EdgeInsets.fromLTRB(12, 6, 12, 6),
      //     child: SvnProText(
      //         text: mcqAnswer,
      //         size: SvnProTextSize.titleSmall,
      //         weight: SvnProTextWeight.medium),
      //   ),
      // );
    }
  }
}
