part of '../svn_pro_mobile_flutter.dart';

enum SvnProListItemItwVariant {
  add,
  remove,
  active,
}

class SvnProListItemItw extends StatelessWidget {
  final String title;
  final String? supportingText;
  final VoidCallback? onTap;
  final SvnProListItemItwVariant variant;
  final SvnProAvatar avatar;

  const SvnProListItemItw({
    super.key,
    required this.title,
    this.supportingText,
    this.onTap,
    this.variant = SvnProListItemItwVariant.add,
    required this.avatar,
  });

  @override
  Widget build(BuildContext context) {
    return SvnProListTile(
      leadingVariant: SvnProListTileSideVariant.avatar,
      leadingAvatar: avatar,
      condition: SvnProListTileCondition.line2,
      title: title,
      supportingText: supportingText,
      onTap: onTap,
      trailingVariant: SvnProListTileSideVariant.icon,
      trailingIcon: genIcon(context),
    );
  }

  genIcon(context) {
    switch (variant) {
      case SvnProListItemItwVariant.add:
        return SvnProIcon(
          iconData: MingCuteIcons.mgc_add_circle_line,
          color: Theme.of(context).colorScheme.primary,
        );
      case SvnProListItemItwVariant.remove:
        return SvnProIcon(
          iconData: MingCuteIcons.mgc_minus_circle_line,
          color: Theme.of(context).colorScheme.error,
        );
      case SvnProListItemItwVariant.active:
        return SvnProIcon(
          iconData: MingCuteIcons.mgc_check_circle_line,
          color: Theme.of(context).extension<CustomColors>()!.successColor,
        );
    }
  }
}
