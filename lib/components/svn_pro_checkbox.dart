part of '../svn_pro_mobile_flutter.dart';

enum SvnProCheckboxState { enabled, disabled, error }

enum SvnProCheckboxStatus { unselected, selected, undetermined }

class SvnProCheckbox extends StatelessWidget {
  final SvnProCheckboxState? state;
  final SvnProCheckboxStatus? status;
  final void Function(bool?)? onChanged;

  const SvnProCheckbox({
    super.key,
    this.state = SvnProCheckboxState.enabled,
    this.status = SvnProCheckboxStatus.unselected,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Checkbox(
      isError: state == SvnProCheckboxState.error ? true : false,
      tristate: status == SvnProCheckboxStatus.undetermined ? true : false,
      value: status == SvnProCheckboxStatus.selected
          ? true
          : (status == SvnProCheckboxStatus.unselected
              ? false
              : (status == SvnProCheckboxStatus.undetermined ? null : false)),
      onChanged: state == SvnProCheckboxState.disabled
          ? null
          : (onChanged ?? (bool? _) {}),
    );
  }
}
