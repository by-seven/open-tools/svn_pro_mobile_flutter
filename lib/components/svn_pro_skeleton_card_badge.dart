part of '../svn_pro_mobile_flutter.dart';

class SvnProSkeletonCardBadge extends StatelessWidget {
  const SvnProSkeletonCardBadge({super.key});

  @override
  Widget build(BuildContext context) {
    return const Skeletonizer(
        enabled: true,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SvnProText(
              text: "Module   example", size: SvnProTextSize.titleMedium),
          Padding(padding: EdgeInsets.only(bottom: 12)),
          SvnProCard(
            child: Padding(
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, left: 16, right: 240),
              child: Row(
                children: [
                  Icon(Icons.ac_unit, size: 64),
                  SizedBox(width: 12),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SvnProText(
                          text: 'The titl', size: SvnProTextSize.titleMedium),
                      SvnProText(text: 'Subtit'),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 40)),
        ]));
  }
}
