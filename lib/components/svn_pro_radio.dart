part of '../svn_pro_mobile_flutter.dart';

enum SvnProRadioState {
  enabled,
  disabled,
}

class SvnProRadio<T> extends StatelessWidget {
  final T value;
  final T? currentlySelected;
  final ValueChanged<T?>? onChanged;
  final SvnProRadioState state;

  const SvnProRadio({
    super.key,
    this.state = SvnProRadioState.enabled,
    required this.value,
    required this.currentlySelected,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Radio<T>(
      value: value,
      groupValue: currentlySelected,
      onChanged:
          state == SvnProRadioState.enabled ? (onChanged ?? (T? _) {}) : null,
    );
  }
}

class SvnProRadioListTile<T> extends StatelessWidget {
  final T value;
  final T? currentlySelected;
  final ValueChanged<T?>? onChanged;
  final SvnProRadioState state;
  final String? label;

  const SvnProRadioListTile({
    super.key,
    this.state = SvnProRadioState.enabled,
    required this.value,
    required this.currentlySelected,
    this.onChanged,
    this.label,
  });

  @override
  Widget build(BuildContext context) {
    return RadioListTile(
      controlAffinity: ListTileControlAffinity.trailing,
      title: label != null
          ? SvnProText(
              text: label,
              size: SvnProTextSize.bodyMedium,
            )
          : null,
      value: value,
      groupValue: currentlySelected,
      onChanged:
          state == SvnProRadioState.enabled ? (onChanged ?? (_) {}) : null,
    );
  }
}
