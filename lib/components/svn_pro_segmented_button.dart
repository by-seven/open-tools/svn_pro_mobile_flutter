part of '../svn_pro_mobile_flutter.dart';

class SvnProSegmentButtonItem<T> {
  final String label;
  final T value;
  final IconData? iconData;

  const SvnProSegmentButtonItem(
      {required this.label, required this.value, this.iconData});
}

class SvnProSegmentedButton<T> extends StatelessWidget {
  final List<SvnProSegmentButtonItem<T>> buttons;
  final T selected;
  final void Function(Set<T>)? onChanged;

  const SvnProSegmentedButton(
      {super.key,
      required this.buttons,
      required this.selected,
      this.onChanged});

  @override
  Widget build(BuildContext context) {
    return SegmentedButton(
      segments: buttons.map((SvnProSegmentButtonItem<T> item) {
        return ButtonSegment(
          icon: SvnProIcon(iconData: item.iconData),
          value: item.value,
          label: SvnProText(
            text: item.label,
            size: SvnProTextSize.bodyMedium,
            weight: SvnProTextWeight.medium,
          ),
        );
      }).toList(),
      selected: <T>{selected},
      onSelectionChanged: onChanged ?? (_) {},
    );
  }
}
