part of '../svn_pro_mobile_flutter.dart';

class SvnProListSkeleton extends StatelessWidget {
  const SvnProListSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        separatorBuilder: (context, index) => const SvnProDivider(),
        itemCount: 15,
        itemBuilder: (context, index) => const SvnProListTile(
          leadingVariant: SvnProListTileSideVariant.icon,
          leadingIcon: SvnProIcon(
            iconData: MingCuteIcons.mgc_border_inner_fill,
          ),
          title: 'toto toto toto toto toto toto toto toto toto ',
        ),
      ),
    );
  }
}
