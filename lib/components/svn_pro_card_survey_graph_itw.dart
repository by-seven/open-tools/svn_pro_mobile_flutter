part of '../svn_pro_mobile_flutter.dart';

enum SvnProCardSurveyGraphItwMode {
  vertical,
  horizontal,
}

class SvnProCardSurveyGraphItw extends StatelessWidget {
  final int maxValue;
  final SvnProCardSurveyGraphItwMode mode;
  final Map<String, int>? dataList;
  final int total;
  final int barWidth = 18;
  final String title;

  const SvnProCardSurveyGraphItw({
    super.key,
    this.mode = SvnProCardSurveyGraphItwMode.vertical,
    this.maxValue = 10,
    required this.title,
    this.total = 0,
    this.dataList,
  });

  @override
  Widget build(BuildContext context) {
    late Widget graph;

    if (dataList != null) {
      if (mode == SvnProCardSurveyGraphItwMode.vertical) {
        graph = SizedBox(
          width: double.maxFinite,
          height: 170,
          child: BarChart(
            BarChartData(
              barGroups: dataList!.entries
                  .mapIndexed(
                    (idx, entry) => BarChartGroupData(
                      x: idx,
                      barRods: [
                        BarChartRodData(
                          color: Theme.of(context).colorScheme.primaryFixedDim,
                          backDrawRodData: BackgroundBarChartRodData(
                            color: Theme.of(context)
                                .colorScheme
                                .outlineVariant
                                .withOpacity(0.2),
                            show: true,
                            fromY: 0,
                            toY: maxValue.toDouble(),
                          ),
                          width: barWidth.toDouble(),
                          fromY: 0,
                          toY: entry.value.toDouble(),
                        ),
                      ],
                    ),
                  )
                  .toList(),
              titlesData: FlTitlesData(
                show: true,
                leftTitles: const AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                topTitles: const AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                rightTitles: const AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                bottomTitles: AxisTitles(
                  sideTitles: SideTitles(
                    showTitles: true,
                    getTitlesWidget: (value, meta) {
                      final idx = int.parse(meta.formattedValue);
                      final elem = dataList!.entries.elementAt(idx);

                      return SvnProText(
                        text: elem.key,
                        size: SvnProTextSize.bodySmall,
                        weight: SvnProTextWeight.medium,
                      );
                    },
                  ),
                ),
              ),
              borderData: FlBorderData(show: false),
              gridData: const FlGridData(show: false),
            ),
          ),
        );
      } else if (mode == SvnProCardSurveyGraphItwMode.horizontal) {
        graph = Column(
          children: dataList!.entries
              .mapIndexed((idx, entry) {
                return [
                  Row(
                    children: [
                      Flexible(
                        child: SvnProText(
                          text: entry.key,
                          size: SvnProTextSize.bodySmall,
                          weight: SvnProTextWeight.medium,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  SizedBox(
                    height: barWidth.toDouble(),
                    child: RotatedBox(
                      quarterTurns: 1,
                      child: BarChart(
                        BarChartData(
                          barGroups: [
                            BarChartGroupData(
                              x: idx,
                              barRods: [
                                BarChartRodData(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .primaryFixedDim,
                                  backDrawRodData: BackgroundBarChartRodData(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .outlineVariant
                                        .withOpacity(0.2),
                                    show: true,
                                    fromY: 0,
                                    toY: maxValue.toDouble(),
                                  ),
                                  width: barWidth.toDouble(),
                                  fromY: 0,
                                  toY: entry.value.toDouble(),
                                ),
                              ],
                            ),
                          ],
                          titlesData: const FlTitlesData(
                            show: false,
                          ),
                          borderData: FlBorderData(show: false),
                          gridData: const FlGridData(show: false),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                ];
              })
              .expand((i) => i)
              .toList(),
        );
      }
    }

    return SvnProCard(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Flexible(
                  child: SvnProText(
                    text: title,
                    size: SvnProTextSize.titleMedium,
                    weight: SvnProTextWeight.medium,
                    color: Theme.of(context).colorScheme.onSurface,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 4,
            ),
            Row(
              children: [
                SvnProText(
                  text: 'answer'.trPlural('answers', total, [total.toString()]),
                  size: SvnProTextSize.bodyMedium,
                  weight: SvnProTextWeight.regular,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                ),
              ],
            ),
            if (dataList != null) ...[
              const SizedBox(
                height: 16,
              ),
              graph,
            ],
          ],
        ),
      ),
    );
  }
}
