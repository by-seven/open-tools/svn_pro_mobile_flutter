import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProQuestionEditItw, path: 'secondary')
Widget playground(BuildContext context) {
  final title =
      context.knobs.string(label: 'Title', initialValue: 'What is your why?');
  final desc = context.knobs.string(
      label: 'Description', initialValue: 'Lorem ipsum dolor sit amet...');
  final type = context.knobs.list(
    label: 'Type',
    initialOption: GlobalQuestionType.text,
    options: [
      GlobalQuestionType.text,
      GlobalQuestionType.number,
      GlobalQuestionType.star,
      GlobalQuestionType.radio,
    ],
  );

  return SvnProQuestionEditItw(
    title: title,
    description: desc,
    status: context.knobs.list(
      label: 'Status',
      initialOption: SvnProQuestionEditItwStatus.enabled,
      options: [
        SvnProQuestionEditItwStatus.enabled,
        SvnProQuestionEditItwStatus.error,
      ],
    ),
    type: type,
    onChanged: (value) => print('Value changed: $value'),
    onCommentChanged:
        context.knobs.boolean(label: 'Has comment?', initialValue: false)
            ? (String comment) => print('Comment: $comment')
            : null,
    openCanBeCommented: context.knobs
            .boolean(label: 'Open comment section?', initialValue: false)
        ? () {}
        : null,
    content: context.knobs.boolean(label: 'Has content?', initialValue: false)
        ? Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.deepPurpleAccent)),
            height: 10,
            width: double.maxFinite,
          )
        : null,
    hasAnswerInput:
        context.knobs.boolean(label: 'Has answer input?', initialValue: true),
    radioList: const ['First choice', 'Second choice'],
    questionInfoTag:
        context.knobs.boolean(label: 'Question Info Tag', initialValue: false)
            ? const SvnProInfoTag(label: 'Status')
            : null,
    deadline: context.knobs.boolean(label: 'Has deadline?', initialValue: false)
        ? DateTime.now()
        : null,
  );
}

@UseCase(
    name: 'SvnProCreateProposalTargetItw',
    type: SvnProQuestionEditItw,
    path: 'secondary')
Widget svnProCreateProposalTargetItw(BuildContext context) {
  return SvnProQuestionEditItw(
    title: 'Create proposal target Lorem ipsum dolor sit amet.',
    description:
        'Description de la target. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacinia tempor neque vitae dictumst luctus ornare id pretium diam.',
    content: Column(
      children: [
        const SvnProDivider(),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Flexible(
              child: SvnProText(
                text:
                    'This target is still a draft. It will be added to Roadmap after submitting all the interviews in the set.',
                size: SvnProTextSize.bodyMedium,
                weight: SvnProTextWeight.regular,
                color: Theme.of(context).colorScheme.onSurfaceVariant,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        const Row(
          children: [
            Expanded(
              child: SvnProBtn(
                variant: SvnProBtnVariants.tonal,
                text: 'edit target',
                iconData: MingCuteIcons.mgc_add_line,
              ),
            ),
            SizedBox(
              width: 4,
            ),
            SvnProIconButton(
              variant: SvnProIconButtonVariant.outlined,
              icon: SvnProIcon(
                iconData: MingCuteIcons.mgc_delete_2_line,
              ),
            ),
          ],
        ),
      ],
    ),
    hasAnswerInput: false,
    questionInfoTag:
        context.knobs.boolean(label: 'question info tag', initialValue: true)
            ? const SvnProInfoTag(
                label: 'status',
              )
            : null,
    deadline: context.knobs.boolean(label: 'deadline', initialValue: true)
        ? DateTime.now()
        : null,
  );
}

@UseCase(
    name: 'SvnProCreateTargetItw',
    type: SvnProQuestionEditItw,
    path: 'secondary')
Widget svnProCreateTargetItw(BuildContext context) {
  return const SvnProQuestionEditItw(
    title: 'New target',
    description:
        'New targets will be added to Roadmap after submitting all the interviews in the set.',
    content: Column(
      children: [
        Row(
          children: [
            Expanded(
              child: SvnProBtn(
                variant: SvnProBtnVariants.tonal,
                text: 'edit target',
                iconData: MingCuteIcons.mgc_add_line,
              ),
            ),
          ],
        ),
      ],
    ),
    hasAnswerInput: false,
  );
}

@UseCase(
    name: 'SectionQuizResultLearn',
    type: SvnProQuestionEditItw,
    path: 'secondary')
Widget sectionQuizResultLearn(BuildContext context) {
  return const SingleChildScrollView(
    child: Column(
      children: [
        SvnProQuestionEditItw(
          title: '👌 Here is one more module acquired',
          description: 'Congratulations, you just acquired a new module!',
          hasAnswerInput: false,
          content: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: SvnProBtn(
                      variant: SvnProBtnVariants.filled,
                      text: 'Next module',
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12),
              Row(
                children: [
                  Expanded(
                    child: SvnProBtn(
                      variant: SvnProBtnVariants.outlined,
                      text: 'Back to training',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SvnProQuestionEditItw(
          title: '💪 Ready to try again?',
          description: 'You can take this quiz as many times as needed.',
          hasAnswerInput: false,
          content: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: SvnProBtn(
                      variant: SvnProBtnVariants.filled,
                      text: 'Retry quiz',
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12),
              Row(
                children: [
                  Expanded(
                    child: SvnProBtn(
                      variant: SvnProBtnVariants.outlined,
                      text: 'Next module',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

@UseCase(
    name: 'SectionEvaluationShowLearn',
    type: SvnProQuestionEditItw,
    path: 'secondary')
Widget sectionEvaluationShowLearnUseCase(BuildContext context) {
  return const SingleChildScrollView(
    child: Column(
      children: [
        SvnProQuestionEditItw(
          title: '🙌 You’ve reached the end! Ready for the quiz?',
          description:
              'To complete this module, evaluate yourself with a quiz.',
          hasAnswerInput: false,
          content: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: SvnProBtn(
                      variant: SvnProBtnVariants.filled,
                      text: 'Start quiz',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
