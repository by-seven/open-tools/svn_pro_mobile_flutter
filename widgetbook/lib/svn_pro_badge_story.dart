import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProBadge, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProBadge(
    variant: context.knobs.list<SvnProBadgeVariant>(
      label: 'Variant',
      initialOption: SvnProBadgeVariant.point,
      options: [
        SvnProBadgeVariant.point,
        SvnProBadgeVariant.digit,
      ],
    ),
    number: context.knobs.int.slider(
      label: 'Number',
      initialValue: 0,
      min: 0,
      max: 1000,
    ),
    child: const SvnProBtn(
      text: 'hello',
    ),
  );
}
