import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProCardSurveyGraphItw, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProCardSurveyGraphItw(
    mode: context.knobs.list(
      label: 'Mode',
      initialOption: SvnProCardSurveyGraphItwMode.vertical,
      options: [
        SvnProCardSurveyGraphItwMode.horizontal,
        SvnProCardSurveyGraphItwMode.vertical,
      ],
    ),
    title: context.knobs
        .string(label: 'Title', initialValue: "How's your job doing?"),
    total: context.knobs.int.slider(label: 'Total', initialValue: 100),
    maxValue:
        context.knobs.int.slider(label: 'Max value of graph', initialValue: 10),
    dataList: context.knobs.boolean(label: 'Show chart?', initialValue: false)
        ? const <String, int>{'1': 2, '2': 5, '3': 3, '4': 6, '5': 4, '6': 10}
        : null,
  );
}
