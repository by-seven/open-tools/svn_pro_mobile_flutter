import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

abstract class SvnProShowDatePicker {}

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProShowDatePicker, path: 'primary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      const SizedBox(
        height: 50,
      ),
      SvnProBtn(
        text: 'Show date picker day',
        onPressed: () {
          svnProShowDatePicker(context, mode: SvnProShowDatePickerMode.day);
        },
      ),
      const SizedBox(
        height: 50,
      ),
      SvnProBtn(
        text: 'Show date picker input',
        onPressed: () {
          svnProShowDatePicker(context, mode: SvnProShowDatePickerMode.input);
        },
      ),
      const SizedBox(
        height: 50,
      ),
      SvnProBtn(
        text: 'Show date picker range full',
        onPressed: () {
          svnProShowDatePicker(context,
              mode: SvnProShowDatePickerMode.rangeFull);
        },
      ),
      const SizedBox(
        height: 50,
      ),
      SvnProBtn(
        text: 'Show date picker range input',
        onPressed: () {
          svnProShowDatePicker(context,
              mode: SvnProShowDatePickerMode.rangeInput);
        },
      ),
    ],
  );
}
