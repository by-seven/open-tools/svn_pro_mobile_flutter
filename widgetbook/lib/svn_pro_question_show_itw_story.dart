import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProQuestionShowItw, path: 'secondary')
Widget playground(BuildContext context) {
  final title = context.knobs.string(label: 'Title', initialValue: '');
  final desc = context.knobs.string(
    label: 'Description (Rich Text)',
    initialValue:
        "<h2><strong>Welcome on BOOKLET/ALEPH!</strong></h2><p>...</p>",
  );

  return SingleChildScrollView(
    child: SvnProQuestionShowItw(
      title: title.isEmpty ? null : title,
      desc: desc,
      youtubeUrl: context.knobs
              .boolean(label: 'Show YouTube link?', initialValue: false)
          ? "https://www.youtube.com/watch?v=Obv4rk5Xs3s"
          : null,
    ),
  );
}
