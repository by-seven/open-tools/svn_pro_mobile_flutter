import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProTiptapShow, path: 'secondary')
Widget playground(BuildContext context) {
  return const SingleChildScrollView(
    child: SvnProTiptapShow(
      html: """
      <div data-type="rootblock"><h1><span><strong>Who inspires you and how would you define inspirational individual?</strong></span></h1></div>
      <div data-type="rootblock"><ul><li>Who inspires you 🌟</li><li>Why this person inspires you 💡</li><li>Top 3 criteria of inspiration 🎯</li></ul></div>
      <div data-type="rootblock"><h1><span><strong>Get Ready to Be Inspired! 🌟</strong></span></h1></div>
      <div data-type="rootblock"><p>Watch this TED Talk by Simon Sinek 🎤 watched by millions.</p></div>
      """,
    ),
  );
}
