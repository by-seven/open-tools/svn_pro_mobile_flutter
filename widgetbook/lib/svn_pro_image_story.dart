import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProImage, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProImage(
    fit: BoxFit.cover,
    imageUrl: context.knobs.string(
        label: 'url',
        initialValue:
            'https://stock.adobe.com/fr/images/happy-smiling-emoji-with-haha-sign/488237693'),
    width: context.knobs.double.slider(label: 'width'),
    height: context.knobs.double.slider(label: 'height'),
  );
}
