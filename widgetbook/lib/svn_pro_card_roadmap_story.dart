import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProCardRoadmap, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProCardRoadmap(
    type: context.knobs.list(
      label: 'Type',
      initialOption: SvnProCardRoadmapType.percentage,
      options: [
        SvnProCardRoadmapType.percentage,
        SvnProCardRoadmapType.number,
        SvnProCardRoadmapType.boolean,
        SvnProCardRoadmapType.multi,
      ],
    ),
    title: context.knobs.string(
        label: 'Title', initialValue: 'Avoir un taux de réussite de 100%'),
    desc: context.knobs.string(
      label: 'Description',
      initialValue:
          'Rhoncusper an eam sodales vocent indoctum dolore principes...',
    ),
    archived: context.knobs.boolean(label: 'Archived?', initialValue: false),
    deadline: context.knobs.boolean(label: 'Has deadline?', initialValue: false)
        ? DateTime.now()
        : null,
    status: context.knobs.list(
      label: 'Status',
      initialOption: SvnProInfoTagType.success,
      options: [
        SvnProInfoTagType.success,
        SvnProInfoTagType.warning,
        SvnProInfoTagType.error,
        SvnProInfoTagType.info,
      ],
    ),
    userAssigned: context.knobs.string(label: 'Assigned To', initialValue: ''),
    labelStatus:
        context.knobs.string(label: 'Label Status', initialValue: '23/25'),
    onTap: context.knobs.boolean(label: 'Pressable?', initialValue: false)
        ? () {}
        : null,
  );
}
