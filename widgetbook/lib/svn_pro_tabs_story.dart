import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProTabsHeadProps, path: 'primary')
Widget playground(BuildContext context) {
  final conf = context.knobs.list<SvnProTabsConfiguration>(
    label: 'Configuration',
    initialOption: SvnProTabsConfiguration.labelOnly,
    options: [
      SvnProTabsConfiguration.labelOnly,
      SvnProTabsConfiguration.iconOnly,
      SvnProTabsConfiguration.labelIcon,
    ],
  );

  return Padding(
    padding: const EdgeInsets.only(top: 100),
    child: SvnProTabs(
      headProps: [
        SvnProTabsHeadProps(
          iconData: Icons.ac_unit,
          label: 'One',
          conf: conf,
        ),
        SvnProTabsHeadProps(
          iconData: Icons.access_alarm,
          label: 'Two',
          conf: conf,
        ),
        SvnProTabsHeadProps(
          iconData: Icons.accessibility_new_sharp,
          label: 'Three',
          conf: conf,
        ),
      ],
      views: const [
        Text('Tab 1'),
        Text('Tab 2'),
        Text('Tab 3'),
      ],
      isScrollable:
          context.knobs.boolean(label: 'Is scrollable?', initialValue: false),
      variant: context.knobs.list<SvnProTabsVariant>(
        label: 'Variant',
        initialOption: SvnProTabsVariant.primary,
        options: [
          SvnProTabsVariant.primary,
          SvnProTabsVariant.secondary,
        ],
      ),
    ),
  );
}
