import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

abstract class SvnProShowTimePicker {}

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProShowTimePicker, path: 'primary')
Widget playground(BuildContext context) {
  final label =
      context.knobs.string(label: 'Label', initialValue: 'Select date');

  return SvnProBtn(
    text: 'Open',
    onPressed: () {
      svnProShowTimePicker(context, label: label);
    },
  );
}
