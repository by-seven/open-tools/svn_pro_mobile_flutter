import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProTopAppBarShow, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProTopAppBarShow(
    title: context.knobs.string(label: 'Title', initialValue: 'Reviewers'),
    elevation: context.knobs.boolean(label: 'Elevation', initialValue: false),
  );
}
