import 'package:colorful_iconify_flutter/icons/noto.dart';
import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProInfoTag, path: 'secondary')
Widget playground(BuildContext context) {
  final leading = context.knobs.list(
    label: 'Leading',
    initialOption: SvnProInfoTagLeading.point,
    options: [
      SvnProInfoTagLeading.point,
      SvnProInfoTagLeading.icon,
      SvnProInfoTagLeading.emojiNoto,
      SvnProInfoTagLeading.none,
    ],
  );
  return SvnProInfoTag(
    label: context.knobs.string(label: 'Label', initialValue: 'Status'),
    type: context.knobs.list(
      label: 'Type',
      initialOption: SvnProInfoTagType.success,
      options: [
        SvnProInfoTagType.success,
        SvnProInfoTagType.primary,
        SvnProInfoTagType.warning,
        SvnProInfoTagType.error,
        SvnProInfoTagType.info,
      ],
    ),
    variant: context.knobs.list(
      label: 'Variant',
      initialOption: SvnProInfoTagVariant.filled,
      options: [
        SvnProInfoTagVariant.filled,
        SvnProInfoTagVariant.outlined,
        SvnProInfoTagVariant.text,
      ],
    ),
    compact: context.knobs.boolean(label: 'Compact', initialValue: false),
    leading: leading,
    icon: leading == SvnProInfoTagLeading.emojiNoto
        ? const SvnProIcon(noto: Noto.index_pointing_up)
        : const SvnProIcon(iconData: MingCuteIcons.mgc_calendar_month_line),
  );
}
