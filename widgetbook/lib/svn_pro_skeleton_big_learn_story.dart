import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProCardBaseBigLearn, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProCardBaseBigLearn(
    imgList: List.generate(
      context.knobs.int
          .slider(label: 'Number of images', initialValue: 0, max: 4),
      (index) => 'https://studio-staging.example.com/harold.jpeg',
    ),
    title: context.knobs.string(
      label: 'Title',
      initialValue:
          'Playlist hygiène niveau 1 pour pizzaiolos niveau 1 pour pizza',
    ),
    infoTagList: context.knobs.boolean(label: 'Tag list?', initialValue: false)
        ? [
            const SvnProInfoTag(label: 'Tag 1'),
            const SvnProInfoTag(label: 'Tag 2'),
            const SvnProInfoTag(label: 'Tag 3'),
          ]
        : null,
    nbrModules:
        context.knobs.boolean(label: 'Number of modules?', initialValue: false)
            ? 23
            : null,
    duration:
        context.knobs.boolean(label: 'Show duration?', initialValue: false)
            ? 3
            : null,
    likes: context.knobs.int.slider(label: 'Likes', initialValue: 0),
    recommended:
        context.knobs.int.slider(label: 'Recommended', initialValue: 0),
    acquired: context.knobs.boolean(label: 'Acquired?', initialValue: false),
  );
}
