import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProChip, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProChip(
    text: context.knobs.string(label: 'Text', initialValue: 'Label'),
    variant: context.knobs.list<SvnProChipVariant>(
      label: 'Variant',
      initialOption: SvnProChipVariant.input,
      options: [
        SvnProChipVariant.input,
        SvnProChipVariant.assist,
        SvnProChipVariant.filter,
        SvnProChipVariant.suggestion,
      ],
    ),
    conf: context.knobs.list<SvnProChipConf>(
      label: 'Configuration',
      initialOption: SvnProChipConf.label,
      options: [
        SvnProChipConf.label,
        SvnProChipConf.labelLeadingIcon,
        SvnProChipConf.labelAvatar,
      ],
    ),
    style: context.knobs.list<SvnProChipStyle>(
      label: 'Style',
      initialOption: SvnProChipStyle.outlined,
      options: [
        SvnProChipStyle.outlined,
        SvnProChipStyle.elevated,
      ],
    ),
    state: context.knobs.list<SvnProChipState>(
      label: 'State',
      initialOption: SvnProChipState.enabled,
      options: [
        SvnProChipState.enabled,
        SvnProChipState.disabled,
      ],
    ),
    leadingIconData: Icons.access_time_filled,
    leadingAvatar:
        const SvnProAvatar(variant: SvnProAvatarVariant.monogram, char: "A"),
    selected: context.knobs.boolean(label: 'Selected', initialValue: false),
    onDeleted: context.knobs.boolean(label: 'Show delete', initialValue: false)
        ? () {}
        : null,
  );
}
