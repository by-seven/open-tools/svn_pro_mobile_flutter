import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProCard, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProCard(
    variant: context.knobs.list<SvnProCardVariant>(
      label: 'Variant',
      initialOption: SvnProCardVariant.outlined,
      options: [
        SvnProCardVariant.outlined,
        SvnProCardVariant.elevated,
        SvnProCardVariant.filled,
      ],
    ),
    onTap: context.knobs.boolean(label: 'Pressable?', initialValue: false)
        ? () {}
        : null,
    child: const SizedBox(
      height: 100,
      width: 100,
      child: Center(child: Text('Body')),
    ),
  );
}
