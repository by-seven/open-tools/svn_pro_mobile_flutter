import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProMedalLearn, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProMedalLearn(
    variant: context.knobs.list(
      label: 'Variant',
      options: [
        SvnProMedalLearnVariant.training,
        SvnProMedalLearnVariant.favorite,
        SvnProMedalLearnVariant.reaction,
        SvnProMedalLearnVariant.module,
      ],
      initialOption: SvnProMedalLearnVariant.training,
    ),
    progression: context.knobs.int
        .slider(label: 'Progression', min: 0, max: 30, initialValue: 15),
  );
}
