import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProSegmentedButton, path: 'primary')
Widget playground(BuildContext context) {
  return const SvnProSegmentedButton(
    buttons: [
      SvnProSegmentButtonItem(
        value: false,
        label: 'False',
      ),
      SvnProSegmentButtonItem(
        value: true,
        label: 'True',
      ),
    ],
    selected: false,
  );
}
