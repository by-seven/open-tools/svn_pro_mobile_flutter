import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProSelectModal, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProSelectDrawer(
    label: 'Label',
    builder: (context) => SvnProSelectModal(
      label: 'field',
      builder: (context) => const SvnProModal(
        title: 'field',
        child: SvnProList(
          tiles: [
            SvnProListTile(
              title: 'item 1',
            ),
          ],
        ),
      ),
    ),
  );
}
