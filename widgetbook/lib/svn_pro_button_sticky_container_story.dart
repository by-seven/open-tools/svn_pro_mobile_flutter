import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProButtonStickyContainer, path: 'secondary')
Widget playground(BuildContext context) {
  final secondary =
      context.knobs.boolean(label: 'Has secondary ?', initialValue: false);
  final textFirst =
      context.knobs.string(label: 'First button text', initialValue: 'OK');
  final textSecond =
      context.knobs.string(label: 'Second button text', initialValue: 'Cancel');

  return SvnProButtonStickyContainer(
    variant: context.knobs.list(
      label: 'Variant',
      initialOption: SvnProButtonStickyContainerVariant.stacked,
      options: [
        SvnProButtonStickyContainerVariant.stacked,
        SvnProButtonStickyContainerVariant.horizontal,
      ],
    ),
    primaryBtn: SvnProBtn(text: textFirst),
    secondaryBtn: secondary
        ? SvnProBtn(variant: SvnProBtnVariants.outlined, text: textSecond)
        : null,
  );
}
