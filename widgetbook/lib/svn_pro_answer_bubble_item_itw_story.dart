import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProAnswerBubbleItemItw, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProAnswerBubbleItemItw(
    avatar: const SvnProAvatar(
      url: 'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
    ),
    title:
        context.knobs.string(label: 'Title', initialValue: 'toto jean pierre'),
    variant: context.knobs.list(
      label: 'Variant',
      initialOption: SvnProAnswerBubbleItemItwVariant.left,
      options: [
        SvnProAnswerBubbleItemItwVariant.left,
        SvnProAnswerBubbleItemItwVariant.right,
        SvnProAnswerBubbleItemItwVariant.cross,
      ],
    ),
    comment: context.knobs.boolean(label: 'Has comment?', initialValue: false)
        ? 'Lorem ipsum dolor sit amet...'
        : null,
    answerType: context.knobs.list(
      label: 'Answer Type',
      initialOption: SvnProQuestionItwType.open,
      options: [
        SvnProQuestionItwType.open,
        SvnProQuestionItwType.rating,
        SvnProQuestionItwType.mcq,
      ],
    ),
    openAnswer: 'blalbla blal my response here...',
    selectedStartAnswer: 2,
    maxStartAnswer: 6,
    mcqAnswer:
        context.knobs.string(label: 'MCQ Answer', initialValue: 'Answer hehe'),
    numberCommentedAnswer:
        context.knobs.boolean(label: 'Commented answer', initialValue: false)
            ? 1
            : null,
  );
}
