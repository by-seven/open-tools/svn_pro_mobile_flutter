import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProCardDashboardShortcut, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProCardDashboardShortcut(
    hasBadge: context.knobs.boolean(label: 'Has Badge', initialValue: false)
        ? 3
        : null,
    variant: context.knobs.list(
      label: 'Variant',
      initialOption: SvnProCardDashboardShortcutVariant.myRoadmap,
      options: [
        SvnProCardDashboardShortcutVariant.myRoadmap,
        SvnProCardDashboardShortcutVariant.myTeamRoadmap,
        SvnProCardDashboardShortcutVariant.myInterview,
        SvnProCardDashboardShortcutVariant.myTeamInterview,
        SvnProCardDashboardShortcutVariant.myTrainings,
        SvnProCardDashboardShortcutVariant.catalog,
      ],
    ),
    onTap: context.knobs.boolean(label: 'Pressable', initialValue: false)
        ? () {}
        : null,
  );
}
