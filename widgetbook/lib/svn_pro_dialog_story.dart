import 'package:colorful_iconify_flutter/icons/noto.dart';
import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProDialog, path: 'primary')
Widget playground(BuildContext context) {
  final icon = context.knobs.boolean(label: 'Icon?', initialValue: false);
  final title =
      context.knobs.string(label: 'Title', initialValue: 'Hey title here');
  final supportingText = context.knobs.string(
      label: 'Support text', initialValue: 'Hey my supporting text here');
  final divider = context.knobs.boolean(label: 'Divider?', initialValue: false);
  final list = context.knobs.boolean(label: 'List?', initialValue: false);
  final textBtnLeft =
      context.knobs.string(label: 'Left button text', initialValue: 'Cancel');
  final textBtnRight =
      context.knobs.string(label: 'Right button text', initialValue: 'OK');

  final tmpList = [
    const SvnProListTile(
        condition: SvnProListTileCondition.line2,
        title: 'Title 1',
        supportingText: 'Supporting text'),
    const SvnProListTile(
        condition: SvnProListTileCondition.line2,
        title: 'Title 2',
        supportingText: 'Supporting text'),
    const SvnProListTile(
        condition: SvnProListTileCondition.line2,
        title: 'Title 3',
        supportingText: 'Supporting text'),
  ];

  return SvnProBtn(
    text: 'Click to open dialog',
    onPressed: () {
      svnProShowDialog(
        context,
        builder: (context) => SvnProDialog(
          title: title,
          icon: icon ? const SvnProIcon(noto: Noto.bone) : null,
          supportingText: supportingText == '' ? null : supportingText,
          divider: divider,
          list: list
              ? ListView.separated(
                  itemBuilder: (context, index) => tmpList[index],
                  separatorBuilder: (context, index) => const SvnProDivider(),
                  itemCount: tmpList.length,
                )
              : null,
          textBtnSecondary: textBtnLeft,
          textBtnPrimary: textBtnRight,
        ),
      );
    },
  );
}
