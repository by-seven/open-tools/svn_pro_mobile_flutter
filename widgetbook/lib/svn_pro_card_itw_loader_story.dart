import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProCardItwLoader, path: 'secondary')
Widget playground(BuildContext context) {
  return const SvnProCardItwLoader();
}
