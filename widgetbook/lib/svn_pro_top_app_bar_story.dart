import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
  name: 'Enhanced Playground',
  type: SvnProTopAppBar,
  path: 'primary',
)
Widget enhancedPlayground(BuildContext context) {
  // Knob pour le titre
  final String title = context.knobs.string(
    label: 'AppBar Title',
    initialValue: 'SvnPro AppBar',
    description: 'Title text displayed in the app bar',
  );

  // Knob pour l'icône leading
  final bool showLeading = context.knobs.boolean(
    label: 'Show Leading Icon',
    initialValue: true,
    description: 'Toggle the leading icon visibility',
  );

  final IconData leadingIcon = context.knobs.list<IconData>(
    label: 'Leading Icon',
    options: [Icons.arrow_back, Icons.menu, Icons.close],
    initialOption: Icons.arrow_back,
    description: 'Select an icon for the leading widget',
  );

  // Knobs pour les actions
  final bool showActions = context.knobs.boolean(
    label: 'Show Actions',
    initialValue: true,
    description: 'Toggle the visibility of actions on the app bar',
  );

  final int actionsCount = context.knobs.int.slider(
    label: 'Number of Actions',
    initialValue: 3,
    description: 'Set the number of action buttons (1 to 5)',
  );

  // Knob pour l'élévation
  final bool enableElevation = context.knobs.boolean(
    label: 'Enable Elevation',
    initialValue: true,
    description: 'Enable or disable app bar elevation (shadow)',
  );

  // Knob pour le centrage du titre
  final bool centerTitle = context.knobs.boolean(
    label: 'Center Title',
    initialValue: true,
    description: 'Center the title text or align it to the start',
  );

  final Color statusBarColor = context.knobs.color(
    label: 'Status Bar Color',
    initialValue: Colors.blue.shade700,
    description: 'Color of the status bar',
  );

  // Knob pour le FAB
  final bool showFab = context.knobs.boolean(
    label: 'Show FAB',
    initialValue: true,
    description: 'Toggle the floating action button visibility',
  );

  final IconData fabIcon = context.knobs.list<IconData>(
    label: 'FAB Icon',
    options: [Icons.add, Icons.edit, Icons.share],
    initialOption: Icons.add,
    description: 'Icon to display in the floating action button',
  );

  // Knob pour le Bottom Sticky Widget
  final bool showBottomSticky = context.knobs.boolean(
    label: 'Show Bottom Sticky Widget',
    initialValue: true,
    description: 'Display a sticky widget at the bottom of the screen',
  );

  // Knob pour la taille du titre
  final SvnProTextSize titleSize = context.knobs.list<SvnProTextSize>(
    label: 'Title Size',
    options: [
      SvnProTextSize.titleSmall,
      SvnProTextSize.titleMl,
      SvnProTextSize.titleLarge,
    ],
    initialOption: SvnProTextSize.titleLarge,
    description: 'Select the size of the app bar title text',
  );

  return SvnProTopAppBar(
    // Configuration de l'AppBar
    title: title.isNotEmpty ? title : null,
    titleSize: titleSize,
    leading: showLeading
        ? SvnProIcon(
            iconData: leadingIcon,
            color: Colors.black,
          )
        : null,
    actions: showActions
        ? List.generate(
            actionsCount,
            (index) => const SvnProIconButton(
              icon: SvnProIcon(iconData: Icons.star),
              variant: SvnProIconButtonVariant.standard,
            ),
          )
        : null,
    elevation: enableElevation,
    centerTitle: centerTitle,
    statutColor: statusBarColor,

    // Sticky Bottom Widget
    bottomSticky: showBottomSticky
        ? Container(
            height: 60,
            color: Colors.grey[200],
            child: const Center(
              child: Text('Sticky Bottom Widget'),
            ),
          )
        : null,

    // Floating Action Button
    fab: showFab
        ? FloatingActionButton(
            onPressed: () {},
            backgroundColor: Colors.orange,
            child: Icon(fabIcon),
          )
        : null,

    // Couleur de l'app bar
    body: const Center(
      child: Text(
        'Content Body SvnProTopAppBar',
        style: TextStyle(color: Colors.black),
      ),
    ),
  );
}
