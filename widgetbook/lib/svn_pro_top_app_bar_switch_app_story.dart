import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProTopAppBarSwitchApp, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProTopAppBarSwitchApp(
    sevenApp: context.knobs.list(
      label: 'Seven App',
      initialOption: SevenApp.interview,
      options: [
        SevenApp.interview,
        SevenApp.roadmap,
        SevenApp.learn,
      ],
    ),
    avatar: const SvnProAvatar(
      url: 'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
    ),
    switchable:
        context.knobs.boolean(label: 'Switchable?', initialValue: false),
  );
}
