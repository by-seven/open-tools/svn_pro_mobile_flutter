import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProTextField, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProTextField(
    readOnly: context.knobs.boolean(label: 'Read only', initialValue: false),
    variant: context.knobs.list<SvnProTextFieldVariant>(
      label: 'Variant',
      initialOption: SvnProTextFieldVariant.filled,
      options: [
        SvnProTextFieldVariant.filled,
        SvnProTextFieldVariant.outlined,
      ],
    ),
    status: context.knobs.list<SvnProTextFieldStatus>(
      label: 'Status',
      initialOption: SvnProTextFieldStatus.enabled,
      options: [
        SvnProTextFieldStatus.enabled,
        SvnProTextFieldStatus.disabled,
        SvnProTextFieldStatus.error,
      ],
    ),
    errorText:
        context.knobs.string(label: 'Error text', initialValue: 'Error text'),
    supportingText:
        context.knobs.string(label: 'Supporting text', initialValue: ''),
    placeHolder:
        context.knobs.string(label: 'Placeholder text', initialValue: ''),
    label: context.knobs.string(label: 'Label text', initialValue: ''),
    prefixIcon:
        context.knobs.boolean(label: 'Prefix icon ?', initialValue: false)
            ? const SvnProIcon(iconData: Icons.search)
            : null,
    suffixIcon:
        context.knobs.boolean(label: 'Suffix icon ?', initialValue: false)
            ? const SvnProIconButton(icon: SvnProIcon(iconData: Icons.clear))
            : null,
    obscureText:
        context.knobs.boolean(label: 'Hide text ?', initialValue: false),
  );
}

@widgetbook.UseCase(name: 'textArea', type: SvnProTextField, path: 'primary')
Widget textArea(BuildContext context) {
  return SvnProTextField(
    readOnly: context.knobs.boolean(label: 'Read only', initialValue: false),
    variant: context.knobs.list<SvnProTextFieldVariant>(
      label: 'Variant',
      initialOption: SvnProTextFieldVariant.filled,
      options: [
        SvnProTextFieldVariant.filled,
        SvnProTextFieldVariant.outlined,
      ],
    ),
    status: context.knobs.list<SvnProTextFieldStatus>(
      label: 'Status',
      initialOption: SvnProTextFieldStatus.enabled,
      options: [
        SvnProTextFieldStatus.enabled,
        SvnProTextFieldStatus.disabled,
        SvnProTextFieldStatus.error,
      ],
    ),
    errorText:
        context.knobs.string(label: 'Error text', initialValue: 'Error text'),
    supportingText:
        context.knobs.string(label: 'Supporting text', initialValue: ''),
    placeHolder:
        context.knobs.string(label: 'Placeholder text', initialValue: ''),
    label: context.knobs.string(label: 'Label text', initialValue: ''),
    prefixIcon:
        context.knobs.boolean(label: 'Prefix icon ?', initialValue: false)
            ? const SvnProIcon(iconData: Icons.search)
            : null,
    suffixIcon:
        context.knobs.boolean(label: 'Suffix icon ?', initialValue: false)
            ? const SvnProIconButton(icon: SvnProIcon(iconData: Icons.clear))
            : null,
    obscureText:
        context.knobs.boolean(label: 'Hide text ?', initialValue: false),
    textArea: true,
    minLines: 8,
    maxLines: 8,
  );
}
