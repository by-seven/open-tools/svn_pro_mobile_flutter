import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProSelectDrawer, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProSelectDrawer(
    label: 'Label',
    builder: (context) => SvnProSelectDrawerSkeleton(
      title: context.knobs.string(label: 'Title', initialValue: 'Select it'),
      radioList: const [
        SvnProRadioListTile(
            label: 'Hello 1', value: true, currentlySelected: false),
        // other options...
      ],
    ),
  );
}
