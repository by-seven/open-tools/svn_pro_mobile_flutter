import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground widget deprecated !',
    type: SvnProList,
    path: 'primary')
Widget playground(BuildContext context) {
  final density = context.knobs.list<VisualDensity>(
    label: 'Density',
    initialOption: VisualDensity.standard,
    options: [
      VisualDensity.standard,
      VisualDensity.comfortable,
      VisualDensity.compact,
    ],
  );
  final dense = context.knobs.boolean(label: 'Dense?', initialValue: false);
  final condition = context.knobs.list<SvnProListTileCondition>(
    label: 'Condition',
    initialOption: SvnProListTileCondition.line1,
    options: [
      SvnProListTileCondition.line1,
      SvnProListTileCondition.line2,
      SvnProListTileCondition.line3Plus,
    ],
  );
  final title =
      context.knobs.string(label: 'Title text', initialValue: 'Title');
  final supportingText =
      context.knobs.string(label: 'Supporting text', initialValue: 'Subtext');
  final variant = context.knobs.list<SvnProListTileSideVariant>(
    label: 'Leading variant',
    initialOption: SvnProListTileSideVariant.none,
    options: [
      SvnProListTileSideVariant.none,
      SvnProListTileSideVariant.icon,
      SvnProListTileSideVariant.image,
      SvnProListTileSideVariant.video,
      SvnProListTileSideVariant.switchButton,
      SvnProListTileSideVariant.checkBox,
      SvnProListTileSideVariant.radioButton,
      SvnProListTileSideVariant.avatar,
    ],
  );
  final pressable =
      context.knobs.boolean(label: 'Pressable?', initialValue: false);

  return SvnProList(
    divider: context.knobs.boolean(label: 'Divider?', initialValue: false)
        ? const SvnProDivider()
        : null,
    tiles: List.generate(
      50,
      (index) => SvnProListTile(
        onTap: pressable ? () {} : null,
        dense: dense,
        density: density,
        condition: condition,
        title: "$title $index",
        supportingText: supportingText,
        leadingVariant: variant,
        leadingIcon: const SvnProIcon(iconData: Icons.ac_unit_sharp),
        trailingVariant: variant,
      ),
    ),
  );
}
