import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProSearch, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProSearch(
    hintText: context.knobs.string(label: 'Hint', initialValue: 'Search'),
    suggestedList: [
      SvnProListTile(
        leadingVariant: SvnProListTileSideVariant.avatar,
        leadingAvatar: const SvnProAvatar(
          url: 'https://example.com/avatar1.jpg',
        ),
        title: 'Harold',
        supportingText: 'Hello',
        onTap: () {
          print('Selected: Harold');
        },
      ),
      SvnProListTile(
        leadingVariant: SvnProListTileSideVariant.avatar,
        leadingAvatar: const SvnProAvatar(
          url: 'https://example.com/avatar2.jpg',
        ),
        title: 'Will',
        supportingText: 'Hello',
        onTap: () {
          print('Selected: Will');
        },
      ),
    ],
    onSelected: (SvnProListTile item) {
      print('Item selected: ${item.leadingAvatar?.url}');
    },
    onSearchChanged: (searchText) {
      print('Search text: $searchText');
    },
  );
}
