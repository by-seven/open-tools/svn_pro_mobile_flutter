import 'package:widgetbook_annotation/widgetbook_annotation.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:flutter/material.dart';

@UseCase(
    name: '1. Playground', type: SvnProSkeletonCardBadge, path: 'secondary')
Widget playground(BuildContext context) {
  return const SvnProSkeletonCardBadge();
}
