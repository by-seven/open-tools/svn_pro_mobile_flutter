import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProAvatar, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProAvatar(
    variant: context.knobs.list<SvnProAvatarVariant>(
      label: 'Variant',
      initialOption: SvnProAvatarVariant.photo,
      options: [
        SvnProAvatarVariant.photo,
        SvnProAvatarVariant.monogram,
        SvnProAvatarVariant.anonym,
        SvnProAvatarVariant.check,
      ],
    ),
    size: context.knobs.list<SvnProAvatarSizes>(
      label: 'Size',
      initialOption: SvnProAvatarSizes.px88,
      options: [
        SvnProAvatarSizes.px88,
        SvnProAvatarSizes.px64,
        SvnProAvatarSizes.px40,
        SvnProAvatarSizes.px32,
        SvnProAvatarSizes.px24,
        SvnProAvatarSizes.px20,
        SvnProAvatarSizes.px16,
      ],
    ),
    url: context.knobs.string(
      label: 'URL',
      initialValue:
          'https://studio-staging-1d24ef396200.herokuapp.com/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBY2M9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--f1634397fb640de8c970dd7737522abd3aee8cdc/harold.jpeg',
    ),
    char: context.knobs.string(label: 'Character', initialValue: 'A')[0],
  );
}
