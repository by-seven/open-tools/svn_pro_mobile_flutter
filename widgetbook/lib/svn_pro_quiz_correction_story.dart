import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProQuizCorrection, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProQuizCorrection(
    text: context.knobs
        .string(label: 'Text', initialValue: 'Lorem ipsum dolor sit amet'),
    variant: context.knobs.list(
      label: 'Variant',
      options: [
        SvnProQuizCorrectionVariant.wrong,
        SvnProQuizCorrectionVariant.partiallyCorrect,
        SvnProQuizCorrectionVariant.correct,
      ],
      initialOption: SvnProQuizCorrectionVariant.wrong,
    ),
  );
}
