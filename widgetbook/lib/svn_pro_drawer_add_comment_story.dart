import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

abstract class SvnProDrawerAddComment {}

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProDrawerAddComment, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProBtn(
    text: 'Open',
    onPressed: () {
      svnProDrawerAddComment(context, onPressed: (String comment) {
        print('Comment: $comment');
      });
    },
  );
}
