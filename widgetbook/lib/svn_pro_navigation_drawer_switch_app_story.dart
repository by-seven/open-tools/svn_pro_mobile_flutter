import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProNavigationDrawer, path: 'secondary')
Widget playground(BuildContext context) {
  final GlobalKey<SvnProNavigationDrawerState> svnProNavigationDrawerStateKey =
      GlobalKey<SvnProNavigationDrawerState>();

  return SvnProNavigationDrawer(
    key: svnProNavigationDrawerStateKey,
    navigationSections: const [
      SvnProNavigationSection(
        header: 'Aleph Apps',
        divider: true,
        destinations: [
          SvnProNavigationDestination(
            'Interview',
            SvnProIcon(iconData: MingCuteIcons.mgc_chat_3_line),
            SvnProIcon(iconData: MingCuteIcons.mgc_chat_3_fill),
          ),
          SvnProNavigationDestination(
            'Learn',
            SvnProIcon(iconData: MingCuteIcons.mgc_mortarboard_line),
            SvnProIcon(iconData: MingCuteIcons.mgc_mortarboard_fill),
          ),
        ],
      ),
    ],
    pages: [
      SvnProBtn(
        text: 'Open 1',
        onPressed: () =>
            svnProNavigationDrawerStateKey.currentState!.openDrawer(),
      ),
      SvnProBtn(
        text: 'Open 2',
        onPressed: () =>
            svnProNavigationDrawerStateKey.currentState!.openDrawer(),
      ),
      SvnProBtn(
        text: 'Open 3',
        onPressed: () =>
            svnProNavigationDrawerStateKey.currentState!.openDrawer(),
      ),
    ],
  );
}
