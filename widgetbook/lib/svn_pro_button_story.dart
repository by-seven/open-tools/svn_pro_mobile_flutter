import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. playground', type: SvnProBtn, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProBtn(
    text: context.knobs.string(label: 'Text', initialValue: 'Email'),
    svnProBtnState: context.knobs.list<SvnProBtnState>(
      label: 'État',
      initialOption: SvnProBtnState.enabled,
      options: [
        SvnProBtnState.enabled,
        SvnProBtnState.disabled,
        SvnProBtnState.loading,
      ],
    ),
    iconData:
        context.knobs.boolean(label: 'Afficher icône', initialValue: false)
            ? MingCuteIcons.mgc_arrow_right_circle_line
            : null,
    variant: context.knobs.list<SvnProBtnVariants>(
      label: 'Variante',
      initialOption: SvnProBtnVariants.filled,
      options: [
        SvnProBtnVariants.filled,
        SvnProBtnVariants.outlined,
        SvnProBtnVariants.text,
        SvnProBtnVariants.elevated,
        SvnProBtnVariants.tonal,
      ],
    ),
  );
}

@widgetbook.UseCase(name: 'Bouton Désactivé', type: SvnProBtn, path: 'primary')
Widget disabledButtonUseCase(BuildContext context) {
  return SvnProBtn(
    text: 'Désactivé',
    svnProBtnState: SvnProBtnState.disabled,
    iconData:
        context.knobs.boolean(label: 'Afficher icône', initialValue: false)
            ? MingCuteIcons.mgc_arrow_right_circle_line
            : null,
    variant: SvnProBtnVariants.outlined,
  );
}

@widgetbook.UseCase(name: 'Bouton Chargement', type: SvnProBtn, path: 'primary')
Widget loadingButtonUseCase(BuildContext context) {
  return SvnProBtn(
    text: 'Chargement...',
    svnProBtnState: SvnProBtnState.loading,
    iconData: context.knobs.boolean(label: 'Afficher icône', initialValue: true)
        ? MingCuteIcons.mgc_arrow_right_circle_line
        : null,
    variant: SvnProBtnVariants.elevated,
  );
}

@widgetbook.UseCase(name: 'Bouton avec Icône', type: SvnProBtn, path: 'primary')
Widget iconButtonUseCase(BuildContext context) {
  return const SvnProBtn(
    text: 'Avec Icône',
    svnProBtnState: SvnProBtnState.enabled,
    iconData: MingCuteIcons.mgc_arrow_right_circle_line,
    variant: SvnProBtnVariants.tonal,
  );
}
