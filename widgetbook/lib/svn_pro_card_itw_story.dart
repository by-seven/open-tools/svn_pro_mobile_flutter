import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProCardItw, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProCardItw(
    type: context.knobs.list(
      label: 'Type',
      initialOption: SvnProCampaignType.oneToOne,
      options: [
        SvnProCampaignType.oneToOne,
        SvnProCampaignType.feedback,
        SvnProCampaignType.survey,
      ],
    ),
    title: context.knobs.string(
        label: 'Title', initialValue: 'Nom de la campagne Feedback ...'),
    deadline: DateTime.now(),
    infoTag: const SvnProInfoTag(
      label: 'skeleton test',
      type: SvnProInfoTagType.warning,
    ),
    featureItem:
        context.knobs.boolean(label: 'Feature item', initialValue: false)
            ? const SvnProFeatureItem(
                featureText: 'Feat text',
                description: 'desc of card',
                color: SvnProFeatureItemColor.defaultColor,
              )
            : null,
    onTap: context.knobs.boolean(label: 'Tappable?', initialValue: false)
        ? () {}
        : null,
  );
}

@UseCase(
    name: 'TODO: every preset design team want, please give us the presets',
    type: SvnProCardItw,
    path: 'secondary')
Widget presetCardItwPersonInChargeUseCase(BuildContext context) {
  return ListView(
    children: [
      SvnProCardItw(
        type: context.knobs.list(
          label: 'Type',
          options: [
            SvnProCampaignType.survey,
            SvnProCampaignType.oneToOne,
            SvnProCampaignType.feedback,
          ],
          initialOption: SvnProCampaignType.survey,
        ),
        title: context.knobs.string(
          label: 'Title',
          initialValue:
              'Nom de la campagne Feedback p dazdzaz dzaadzdza dazdza',
        ),
        featureItem: const SvnProFeatureItem(
          featureText: 'Submitted',
          description: '0/68',
          color: SvnProFeatureItemColor.defaultColor,
          icon: SvnProIcon(
            iconData: MingCuteIcons.mgc_check_circle_line,
          ),
        ),
        infoTag: context.knobs.list(
          label: 'Info Tag',
          options: [
            const SvnProInfoTag(
              type: SvnProInfoTagType.error,
              label: '0% done',
            ),
            const SvnProInfoTag(
              type: SvnProInfoTagType.warning,
              label: '40% done',
            ),
            const SvnProInfoTag(
              type: SvnProInfoTagType.success,
              label: '100% done',
            ),
          ],
          initialOption: const SvnProInfoTag(
            type: SvnProInfoTagType.error,
            label: '0% done',
          ),
        ),
        deadline: DateTime.now(),
      ),
      SvnProCardItw(
        type: context.knobs.list(
          label: 'Type',
          options: [
            SvnProCampaignType.survey,
            SvnProCampaignType.oneToOne,
            SvnProCampaignType.feedback,
          ],
          initialOption: SvnProCampaignType.survey,
        ),
        title: context.knobs.string(
          label: 'Title',
          initialValue:
              'Nom de la campagne Feedback p dazdzaz dzaadzdza dazdza',
        ),
        featureItem: const SvnProFeatureItem(
          featureText: 'Submitted',
          description: '45/86',
          color: SvnProFeatureItemColor.defaultColor,
          icon: SvnProIcon(
            iconData: MingCuteIcons.mgc_check_circle_line,
          ),
        ),
        infoTag: context.knobs.list(
          label: 'Info Tag',
          options: [
            const SvnProInfoTag(
              type: SvnProInfoTagType.warning,
              label: '40% done',
            ),
            const SvnProInfoTag(
              type: SvnProInfoTagType.success,
              label: '100% done',
            ),
          ],
          initialOption: const SvnProInfoTag(
            type: SvnProInfoTagType.warning,
            label: '40% done',
          ),
        ),
        deadline: DateTime.now(),
      ),
    ],
  );
}
