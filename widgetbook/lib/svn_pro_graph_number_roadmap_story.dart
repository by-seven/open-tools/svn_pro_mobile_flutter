import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProGraphNumberRoadmap, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProGraphNumberRoadmap(
    start: context.knobs.int.slider(label: 'Start', initialValue: 4),
    current: context.knobs.int.slider(label: 'Current', initialValue: 6),
    target: context.knobs.int.slider(label: 'Target', initialValue: 8),
    percentage:
        context.knobs.boolean(label: 'In percentage?', initialValue: false),
    color: Colors.yellow,
  );
}
