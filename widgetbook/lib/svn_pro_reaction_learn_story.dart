import 'package:colorful_iconify_flutter/icons/noto.dart';
import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProReactionLearn, path: 'secondary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      SvnProReactionLearn(
        label: 'I like',
        noto: Noto.thumbs_up,
        onSelected: (bool? value) {},
      ),
      SvnProReactionLearn(
        label: 'I recommend',
        noto: Noto.star_struck,
        value: SvnProCheckboxStatus.selected,
        onSelected: (bool? value) {},
      ),
    ],
  );
}
