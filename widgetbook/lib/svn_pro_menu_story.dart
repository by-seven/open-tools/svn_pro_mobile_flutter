import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProMenu, path: 'primary')
Widget playground(BuildContext context) {
  final subText = context.knobs.string(label: 'Sub text', initialValue: '');
  final nbrItem =
      context.knobs.int.slider(label: 'Number of items', initialValue: 12);
  final text = context.knobs.string(label: 'Title', initialValue: 'Title');
  final leadingVariant = context.knobs.list<SvnProMenuTileLeadingVariant>(
    label: 'Leading variant',
    initialOption: SvnProMenuTileLeadingVariant.none,
    options: [
      SvnProMenuTileLeadingVariant.none,
      SvnProMenuTileLeadingVariant.icon,
      SvnProMenuTileLeadingVariant.indent,
      SvnProMenuTileLeadingVariant.checkbox,
    ],
  );
  final trailingVariant = context.knobs.list<SvnProMenuTileTrailingVariant>(
    label: 'Trailing variant',
    initialOption: SvnProMenuTileTrailingVariant.none,
    options: [
      SvnProMenuTileTrailingVariant.none,
      SvnProMenuTileTrailingVariant.icon,
      SvnProMenuTileTrailingVariant.text,
    ],
  );
  final density = context.knobs.list<VisualDensity>(
    label: 'Density',
    initialOption: VisualDensity.standard,
    options: [
      VisualDensity.standard,
      VisualDensity.comfortable,
      VisualDensity.compact,
    ],
  );

  return SvnProMenu(
    activatorBuilder: (context, controller, child) {
      return SvnProBtn(
        text: 'Open Menu',
        onPressed: () => SvnProMenu.handleController(controller),
      );
    },
    tiles: List.generate(nbrItem, (index) {
      return SvnProMenuTile(
        density: density,
        title: text,
        supportingText: subText == '' ? null : subText,
        leadingVariant: leadingVariant,
        leadingIcon: Icons.ac_unit_rounded,
        leadingCheckbox: const SvnProCheckbox(),
        trailingVariant: trailingVariant,
        trailingIconData: Icons.ac_unit_rounded,
        trailingText: 'cc',
      );
    }),
  );
}
