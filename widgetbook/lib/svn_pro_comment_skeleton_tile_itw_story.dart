import 'package:colorful_iconify_flutter/icons/noto.dart';
import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProCommentBaseTile, path: 'secondary')
Widget playground(BuildContext context) {
  final ratingField =
      context.knobs.boolean(label: 'rating field ?', initialValue: false);
  final tileDesc = context.knobs.string(label: 'tile desc', initialValue: '');
  final tileDiv =
      context.knobs.boolean(label: 'tile divider', initialValue: false);

  return ListView(
    children: [
      SvnProCommentBaseTile(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Willex bien flex',
        supportingDate: context.knobs
                .boolean(label: 'supporting date ? ', initialValue: false)
            ? DateTime.now()
            : null,
        titleIcon:
            context.knobs.boolean(label: 'tile icon ?', initialValue: false)
                ? const SvnProIcon(
                    iconData: MingCuteIcons.mgc_pen_line,
                  )
                : null,
        tileIconBtnToggleable: context.knobs.boolean(
                label: 'tile icon button toggleable ?', initialValue: false)
            ? const SvnProIconButton(
                variant: SvnProIconButtonVariant.standard,
                icon: SvnProIcon(
                    iconData: MingCuteIcons.mgc_pin_2_line, size: 24),
                selectedIcon: SvnProIcon(
                    iconData: MingCuteIcons.mgc_pin_2_fill, size: 24),
                type: SvnProIconButtonType.toggleable,
              )
            : null,
        tileInfoTag:
            context.knobs.boolean(label: 'tile info tag', initialValue: false)
                ? const SvnProInfoTag(
                    label: 'hello answer',
                    leading: SvnProInfoTagLeading.none,
                  )
                : null,
        selectedStart: ratingField ? 2 : null,
        maxStart: ratingField ? 7 : null,
        tileDescription: tileDesc != '' ? tileDesc : null,
        tileSubInfoTagList: context.knobs
                .boolean(label: 'tile sub info tab list ?', initialValue: false)
            ? List.generate(
                12,
                (index) => const SvnProInfoTag(label: 'ummm'),
              )
            : null,
        tileAction:
            context.knobs.boolean(label: 'tile action', initialValue: false)
                ? const SvnProBtn(
                    text: 'reply',
                  )
                : null,
        tileDivider: tileDiv ? 4 : null,
      ),
      ...List.generate(
          4,
          (index) => [
                SvnProCommentBaseTile(
                  avatar: const SvnProAvatar(
                    url:
                        'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
                  ),
                  title: 'Willex bien flex',
                  supportingDate: DateTime.now(),
                  tileDescription:
                      'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
                ),
                const SizedBox(height: 24),
              ]).expand((i) => i),
    ],
  );
}

@UseCase(
    name: 'ProCommentSkeletonTileItw',
    type: SvnProCommentBaseTile,
    path: 'secondary')
Widget proCommentSkeletonTileItw(BuildContext context) {
  return ListView(
    children: [
      SvnProCommentBaseTile(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Willex bien flex',
        tileIconBtnToggleable: const SvnProIconButton(
          variant: SvnProIconButtonVariant.standard,
          icon: SvnProIcon(iconData: MingCuteIcons.mgc_pin_2_line, size: 24),
          type: SvnProIconButtonType.toggleable,
        ),
        tileInfoTag: const SvnProInfoTag(
          label: 'hello answer',
          leading: SvnProInfoTagLeading.none,
        ),
        tileDescription:
            'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
        tileSubInfoTagList: List.generate(
          12,
          (index) => const SvnProInfoTag(label: 'ummm'),
        ),
        tileDivider: 4,
      ),
      ...List.generate(
          4,
          (index) => [
                SvnProCommentBaseTile(
                  avatar: const SvnProAvatar(
                    url:
                        'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
                  ),
                  title: 'Willex bien flex',
                  supportingDate: DateTime.now(),
                  tileDescription:
                      'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
                ),
                const SizedBox(height: 24),
              ]).expand((i) => i),
    ],
  );
}

@UseCase(
    name: 'ProCommentSkeletonTileLearn',
    type: SvnProCommentBaseTile,
    path: 'secondary')
Widget proCommentSkeletonTileLearnUseCase(BuildContext context) {
  return ListView(
    children: [
      SvnProCommentBaseTile(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Willex bien flex',
        supportingDate: DateTime.now(),
        tileInfoTag: const SvnProInfoTag(
          label: 'hello answer',
          leading: SvnProInfoTagLeading.emojiNoto,
          icon: SvnProIcon(noto: Noto.index_pointing_up),
        ),
        tileDescription:
            'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
        tileDivider: 4,
      ),
      ...List.generate(
          4,
          (index) => [
                SvnProCommentBaseTile(
                  avatar: const SvnProAvatar(
                    url:
                        'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
                  ),
                  title: 'Willex bien flex',
                  supportingDate: DateTime.now(),
                  tileDescription:
                      'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
                ),
                const SizedBox(height: 24),
              ]).expand((i) => i),
    ],
  );
}

@UseCase(
    name: 'ProCommentSkeletonTileSurveyItw',
    type: SvnProCommentBaseTile,
    path: 'secondary')
Widget proCommentSkeletonTileSurveyItwUseCase(BuildContext context) {
  return SvnProCard(
    variant: SvnProCardVariant.outlined,
    onTap: () {},
    child: Padding(
      padding: const EdgeInsets.fromLTRB(24, 16, 24, 16),
      child: SvnProCommentBaseTile(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Willex bien flex',
        tileIconBtnToggleable: const SvnProIconButton(
          variant: SvnProIconButtonVariant.standard,
          icon: SvnProIcon(iconData: MingCuteIcons.mgc_pin_2_line, size: 24),
          type: SvnProIconButtonType.toggleable,
        ),
        tileInfoTag: const SvnProInfoTag(
          label: 'hello answer',
          leading: SvnProInfoTagLeading.none,
        ),
        tileDescription:
            'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
        tileSubInfoTagList: List.generate(
          12,
          (index) => const SvnProInfoTag(label: 'ummm'),
        ),
      ),
    ),
  );
}
