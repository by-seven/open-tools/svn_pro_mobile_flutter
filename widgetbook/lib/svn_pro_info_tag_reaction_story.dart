import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProInfoTagReaction, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProInfoTagReaction(
    likes: context.knobs.int.slider(label: 'Likes', initialValue: 0),
    recommended:
        context.knobs.int.slider(label: 'Recommended', initialValue: 0),
  );
}
