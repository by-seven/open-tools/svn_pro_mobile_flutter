import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProIconButton, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProIconButton(
    variant: context.knobs.list<SvnProIconButtonVariant>(
      label: 'Variant',
      initialOption: SvnProIconButtonVariant.filled,
      options: [
        SvnProIconButtonVariant.filled,
        SvnProIconButtonVariant.outlined,
        SvnProIconButtonVariant.standard,
        SvnProIconButtonVariant.tonal,
      ],
    ),
    enable: context.knobs.boolean(label: 'Enable ?', initialValue: true),
    icon: const SvnProIcon(iconData: MingCuteIcons.mgc_settings_5_line),
    isSelected:
        context.knobs.boolean(label: 'Is selected ?', initialValue: false),
    type: context.knobs.list<SvnProIconButtonType>(
      label: 'Type',
      initialOption: SvnProIconButtonType.button,
      options: [
        SvnProIconButtonType.button,
        SvnProIconButtonType.toggleable,
      ],
    ),
  );
}
