import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProForm, path: 'secondary')
Widget playground(BuildContext context) {
  final key = GlobalKey<SvnProFormState>();
  final svnProForm = SvnProForm(
    key: key,
    child: ListView(
      children: [
        SvnProFormFieldQuestionEditItw(
          type: GlobalQuestionType.text,
          title: 'hey ? (open) type: "right" to validate the form',
          initialValue: 'my answer',
          validator: (value) {
            return (value == 'right' ? null : 'not valid !!');
          },
        ),
        SvnProFormFieldQuestionEditItw(
          type: GlobalQuestionType.number,
          title: 'hey ? (number) type: "100" to validate the form',
          initialValue: 10,
          validator: (value) {
            return (value == 100 ? null : 'not valid !!');
          },
        ),
        SvnProFormFieldQuestionEditItw(
          type: GlobalQuestionType.star,
          title: 'hey ? (star) click on the fourth star to validate the form',
          initialValue: 0,
          validator: (value) {
            return (value == 4 ? null : 'not valid !!');
          },
          maxStars: 6,
        ),
        SvnProFormFieldQuestionEditItw(
          type: GlobalQuestionType.radio,
          title: 'hey ? (radio) click on the last item to validate the form',
          initialValue: null,
          validator: (value) {
            return (value == 'second choice' ? null : 'not valid !!');
          },
          radioList: const [
            'first choice',
            'second choice',
          ],
        ),
        SvnProFormFieldMultiChoiceList(
          validator: (dynamic list) {
            if (list == null) {
              return "Cannot be empty or null";
            }
            if (list.length == 2) {
              return "Cannot be of size 2";
            }
            return null;
          },
          onChanged: (List<SvnProMultiChoiceData> list) {
            print('======= testing multi choice =======');
            for (final item in list) {
              print(item.value);
            }
          },
          initialValue: [
            SvnProMultiChoiceData(
              value: 'one',
              validated: true,
              validator: (dynamic str) {
                if (str == null || str == '') {
                  return "Can't be blank";
                }
                return null;
              },
            ),
            SvnProMultiChoiceData(
              value: '',
              validated: false,
              validator: (dynamic str) {
                if (str == null || str == '') {
                  return "Can't be blank";
                }
                return null;
              },
            ),
          ],
        ),
        SvnProBtn(
          text: 'submit',
          onPressed: () {
            var validate = key.currentState!.validate();
            print('=== svn form validation ===');
            print(validate);
          },
        ),
      ],
    ),
  );
  return svnProForm;
}
