import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProMultiChoiceList, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProMultiChoiceList(
    readOnly: context.knobs.boolean(label: 'Read only?', initialValue: false),
    initialValue: [
      SvnProMultiChoiceData(
        label: 'Choice 1',
        value: 'one',
        validated: true,
      ),
      SvnProMultiChoiceData(
        label: 'Choice 2',
        value: 'two',
        validated: false,
      ),
    ],
    onChanged: (list) {
      list.forEach((item) => print(item.value));
    },
  );
}
