import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProFeatureListItem, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProFeatureListItem(
    list: List.generate(
      10,
      (index) => SvnProFeatureItem(
        featureText: context.knobs
            .string(label: 'Feature Text', initialValue: 'feature'),
        description: context.knobs
            .string(label: 'Description Text', initialValue: 'description'),
        color: context.knobs.list(
          label: 'Color',
          initialOption: SvnProFeatureItemColor.defaultColor,
          options: [
            SvnProFeatureItemColor.defaultColor,
            SvnProFeatureItemColor.primary,
          ],
        ),
      ),
    ),
  );
}
