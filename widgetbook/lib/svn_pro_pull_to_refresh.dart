import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProPullToRefresh, path: 'secondary')
Widget playground(BuildContext context) {
  return RefreshIndicator(
    onRefresh: () async {
      // Logique de rafraîchissement ici
      await Future.delayed(Duration(seconds: 1));
    },
    child: ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: 20,
      itemBuilder: (context, index) => ListTile(
        title: Text('Élément $index'),
      ),
    ),
  );
}
