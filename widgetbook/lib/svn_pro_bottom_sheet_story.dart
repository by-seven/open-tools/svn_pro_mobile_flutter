import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

abstract class SvnProShowBottomSheet {}

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProShowBottomSheet, path: 'primary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      SvnProBtn(
        text: 'Show bottom sheet modal',
        onPressed: () {
          svnProShowBottomSheet(
            context,
            mode: SvnProBottomSheetMode.modal,
            showHandle: true,
            builder: (context) => SizedBox(
              height: 200,
              child: Center(
                child: ElevatedButton(
                  child: const Text('Close BottomSheet'),
                  onPressed: () => Navigator.pop(context),
                ),
              ),
            ),
          );
        },
      ),
      const SizedBox(height: 50),
      SvnProBtn(
        text: 'Show bottom sheet persistent',
        onPressed: () {
          svnProShowBottomSheet(
            context,
            mode: SvnProBottomSheetMode.persistent,
            showHandle: true,
            builder: (context) => SizedBox(
              height: 200,
              child: Center(
                child: ElevatedButton(
                  child: const Text('Close BottomSheet'),
                  onPressed: () => Navigator.pop(context),
                ),
              ),
            ),
          );
        },
      ),
    ],
  );
}
