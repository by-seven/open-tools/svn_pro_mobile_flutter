import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProLogsRoadmap, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProLogsRoadmap(
    avatar: const SvnProAvatar(
      url: 'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
    ),
    title: context.knobs.string(label: 'Title', initialValue: 'Harold'),
    date: DateTime.now(),
    variant: context.knobs.list(
      label: 'Variant',
      initialOption: SvnProLogsVariantRoadmap.completion,
      options: [
        SvnProLogsVariantRoadmap.completion,
        SvnProLogsVariantRoadmap.targetInfo,
        SvnProLogsVariantRoadmap.achieved,
        SvnProLogsVariantRoadmap.comment,
      ],
    ),
    infoTag: context.knobs.boolean(label: 'Show progress?', initialValue: false)
        ? const SvnProInfoTag(label: 'Hello')
        : null,
    previousValue:
        context.knobs.string(label: 'Previous Value', initialValue: '0%'),
    currentValue:
        context.knobs.string(label: 'Current Value', initialValue: '10%'),
    targetInfoType: 'Description updated',
    comment: context.knobs.boolean(label: 'Has comment?', initialValue: true)
        ? 'Lorem ipsum dolor sit amet...'
        : null,
  );
}
