import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

abstract class SvnProShowModal {}

@UseCase(name: '1. Playground', type: SvnProShowModal, path: 'secondary')
Widget playground(BuildContext context) {
  final variant = context.knobs.list(
    label: 'Variant',
    initialOption: SvnProShowModalVariant.topAction,
    options: [
      SvnProShowModalVariant.topAction,
      SvnProShowModalVariant.bottomActionStacked,
    ],
  );
  final secondary =
      context.knobs.boolean(label: 'Has Secondary?', initialValue: false);

  return SvnProBtn(
    text: 'Open',
    onPressed: () async {
      await svnProShowModal(
        context,
        builder: (context) => SvnProModal(
          title: 'Title',
          variant: variant,
          primaryBtn: const SvnProBtn(text: 'Save'),
          secondaryBtn: secondary
              ? const SvnProBtn(
                  variant: SvnProBtnVariants.outlined,
                  text: 'Close',
                )
              : null,
        ),
      );
    },
  );
}
