import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProSearchField, path: 'secondary')
Widget playground(BuildContext context) {
  final showIcon =
      context.knobs.boolean(label: 'Show Icon?', initialValue: false);
  final enableFilter =
      context.knobs.boolean(label: 'Enable Filter?', initialValue: false);
  final groupFilterCount =
      context.knobs.int.slider(label: 'Group Filter Count', max: 10);
  final groupItemCount =
      context.knobs.int.slider(label: 'Group Item Count', max: 10);

  return SvnProSearchField(
    prefixIcon: showIcon,
    onFilter: enableFilter
        ? () async {
            final result = await svnProShowFilterDrawer(
              context,
              builder: (context) => SvnProFilterDrawer(
                groups: List.generate(
                  groupFilterCount,
                  (index) => SvnProFilterDrawerGroup(
                    label: 'Theme $index',
                    checkboxList: List.generate(
                      groupItemCount,
                      (idx) => SvnProCheckboxTile(
                        title: 'Item $idx',
                        value: false,
                        onChanged: (value) =>
                            print('Checkbox changed: Group $index, Item $idx'),
                      ),
                    ),
                  ),
                ),
              ),
            );
            print('Filter applied: $result');
          }
        : null,
  );
}
