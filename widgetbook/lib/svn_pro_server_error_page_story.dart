import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProServerErrorPage, path: 'secondary')
Widget playground(BuildContext context) {
  return const SvnProServerErrorPage();
}
