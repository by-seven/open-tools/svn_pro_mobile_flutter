import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProDateField, path: 'secondary')
Widget playground(BuildContext context) {
  final controller = TextEditingController();

  return SvnProDateField(
    controller: controller,
    label: context.knobs.string(label: 'Label', initialValue: 'Hire date'),
    placeholder:
        context.knobs.string(label: 'Placeholder', initialValue: 'mm/dd/yyyy'),
  );
}
