import 'package:flutter/material.dart';
import 'package:ming_cute_icons/ming_cute_icons.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

abstract class SvnProShowActionMenu {}

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProShowActionMenu, path: 'secondary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      const SizedBox(height: 50),
      SvnProBtn(
        text: 'Open barebones action menu',
        onPressed: () => svnProShowActionMenu(
          context,
          list: List.generate(
            40,
            (index) => SvnProActionMenuItem(
              const SvnProIcon(
                  iconData: MingCuteIcons.mgc_arrow_right_circle_line),
              'Action $index',
              () => print('Callback action $index'),
            ),
          ),
        ),
      ),
      const SizedBox(height: 40),
      SvnProBtn(
        text: 'Open Participant Feedback Menu',
        onPressed: () => svnProShowActionMenu(
          context,
          list: [
            SvnProActionMenuItem(
              const SvnProIcon(
                  iconData: MingCuteIcons.mgc_arrow_right_circle_line),
              'Start feedback',
              () => print('Start feedback'),
            ),
            SvnProActionMenuItem(
              const SvnProIcon(
                  iconData: MingCuteIcons.mgc_arrow_right_circle_line),
              'Continue feedback',
              () => print('Continue feedback'),
            ),
          ],
        ),
      ),
    ],
  );
}
