import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProDivider, path: 'primary')
Widget playground(BuildContext context) {
  final vertical =
      context.knobs.boolean(label: 'Vertical?', initialValue: false);

  return vertical
      ? Row(
          children: [
            const Expanded(
              child: SvnProCard(child: Center(child: Text('Above'))),
            ),
            SvnProDivider(
              variant: context.knobs.list<SvnProDividerVariant>(
                label: 'Variant',
                initialOption: SvnProDividerVariant.full,
                options: [
                  SvnProDividerVariant.full,
                  SvnProDividerVariant.inset,
                  SvnProDividerVariant.middleInset,
                  SvnProDividerVariant.middleInsetSubHead,
                ],
              ),
              vertical: vertical,
              title: context.knobs
                  .string(label: 'Subtitle', initialValue: 'Subtitle here'),
              padding: context.knobs.list<SvnProDividerPadding>(
                label: 'Padding',
                initialOption: SvnProDividerPadding.px0,
                options: [
                  SvnProDividerPadding.px0,
                  SvnProDividerPadding.px4,
                  SvnProDividerPadding.px8,
                  SvnProDividerPadding.px16,
                  SvnProDividerPadding.px24,
                  SvnProDividerPadding.px40,
                  SvnProDividerPadding.px48,
                  SvnProDividerPadding.px64,
                ],
              ),
            ),
            const Expanded(
              child: SvnProCard(child: Center(child: Text('Below'))),
            ),
          ],
        )
      : Column(
          children: [
            const Expanded(
              child: SvnProCard(child: Center(child: Text('Above'))),
            ),
            SvnProDivider(
              variant: context.knobs.list<SvnProDividerVariant>(
                label: 'Variant',
                initialOption: SvnProDividerVariant.full,
                options: [
                  SvnProDividerVariant.full,
                  SvnProDividerVariant.inset,
                  SvnProDividerVariant.middleInset,
                  SvnProDividerVariant.middleInsetSubHead,
                ],
              ),
              vertical: vertical,
              title: context.knobs
                  .string(label: 'Subtitle', initialValue: 'Subtitle here'),
              padding: context.knobs.list<SvnProDividerPadding>(
                label: 'Padding',
                initialOption: SvnProDividerPadding.px0,
                options: [
                  SvnProDividerPadding.px0,
                  SvnProDividerPadding.px4,
                  SvnProDividerPadding.px8,
                  SvnProDividerPadding.px16,
                  SvnProDividerPadding.px24,
                  SvnProDividerPadding.px40,
                  SvnProDividerPadding.px48,
                  SvnProDividerPadding.px64,
                ],
              ),
            ),
            const Expanded(
              child: SvnProCard(child: Center(child: Text('Below'))),
            ),
          ],
        );
}
