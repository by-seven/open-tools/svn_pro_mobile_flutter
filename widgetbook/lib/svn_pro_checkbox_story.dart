import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProCheckbox, path: 'primary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      SvnProCheckbox(
        state: context.knobs.list<SvnProCheckboxState>(
          label: 'State',
          initialOption: SvnProCheckboxState.enabled,
          options: [
            SvnProCheckboxState.enabled,
            SvnProCheckboxState.disabled,
            SvnProCheckboxState.error,
          ],
        ),
        status: context.knobs.list<SvnProCheckboxStatus>(
          label: 'Status',
          initialOption: SvnProCheckboxStatus.unselected,
          options: [
            SvnProCheckboxStatus.unselected,
            SvnProCheckboxStatus.selected,
            SvnProCheckboxStatus.undetermined,
          ],
        ),
      ),
      const SizedBox(height: 40),
      const SvnProCheckboxTile(
        value: false,
        title: 'Lorem ipsum dolor sit amet consectetur',
      ),
      SvnProCheckboxTile(
        value: false,
        title: 'Lorem ipsum dolor sit amet consectetur',
        onChanged: (_) {},
      ),
      SvnProCheckboxTile(
        value: true,
        title: 'Lorem ipsum dolor sit amet consectetur',
        onChanged: (_) {},
      ),
    ],
  );
}
