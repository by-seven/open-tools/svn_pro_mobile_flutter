import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProRadio, path: 'primary')
Widget playground(BuildContext context) {
  final options2 = context.knobs.list<SvnProRadioState>(
    label: 'State',
    initialOption: SvnProRadioState.enabled,
    options: [
      SvnProRadioState.enabled,
      SvnProRadioState.disabled,
    ],
  );
  final boolean = context.knobs.boolean(label: 'Value', initialValue: false);

  final radioList = {
    'Index 1': 1,
    'Index 2': 2,
    'Index 3': 3,
  };

  return Column(
    children: [
      SvnProRadio(
        state: options2,
        value: boolean,
        currentlySelected: true,
      ),
      const SizedBox(height: 40),
      ...radioList.entries.map((mapEntry) => SvnProRadioListTile(
            value: mapEntry.value,
            currentlySelected: 2,
            label: mapEntry.key,
          )),
    ],
  );
}
