import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProProgress, path: 'primary')
Widget playground(BuildContext context) {
  final style = context.knobs.list<SvnProProgressStyle>(
    label: 'Style',
    initialOption: SvnProProgressStyle.circular,
    options: [
      SvnProProgressStyle.circular,
      SvnProProgressStyle.linear,
    ],
  );
  final isIndeterminate =
      context.knobs.boolean(label: 'Is indeterminate?', initialValue: true);
  final totalStep = context.knobs.int
      .slider(label: 'Total determinate', initialValue: 100, min: 0, max: 100);
  final currentStep = context.knobs.int
      .slider(label: 'Current step', initialValue: 10, min: 0, max: 100);

  return SizedBox(
    width: style == SvnProProgressStyle.circular ? 50 : 360,
    height: 50,
    child: SvnProProgress(
      style: style,
      isIndeterminate: isIndeterminate,
      totalStep: totalStep,
      step: currentStep,
    ),
  );
}
