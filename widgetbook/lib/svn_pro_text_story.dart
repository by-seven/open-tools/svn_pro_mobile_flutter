import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1.Playground', type: SvnProText, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProText(
    text: context.knobs.string(label: 'Text', initialValue: 'My text etc'),
    size: context.knobs.list<SvnProTextSize>(
      label: 'Size',
      initialOption: SvnProTextSize.labelSmall,
      options: [
        SvnProTextSize.labelSmall,
        SvnProTextSize.labelMedium,
        SvnProTextSize.labelLarge,
        SvnProTextSize.bodySmall,
        SvnProTextSize.bodyMedium,
        SvnProTextSize.bodyLarge,
        SvnProTextSize.titleSmall,
        SvnProTextSize.titleMedium,
        SvnProTextSize.titleMl,
        SvnProTextSize.titleLarge,
        SvnProTextSize.headlineSmall,
        SvnProTextSize.headlineMedium,
        SvnProTextSize.headlineLarge,
        SvnProTextSize.displaySmall,
        SvnProTextSize.displayMedium,
        SvnProTextSize.displayLarge,
      ],
    ),
    weight: context.knobs.list<FontWeight>(
      label: 'Weight',
      initialOption: SvnProTextWeight.regular,
      options: [
        SvnProTextWeight.regular,
        SvnProTextWeight.medium,
        SvnProTextWeight.semiBold,
        SvnProTextWeight.bold,
      ],
    ),
    underlined: context.knobs.boolean(label: "Underlined", initialValue: false),
  );
}

@widgetbook.UseCase(
    name: 'Text with Small Size', type: SvnProText, path: 'primary')
Widget smallTextUseCase(BuildContext context) {
  return const SvnProText(
    text: 'Petit texte',
    size: SvnProTextSize.bodySmall,
    weight: SvnProTextWeight.regular,
    underlined: false,
  );
}

@widgetbook.UseCase(
    name: 'Text with Medium Size', type: SvnProText, path: 'primary')
Widget mediumTextUseCase(BuildContext context) {
  return const SvnProText(
    text: 'Texte moyen',
    size: SvnProTextSize.bodyMedium,
    weight: SvnProTextWeight.regular,
    underlined: false,
  );
}

@widgetbook.UseCase(
    name: 'Text with Large Size', type: SvnProText, path: 'primary')
Widget largeTextUseCase(BuildContext context) {
  return const SvnProText(
    text: 'Grand texte',
    size: SvnProTextSize.bodyLarge,
    weight: SvnProTextWeight.regular,
    underlined: false,
  );
}

@widgetbook.UseCase(name: 'Bold Text', type: SvnProText, path: 'primary')
Widget boldTextUseCase(BuildContext context) {
  return const SvnProText(
    text: 'Texte en gras',
    size: SvnProTextSize.bodyMedium,
    weight: SvnProTextWeight.bold,
    underlined: false,
  );
}

@widgetbook.UseCase(name: 'Underlined Text', type: SvnProText, path: 'primary')
Widget underlinedTextUseCase(BuildContext context) {
  return const SvnProText(
    text: 'Texte souligné',
    size: SvnProTextSize.bodyMedium,
    weight: SvnProTextWeight.regular,
    underlined: true,
  );
}
