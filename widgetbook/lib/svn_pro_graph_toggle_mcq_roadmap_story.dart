import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProGraphToggleMcqRoadmap, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProGraphToggleMcqRoadmap(
    status: context.knobs.list(
      label: 'Status',
      initialOption: SvnProTargetStatusRoadmap.notSet,
      options: [
        SvnProTargetStatusRoadmap.notSet,
        SvnProTargetStatusRoadmap.notValid,
        SvnProTargetStatusRoadmap.inProgress,
        SvnProTargetStatusRoadmap.validated,
      ],
    ),
    labelStatus: context.knobs.string(label: 'Label', initialValue: ''),
  );
}
