import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

// This file does not exist yet,
// it will be generated in the next step
import 'main.directories.g.dart';

void main() {
  runApp(const WidgetbookApp());
}

@widgetbook.App()
class WidgetbookApp extends StatelessWidget {
  const WidgetbookApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Widgetbook.material(
      // The [directories] variable does not exist yet,
      // it will be generated in the next step
      directories: directories,
      addons: [
        DeviceFrameAddon(
          devices: [
            Devices.ios.iPhone13,
          ],
          initialDevice: Devices.ios.iPhone13,
        ),
        InspectorAddon(),
        ThemeAddon(
          themes: [
            WidgetbookTheme(
              name: 'Light',
              data: svnProThemeData(context),
            ),
            WidgetbookTheme(
              name: 'Dark',
              data: svnProThemeData(context, light: false),
            ),
          ],
          themeBuilder: (context, theme, child) => MaterialApp(
            theme: theme,
            home: SafeArea(
              child: Scaffold(
                body: Center(child: child),
              ),
            ),
          ),
        ),
        AlignmentAddon(),
        BuilderAddon(
          name: 'Building',
          builder: (_, child) => child,
        ),
      ],
    );
  }
}
