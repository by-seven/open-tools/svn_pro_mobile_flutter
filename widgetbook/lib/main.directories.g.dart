// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: unused_import, prefer_relative_imports, directives_ordering

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AppGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:widgetbook/widgetbook.dart' as _i1;
import 'package:widgetbook_workspace/svn_pro_action_menu_story.dart' as _i62;
import 'package:widgetbook_workspace/svn_pro_answer_bubble_item_itw_story.dart'
    as _i29;
import 'package:widgetbook_workspace/svn_pro_avatar_story.dart' as _i2;
import 'package:widgetbook_workspace/svn_pro_badge_story.dart' as _i3;
import 'package:widgetbook_workspace/svn_pro_bottom_sheet_story.dart' as _i21;
import 'package:widgetbook_workspace/svn_pro_button_sticky_container_story.dart'
    as _i30;
import 'package:widgetbook_workspace/svn_pro_button_story.dart' as _i4;
import 'package:widgetbook_workspace/svn_pro_card_base_small_learn_story.dart'
    as _i32;
import 'package:widgetbook_workspace/svn_pro_card_dashboard_shortcut_story.dart'
    as _i33;
import 'package:widgetbook_workspace/svn_pro_card_itw_loader_story.dart'
    as _i35;
import 'package:widgetbook_workspace/svn_pro_card_itw_story.dart' as _i34;
import 'package:widgetbook_workspace/svn_pro_card_roadmap_story.dart' as _i36;
import 'package:widgetbook_workspace/svn_pro_card_story.dart' as _i5;
import 'package:widgetbook_workspace/svn_pro_card_survey_graph_itw_story.dart'
    as _i37;
import 'package:widgetbook_workspace/svn_pro_checkbox_story.dart' as _i6;
import 'package:widgetbook_workspace/svn_pro_chip_story.dart' as _i7;
import 'package:widgetbook_workspace/svn_pro_comment_skeleton_tile_itw_story.dart'
    as _i38;
import 'package:widgetbook_workspace/svn_pro_date_field_story.dart' as _i39;
import 'package:widgetbook_workspace/svn_pro_datetime_story.dart' as _i63;
import 'package:widgetbook_workspace/svn_pro_dialog_story.dart' as _i8;
import 'package:widgetbook_workspace/svn_pro_divider_story.dart' as _i9;
import 'package:widgetbook_workspace/svn_pro_drawer_add_comment_story.dart'
    as _i40;
import 'package:widgetbook_workspace/svn_pro_empty_state_story.dart' as _i41;
import 'package:widgetbook_workspace/svn_pro_feature_item_story.dart' as _i42;
import 'package:widgetbook_workspace/svn_pro_floating_action_button_story.dart'
    as _i10;
import 'package:widgetbook_workspace/svn_pro_form_story.dart' as _i43;
import 'package:widgetbook_workspace/svn_pro_graph_number_roadmap_story.dart'
    as _i44;
import 'package:widgetbook_workspace/svn_pro_graph_toggle_mcq_roadmap_story.dart'
    as _i45;
import 'package:widgetbook_workspace/svn_pro_icon_button_story.dart' as _i11;
import 'package:widgetbook_workspace/svn_pro_image_story.dart' as _i12;
import 'package:widgetbook_workspace/svn_pro_info_tag_reaction_story.dart'
    as _i13;
import 'package:widgetbook_workspace/svn_pro_info_tag_story.dart' as _i46;
import 'package:widgetbook_workspace/svn_pro_list_item_itw_story.dart' as _i47;
import 'package:widgetbook_workspace/svn_pro_list_story.dart' as _i14;
import 'package:widgetbook_workspace/svn_pro_logs_roadmap_story.dart' as _i48;
import 'package:widgetbook_workspace/svn_pro_medal_learn_story.dart' as _i49;
import 'package:widgetbook_workspace/svn_pro_menu_story.dart' as _i15;
import 'package:widgetbook_workspace/svn_pro_multi_choice_list_story.dart'
    as _i50;
import 'package:widgetbook_workspace/svn_pro_navigation_drawer_story.dart'
    as _i16;
import 'package:widgetbook_workspace/svn_pro_navigation_drawer_switch_app_story.dart'
    as _i51;
import 'package:widgetbook_workspace/svn_pro_progress_story.dart' as _i17;
import 'package:widgetbook_workspace/svn_pro_pull_to_refresh.dart' as _i52;
import 'package:widgetbook_workspace/svn_pro_question_edit_itw_story.dart'
    as _i53;
import 'package:widgetbook_workspace/svn_pro_question_show_itw_story.dart'
    as _i54;
import 'package:widgetbook_workspace/svn_pro_quiz_correction_story.dart'
    as _i55;
import 'package:widgetbook_workspace/svn_pro_radio_story.dart' as _i18;
import 'package:widgetbook_workspace/svn_pro_reaction_learn_story.dart' as _i56;
import 'package:widgetbook_workspace/svn_pro_search_field_story.dart' as _i57;
import 'package:widgetbook_workspace/svn_pro_search_story.dart' as _i19;
import 'package:widgetbook_workspace/svn_pro_segmented_button_story.dart'
    as _i20;
import 'package:widgetbook_workspace/svn_pro_select_modal.dart' as _i60;
import 'package:widgetbook_workspace/svn_pro_select_story.dart' as _i58;
import 'package:widgetbook_workspace/svn_pro_server_error_page_story.dart'
    as _i61;
import 'package:widgetbook_workspace/svn_pro_show_date_picker_story.dart'
    as _i22;
import 'package:widgetbook_workspace/svn_pro_show_modal_story.dart' as _i64;
import 'package:widgetbook_workspace/svn_pro_show_select_drawer_story.dart'
    as _i59;
import 'package:widgetbook_workspace/svn_pro_show_snackbar_story.dart' as _i65;
import 'package:widgetbook_workspace/svn_pro_skeleton_big_learn_story.dart'
    as _i31;
import 'package:widgetbook_workspace/svn_pro_skeleton_card_badge_story.dart'
    as _i66;
import 'package:widgetbook_workspace/svn_pro_switch_story.dart' as _i24;
import 'package:widgetbook_workspace/svn_pro_tabs_story.dart' as _i25;
import 'package:widgetbook_workspace/svn_pro_text_field_story.dart' as _i27;
import 'package:widgetbook_workspace/svn_pro_text_story.dart' as _i26;
import 'package:widgetbook_workspace/svn_pro_time_picker_story.dart' as _i23;
import 'package:widgetbook_workspace/svn_pro_tiptap_show_story.dart' as _i67;
import 'package:widgetbook_workspace/svn_pro_top_app_bar_show_story.dart'
    as _i68;
import 'package:widgetbook_workspace/svn_pro_top_app_bar_story.dart' as _i28;
import 'package:widgetbook_workspace/svn_pro_top_app_bar_switch_app_story.dart'
    as _i69;

final directories = <_i1.WidgetbookNode>[
  _i1.WidgetbookFolder(
    name: 'primary',
    children: [
      _i1.WidgetbookLeafComponent(
        name: 'SvnProAvatar',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i2.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProBadge',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i3.playground,
        ),
      ),
      _i1.WidgetbookComponent(
        name: 'SvnProBtn',
        useCases: [
          _i1.WidgetbookUseCase(
            name: '1. playground',
            builder: _i4.playground,
          ),
          _i1.WidgetbookUseCase(
            name: 'Bouton Chargement',
            builder: _i4.loadingButtonUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'Bouton Désactivé',
            builder: _i4.disabledButtonUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'Bouton avec Icône',
            builder: _i4.iconButtonUseCase,
          ),
        ],
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCard',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i5.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCheckbox',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i6.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProChip',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i7.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProDialog',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i8.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProDivider',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i9.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProFloatingActionButton',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i10.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProIconButton',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i11.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProImage',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i12.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProInfoTagReaction',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i13.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProList',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground widget deprecated !',
          builder: _i14.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProMenu',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i15.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProNavigationDrawer',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i16.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProProgress',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i17.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProRadio',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i18.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSearch',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i19.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSegmentedButton',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i20.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowBottomSheet',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i21.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowDatePicker',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i22.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowTimePicker',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i23.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSwitch',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i24.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProTabsHeadProps',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i25.playground,
        ),
      ),
      _i1.WidgetbookComponent(
        name: 'SvnProText',
        useCases: [
          _i1.WidgetbookUseCase(
            name: '1.Playground',
            builder: _i26.playground,
          ),
          _i1.WidgetbookUseCase(
            name: 'Bold Text',
            builder: _i26.boldTextUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'Text with Large Size',
            builder: _i26.largeTextUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'Text with Medium Size',
            builder: _i26.mediumTextUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'Text with Small Size',
            builder: _i26.smallTextUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'Underlined Text',
            builder: _i26.underlinedTextUseCase,
          ),
        ],
      ),
      _i1.WidgetbookComponent(
        name: 'SvnProTextField',
        useCases: [
          _i1.WidgetbookUseCase(
            name: '1. Playground',
            builder: _i27.playground,
          ),
          _i1.WidgetbookUseCase(
            name: 'textArea',
            builder: _i27.textArea,
          ),
        ],
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProTopAppBar',
        useCase: _i1.WidgetbookUseCase(
          name: 'Enhanced Playground',
          builder: _i28.enhancedPlayground,
        ),
      ),
    ],
  ),
  _i1.WidgetbookFolder(
    name: 'secondary',
    children: [
      _i1.WidgetbookLeafComponent(
        name: 'SvnProAnswerBubbleItemItw',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i29.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProButtonStickyContainer',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i30.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCardBaseBigLearn',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i31.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCardBaseSmallLearn',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i32.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCardDashboardShortcut',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i33.playground,
        ),
      ),
      _i1.WidgetbookComponent(
        name: 'SvnProCardItw',
        useCases: [
          _i1.WidgetbookUseCase(
            name: '1. Playground',
            builder: _i34.playground,
          ),
          _i1.WidgetbookUseCase(
            name:
                'TODO: every preset design team want, please give us the presets',
            builder: _i34.presetCardItwPersonInChargeUseCase,
          ),
        ],
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCardItwLoader',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i35.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCardRoadmap',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i36.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProCardSurveyGraphItw',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i37.playground,
        ),
      ),
      _i1.WidgetbookComponent(
        name: 'SvnProCommentBaseTile',
        useCases: [
          _i1.WidgetbookUseCase(
            name: '1. Playground',
            builder: _i38.playground,
          ),
          _i1.WidgetbookUseCase(
            name: 'ProCommentSkeletonTileItw',
            builder: _i38.proCommentSkeletonTileItw,
          ),
          _i1.WidgetbookUseCase(
            name: 'ProCommentSkeletonTileLearn',
            builder: _i38.proCommentSkeletonTileLearnUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'ProCommentSkeletonTileSurveyItw',
            builder: _i38.proCommentSkeletonTileSurveyItwUseCase,
          ),
        ],
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProDateField',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i39.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProDrawerAddComment',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i40.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProEmptyState',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i41.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProFeatureListItem',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i42.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProForm',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i43.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProGraphNumberRoadmap',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i44.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProGraphToggleMcqRoadmap',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i45.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProInfoTag',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i46.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProListItemItw',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i47.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProLogsRoadmap',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i48.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProMedalLearn',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i49.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProMultiChoiceList',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i50.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProNavigationDrawer',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i51.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProPullToRefresh',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i52.playground,
        ),
      ),
      _i1.WidgetbookComponent(
        name: 'SvnProQuestionEditItw',
        useCases: [
          _i1.WidgetbookUseCase(
            name: '1. Playground',
            builder: _i53.playground,
          ),
          _i1.WidgetbookUseCase(
            name: 'SectionEvaluationShowLearn',
            builder: _i53.sectionEvaluationShowLearnUseCase,
          ),
          _i1.WidgetbookUseCase(
            name: 'SectionQuizResultLearn',
            builder: _i53.sectionQuizResultLearn,
          ),
          _i1.WidgetbookUseCase(
            name: 'SvnProCreateProposalTargetItw',
            builder: _i53.svnProCreateProposalTargetItw,
          ),
          _i1.WidgetbookUseCase(
            name: 'SvnProCreateTargetItw',
            builder: _i53.svnProCreateTargetItw,
          ),
        ],
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProQuestionShowItw',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i54.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProQuizCorrection',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i55.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProReactionLearn',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i56.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSearchField',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i57.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSelect',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i58.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSelectDrawer',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i59.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSelectModal',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i60.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProServerErrorPage',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i61.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowActionMenu',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i62.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowDatePicker',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i63.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowModal',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i64.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProShowSnackbar',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i65.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProSkeletonCardBadge',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i66.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProTiptapShow',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i67.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProTopAppBarShow',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i68.playground,
        ),
      ),
      _i1.WidgetbookLeafComponent(
        name: 'SvnProTopAppBarSwitchApp',
        useCase: _i1.WidgetbookUseCase(
          name: '1. Playground',
          builder: _i69.playground,
        ),
      ),
    ],
  ),
];
