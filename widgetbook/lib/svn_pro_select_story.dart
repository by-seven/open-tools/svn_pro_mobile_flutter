import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProSelect, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProSelect(
    label: context.knobs.string(label: 'Label', initialValue: 'Select label'),
    items: List.generate(20, (index) => SvnProSelectItem(label: 'Item $index')),
    onSelected: (SvnProSelectItem? item) {
      print(item!.label);
    },
  );
}
