import 'package:colorful_iconify_flutter/icons/noto.dart';
import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProEmptyState, path: 'secondary')
Widget playground(BuildContext context) {
  final icon = context.knobs.list(
    label: 'Icon',
    initialOption: const SvnProIcon(noto: Noto.winking_face),
    options: [
      const SvnProIcon(noto: Noto.winking_face),
      const SvnProIcon(noto: Noto.face_with_monocle),
    ],
  );
  final title = context.knobs.string(label: 'Title', initialValue: 'My Title');
  final paragraph =
      context.knobs.string(label: 'Paragraph', initialValue: 'Lorem ipsum...');
  final primary =
      context.knobs.boolean(label: 'Show Primary Button', initialValue: false);
  final secondary = context.knobs
      .boolean(label: 'Show Secondary Button', initialValue: false);

  return SvnProEmptyState(
    icon: icon,
    title: title,
    paragraph: paragraph,
    primary: primary ? const SvnProBtn(text: 'Primary Button') : null,
    secondary: secondary
        ? const SvnProBtn(
            text: 'Secondary Button', variant: SvnProBtnVariants.tonal)
        : null,
  );
}
