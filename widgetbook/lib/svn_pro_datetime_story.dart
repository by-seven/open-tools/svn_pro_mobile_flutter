import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

abstract class SvnProShowDatePicker {}

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProShowDatePicker, path: 'secondary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      SvnProBtn(
        text: 'Show date picker day',
        onPressed: () {
          svnProShowDatePicker(context, mode: SvnProShowDatePickerMode.day);
        },
      ),
      SvnProBtn(
        text: 'Show date picker input',
        onPressed: () {
          svnProShowDatePicker(context, mode: SvnProShowDatePickerMode.input);
        },
      ),
      SvnProBtn(
        text: 'Show date picker range full',
        onPressed: () {
          svnProShowDatePicker(context,
              mode: SvnProShowDatePickerMode.rangeFull);
        },
      ),
      SvnProBtn(
        text: 'Show date picker range input',
        onPressed: () {
          svnProShowDatePicker(context,
              mode: SvnProShowDatePickerMode.rangeInput);
        },
      ),
    ],
  );
}
