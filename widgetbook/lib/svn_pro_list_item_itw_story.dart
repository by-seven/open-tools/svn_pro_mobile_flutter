import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(name: '1. Playground', type: SvnProListItemItw, path: 'secondary')
Widget playground(BuildContext context) {
  return Column(
    children: [
      SvnProListItemItw(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Full name',
        supportingText: 'Job Title or interview status',
        onTap: () {},
        variant: SvnProListItemItwVariant.add,
      ),
      SvnProListItemItw(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Full name',
        supportingText: 'Job Title or interview status',
        onTap: () {},
        variant: SvnProListItemItwVariant.remove,
      ),
      SvnProListItemItw(
        avatar: const SvnProAvatar(
          url:
              'https://studio-staging-1d24ef396200.herokuapp.com/.../harold.jpeg',
        ),
        title: 'Full name',
        supportingText: 'Job Title or interview status',
        onTap: () {},
        variant: SvnProListItemItwVariant.active,
      ),
    ],
  );
}
