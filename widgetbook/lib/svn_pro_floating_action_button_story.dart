import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProFloatingActionButton, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProFloatingActionButton(
    variant: context.knobs.list<SvnProFloatingActionButtonVariant>(
      label: 'Variant',
      initialOption: SvnProFloatingActionButtonVariant.surface,
      options: [
        SvnProFloatingActionButtonVariant.surface,
        SvnProFloatingActionButtonVariant.primary,
        SvnProFloatingActionButtonVariant.secondary,
        SvnProFloatingActionButtonVariant.tertiary,
      ],
    ),
    size: context.knobs.list<SvnProFloatingActionButtonSize>(
      label: 'Size',
      initialOption: SvnProFloatingActionButtonSize.small,
      options: [
        SvnProFloatingActionButtonSize.small,
        SvnProFloatingActionButtonSize.regular,
        SvnProFloatingActionButtonSize.large,
        SvnProFloatingActionButtonSize.extended,
      ],
    ),
    text: context.knobs.string(label: 'Extended text', initialValue: 'hello'),
    icon: const SvnProIcon(iconData: Icons.access_alarm),
  );
}
