import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

abstract class SvnProShowSnackbar {}

@UseCase(name: '1. Playground', type: SvnProShowSnackbar, path: 'secondary')
Widget playground(BuildContext context) {
  final text =
      context.knobs.string(label: 'Snackbar Text', initialValue: 'Hello');
  final actionLabel =
      context.knobs.string(label: 'Action Label', initialValue: '');
  final closable =
      context.knobs.boolean(label: 'Closable?', initialValue: false);
  final longAction =
      context.knobs.boolean(label: 'Long Action?', initialValue: false);

  return SvnProBtn(
    text: 'Show Snackbar',
    onPressed: () {
      svnProShowSnackbar(
        context,
        text: text,
        closable: closable,
        labelAction: actionLabel.isEmpty ? null : actionLabel,
        longAction: longAction,
      );
    },
  );
}
