import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@UseCase(
    name: '1. Playground', type: SvnProCardBaseSmallLearn, path: 'secondary')
Widget playground(BuildContext context) {
  return SvnProCardBaseSmallLearn(
    imgList: List.generate(
      context.knobs.int
          .slider(label: 'Number of images', initialValue: 0, max: 4),
      (index) => 'https://example.com/image.png',
    ),
    title:
        context.knobs.string(label: 'Title', initialValue: 'Development 101'),
    showModule:
        context.knobs.boolean(label: 'Show module?', initialValue: false),
    acquired: context.knobs.boolean(label: 'Acquired?', initialValue: false),
    currentProgression:
        context.knobs.boolean(label: 'Show progression?', initialValue: false)
            ? 50
            : null,
    targetProgression:
        context.knobs.boolean(label: 'Show progression?', initialValue: false)
            ? 100
            : null,
  );
}
