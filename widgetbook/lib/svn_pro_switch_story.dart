import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(name: '1. Playground', type: SvnProSwitch, path: 'primary')
Widget playground(BuildContext context) {
  return SvnProSwitch(
    state: context.knobs.list<SvnProSwitchState>(
      label: 'State',
      initialOption: SvnProSwitchState.enabled,
      options: [
        SvnProSwitchState.enabled,
        SvnProSwitchState.disabled,
      ],
    ),
    selected: context.knobs.boolean(label: 'Selected', initialValue: false),
    withIcons: context.knobs.boolean(label: 'With icons', initialValue: false),
  );
}
