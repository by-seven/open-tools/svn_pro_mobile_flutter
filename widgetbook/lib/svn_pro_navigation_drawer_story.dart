import 'package:flutter/material.dart';
import 'package:svn_pro_mobile_flutter/svn_pro_mobile_flutter.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

@widgetbook.UseCase(
    name: '1. Playground', type: SvnProNavigationDrawer, path: 'primary')
Widget playground(BuildContext context) {
  final GlobalKey<SvnProNavigationDrawerState> svnProNavigationDrawerStateKey =
      GlobalKey<SvnProNavigationDrawerState>();
  final firstHeader =
      context.knobs.string(label: 'First Header', initialValue: '');
  final firstDestination = context.knobs
      .string(label: 'First Destination', initialValue: 'Messages');
  final secondHeader =
      context.knobs.string(label: 'Second Header', initialValue: '');

  return SvnProNavigationDrawer(
    key: svnProNavigationDrawerStateKey,
    navigationSections: [
      SvnProNavigationSection(
        header: firstHeader.isNotEmpty ? firstHeader : null,
        divider:
            context.knobs.boolean(label: 'First Divider', initialValue: false),
        destinations: [
          SvnProNavigationDestination(
            firstDestination,
            const Icon(Icons.widgets_outlined),
            const Icon(Icons.widgets),
          ),
          const SvnProNavigationDestination(
            'Profile',
            Icon(Icons.format_paint_outlined),
            Icon(Icons.format_paint),
          ),
        ],
      ),
      SvnProNavigationSection(
        header: secondHeader.isNotEmpty ? secondHeader : null,
        divider:
            context.knobs.boolean(label: 'Second Divider', initialValue: false),
        destinations: [
          const SvnProNavigationDestination(
            'Settings',
            Icon(Icons.settings_outlined),
            Icon(Icons.settings),
          ),
        ],
      ),
    ],
    pages: [
      SvnProBtn(
        text: 'Open 1',
        onPressed: () =>
            svnProNavigationDrawerStateKey.currentState!.openDrawer(),
      ),
      SvnProBtn(
        text: 'Open 2',
        onPressed: () =>
            svnProNavigationDrawerStateKey.currentState!.openDrawer(),
      ),
      SvnProBtn(
        text: 'Open 3',
        onPressed: () =>
            svnProNavigationDrawerStateKey.currentState!.openDrawer(),
      ),
    ],
  );
}
